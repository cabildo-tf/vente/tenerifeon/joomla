<?php

namespace Studiogenesis\Component\Sgoauth\Administrator\Controller;

defined('_JEXEC') or die;

use Joomla\CMS\MVC\Controller\BaseController;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_sgoauth
 *
 * @copyright   Copyright (C) 2020 Studiogenesis. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Default Controller of Sgoauth component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_sgoauth
 */
class DisplayController extends BaseController {
    /**
     * The default view for the display method.
     *
     * @var string
     */
    protected $default_view = 'sgoauth';
    
    public function display($cachable = false, $urlparams = array()) {
        return parent::display($cachable, $urlparams);
    }
    
}