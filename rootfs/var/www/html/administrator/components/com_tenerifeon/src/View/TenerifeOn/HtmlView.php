<?php

namespace Studiogenesis\Component\TenerifeOn\Administrator\View\TenerifeOn;

defined('_JEXEC') or die;

use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_tenerifeon
 *
 * @copyright   Copyright (C) 2022 Studiogenesis. All rights reserved.
 */

/**
 * Main "TenerifeOn" Admin View
 */
class HtmlView extends BaseHtmlView {
    
    /**
     * Display the main "TenerifeOn" view
     *
     * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
     * @return  void
     */
    function display($tpl = null) {
        parent::display($tpl);
    }


}