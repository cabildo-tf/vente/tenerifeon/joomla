<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_vente
 *
 * @copyright   Copyright (C) 2020 John Smith. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

 // No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<h2>Importación de datos de ARGYS a Tenerife | ON</h2>
<div class="row my-5">
    <div class="col-md-4 text-center">
        <a href=""><button type="button" class="btn btn-primary js-pstats-btn-allow-always">Itinerarios</button></a>
    </div>
    <div class="col-md-4 text-center">
        <a href=""><button type="button" class="btn btn-primary js-pstats-btn-allow-always">Equipamientos</button></a>
    </div>
    <div class="col-md-4 text-center">
        <a href=""><button type="button" class="btn btn-primary js-pstats-btn-allow-always">Localizaciones</button></a>
    </div>
</div>