<?php return array(
    'root' => array(
        'pretty_version' => '2.x-dev',
        'version' => '2.9999999.9999999.9999999-dev',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../../../',
        'aliases' => array(),
        'reference' => 'e31d406cded0e6c3f362360e3e127ae540ff0c1e',
        'name' => 'akeeba/engage',
        'dev' => false,
    ),
    'versions' => array(
        'akeeba/engage' => array(
            'pretty_version' => '2.x-dev',
            'version' => '2.9999999.9999999.9999999-dev',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../../../',
            'aliases' => array(),
            'reference' => 'e31d406cded0e6c3f362360e3e127ae540ff0c1e',
            'dev_requirement' => false,
        ),
        'ezyang/htmlpurifier' => array(
            'pretty_version' => 'v4.13.0',
            'version' => '4.13.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ezyang/htmlpurifier',
            'aliases' => array(),
            'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
            'dev_requirement' => false,
        ),
    ),
);
