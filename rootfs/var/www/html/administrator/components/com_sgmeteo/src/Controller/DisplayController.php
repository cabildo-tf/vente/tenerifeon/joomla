<?php

namespace Studiogenesis\Component\Sgmeteo\Administrator\Controller;

defined('_JEXEC') or die;

use Joomla\CMS\MVC\Controller\BaseController;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_sgmeteo
 *
 * @copyright   Copyright (C) 2020 Studiogenesis. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Default Controller of Sgmeteo component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_sgmeteo
 */
class DisplayController extends BaseController {
    /**
     * The default view for the display method.
     *
     * @var string
     */
    protected $default_view = 'sgmeteo';
    
    public function display($cachable = false, $urlparams = array()) {
        return parent::display($cachable, $urlparams);
    }
    
}