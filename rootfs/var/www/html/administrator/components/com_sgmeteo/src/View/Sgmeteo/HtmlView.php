<?php

namespace Studiogenesis\Component\Sgmeteo\Administrator\View\Sgmeteo;

defined('_JEXEC') or die;

use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_sgmeteo
 *
 * @copyright   Copyright (C) 2020 Studiogenesis. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Main "SG Meteo" Admin View
 */
class HtmlView extends BaseHtmlView {
    
    /**
     * Display the main SG Meteo" view
     *
     * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
     * @return  void
     */
    function display($tpl = null) {
        parent::display($tpl);
    }


}