<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_sgmeteo
 *
 * @copyright   Copyright (C) 2020 Studiogenesis. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

 // No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<h2>SG Meteo is working behind the scenes</h2>
<p class="actions">
    <form action="/administrator/index.php?option=com_sgmeteo" method="post" enctype="multipart/form">
        <input type="hidden" name="meteo" value="meteo">
        <button type="submit" class="btn btn-primary js-pstats-btn-allow-always">Meteo</button>
    </form>
</p>
<?php $siteUrl = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://'.$_SERVER['SERVER_NAME']; ?>
<?php
if(isset($_POST['meteo'])){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $siteUrl .'/api/index.php/v1/sgmeteo/syncmeteo/syncmeteo',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer c2hhMjU2Ojg0ODplMGRmOWRkOGVjNThhYWFkYTZiNjcwMDFiZTEyYWEwMjk5NjNiMzIyZTgzMTI4YzRiMmEyYjRmMzZmMTRhMGJj'
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    date_default_timezone_set('Europe/Madrid');    
    $DateAndTime = date('d-m-Y h:i:s a', time());  
    echo "El proceso se ha iniciado a las  $DateAndTime.\n";
}