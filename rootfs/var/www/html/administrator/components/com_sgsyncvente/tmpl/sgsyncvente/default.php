<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_sgsyncvente
 *
 * @copyright   Copyright (C) 2020 Studiogenesis. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

use Joomla\CMS\Factory;

 // No direct access to this file
defined('_JEXEC') or die('Restricted Access');
echo exec("ping -c 3 vente.tenerife");
?>
<h2>SG Sync Vente is working behind the scenes</h2>
<div class="mt-5">
    <div class="row">
        <div class="col-md-4">
            <form action="/administrator/index.php?option=com_sgsyncvente" method="post" enctype="multipart/form">
                <input type="hidden" name="contentType" value="syncrutas">
                <button type="submit" class="btn btn-primary js-pstats-btn-allow-always">Itinerarios</button>
            </form>
        </div>
        <div class="col-md-4">
            <form action="/administrator/index.php?option=com_sgsyncvente" method="post" enctype="multipart/form">
                <input type="hidden" name="contentType" value="syncequipamientos">
                <button type="submit" class="btn btn-primary js-pstats-btn-allow-always">Equipamientos</button>
            </form>
        </div>
        <div class="col-md-4">
            <form action="/administrator/index.php?option=com_sgsyncvente" method="post" enctype="multipart/form">
                <input type="hidden" name="contentType" value="syncpuntosinteres">
                <button type="submit" class="btn btn-primary js-pstats-btn-allow-always">Puntos interés</button>
            </form>
        </div>
    </div>

</div>
<div class="mt-5">
    <div class="row">
        <div class="col-md-4">
            <form action="/administrator/index.php?option=com_sgsyncvente" method="post" enctype="multipart/form">
                <input type="hidden" name="contentType" value="syncalertas">
                <button type="submit" class="btn btn-primary js-pstats-btn-allow-always">Alertas</button>
            </form>
        </div>
        <div class="col-md-4">
            <form action="/administrator/index.php?option=com_sgsyncvente" method="post" enctype="multipart/form">
                <input type="hidden" name="contentType" value="syncincidencias">
                <button type="submit" class="btn btn-primary js-pstats-btn-allow-always">Incidencias</button>
            </form>
        </div>
        <div class="col-md-4">
            <form action="/administrator/index.php?option=com_sgsyncvente" method="post" enctype="multipart/form">
                <input type="hidden" name="contentType" value="syncsearchablepoints">
                <button type="submit" class="btn btn-primary js-pstats-btn-allow-always">Planificador</button>
            </form>
        </div>

    </div>

</div>
<div class="actions mt-3"></div>
<?php   $domain = $_SERVER['SERVER_NAME'];    
        $domainTld = substr($domain, strpos($domain, ".") + 1);    
        $domainTld == 'test' ? $protocool = 'http' : $protocool = 'https';
        $siteUrl = $protocool.'://'.$_SERVER['SERVER_NAME']; 
        $siteUrl = 'http://'.$_SERVER['SERVER_NAME']; ?>
<?php
$user = Factory::getUser();
$userToken = getUserToken($user);

if(isset($_POST['contentType']) )
{
    $contentType = $_POST['contentType'];
    $sync_id = checkSyncProcess($contentType);
    if($sync_id != false){
        echo '<script>
            text = `<div class="alert alert-success" role="alert">Nuevo proceso iniciado, recibirás un correo cuando haya terminado</div>`;
                    document.getElementsByClassName("actions")[0].innerHTML += text;
        </script>';

        exec("curl --location --request GET '".$siteUrl."/api/index.php/v1/sgsyncvente/".$contentType."/".$contentType."?sync_id=".$sync_id."' --header 'Authorization: Bearer ".$userToken."'  > /dev/null 2>/dev/null &");

    }else{
        echo '<script>
        text = `<div class="alert alert-danger" role="alert">Ya hay un proceso iniciado de sincronización, no se pueden realizar más de uno a la vez.</div>`;
                document.getElementsByClassName("actions")[0].innerHTML += text;
                </script>';
    }
}

function checkSyncProcess($contentType) {
    $db = Factory::getDbo();
    $query = $db
    ->getQuery(true)
    ->select(array('user_id, start_sync_date'))
    ->from($db->quoteName('#__sg_syncro_vente'))
    ->where($db->quoteName('content_sync') . " = " . $db->quote($contentType))
    ->where($db->quoteName('end_sync_date') . " is null"); 

    $db->setQuery($query);
    $syncInProcess = $db->loadObjectList();
    // Load the results as a list of stdClass objects (see later for more options on retrieving data).

    if(sizeOf($syncInProcess) == 0){ //if there is not any proces running
        
        $user = Factory::getUser();
        date_default_timezone_set('Europe/Madrid');
        $dateTime = strftime("%Y-%m-%d %X");
        $columns = array('user_id', 'content_sync', 'start_sync_date');
        $values = $user->id.", '".$contentType."', '".$dateTime."'";
        $query = $db
        ->getQuery(true)
        ->insert($db->quoteName('#__sg_syncro_vente'))
        ->columns($db->quoteName($columns))
        ->values(''.$values.'');
        $db->setQuery($query);
        //dd($query);
        $newSyncProcess = $db->execute();
        $new_row_id = $db->insertid();
        //$new_row_id = 40;


        return  $new_row_id;
    }
    else{
        return false;
    }
}
function getUserToken($user)
{
    $db = Factory::getDbo();
    $query = $db->getQuery(true)
        ->select('profile_value')
        ->from($db->quoteName('#__user_profiles'))
        ->where($db->quoteName('profile_key') . " = " . $db->quote('joomlatoken.token'))
        ->where($db->quoteName('user_id') . " = " . $db->quote($user->id));

    // Reset the query using our newly populated query object.
    $db->setQuery($query);
    //get the user.profile token saved in db
    $tokenSeed = $db->loadResult();

    $siteSecret = Factory::getApplication()->get('secret');
    $algorithm = "sha256";
    $rawToken  = base64_decode($tokenSeed);
    $tokenHash = hash_hmac($algorithm, $rawToken, $siteSecret);
    $userId    = $user->id;
    $userToken   = base64_encode("$algorithm:$userId:$tokenHash");

    return $userToken;
}
?>
