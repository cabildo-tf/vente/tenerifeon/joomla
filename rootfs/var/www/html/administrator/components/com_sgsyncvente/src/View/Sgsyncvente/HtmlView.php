<?php

namespace Studiogenesis\Component\Sgsyncvente\Administrator\View\Sgsyncvente;

defined('_JEXEC') or die;

use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_sgsyncvente
 *
 * @copyright   Copyright (C) 2020 Studiogenesis. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Main "SG Oauth" Admin View
 */
class HtmlView extends BaseHtmlView {
    
    /**
     * Display the main SG Oauth" view
     *
     * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
     * @return  void
     */
    function display($tpl = null) {
        parent::display($tpl);
    }


}