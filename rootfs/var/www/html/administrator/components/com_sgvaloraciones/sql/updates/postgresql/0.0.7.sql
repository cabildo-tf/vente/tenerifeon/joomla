--
-- Table structure for table `#__sg_valoraciones`
--
DROP TABLE IF EXISTS `#__sg_valoraciones`;


CREATE TABLE IF NOT EXISTS "#__sg_valoraciones" (
  "id" serial NOT NULL,
  "date_added" timestamp without time zone NOT NULL,
  "user_id"  int NOT NULL,
  "item_id"  int NOT NULL,
  "agent" varchar (255),
   "rating" int NOT NULL,
  PRIMARY KEY (id),
   CONSTRAINT fk_user
      FOREIGN KEY (user_id )
	  REFERENCES #__users(id)
	  ON DELETE CASCADE,
   CONSTRAINT fk_item
      FOREIGN KEY (item_id )
	  REFERENCES #__content(id)
	  ON DELETE CASCADE
);
CREATE INDEX "#__sg_valoraciones_idx_user" on "#__sg_valoraciones" ("user_id");
CREATE INDEX "#__sg_valoraciones_idx_item" on "#__sg_valoraciones" ("item_id");