<?php

namespace Studiogenesis\Component\Sgvaloraciones\Administrator\Controller;

defined('_JEXEC') or die;

use Joomla\CMS\MVC\Controller\BaseController;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_sgvaloraciones
 *
 * @copyright   Copyright (C) 2020 Studiogenesis. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Default Controller of Sgvaloraciones component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_sgvaloraciones
 */
class DisplayController extends BaseController {
    /**
     * The default view for the display method.
     *
     * @var string
     */
    protected $default_view = 'sgvaloraciones';
    
    public function display($cachable = false, $urlparams = array()) {
        return parent::display($cachable, $urlparams);
    }
    
}