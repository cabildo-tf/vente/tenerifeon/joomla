<?php

/**
 * Studiogenesis
 *
 * @copyright  (C) 2014 Open Source Matters, Inc. <https://www.joomla.org>
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Joomla\CMS\Arcgis;

use Exception;
use Joomla\CMS\Factory;

\defined('JPATH_PLATFORM') or die;


class ArcgisClient
{

    private $baseUrl;
    protected $token;

    public function __construct($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    protected function getToken(){
        $config = Factory::getConfig();
			
        //Get token drom cache
        $cache   = Factory::getCache('com_arcgis', '');
        //If token is set in cache
        if($cache->contains('arcgis_token')){

            $tokenResponse =  $cache->get('arcgis_token');
            function getNowMilliseconds() {
                $mt = explode(' ', microtime());
                return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
            }
            //Get todays datetime to compare with token expiration date
            $todayNow = getNowMilliseconds();
            //If token is expired, get new token and cache it
            if($tokenResponse->expires < $todayNow){
                $response = $this->post('/portal/sharing/rest/generateToken', ['username' => $config->get('venteUser'),'password' => $config->get('ventePsswd'),'expiration' => '20160','referer' => $config->get('venteServicesUrl'),'f' => 'json'], false);
                $cache->store($response, 'arcgis_token');
                return $response->token;
            } 
            //If is not yet expired, return token
            else{
                // Si no ha expirado devolverlo
                return $tokenResponse->token;
            }
        } 
        else{
                // Si no existe enviar petición de login para obtener token y guardarlo en cache
                $response = $this->post('/portal/sharing/rest/generateToken', ['username' => $config->get('venteUser'),'password' => $config->get('ventePsswd'),'expiration' => '20160','referer' => $config->get('venteServicesUrl'),'f' => 'json'], false);
                $cache->store($response, 'arcgis_token');
                return $response->token;
        }
           

    }

    public function get(string $path, array $params = [], Bool $includeToken = true)
    {
        $token = $this->getToken();
        return "Se ha realizado una llamada get".$token;
    }

    public function post(string $path, array $body = [], Bool $includeToken = true)
    {
        if($includeToken){
            $token = $this->getToken();
        }

        $curl = curl_init();

        // Disables SSL check
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $this->baseUrl.$path,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 10,
          //CURLOPT_CONNECTTIMEOUT_MS => 10,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => $body,
		  CURLOPT_HTTPHEADER => array(
			$includeToken != null ? 'Authorization: Bearer '.$token : ''
		  ),
		));
		
		$response = curl_exec($curl);

        //TODO: when curl error throw an exception
        if(curl_error($curl)){
            $error = ["error" => (object)["code"=> 500,
            "message"=>"Unable to complete operation."]];
            return ((object)($error));
            //throw new Exception("Arcgis API Error: " . curl_error($curl));
             
        }
		
		curl_close($curl);

        return json_decode($response);
    }
}