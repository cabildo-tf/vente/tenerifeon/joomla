var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
return new bootstrap.Tooltip(tooltipTriggerEl)
})

$('.hero-slick').slick({
  // arrows: false
  autoplay: true,
  autoplaySpeed: 5000
});

$('.multiple-items').slick({
  arrows: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [{
      breakpoint: 1350,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 1000,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


$('.slick-enp').slick({
  arrows: true,
  dots: false,
});

$('.slick-itinerarios').slick({
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: false,
  dots: true,
  responsive: [{
      breakpoint: 1399,
      settings: {
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 1000,
      settings: {
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});

$("#open-lang").click(function () {
  if ($(".cont-idiomas ul.idiomas").hasClass("opened")) {
    $(".cont-idiomas ul.idiomas").removeClass("opened");
  } else {
    $(".cont-idiomas ul.idiomas").addClass("opened");
  }
});

$("#closeMenu").click(function () {
  if($("#closeMenu").hasClass("active")){
    $("#closeMenu").removeClass("active");
  }else{
    $("#closeMenu").addClass("active");
  }
  $("#content-menu").removeClass("opened");
  $("body").css("overflow-y", "auto");
  if ($(window).width() >= 992) {
    var autoLeft = parseInt($(".container").first().css("marginLeft")) + 32;
    $("#content-menu").css("padding-right", 0);
  }
});

$("#openMenu").click(function () {
  $("#content-menu").addClass("opened");
  $("body").css("overflow-y", "hidden");
  if ($(window).width() >= 992) {
    var autoLeft = parseInt($(".container").first().css("marginLeft")) + 32;
    $("#content-menu.opened").css("padding-right", autoLeft);
  }
});

var heightHeader = $(".menuShadow").height();
var prehederHeight = $(".preheader").height() + 32;

if ($(window).width() <= 768) {
  $(window).scroll(function () {
    if ($(window).scrollTop() >= 100) {
      $("#openMenu").addClass("pined");
    } else {
      $("#openMenu").removeClass("pined");
    }
  });
}
if ($(window).width() >= 768) {
  $(window).scroll(function () {
    if ($(window).scrollTop() >= 150) {
      $(".menuShadow").addClass("pined");  
    } else {
      $(".menuShadow").removeClass("pined");
    }
  });
}

// detect slick actions
$('ul.slick-dots li button').click(function () {
  quevamosareutilizar($(this));
});
$('button.slick-next.slick-arrow').click(function () {
  quevamosareutilizar($('ul.slick-dots li.slick-active button'));
});
$('button.slick-prev.slick-arrow').click(function () {
  quevamosareutilizar($('ul.slick-dots li.slick-active button'));
});

$(".slick-list.draggable").bind("mouseup touchend", function (e) {
  quevamosareutilizar($('ul.slick-dots li.slick-active button'));
});
quevamosareutilizar($('ul.slick-dots li.slick-active button'));

function quevamosareutilizar(button) {
  var curr = button;
  var prev = curr.prev('li button')
  $('ul.slick-dots li button').removeClass('consecutives');

  if (prev.length == 0) {
    lastaOfPrevLi = curr.parent().prev();
    if (lastaOfPrevLi.length != 0) {
      lastaOfPrevLi.find('button:last').addClass('consecutives');
    } else {
      prev.addClass('consecutives');
    }
  }
  var next = curr.next('li button');
  if (next.length == 0) {
    lastaOfNextLi = curr.parent().next();
    if (lastaOfNextLi.length != 0) {
      lastaOfNextLi.find('button:first').addClass('consecutives');
    } else {
      next.addClass('consecutives');
    }
  }
}

// mide y redimensiona todo lo que necesita redimension
$(window).resize(function () {
  resizeAll(true);
});
$(document).ready(function () {
  resizeAll();
  $("#system-message-container").addClass("container");
});



var widthFigureItemFilter = $('.sizeingImg').width();

function resizeAll(except=false) {
  
  if(except){
    heightHeader = $(".menuShadow").height();
    prehederHeight = $(".preheader").height() + 32;
  } 
  widthFigureItemFilter = $('.sizeingImg').width();
  // $(".containerShadow").height(heightHeader);
  $('.sizeingImg').height(widthFigureItemFilter);
  var autoLeft = parseInt($(".container").first().css("marginLeft")) + 32;
  $(".hero-slick  button.slick-arrow").css("left", autoLeft);
  $(".hero-slick  button.slick-arrow.slick-next").css("left", autoLeft + 60);

  if ($(window).width() >= 768){
    $(".tag-equipamiento").css("right", (autoLeft -32));
  }else{
    $(".tag-equipamiento").css("right", (autoLeft));
  }

  if ($(window).width() >= 992) {
    // calcula dinamicamente el height de la pantalla y le resta el alto sumado de los headers, devuelve un height y se lo da al section
    $(".view_rezizer").height(window.innerHeight - (heightHeader + prehederHeight + 55));
    $(".map-equipamiento").height(window.innerHeight - (heightHeader + prehederHeight + 55));
    $(".control-column").height(window.innerHeight - (heightHeader + prehederHeight + 55 + 55));

    var itinerariosHeight = $("section.planificador.view_rezizer").height();

    if ($(".filter-row").length > 0) {
      var restaSumada = itinerariosHeight - $(".filter-row").height();
    } else {
      var restaSumada = itinerariosHeight;
    }
    $("#itinerarios-column").height(window.innerHeight - (heightHeader + prehederHeight + 5 + 105));
    $(".content-map-desktop.planificador").height(restaSumada);
    $("#planificador-column").height(restaSumada);
    // If map is not null and is not undefined
    // if(map !== undefined){
    //   map.panTo(new L.LatLng(28.291564, -16.629130));
    // }
  }
  

}

// comprueba chekeds en los inputs y activa al btn padre
$("input.form-check-input").click(function () {
  var ulChecks = $(this).parent().parent().parent();

  if ($(this).is(":checked")) {
    ulChecks.prev().addClass("active");
  } else {
    if (ulChecks.find("input:checked").length == 0) {
      ulChecks.prev().removeClass("active");
    }
  }
});

$("button.show-map").click(function () {
  if ($("button.show-map").hasClass("active")) {
    $(".content-map-desktop").removeClass("active");
    $("button.show-map").removeClass("active");
    $("body").css("overflow-y", "initial");

  } else {
    $(".content-map-desktop").addClass("active");
    $("button.show-map").addClass("active");
    $("body").css("overflow-y", "hidden");

    $(".filtros-mobil").removeClass("active");
    $("button.show-filters").removeClass("active");
  }
});


$("button.show-filters").click(function () {
  if ($("button.show-filters").hasClass("active")) {

    $(".filtros-mobil").removeClass("active");
    $("button.show-filters").removeClass("active");

    $("body").css("overflow-y", "initial");
  } else {
    // $(".map-mobil").removeClass("active");
    // $("button.show-map").removeClass("active");

    $("button.show-filters").addClass("active");
    $(".filtros-mobil").addClass("active");

    $("body").css("overflow-y", "hidden");
  }
});



if ($(window).width() <= 992) {
  $(window).scroll(function () {
    if ($(window).scrollTop() >= (prehederHeight + heightHeader + $("#itinerarios-column").height() - $(".mas-valorados").height())) {
      $(".cont-btn-mobile").addClass("d-none");
    } else {
      $(".cont-btn-mobile").removeClass("d-none");
    }
  });
}



// open Off Canvas custom 

$("button.open-rout-btn").click(function () {
  if ($(".offCanvascontainer").hasClass("active")) {
    $(".offCanvascontainer").removeClass("active");


  } else {
    $(".offCanvascontainer").addClass("active");
    $("section.filtros-itinerarios .map-mobil").css("z-index", "4");
  }
});

$("#show__filters__user").click(function () {
  if ($("#filters__user").hasClass("active")) {
    $("#filters__user").removeClass("active")
    $("body").css("overflow-y", "initial");


  } else {
    $("#filters__user").addClass("active")
    $("body").css("overflow-y", "hidden");

  }
});


function downloadGpx(id, name) {
  event.preventDefault();
  var baseUrl = "/index.php?option=com_ajax&format=file&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&download=" + id;

  Joomla.request({
    url: baseUrl + "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,
    onSuccess: function onSuccess(resp) {
      try {
        filename = name + ".gpx";
        var element = document.createElement('a');
        element.setAttribute('href', 'data:application/gpx+xml;charset=utf-8,' + encodeURIComponent(resp));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);

      } catch (e) {
        Joomla.renderMessages({
          error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
        }, ".sampledata-steps-");
        return;
      }

    },
    /*    */

    onError: function onError() {
      Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
      });
    }
  });

};

function downloadKml(id, name) {
  event.preventDefault();
  var baseUrl = "/index.php?option=com_ajax&format=file&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&downloadkml=" + id;

  Joomla.request({
    url: baseUrl + "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,
    onSuccess: function onSuccess(resp) {
      try {
        filename = name + ".kml";
        var element = document.createElement('a');
        element.setAttribute('href', 'data:application/gpx+xml;charset=utf-8,' + encodeURIComponent(resp));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);

      } catch (e) {
        Joomla.renderMessages({
          error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
        }, ".sampledata-steps-");
        return;
      }

    },
    /*    */

    onError: function onError() {
      Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
      });
    }
  });

};

// cosas dragalbles
$('.draggables').sortable({
  axis: 'y',
  update: function (event, ui) {
    var data = $(this).sortable('serialize');
  }
});

$( ".draggables.planificador_ruta" ).sortable({
  axis: 'y',
  update: function (event, ui) {
    var data = $(this).sortable('serialize');
    positions = data.split('&');

    positions.forEach(sortPositions);
    function sortPositions(item, index, positions){
      
        pointsList[index+1] = aux[parseInt(positions[index].substring(positions[index].indexOf('=') + 1))]
    }

    getPlanificador(pointsList);
  }
  });



$("#faqtabs-ratings").click(function (event) {
  event.preventDefault();

  var user = $(this).data('userid');

  var baseUrl = "/index.php?option=com_ajax&format=file&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&myratings=" + user;

  Joomla.request({
    url: baseUrl + "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,

    onSuccess: function onSuccess(resp) {
      try {
        var myrated = JSON.parse(resp);

        /* PINTAMOS LO EQUIPAMIENTOS */

        if (myrated.length) {


          const obj = (myrated);
          var layout = "";
          myrated.forEach(printMyratings);

          function printMyratings(item) {

            layout += `<div class="my__rates__rate"  id="${item.id}-${user}">
                                <div class="row">
                                    <div class="col-lg-4 my-4">   
                                    <div class="card-default">
                                    <a class="hyper-link" href="${item.link}" tabindex="0"></a>
                                    <button class="like-post" tabindex="0"  onclick="removeItem('${item.id}-valoraciones-${user}')"><i class="fas fa-trash"></i>
                                    </button>
                                    <figure>
                                        <div class="gradient"></div>
                                        <img src="/${item.images.src}" alt="${item.images.alt}">
                                    </figure>
                                    <div class="caption">
                                        <div class="head-caption">
                                            <span></span>
                                        </div>
                                        <div class="body-caption">
                                            <h3>${item.title}</h3>
                                        </div>
                                    </div>
                                    <div class="link-card">
                                        <p>Más información</p>
                                    </div>
                                </div>

                                    </div>
                                    <div class="col-lg-8">
                                        <div class="my__rates__rate__text">
                                            <p><small>Realizada el ${item.date_added}</small></p>
                                            <p><strong>En: ${item.title}</strong></p>
                                            <div class="content_rating"><ul>`;
            for (i = 0; i < item.rating; i++) {
              layout += `<li class="vote-star"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                              </li>`;
            };
            for (i = 0; i < (5 - item.rating); i++) {
              layout += `<li class="vote-star-empty"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                              </li>`
            }
            layout += `</ul></div>
                                        </div>
                                    </div>
                                </div>
                                </div>`;
          }
          $('#profile-ratings').html(layout);
        }

      } catch (e) {
        Joomla.renderMessages({
          error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
        }, ".sampledata-steps-");
        return;
      }

    },
    /*    */

    onError: function onError() {
      Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
      });
    }
  });

});
// Add favourites to user profile
$("#faqtabs-favourites").click(function (event) {
  event.preventDefault();

  var user = $(this).data('userid');

  var baseUrl = "/index.php?option=com_ajax&format=file&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&myfavourites=" + user;

  Joomla.request({
    url: baseUrl + "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,

    onSuccess: function onSuccess(resp) {
      try {
        var favourites = JSON.parse(resp);

        /* PINTAMOS LO EQUIPAMIENTOS */

        if (favourites.length) {


          const obj = (favourites);
          var layout = "<div class='row'>";
          favourites.forEach(printMyratings);

          function printMyratings(item) {
            layout += `<div class="col-lg-4 col-md-6 my-4"  id="${item.id}-${user}">
                                        <div class="card-default">
                                          <a title="Enlace al contenido" class="hyper-link" href="${item.link}" tabindex="0"> </a>

                                            <button class="like-post" tabindex="0" onclick="removeItem('${item.id}-favourites-${user}')"><i class="fas fa-trash"></i>
                                            </button>
                                            <figure>
                                                <div class="gradient"></div>
                                                <img src="/${item.images.src}" alt="${item.images.alt}">
                                            </figure>
                                            <div class="caption">
                                                <div class="head-caption">
                                                    <span></span>
                                                </div>
                                                <div class="body-caption">
                                                    <h3>${item.title}</h3>
                                                    <p>${item.catName}</p>
                                                </div>
                                                <div class="icons-caption">
                                                <div class="content_rating"><ul>`;

            for (i = 0; i < item.rating; i++) {
              layout += `<li class="vote-star"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                                  </li>`;
            };
            for (i = 0; i < (5 - item.rating); i++) {
              layout += `<li class="vote-star-empty"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                                  </li>`
            }

            layout += `</ul></div>
                                              </div>
                                            </div>
                                            <div class="link-card">
                                                <p>Más información</p>
                                            </div>
                                        </div>
                                    </div>`;
          }
          layout += "</div>";
          $('#profile-favourites').html(layout);
        }

      } catch (e) {
        Joomla.renderMessages({
          error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
        }, ".sampledata-steps-");
        return;
      }

    },
    /*    */

    onError: function onError() {
      Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
      });
    }
  });

});
var activities = "";
// Add activities to user profile
$("#faqtabs-activities").click(function (event) {
  event.preventDefault();

  var user = $(this).data('userid');

  var baseUrl = "/index.php?option=com_ajax&format=file&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&myactivities=" + user;

  Joomla.request({
    url: baseUrl + "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,

    onSuccess: function onSuccess(resp) {
      try {
        activities = JSON.parse(resp);

        /* PINTAMOS LO EQUIPAMIENTOS */

        if (activities.length) {


          const obj = (activities);
          var layout = `<div class="list__routes__horizontal">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <h2>Tienes ${activities.length} actividades guardadas</h2>
                                        </div>
                                    </div>`;
          activities.forEach(printMyratings);

          function printMyratings(item, index) {
            layout += `<div class="col" id="${item.id}-${user}">
                        <div class="route__horizontal">
                            <div class="row">
                                <div class="col-lg-4">
                                <figure style="height:300px" class="map" id="map-activity-${index+1}">
                               
                                </figure>
                                </div>
                                <div class="col-9 col-lg-6">
                                    <div class="route__horizontal__info">
                                        <p>${item.type_name}</p>
                                        <h3>${item.activity_name}</h3>`;
                                        if(item.name){
                                          layout += `<h4>${item.name}</h4>`;
                                        }
  
                        layout += ` <div class="route__horizontal__features">`;
                                        if(item.activity_distance != null){
                                          layout += `<div class="content__route">
                                                      <i class="fas fa-long-arrow-alt-right"></i>
                                                      ${(item.activity_distance/1000).toFixed(2)}km
                                                  </div>` ;
                                        }
                                        
                                        if(item.activity_duration != null){
                                          layout += `<div class="content__route">
                                                      <i class="far fa-clock"></i>
                                                      ${timeConvert(item.activity_duration)}
                                                  </div>` ;
                                        }
                                        if(item.activity_gain != null){
                                          layout += `<div class="content__route">
                                                        <i class="fas fa-level-up-alt"></i>
                                                        ${parseInt(item.activity_gain).toFixed(0)}m
                                                    </div>` ;
                                        }
                                        if(item.activity_gain != null){
                                          layout += `<div class="content__route">
                                                        <i class=" fas fa-level-down-alt"></i>
                                                        ${parseInt(item.activity_lost).toFixed(0)}m
                                                    </div>` ;
                                        }
                                        
                            layout += `</div>
                                    </div>
                                </div>
                                <div class="col-3 col-lg-2 d-flex pad-mobil">
                                    <div class="route__horizontal__actions">
                                        <div class="dropdown dropdown-menu-end dropdown--actions">
                                            <button class="btn btn-light" type="button" id="route-actions" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                            <ul class="dropdown-menu" aria-labelledby="route-actions">
                                                <li><a class="dropdown-item" href="#" onclick="downloadMyGpx(${index+1},'activity')">Descargar GPX</a></li>
                                                <li><a class="dropdown-item delete" href="#" onclick="removeItem('${item.id}-my_activities-${user}')">Eliminar</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div >`;


          }
          layout += `</div>
                          </div>`;

          function timeConvert(n) {
            var num = n;
            var hours = (num / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            return rhours + "h " + rminutes + "m";
          }

          $('#profile-activities').html(layout);

          activities.forEach(fillMap);

          function fillMap(item, index) {

            var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
            var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoidGVuZXJpZmVvbiIsImEiOiJjbDJ0NDEwODMwMHFlM25vMnFhMHkwcjd2In0.wKD1dQHphg8RAyClqN-UFA';

            var tenerifeon = L.tileLayer(mbUrl, {
              id: 'tenerifeon/cl2pyxrtp001b15lfuq3x23z5',
              tileSize: 512,
              zoomOffset: -1,
              attribution: mbAttr
            });

            var map = L.map('map-activity-' + (index + 1), {
              center: [28.291565, -16.629129],
              zoom: 12,
              scrollWheelZoom: false,
              layers: [tenerifeon]
            });
            map.dragging.disable();



            var jsonLayer = L.geoJSON(JSON.parse(item.activity_geom)).addTo(map);
            map.fitBounds(jsonLayer.getBounds());

          }
        }

      } catch (e) {
        Joomla.renderMessages({
          error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
        }, ".sampledata-steps-");
        return;
      }

    },
    /*    */

    onError: function onError() {
      Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
      });
    }
  });

});
//Download gpx my activities              
function downloadMyGpx(itemId, type) {
  if (type == 'activity') {
    var coordinates = JSON.parse(activities[itemId - 1].activity_geom);
    coordinates = coordinates.features[0].geometry.coordinates;
    var gpxFile = `<?xml version="1.0" encoding="UTF-8"?>
                      <gpx creator="Tenerife ON">
                      <metadata>
                      <time>${activities[itemId-1].route_date}</time>
                      </metadata>
                      <trk>
                      <name>${activities[itemId-1].activity_name}</name>
                      <type>9</type>
                      <trkseg>
                  `;
  } else if (type == 'route') {
  
    var coordinates = JSON.parse(routes[itemId - 1].route_geom);
    coordinates = coordinates.features[0].geometry.coordinates;
    var gpxFile = 
  `<?xml version="1.0" encoding="UTF-8"?>
    <gpx creator="Tenerife ON">
    <metadata>
    <time>${routes[itemId-1].route_date}</time>
    </metadata>
    <trk>
    <name>${routes[itemId-1].route_name}</name>
    <type>9</type>
    <trkseg>
  `;

  }
  if (coordinates.length > 0) {


    var gpxContent = "";
    if (type == 'activity') {

      //Get array of linestrings coordinates
      coordinates.forEach(getCoordArray);

      function getCoordArray(coordArray) {

        //For each array of coordinates, fill gpx content 
        coordArray.forEach(getGpx);

        function getGpx(item) {

          gpxContent += `<trkpt lat="${item[1]}" lon="${item[0]}">
                <ele>"${item[2]}"</ele>
              </trkpt>`
        }

      }
    } else if (type == 'route') {
      coordinates.forEach(getGpx);

      function getGpx(item) {

        gpxContent += `<trkpt lat="${item[1]}" lon="${item[0]}">
          <ele>"${item[2]}"</ele>
          </trkpt>`
      }
    }

    //Fill gpx content


    gpxFile += gpxContent + `</trkseg>
                    </trk>
                  </gpx>`;

  }


  try {
    if (type == 'activity') {
      filename = activities[itemId - 1].activity_name + ".gpx";
    } else if (type == 'route') {
      filename = routes[itemId - 1].route_name + ".gpx";
    }
    var element = document.createElement('a');
    element.setAttribute('href', 'data:application/gpx+xml;charset=utf-8,' + encodeURIComponent(gpxFile));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);

  } catch (e) {
    Joomla.renderMessages({
      error: [Joomla.Text._('Error con la creacion del gpx')]
    }, ".sampledata-steps-");
    return;
  }


};
//Remove item from database
function removeItem(item) {
  var item = item.split("-");
  var id = item[0];
  var contentType = item[1];
  var user = item[2];

  var baseUrl = "/index.php?option=com_ajax&format=file&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&contentType=" + contentType + "&id=" + id;

  Joomla.request({
    url: baseUrl + "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,

    onSuccess: function onSuccess(resp) {

      response = JSON.parse(resp);
      if (response.success) {
        $('#' + id + '-' + user + '').remove();
      }

    },
    onError: function onError() {
      Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
      });
    }
  });

};
function viewMyRoute(id) {
  $("#sendMyRoute"+id+"").submit();
}
// Add routes to user profile+
var routes = "";
$("#faqtabs-routes").click(function (event) {
  event.preventDefault();

  var user = $(this).data('userid');

  var baseUrl = "/index.php?option=com_ajax&format=file&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&myroutes=" + user;

  Joomla.request({
    url: baseUrl + "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,

    onSuccess: function onSuccess(resp) {
      try {
        routes = JSON.parse(resp);

        /* PINTAMOS LO EQUIPAMIENTOS */

        if (routes.length) {



          const obj = (routes);
          var layout = `<div class="list__routes__horizontal">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <h2>Tienes ${routes.length} rutas guardadas</h2>
                                        </div>
                                    </div>`;
          routes.forEach(printMyroutes);

          function printMyroutes(item, index) {
            layout += `<div class="col" id="${item.id}-${user}">
                        <div class="route__horizontal">
                            <div class="row">
                                <div class="col-lg-4">
                                <figure style="height:300px" class="map" id="map-route-${index+1}">
                               
                                </figure>
                                </div>
                                <div class="col-9 col-lg-6">
                                    <div class="route__horizontal__info">
                                        <p>${item.type_name}</p>
                                        <h3>${item.route_name}</h3>`;
                                      if(item.name){
                                        layout += `<h4>${item.name}</h4>`;
                                      }

                            layout += `<div class="route__horizontal__features">
                                            <div class="content__route">
                                                <i class="fas fa-long-arrow-alt-right"></i>
                                                ${(item.route_distance)}km
                                            </div>
                                           <div class="content__route">
                                                <i class="far fa-clock"></i>
                                                ${(item.route_duration/60).toFixed(0)}H ${item.route_duration - (((item.route_duration/60).toFixed(0))*60)}M
                                            </div>
                                            <div class="content__route">
                                                <i class="fas fa-level-up-alt"></i>
                                                ${parseInt(item.route_gain).toFixed(0)}m
                                            </div>
                                            <div class="content__route">
                                                <i class=" fas fa-level-down-alt"></i>
                                                ${parseInt(item.route_lost).toFixed(0)}m
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3 col-lg-2 d-flex pad-mobil">
                                    <div class="route__horizontal__actions">
                                        <div class="dropdown dropdown-menu-end dropdown--actions">
                                            <button class="btn btn-light" type="button" id="route-actions" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                            <ul class="dropdown-menu" aria-labelledby="route-actions">
                                                <li><a class="dropdown-item" href="#" onclick="downloadMyGpx(${index+1},'route')">Descargar GPX</a></li>
                                                <li><a class="dropdown-item delete" href="#" onclick="viewMyRoute(${index+1})">View</a></li>                                           
                                                <li><a class="dropdown-item delete" href="#" onclick="removeItem('${item.id}-my_routes-${user}')">Eliminar</a></li>
                                                <form method="post" action="/planificador" id="sendMyRoute${index+1}">
                                                    <input id="myRoute" name="myRoute"}" type="hidden" value='${JSON.stringify(item)}' />
                                                </form> 
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div >`;


          }
          layout += `</div>
                          </div>`;

          function timeConvert(n) {
            var num = n;
            var hours = (num / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            return rhours + "h " + rminutes + "m";
          }


          $('#profile-routes').html(layout);

          routes.forEach(fillMap);

          function fillMap(item, index) {

            var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
            var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoidGVuZXJpZmVvbiIsImEiOiJjbDJ0NDEwODMwMHFlM25vMnFhMHkwcjd2In0.wKD1dQHphg8RAyClqN-UFA';

            var tenerifeon = L.tileLayer(mbUrl, {
              id: 'tenerifeon/cl2pyxrtp001b15lfuq3x23z5',
              tileSize: 512,
              zoomOffset: -1,
              attribution: mbAttr
            });

            var map = L.map('map-route-' + (index + 1), {
              center: [28.291565, -16.629129],
              zoom: 12,
              scrollWheelZoom: false,
              layers: [tenerifeon]
            });
            map.dragging.disable();



            var jsonLayer = L.geoJSON(JSON.parse(item.route_geom)).addTo(map);
            map.fitBounds(jsonLayer.getBounds());

          }
        }

      } catch (e) {
        Joomla.renderMessages({
          error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
        }, ".sampledata-steps-");
        return;
      }

    },
    /*    */
    onError: function onError() {
      Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
      });
    }
  });

});

$("#openPlnificador").click(function () {
  if ($("#openPlnificador").hasClass("opened")) {
    $("#openPlnificador").removeClass("opened");
    $("#planificador-column").removeClass("opened");
  } else {
    $("#openPlnificador").addClass("opened");
    $("#planificador-column").addClass("opened");
  }
});


if ($(window).width() >= 768) {
  $("#openData").click(function () {
    if ($("#openData").hasClass("opened")) {
      $("#openData").removeClass("opened");
      $(".leaflet-touch .leaflet-control-layers").removeClass("opened");
    } else {
      $(".leaflet-touch .leaflet-control-layers").addClass("opened");
      $("#openData").addClass("opened");
    }
  });
  $("#openLegend").click(function () {
    if ($("#openLegend").hasClass("opened")) {
        $("#customLegend").removeClass("opened");
        $("#openLegend").removeClass("opened"); 
        $(".leaflet-control-zoom.leaflet-bar.leaflet-control").css("left", "inherit");
    } else {
      $("#openLegend").addClass("opened");
      $("#customLegend").addClass("opened");
      $(".leaflet-control-zoom.leaflet-bar.leaflet-control").css("left", "300px");
    }
  });
} else {
  $("#openLegend").click(function () {
    if($("#openLegend").hasClass("opened") && ($("#openData").hasClass("opened")) && $(".leaflet-touch .leaflet-control-layers").hasClass("opened")) {
      $("#customLegend").addClass("opened");
      $(".leaflet-touch .leaflet-control-layers").removeClass("opened");
    } else if ($("#openLegend").hasClass("opened")) {
      $("#openLegend").removeClass("opened");
      $("#customLegend").removeClass("opened");
      $("#openData").removeClass("opened");
      $(".leaflet-touch .leaflet-control-layers").removeClass("opened");
    } else {
      $("#openLegend").addClass("opened");
      $("#openData").addClass("opened");
      $("#customLegend").addClass("opened");
      $(".leaflet-touch .leaflet-control-layers").removeClass("opened");
    }
  });
  $("#openData").click(function () {
    if($("#openLegend").hasClass("opened") && ($("#openData").hasClass("opened")) && $("#customLegend").hasClass("opened")) {
      $("#customLegend").removeClass("opened");
      $(".leaflet-touch .leaflet-control-layers").addClass("opened");
    } else if ($("#openData").hasClass("opened")) {
      $("#openData").removeClass("opened");
      $(".leaflet-touch .leaflet-control-layers").removeClass("opened");
      $("#customLegend").removeClass("opened");
      $("#openLegend").removeClass("opened");
    } else {
      $("#openData").addClass("opened");
      $("#openLegend").addClass("opened");
      $(".leaflet-touch .leaflet-control-layers").addClass("opened");
      $("#customLegend").removeClass("opened");
    }
  });
}
//Map constants
  var typeNatural = {
        "Árbol" : 1,
        "Barranco" : 2,
        "Era" : 6,
        "Playa" : 12,
        "Roque" : 14
    }
    var typeCultural = {
        "Acueducto" : 0,
        "BIC" : 3,
        "Caserío" : 4,
        "Construcción singular" : 5,
        "Faro" : 7,
        "Horno" : 8,
        "Lagar" : 9,
        "Lavadero y fuento" : 10,
        "Tagoro" : 15,
        "Museo" : 11,
        "Punto información" : 13
    };
    var typeGeneral = {};

    var siteUrl =  window.location.protocol+"//"+window.location.host;

    var pointsIcons = {
      cultural : L.icon({ //1
          iconUrl: siteUrl+'/templates/gesplan/images/icono-cultural-revisado.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                  popupAnchor: [0, -34]        }),
      info : L.icon({ //2
          iconUrl: siteUrl+'/templates/gesplan/images/i-info.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                  popupAnchor: [0, -34]        }),
      natural : L.icon({ //3
          iconUrl: siteUrl+'/templates/gesplan/images/i-natural.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                  popupAnchor: [0, -34]        }),
      publico : L.icon({
          iconUrl: siteUrl+'/templates/gesplan/images/i-publico.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                  popupAnchor: [0, -34]        }),
      general : L.icon({
          iconUrl: siteUrl+'/templates/gesplan/images/i-basic.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                  popupAnchor: [0, -34]        })
   };
   var equipmentsIcons = {

      acampada : L.icon({ //3
          iconUrl: siteUrl+'/templates/gesplan/images/i-acampada.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                  popupAnchor: [0, -34]        }),
      recreativo : L.icon({
          iconUrl: siteUrl+'/templates/gesplan/images/i-recreativo.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                  popupAnchor: [0, -34]        }),
      campamento : L.icon({
          iconUrl: siteUrl+'/templates/gesplan/images/i-campamentos.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                  popupAnchor: [0, -34]        }),
      visitantes : L.icon({
          iconUrl: siteUrl+'/templates/gesplan/images/i-publico.png',
          iconSize:     [26, 34], // size of the icon
          iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                popupAnchor: [0, -34]        })
  };
  var markersIcons = {

    start : L.icon({ //3
        iconUrl: siteUrl+'/templates/gesplan/images/i-start.png',
        iconSize:     [26, 34], // size of the icon
        iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -34]
    }),
    end : L.icon({
        iconUrl: siteUrl+'/templates/gesplan/images/i-end.png',
        iconSize:     [26, 34], // size of the icon
        iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -34]
    }),
    basic : L.icon({ //3
        iconUrl: siteUrl+'/templates/gesplan/images/i-basic.png',
        iconSize:     [26, 34], // size of the icon
        iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -34]

    })
};
var servicesIcons = {
  descanso : L.icon({ //1
      iconUrl: siteUrl+'/templates/gesplan/images/icono-campamentos-revisado.png',
      iconSize:     [26, 34], // size of the icon
      iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
              popupAnchor: [0, -34]        }),
  aparcamiento : L.icon({ //2
      iconUrl: siteUrl+'/templates/gesplan/images/i-parking.png',
      iconSize:     [26, 34], // size of the icon
      iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
              popupAnchor: [0, -34]        })
};
var incidencesIcons = {
  routeWarning : L.icon({ //1
      iconUrl: siteUrl+'/templates/gesplan/images/icon-warning.png',
      iconSize:     [26, 26], // size of the icon
      iconAnchor:   [13, 26], // point of the icon which will correspond to marker's location
              popupAnchor: [0, -26]        })
};
var incidencesForMap = [];
function getRoutesIncidences(){
    var routeIncidencesCategory = 92;
    var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + routeIncidencesCategory + "";

    Joomla.request({
        url: baseUrl +  "&plugin=Listajaxcontent",
        method: 'GET',
        perform: true,
        onSuccess: function onSuccess(resp) {
            // Remove loader image

            var response = {};

            try {
                response = JSON.parse(resp);
            } catch (e) {
            Joomla.renderMessages({
                error: [Joomla.Text._('Respuesta invalida')]
            }, "");
            return;
            }
            var routeIncidences = response.data[0].data;
           
            /* PINTAMOS LO EQUIPAMIENTOS */
            if(routeIncidences.length){

                const obj =(routeIncidences);
                routeIncidences.forEach(printDivContent);

                function printDivContent(item, index) {
                    var title = item.title; 
                    var itemId = item.id;
                    var endDate = item.jcfields[239].rawvalue;
                    var gemometry = item.jcfields[251].rawvalue;

                        var parsedGeometry = JSON.parse(gemometry);
                        var coords = parsedGeometry.features[0].geometry.coordinates;
                        console.log(parsedGeometry);
                        var geometryCenter = coords[parseInt(coords.length / 2)];
                        if(geometryCenter.length > 0){
                          if((parsedGeometry.features[0].geometry.type != "MultiLineString")){
                            let lat_lng = [geometryCenter[1], geometryCenter[0]];
                            L.marker(lat_lng, {icon: incidencesIcons.routeWarning}).addTo(incidences).bindPopup(title);  
                          }
                          else{
                              var geometryCenter = coords[0][parseInt( coords[0].length / 2)];
                              let lat_lng = [geometryCenter[1], geometryCenter[0]];
                              L.marker(lat_lng, {icon: incidencesIcons.routeWarning}).addTo(incidences).bindPopup(title);  
                          }
                         L.geoJSON(parsedGeometry, {style: {"color": "#ffeb3b", "className": "incidences", "weight": "2"}}).addTo(incidences);
                        }
                       
                }

            }	
        },
        onError: function onError() {
            Joomla.renderMessages({
            error: ['Something went wrong! Please close and reopen the browser and try again!']
            });
        }
    });
  }
  function getEquipmentIncidences(){

    var equipIncidencesCategory = 91;
    var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + equipIncidencesCategory + "";

    Joomla.request({
        url: baseUrl +  "&plugin=Listajaxcontent",
        method: 'GET',
        perform: true,
        onSuccess: function onSuccess(resp) {
            // Remove loader image

            var response = {};

            try {
                response = JSON.parse(resp);
            } catch (e) {
            Joomla.renderMessages({
                error: [Joomla.Text._('Respuesta invalida')]
            }, "");
            return;
            }
            var equipIncidences = response.data[0].data;

            /* PINTAMOS LO EQUIPAMIENTOS */
            if(equipIncidences.length){

                const obj =(equipIncidences);
                equipIncidences.forEach(printDivContent);

                function printDivContent(item, index) {
                    var title = item.title; 
                    var itemId = item.id;
                    var endDate = item.jcfields[239].rawvalue;
                    var incidenceLat = item.jcfields[249].rawvalue;
                    var incidenceLon = item.jcfields[250].rawvalue;
                    
                    let lat_lng = [incidenceLat, incidenceLon];

                    L.marker(lat_lng, {icon: incidencesIcons.routeWarning}).addTo(incidences).bindPopup(title); 
    
                }

            }	
        },
        onError: function onError() {
            Joomla.renderMessages({
            error: ['Something went wrong! Please close and reopen the browser and try again!']
            });
        }
    });
  }