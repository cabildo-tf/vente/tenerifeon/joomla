<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.cassiopeia
 *
 * @copyright   (C) 2017 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
if (($this->error->getCode()) == '404') {
	header('Location: ' . Jroute::_("index.php?option=com_content&view=article&id=1175Itemid=1175", false));
	exit;
	}

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;

/** @var Joomla\CMS\Document\ErrorDocument $this */

$app = Factory::getApplication();
$wa  = $this->getWebAssetManager();

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');
$menu     = $app->getMenu()->getActive();
$pageclass = $menu !== null ? $menu->getParams()->get('pageclass_sfx', '') : '';

// Template path
$templatePath = 'templates/' . $this->template;

// Color Theme
$paramsColorName = $this->params->get('colorName', 'colors_standard');
$assetColorName  = 'theme.' . $paramsColorName;
$wa->registerAndUseStyle($assetColorName, $templatePath . '/css/global/' . $paramsColorName . '.css');

// Use a font scheme if set in the template style options
$paramsFontScheme = $this->params->get('useFontScheme', false);
$fontStyles       = '';

if ($paramsFontScheme)
{
	if (stripos($paramsFontScheme, 'https://') === 0)
	{
		$this->getPreloadManager()->preconnect('https://fonts.googleapis.com/', []);
		$this->getPreloadManager()->preconnect('https://fonts.gstatic.com/', []);
		$this->getPreloadManager()->preload($paramsFontScheme, ['as' => 'style']);
		$wa->registerAndUseStyle('fontscheme.current', $paramsFontScheme, [], ['media' => 'print', 'rel' => 'lazy-stylesheet', 'onload' => 'this.media=\'all\'']);

		if (preg_match_all('/family=([^?:]*):/i', $paramsFontScheme, $matches) > 0)
		{
			$fontStyles = '--cassiopeia-font-family-body: "' . str_replace('+', ' ', $matches[1][0]) . '", sans-serif;
			--cassiopeia-font-family-headings: "' . str_replace('+', ' ', isset($matches[1][1]) ? $matches[1][1] : $matches[1][0]) . '", sans-serif;
			--cassiopeia-font-weight-normal: 400;
			--cassiopeia-font-weight-headings: 700;';
		}
	}
	else
	{
		$wa->registerAndUseStyle('fontscheme.current', $paramsFontScheme, ['version' => 'auto'], ['media' => 'print', 'rel' => 'lazy-stylesheet', 'onload' => 'this.media=\'all\'']);
		$this->getPreloadManager()->preload($wa->getAsset('style', 'fontscheme.current')->getUri() . '?' . $this->getMediaVersion(), ['as' => 'style']);
	}
}

// Enable assets
$wa->usePreset('template.cassiopeia.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr'))
	->useStyle('template.active.language')
	->useStyle('template.user')
	->useScript('template.user')
	->addInlineStyle(":root {
		--hue: 214;
		--template-bg-light: #f0f4fb;
		--template-text-dark: #495057;
		--template-text-light: #ffffff;
		--template-link-color: #2a69b8;
		--template-special-color: #001B4C;
		$fontStyles
	}");

// Override 'template.active' asset to set correct ltr/rtl dependency
$wa->registerStyle('template.active', '', [], [], ['template.cassiopeia.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr')]);

// Browsers support SVG favicons
$this->addHeadLink(HTMLHelper::_('image', 'joomla-favicon.svg', '', [], true, 1), 'icon', 'rel', ['type' => 'image/svg+xml']);
$this->addHeadLink(HTMLHelper::_('image', 'favicon.ico', '', [], true, 1), 'alternate icon', 'rel', ['type' => 'image/vnd.microsoft.icon']);
$this->addHeadLink(HTMLHelper::_('image', 'joomla-favicon-pinned.svg', '', [], true, 1), 'mask-icon', 'rel', ['color' => '#000']);

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . htmlspecialchars(Uri::root() . $this->params->get('logoFile'), ENT_QUOTES, 'UTF-8') . '" alt="' . $sitename . '">';
}
elseif ($this->params->get('siteTitle'))
{
	$logo = '<span title="' . $sitename . '">' . htmlspecialchars($this->params->get('siteTitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
	$logo = '<img src="' . $this->baseurl . '/' . $templatePath. '/images/logo.svg" class="logo d-inline-block" alt="' . $sitename . '">';
}

// Container
$wrapper = $this->params->get('fluidContainer') ? 'wrapper-fluid' : 'wrapper-static';

$this->setMetaData('viewport', 'width=device-width, initial-scale=1');

// Defer font awesome
$wa->getAsset('style', 'fontawesome')->setAttribute('rel', 'lazy-stylesheet');
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<jdoc:include type="metas" />
	<jdoc:include type="styles" />
	<jdoc:include type="scripts" />
</head>

<body class="site error_site <?php echo $option
	. ' ' . $wrapper
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ' ' . $pageclass;
	echo ($this->direction == 'rtl' ? ' rtl' : '');
?>">
	<header class="header container-header full-width">
		<?php if ($this->params->get('brand', 1)) : ?>
			<div class="grid-child">
				<div class="navbar-brand">
					<a class="brand-logo" href="<?php echo $this->baseurl; ?>/">
						<?php echo $logo; ?>
					</a>
					<?php if ($this->params->get('siteDescription')) : ?>
						<div class="site-description"><?php echo htmlspecialchars($this->params->get('siteDescription')); ?></div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if ($this->countModules('menu') || $this->countModules('search')) : ?>
			<div class="grid-child container-nav">
				<?php if ($this->countModules('menu')) : ?>
					<jdoc:include type="modules" name="menu" style="none" />
				<?php endif; ?>
				<?php if ($this->countModules('search')) : ?>
					<div class="container-search">
						<jdoc:include type="modules" name="search" style="none" />
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</header>

	<div class="site-grid">
		

		<!-- Quique aquí va tu contenido -->
		
		
	</div>
	<?php if ($this->countModules('footer', true)) : ?>
	<footer class="container-footer footer full-width">
		<div class="grid-child">
			<jdoc:include type="modules" name="footer" style="none" />
		</div>
	</footer>
	<?php endif; ?>
	<?php
		$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
		$appParams = JFactory::getApplication()->getParams();
		$model->setState('params', $appParams);
		$model->setState('filter.article_id', 64); //change that to your Category ID
		$nosotros =   $model->getItems();
		// There is only one article in the array, the this is the article
		$nosotros = $nosotros[0];

		$fields = FieldsHelper::getFields('com_content.article', $nosotros, true);
		// Adding the fields to the object
		$nosotros->jcfields = array();

		foreach ($fields as $key => $field)
		{
		$model->setState('filter.published', '1');

			$nosotros->jcfields[$field->id] = $field;
		}
	?>
	<!-- Get colaboradores fields group of article 64 "Sobre nosotros" -->
	<?php $colaboradores = ($nosotros->jcfields[119]->subform_rows) ?>
	<!-- Para descomentar COLABORADORES hay que quitar el ! del if(!count...) -->
	<?php if(!count($colaboradores)) : ?>
		<section class="colaboradores">
		<div class="container">
			<div class="row">
				<div class="col-md-4 offset-md-4">
					<h2 class="text-center"><?= Text::_("Colaboradores"); ?></h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<!-- loop -->
				<?php foreach($colaboradores as $key => $colaborador) : ?>
					<div class="col-md-2">
						<?php  $imgUrl = $colaborador["imagen"]->rawvalue["imagefile"]; ?>
						<?php if(!empty($imgUrl)) : ?>
							<a href="<?php echo $colaborador["enlace-imagen"]->rawvalue;?>" target="_blank" title="<?= Text::_("TPL_GESPLAN_LINK_TO_SITE") ?> <?php echo $colaborador["titulo-imagen"]->rawvalue;?>"> <img class=" w-100" src="<?php echo $imgUrl;?>" alt="<?php  echo($block["imagen-parrafo"]->rawvalue["alt_text"]); ?>"></a>
						<?php endif; ?>		
					</div>	

				<?php endforeach; ?>

				<!-- /loop -->
			</div>
		</div>
		</section>
	<?php endif; ?>
	<footer>
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-md-8">
                <h2>
                    <?= Text::_("TPL_GESPLAN_FOLLOW_US"); ?>
                </h2>
                <ul class="redes">
					<!-- <li><a href="<?php echo $nosotros->jcfields[123]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Youtube") ?>"><?= HTMLHelper::_('image','youtube-preh.svg', Text::_("TPL_GESPLAN_YOUTUBE_ICON"), null, true, 0) ?></a></li> -->
					<li><a href="<?php echo $nosotros->jcfields[124]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Instagram") ?>"><?= HTMLHelper::_('image','instagram-preh.svg', Text::_("Icono de Instagram"), null, true, 0) ?></a></li>
					<li><a href="<?php echo $nosotros->jcfields[122]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Twiter") ?>"><?= HTMLHelper::_('image','twitter-preh.svg', Text::_("Icono de Twitter"), null, true, 0) ?></a></li>
					<li><a href="<?php echo $nosotros->jcfields[120]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Facebook") ?>"><?= HTMLHelper::_('image','facebook-preh.svg', Text::_("Icono de Facebook"), null, true, 0) ?></a></li>
                </ul>
            </div>
        </div>
        <div class="row" style="display:none;">
            <div class="col-md-4">
                <p class="ul-title">El proyecto</p>
				<jdoc:include type="modules" name="menu-proyecto" style="xhtml" />
                <!-- <ul>
                    <li><a href="index.php?option=com_content&view=article&id=64" title=""> Sobre nosotros</a></li>
                    <li><a href="" title=""> Ayuda</a></li>
                    <li><?php echo HTMLHelper::_('link', RouteHelper::getCategoryRoute(10, $this->item->category_language), 'FAQ'); ?></li>
                    <li><a href="" title=""> Contacta</a></li>
                </ul> -->
            </div>
            <div class="col-md-4">
                <p class="ul-title">Te interesa</p>
                <ul class="doble-col">
                    <li><a href="" title=""> Itinerarios</a></li>
                    <li><a href="" title=""> Equipamientos</a></li>
                    <li><a href="" title=""> Actividades</a></li>
                    <li><a href="" title=""> Mapa interactivo</a></li>

                    <li><a href="" title=""> Itinerarios</a></li>
                    <li><a href="" title=""> Equipamientos</a></li>
                    <li><a href="" title=""> Actividades</a></li>
                    <li><a href="" title=""> Mapa interactivo</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <p class="ul-title">NO TE LO PIERDAS</p>
                <ul>
                    <li><a href="" title=""> Subida al Teide</a></li>
                    <li><a href="" title=""> Ruta de bikepacking por la Montaña Rajada</a></li>
                    <li><a href="" title=""> Ruta de costa a costa en 4x4</a></li>
                    <li><a href="" title=""> Itinerarios de pequeños lagos</a></li>
                </ul>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <blockquote>
                    <p><span class="tenerife-on-slogan">Tenerife ON</span><br/>
                        La guía oficial para planificar tu ocio de naturaleza en la isla
                    </p>
                </blockquote>
            </div>
        </div>
		<!-- Footer provisional -->

		<div class="row">
            <div class="col-md-6 offset-md-3 text-center">
				<ul class="list-links-footer">
					<li>
						<a href="/es/sobre-nosotros">Sobre nosotros</a>
					</li>
					<li>
						<a href="/es/faq">FAQ</a>
					</li>
				</ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="politica-cookis">
					<?php 
						//Enlaces a los artículos de la categoría 15- TEXTOS LEGALES
						$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
						$appParams = JFactory::getApplication()->getParams();
						$model->setState('params', $appParams);
						$model->setState('filter.category_id', 15); //change that to your Category ID
						$model->setState('filter.published', '1');
						$model->setState('list.ordering', 'id');
						$model->setState('list.direction', 'ASC');

						$articlesLegalText =   $model->getItems();
					?>
					<?php foreach ($articlesLegalText as &$article) :
					?>
                    <li><?php echo HTMLHelper::_('link', Route::_(RouteHelper::getArticleRoute($article->id, $article->catid, $article->language)), htmlspecialchars($article->title, ENT_COMPAT, 'UTF-8')); ?></li>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</footer>

	<?php if ($this->params->get('backTop') == 1) : ?>
		<a href="#top" id="back-top" class="back-to-top-link" aria-label="<?php echo Text::_('TPL_CASSIOPEIA_BACKTOTOP'); ?>">
			<span class="icon-arrow-up icon-fw" aria-hidden="true"></span>
		</a>
	<?php endif; ?>

	<jdoc:include type="modules" name="debug" style="none" />	
	<jdoc:include type="scripts" />	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-CVKDE4SKMH"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-CVKDE4SKMH');
	</script>
	<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

</body>
</html>
