<?php
/**
 * @package     Gesplan.Site
 * @subpackage  Templates.gesplan
 *
 * @copyright   (C) 2022 studiogenesis. <https://www.studiogenesis.es>
 */

defined('_JEXEC') or die('Acceso restringido');

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Router\Route;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Language\Associations;


/** @var Joomla\CMS\Document\HtmlDocument $this */

$app = Factory::getApplication();
$wa  = $this->getWebAssetManager();

// all favicons
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-57x57.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '57x57']);
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-60x60.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '60x60']);
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-72x72.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '72x72']);
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-76x76.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '76x76']);
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-114x114.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '114x114']);
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-120x120.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '120x120']);
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-144x144.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '144x144']);
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-152x152.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '152x152']);
$this->addHeadLink(HTMLHelper::_('image', 'apple-icon-180x180.png', '', [], true, 1), 'apple-touch-icon', 'rel', ['sizes' => '180x180']);
$this->addHeadLink(HTMLHelper::_('image', 'android-icon-192x192.png', '', [], true, 1), 'icon', 'rel', ['sizes' => '192x192', 'type' => 'image/png']);
$this->addHeadLink(HTMLHelper::_('image', 'favicon-32x32.png', '', [], true, 1), 'icon', 'rel', ['sizes' => '32x32', 'type' => 'image/png']);
$this->addHeadLink(HTMLHelper::_('image', 'favicon-96x96.png', '', [], true, 1), 'icon', 'rel', ['sizes' => '96x96', 'type' => 'image/png']);
$this->addHeadLink(HTMLHelper::_('image', 'favicon-16x16.png', '', [], true, 1), 'icon', 'rel', ['sizes' => '16x16', 'type' => 'image/png']);
$this->addHeadLink(HTMLHelper::_('image', 'manifest.json', '', [], true, 1), 'manifest', 'rel', null);
$this->setMetaData('msapplication-TileColor','msapplication-TileColor');
$this->setMetaData('msapplication-TileImage',HTMLHelper::_('image','ms-icon-144x144.png', '', [], true, 1));
$this->setMetaData('theme-color', '#ffffff');

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');
$menu     = $app->getMenu()->getActive();
$pageclass = $menu !== null ? $menu->getParams()->get('pageclass_sfx', '') : '';

// Template path
$templatePath = 'templates/' . $this->template;

// Color Theme
$paramsColorName = $this->params->get('colorName', 'colors_standard');
$assetColorName  = 'theme.' . $paramsColorName;
//$wa->registerAndUseStyle($assetColorName, $templatePath . '/css/global/' . $paramsColorName . '.css');

// Use a font scheme if set in the template style options
$paramsFontScheme = $this->params->get('useFontScheme', false);
$fontStyles       = '';

if ($paramsFontScheme)
{
	if (stripos($paramsFontScheme, 'https://') === 0)
	{
		$this->getPreloadManager()->preconnect('https://fonts.googleapis.com/', []);
		$this->getPreloadManager()->preconnect('https://fonts.gstatic.com/', []);
		$this->getPreloadManager()->preload($paramsFontScheme, ['as' => 'style']);
		$wa->registerAndUseStyle('fontscheme.current', $paramsFontScheme, [], ['media' => 'print', 'rel' => 'lazy-stylesheet', 'onload' => 'this.media=\'all\'']);

		if (preg_match_all('/family=([^?:]*):/i', $paramsFontScheme, $matches) > 0)
		{
			$fontStyles = '--cassiopeia-font-family-body: "' . str_replace('+', ' ', $matches[1][0]) . '", sans-serif;
			--cassiopeia-font-family-headings: "' . str_replace('+', ' ', isset($matches[1][1]) ? $matches[1][1] : $matches[1][0]) . '", sans-serif;
			--cassiopeia-font-weight-normal: 400;
			--cassiopeia-font-weight-headings: 700;';
		}
	}
	else
	{
		$wa->registerAndUseStyle('fontscheme.current', $paramsFontScheme, ['version' => 'auto'], ['media' => 'print', 'rel' => 'lazy-stylesheet', 'onload' => 'this.media=\'all\'']);
		$this->getPreloadManager()->preload($wa->getAsset('style', 'fontscheme.current')->getUri() . '?' . $this->getMediaVersion(), ['as' => 'style']);
	}
}

// Enable assets

$wa//->usePreset('template.cassiopeia.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr'))
	->useStyle('template.active.language')
	->useStyle('template.user')
	->useScript('template.user')
	->useStyle('template.gesplan.bootstrap')
	//->useStyle('template.gesplan.modules')
	->useStyle('slick')
	->useStyle('template.gesplan.custom')	
	->useStyle('template.gesplan.pagination')	
	->useStyle('template.gesplan.jquery-ui')	
	//->useScript('template.gesplan.bootstrap.script')
	->useScript('slick.script')
	->useScript('template.gesplan.jquery-ui')
	->useScript('template.gesplan.bootstrap.bundle')
	->useScript('template.gesplan.custom.script')
	->useScript('template.gesplan.register.script')
	->useScript('template.gesplan.pagination')
	->useScript('template.gesplan.leaflet-geometryutil')
	->useScript('template.gesplan.wise-leaflet-pip')
	->useScript('template.gesplan.leaflet.almostover')
	// ->useScript('template.gesplan.groupedlayercontrol')
	->addInlineStyle(":root {
		--hue: 214;
		--template-bg-light: #f0f4fb;
		--template-text-dark: #495057;
		--template-text-light: #ffffff;
		--template-link-color: #2a69b8;
		--template-special-color: #001B4C;
		$fontStyles
	}");

// Override 'template.active' asset to set correct ltr/rtl dependency
//$wa->registerStyle('template.active', '', [], [], ['template.cassiopeia.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr')]);

//Registen Gesplan CSS
//$wa->registerAndUseStyle('modules', $templatePath . '/css/modules.min.css');

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . Uri::root(true) . '/' . htmlspecialchars($this->params->get('logoFile'), ENT_QUOTES) . '" alt="' . $sitename . '">';
}
elseif ($this->params->get('siteTitle'))
{
	$logo = '<span title="' . $sitename . '">' . htmlspecialchars($this->params->get('siteTitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
	$logo = HTMLHelper::_('image', 'logo.svg', $sitename, ['class' => 'logo d-inline-block'], true, 0);
}

$hasClass = '';

if ($this->countModules('sidebar-left', true))
{
	$hasClass .= ' has-sidebar-left';
}

if ($this->countModules('sidebar-right', true))
{
	$hasClass .= ' has-sidebar-right';
}

// Container
$wrapper = $this->params->get('fluidContainer') ? 'wrapper-fluid' : 'wrapper-static';

$this->setMetaData('viewport', 'width=device-width, initial-scale=1');

$stickyHeader = $this->params->get('stickyHeader') ? 'position-sticky sticky-top' : '';

// Defer font awesome
$wa->getAsset('style', 'fontawesome')->setAttribute('rel', 'lazy-stylesheet');
//$wa->getAsset('style', 'fontawesome')->setAttribute('rel', 'lazy-stylesheet');
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<!-- Cookie Consent by TermsFeed Privacy Policy and Consent Generator https://www.TermsFeed.com -->
	<script type="text/javascript" src="//www.termsfeed.com/public/cookie-consent/4.0.0/cookie-consent.js" charset="UTF-8"></script>
	<script type="text/javascript" charset="UTF-8">
	document.addEventListener('DOMContentLoaded', function () {
	cookieconsent.run({"notice_banner_type":"simple","consent_type":"express","palette":"dark","language":"<?= substr($this->language,0,2); ?>","page_load_consent_levels":["strictly-necessary"],"notice_banner_reject_button_hide":true,"preferences_center_close_button_hide":false,"page_refresh_confirmation_buttons":false,"website_name":"Tenerife On","website_privacy_policy_url":"/politica-de-privacidad"});
	});
	</script>

	<!-- Google Analytics -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script type="text/plain" cookie-consent="tracking" async src="https://www.googletagmanager.com/gtag/js?id=UA-230011021-1"></script>
	<script type="text/plain" cookie-consent="tracking">
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-230011021-1');
	</script>
	<!-- end of Google Analytics-->

	<noscript>ePrivacy and GPDR Cookie Consent management by <a href="https://www.termsfeed.com/" rel="nofollow noopener">TermsFeed Privacy Policy and Consent Generator</a></noscript>
	<!-- End Cookie Consent by TermsFeed Privacy and Consent Generator https://www.TermsFeed.com -->

	<link rel="dns-prefetch" href="https://tile.opentopomap.org">
	<link rel="dns-prefetch preconnect" href="https://unpkg.com">
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css">
	<link href="/templates/gesplan/css/gesplan/all.css" rel="lazy-stylesheet" data-asset-name="fontawesome" />
	<jdoc:include type="metas" />	
	<jdoc:include type="styles" />
</head>

<body class="site <?php echo $option
	. ' ' . $wrapper
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($pageclass ? ' ' . $pageclass : '')
	. $hasClass
	. ($this->direction == 'rtl' ? ' rtl' : '');
?>">

	<header>
		<div class="preheader">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-6">
						<?= HTMLHelper::_('image', 'logo_cabildo_web.svg', 'Logo del cabildo', null, true, 0) ?>
					</div>
					<div class="col-md-8 col-6">
						<div class="cnt-uls">
							<div>
								<ul class="redes">
									<?php
										$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
										$appParams = JFactory::getApplication()->getParams();
										$model->setState('params', $appParams);
										$model->setState('filter.article_id', 64); //change that to your Category ID
										$model->setState('filter.published', '1');
				
										$nosotros =   $model->getItems();
										// There is only one article in the array, the this is the article
										$nosotros = $nosotros[0];

										$fields = FieldsHelper::getFields('com_content.article', $nosotros, true);
										// Adding the fields to the object
										$nosotros->jcfields = array();
					
										foreach ($fields as $key => $field)
										{
											$nosotros->jcfields[$field->id] = $field;
										}
									?>
									<li><a href="<?php echo $nosotros->jcfields[122]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Twiter") ?>"><?= HTMLHelper::_('image','twitter-preh-black.svg', Text::_("Icono de Twitter"), null, true, 0) ?></a></li>
									<!-- <li><a href="<?php echo $nosotros->jcfields[123]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Youtube") ?>"><?= HTMLHelper::_('image','youtube-preh-black.svg', Text::_("TPL_GESPLAN_YOUTUBE_ICON"), null, true, 0) ?></a></li> -->
									<li><a href="<?php echo $nosotros->jcfields[124]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Instagram") ?>"><?= HTMLHelper::_('image','instagram-preh-black.svg', Text::_("Icono de Instagram"), null, true, 0) ?></a></li>
									<li><a href="<?php echo $nosotros->jcfields[120]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Facebook") ?>"><?= HTMLHelper::_('image','facebook-preh-black.svg', Text::_("Icono de Facebook"), null, true, 0) ?></a></li>
								</ul>
							</div>
			
							<div class="cont-idiomas">
							<jdoc:include type="modules" name="top-languages" style="xhtml" />

								<!-- <button id="open-lang">ES<i class="fa fa-chevron-down"></i></button> -->
								<!-- <ul class="idiomas">
									<li><a href="" class="active" title="">es</a></li>
									<li><a href="" title="">en</a></li>
									<li><a href="" title="">de</a></li>
								</ul>  -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		

		<!--   ******************          -->
		<?php if ($this->countModules('topbar')) : ?>
			<div class="container-topbar">
			<jdoc:include type="modules" name="topbar" style="none" />
			</div>
		<?php endif; ?>

		<?php if ($this->countModules('below-top')) : ?>
			<div class="grid-child container-below-top">
				<jdoc:include type="modules" name="below-top" style="none" />
			</div>
		<?php endif; ?>
		<div class="containerShadow">
			<div class="menuShadow">
				<div id="openMenu" class="container">
					<button aria-label="Abre menú principal desplegable">
						<i class="fa fa-bars active"></i>
					</button>
				</div> 
				<div class="container head-sizeing">
					<div class="row">
						<div class="col-md-2 d-flex">
							<a class="navbar-brand" href="<?php echo $this->baseurl; ?>/">
							<?= HTMLHelper::_('image', 'Tenerife_horizontal_def.svg', 'logotipo Tenerife On', ['id' => 'desktopLogo'], true, 0); ?>
							<?= HTMLHelper::_('image', 'Tenerife_horizontal_def.svg', 'logotipo Tenerife On', ['id' => 'mobilLogo'], true, 0); ?>
							</a>
						</div>
						<div class="col-lg-10">
						
							

							<?php if ($this->countModules('menu', true) || $this->countModules('search', true)) : ?>
								<nav class="nav-desktop">
									
									<?php if ($this->countModules('menu', true)) : ?>
										<jdoc:include type="modules" name="menu" style="none" />
									<?php endif; ?>
									<?php if ($this->countModules('search', true)) : ?>
										<div class="container-search">
											<jdoc:include type="modules" name="search" style="none" />
										</div>
									<?php endif; ?>
									</nav>
							<?php endif; ?>

						
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- /////////************** -->
	</header>
	<div class="site-grid">
		<?php if ($this->countModules('banner', true)) : ?>
			<div class="container-banner full-width">
				<jdoc:include type="modules" name="banner" style="none" />
			</div>
		<?php endif; ?>

		<?php if ($this->countModules('top-a', true)) : ?>
		<div class="grid-child container-top-a">
			<jdoc:include type="modules" name="top-a" style="card" />
		</div>
		<?php endif; ?>

		<?php if ($this->countModules('top-b', true)) : ?>
		<div class="grid-child container-top-b">
			<jdoc:include type="modules" name="top-b" style="card" />
		</div>
		<?php endif; ?>

		<?php if ($this->countModules('sidebar-left', true)) : ?>
		<div class="grid-child container-sidebar-left">
			<jdoc:include type="modules" name="sidebar-left" style="card" />
		</div>
		<?php endif; ?>

		<div class="grid-child container-component">
			<div class="">
				<jdoc:include type="modules" name="breadcrumbs" style="none" />
			</div>
			<jdoc:include type="modules" name="main-top" style="card" />
			<jdoc:include type="message" />
			<main>
			<jdoc:include type="component" />
			</main>
			<jdoc:include type="modules" name="main-bottom" style="card" />
		</div>

		<?php if ($this->countModules('sidebar-right', true)) : ?>
		<div class="grid-child container-sidebar-right">
			<jdoc:include type="modules" name="sidebar-right" style="card" />
		</div>
		<?php endif; ?>

		<?php if ($this->countModules('bottom-a', true)) : ?>
		<div class="grid-child container-bottom-a">
			<jdoc:include type="modules" name="bottom-a" style="card" />
		</div>
		<?php endif; ?>

		<?php if ($this->countModules('bottom-b', true)) : ?>
		<div class="grid-child container-bottom-b">
			<jdoc:include type="modules" name="bottom-b" style="card" />
		</div>
		<?php endif; ?>
	</div>

	<?php if ($this->countModules('footer', true)) : ?>
	<footer class="container-footer footer full-width">
		<div class="grid-child">
			<jdoc:include type="modules" name="footer" style="none" />
		</div>
	</footer>
	<?php endif; ?>
	
	<!-- Get colaboradores fields group of article 64 "Sobre nosotros" -->
	<?php $colaboradores = ($nosotros->jcfields[119]->subform_rows) ?>
	<!-- Para descomentar COLABORADORES hay que quitar el ! del if(!count...) -->
	<?php if(!count($colaboradores)) : ?>
		<section class="colaboradores">
		<div class="container">
			<div class="row">
				<div class="col-md-4 offset-md-4">
					<h2 class="text-center"><?= Text::_("Colaboradores"); ?></h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<!-- loop -->
				<?php foreach($colaboradores as $key => $colaborador) : ?>
					<div class="col-md-2">
						<?php  $imgUrl = $colaborador["imagen"]->rawvalue["imagefile"]; ?>
						<?php if(!empty($imgUrl)) : ?>
							<a href="<?php echo $colaborador["enlace-imagen"]->rawvalue;?>" target="_blank" title="<?= Text::_("TPL_GESPLAN_LINK_TO_SITE") ?> <?php echo $colaborador["titulo-imagen"]->rawvalue;?>"> <img class=" w-100" src="<?php echo $imgUrl;?>" alt="<?php  echo($block["imagen-parrafo"]->rawvalue["alt_text"]); ?>"></a>
						<?php endif; ?>		
					</div>	

				<?php endforeach; ?>

				<!-- /loop -->
			</div>
		</div>
		</section>
	<?php endif; ?>
	<footer>
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-md-8">
                <h2>
                    <?= Text::_("TPL_GESPLAN_FOLLOW_US"); ?>
                </h2>
                <ul class="redes">
					<!-- <li><a href="<?php echo $nosotros->jcfields[123]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Youtube") ?>"><?= HTMLHelper::_('image','youtube-preh.svg', Text::_("TPL_GESPLAN_YOUTUBE_ICON"), null, true, 0) ?></a></li> -->
					<li><a href="<?php echo $nosotros->jcfields[124]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Instagram") ?>"><?= HTMLHelper::_('image','instagram-preh.svg', Text::_("Icono de Instagram"), null, true, 0) ?></a></li>
					<li><a href="<?php echo $nosotros->jcfields[122]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Twiter") ?>"><?= HTMLHelper::_('image','twitter-preh.svg', Text::_("Icono de Twitter"), null, true, 0) ?></a></li>
					<li><a href="<?php echo $nosotros->jcfields[120]->rawvalue;?>" target="_blank" title="<?= Text::_("Enlace al perfil de Facebook") ?>"><?= HTMLHelper::_('image','facebook-preh.svg', Text::_("Icono de Facebook"), null, true, 0) ?></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <blockquote>
                    <p><span class="tenerife-on-slogan">Tenerife ON</span><br/>
                        <?= Text::_("TPL_GESPLAN_TENERIFEON_SLOGAN") ?>
                    </p>
                </blockquote>
            </div>
        </div>
		<!-- Footer provisional -->

		<div class="row">
            <div class="col-md-6 offset-md-3 text-center">
				<ul class="list-links-footer">
					<li>
						<?php 
							$nosotrosId = 64; //Nosotros  page id
							$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
							$appParams = JFactory::getApplication()->getParams();
							$model->setState('params', $appParams);
							$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $nosotrosId );
							$articleLang = Factory::getLanguage()->getTag();
							$idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
							$catidLang =  intval(explode(":", $associations[$articleLang]->catid)[0]);
							$model->setState('filter.article_id', $idLang); 
							$model->setState('filter.category_id', $catidLang); //change that to your Category ID 
							$model->setState('filter.published', '1');
	
							$nosotros =   $model->getItems();
						?>
						<a href="<?php echo Route::_(RouteHelper::getArticleRoute($idLang, $catidLang, $articleLang));?>"><?= Text::_("TPL_GESPLAN_ABOUT_US")?></a>
					</li>
					<li>
						<?php 
							$categoryId = 10; //FAQ category original
							$associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($categoryId)); 
							$catLang =   Factory::getLanguage()->getTag() ;
							$catId = $associations[ $catLang];
						?>
						<a href="index.php?option=com_content&view=category&layout=gesplan:faq&id=<?=$catId?>">FAQ</a>
					</li>
					<li>
						<a href="#" id="open_preferences_center"><?= Text::_("TPL_GESPLAN_COOKIES_PREFERENCES"); ?></a>
					</li>
				</ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="politica-cookis">
					<?php 

						$categoryId = 15; //textos legales category original
						$associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($categoryId)); 
						$catLang =   Factory::getLanguage()->getTag() ;
						$catId = $associations[ $catLang];
						//Enlaces a los artículos de la categoría 15- TEXTOS LEGALES
						$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
						$appParams = JFactory::getApplication()->getParams();
						$model->setState('params', $appParams);
						$model->setState('filter.category_id', $catId); //change that to your Category ID
						$model->setState('filter.published', '1');
						$model->setState('list.ordering', 'id');
						$model->setState('list.direction', 'ASC');

						$articlesLegalText =   $model->getItems();
					?>
					<?php foreach ($articlesLegalText as &$article) :
					?>
                    <li><?php echo HTMLHelper::_('link', Route::_(RouteHelper::getArticleRoute($article->id, $article->catid, $article->language)), htmlspecialchars($article->title, ENT_COMPAT, 'UTF-8')); ?></li>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</footer>

	<?php if ($this->params->get('backTop') == 1) : ?>
		<a href="#top" id="back-top" class="back-to-top-link" aria-label="<?php echo Text::_('TPL_CASSIOPEIA_BACKTOTOP'); ?>">
			<span class="icon-arrow-up icon-fw" aria-hidden="true"></span>
		</a>
	<?php endif; ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
    integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<jdoc:include type="modules" name="debug" style="none" />	
	<jdoc:include type="scripts" />	

</body>
</html>
