<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   (C) 2010 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;
use Joomla\CMS\Language\Text;

if (!$list)
{
	return;
}

?>
 <div class="col-md-12">
	<div class="multiple-items">
		<?php $items = $list; ?>
		<?php require ModuleHelper::getLayoutPath('mod_articles_category', $params->get('layout', 'itinerario') . '_items'); ?>
	</div>
</div>
