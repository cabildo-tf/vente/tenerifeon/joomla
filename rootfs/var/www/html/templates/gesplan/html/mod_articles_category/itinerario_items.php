<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   (C) 2020 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;


?>
<?php foreach ($items as $item) : ?>
	<?php
		$fields = FieldsHelper::getFields('com_content.article', $item, true);
		//create  article link
		$articleLink = (RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));
		$articleLink = Route::_($articleLink);
		// Adding the fields to the object
		$item->jcfields = array();

		foreach ($fields as $key => $field)
		{
			if($field->id == 90){
				$item->jcfields[$field->id] = $field;
			}
			if($field->id == 57){
				$item->jcfields[$field->id] = $field;
			}
		}
	?>

	<div class="card-default">
		<button class="like-post"><i class="far fa-heart"></i></button>
		<figure>
			<div class="gradient"></div>
			<?php
			$images = json_decode($item->images); 
			if(isset($images->image_intro)) :
				$listImage['src'] = $images->image_intro ;
				$listImage['alt'] = $images->image_intro_alt;
			else :
				$listImage['src'] = "templates/gesplan/images/neutra-300.jpg";
				$listImage['alt'] = "Tenerife ON";
			endif;
			?>
			<img src="<?php echo $listImage['src'] ; ?>" alt="<?php echo $listImage['alt'] ; ?>" loading="lazy">
		</figure>
		<div class="caption">
			<div class="head-caption">
				<span><?= $item->jcfields[57]->rawvalue; // ubicación?></span>
			</div>
			<div class="body-caption">
				<h3><?php echo $item->title; ?></h3>
			</div>
			<div class="icons-caption">
				 <!-- Rating on article list view -->
				 <?php 
				   //Prepare icons for rating on articles
				   $iconStar     = HTMLHelper::_('image', 'plg_content_vote/vote-star.svg', '', '', true, true);
				   $iconHalfstar = HTMLHelper::_('image', 'plg_content_vote/vote-star-half.svg', '', '', true, true);
				   
				   // If you can't find the icons then skip it
				   if ($iconStar === null || $iconHalfstar === null)
				   {
					   return;
				   }
				   
				   // Get paths to icons
				   $pathStar     = JPATH_ROOT . substr($iconStar, strlen(Uri::root(true)));
				   $pathHalfstar = JPATH_ROOT . substr($iconHalfstar, strlen(Uri::root(true)));
				   
				   // Write inline '<svg>' elements
				   $star     = file_exists($pathStar) ? file_get_contents($pathStar) : '';
				   $halfstar = file_exists($pathHalfstar) ? file_get_contents($pathHalfstar) : '';

					$rating = (float) $item->rating;
					$rcount = (int) $item->rating_count;
					// Round to 0.5
					$rating = round($rating / 0.5) * 0.5;

					// Determine number of stars
					$stars = $rating;
					$img   = '';

					for ($i = 0; $i < floor($stars); $i++)
					{
						$img .= '<li class="vote-star">' . $star . '</li>';
					}

					if (($stars - floor($stars)) >= 0.5)
					{
						$img .= '<li class="vote-star-empty">' . $star . '</li>';
						$img .= '<li class="vote-star-half">' . $halfstar . '</li>';

						++$stars;
					}

					for ($i = $stars; $i < 5; $i++)
					{
						$img .= '<li class="vote-star-empty">' . $star . '</li>';
					}

					?>
					<div class="content_rating" role="img" aria-label="<?php echo Text::sprintf('PLG_VOTE_STAR_RATING', $rating); ?>">
						<?php if ($rcount) : ?>
							<p class="visually-hidden" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
								<?php echo Text::sprintf('PLG_VOTE_USER_RATING', '<span itemprop="ratingValue">' . $rating . '</span>', '<span itemprop="bestRating">5</span>'); ?>
								<meta itemprop="ratingCount" content="<?php echo $rcount; ?>">
								<meta itemprop="worstRating" content="1">
							</p>
						<?php endif; ?>
						<ul>
							<?php echo $img; ?>
						</ul>
					</div> 

					<!-- /Rating -->
				
			</div>
		</div>
		<div class="link-card">
			<a href="<?php echo $item->link; ?>"><?= Text::_("TPL_GESPLAN_MORE_INFO")?></a>
		</div>
	</div>

<?php endforeach; ?>
