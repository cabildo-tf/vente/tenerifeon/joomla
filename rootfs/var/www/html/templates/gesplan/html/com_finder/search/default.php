<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_finder
 *
 * @copyright   (C) 2011 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;

defined('_JEXEC') or die;

$this->document->getWebAssetManager()
	->useStyle('com_finder.finder')
	->useScript('com_finder.finder');

?>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->

<section>
	<div class="com-finder finder">
		<?php if ($this->params->get('show_page_heading')) : ?>
		<h1>
			<?php if ($this->escape($this->params->get('page_heading'))) : ?>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
			<?php else : ?>
			<?php echo $this->escape($this->params->get('page_title')); ?>
			<?php endif; ?>
		</h1>
		<?php endif; ?>
		<?php if ($this->params->get('show_search_form', 1)) : ?>
		<div id="search-form" class="com-finder__form">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1><?= $this->params->get('page_title'); ?></h1>
						<p><?= Text::_("TPL_GESPLAN_SEARCH_IN_WEB");?></p>
						<!-- buscador -->
						<?php echo $this->loadTemplate('form'); ?>
						<?php // Load the search results layout if we are performing a search. ?>
						<?php if ($this->query->search === true) : ?>
						<?php echo $this->loadTemplate('results'); ?>
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						<?= HTMLHelper::_('image', 'banner-buscador.svg', 'Título del banner', ['class' => 'w-100 mt-4' ], true, 0) ?>
					</div>
				</div>
			</div>
	
		</div>
		<?php endif; ?>
	</div>

</psection	