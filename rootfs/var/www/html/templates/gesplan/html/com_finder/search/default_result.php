<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_finder
 *
 * @copyright   (C) 2011 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Component\Finder\Administrator\Indexer\Helper;
use Joomla\Component\Finder\Administrator\Indexer\Taxonomy;
use Joomla\String\StringHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;

	// Calculate number of characters to display around the result
	$term_length = StringHelper::strlen($this->query->input);
	$desc_length = $this->params->get('description_length', 255);
	$pad_length  = $term_length < $desc_length ? (int) floor(($desc_length - $term_length) / 2) : 0;

	// Make sure we highlight term both in introtext and fulltext
	$full_description = $this->result->description;
	if (!empty($this->result->summary) && !empty($this->result->body))
	{
		$full_description = Helper::parse($this->result->summary . $this->result->body);
	}

	// Find the position of the search term
	$pos = $term_length ? StringHelper::strpos(StringHelper::strtolower($full_description), StringHelper::strtolower($this->query->input)) : false;

	// Find a potential start point
	$start = ($pos && $pos > $pad_length) ? $pos - $pad_length : 0;

	// Find a space between $start and $pos, start right after it.
	$space = StringHelper::strpos($full_description, ' ', $start > 0 ? $start - 1 : 0);
	$start = ($space && $space < $pos) ? $space + 1 : $start;

	$description = HTMLHelper::_('string.truncate', StringHelper::substr($full_description, $start), $desc_length, true);

$icon = '';
if (!empty($this->result->mime)) :
	$icon = '<span class="icon-file-' . $this->result->mime . '" aria-hidden="true"></span> ';
endif;

//Todas las categorías
$categories = JCategories::getInstance('Content');
//dd($this->result);
//Obtengo la categoría padre
switch($this->result->type_id){
	case 1: //Categoría		
		$catFatherId = $this->result->getElements()["parent_id"];
		$categoryType = $categories->get($catFatherId);
		$image = $categoryType->getParams()->get('image');
		$category = $categoryType->title;
		break;
	case 3: //Artículo
		$category = $this->result->getElements()["category"];
		$catId = $this->result->getElements()["catid"];
		$categoryType = $categories->get($catId);
		$parent_id = $categoryType->parent_id;
		while($parent_id != 0){
			$categoryType = $categories->get($parent_id);			
			$parent_id = $categoryType->parent_id;
		}
		//$category = $categoryType->title;
		$image = $categoryType->getParams()->get('image');
		$ArticleId = $this->result->getElements()["id"];		
		break;
	default:
		break;
}

?>
<div class="itemBusqueda">
	<?php if($image) { ?>
		<div class="icon">
			<img class="img-fluid" src="<?= $image ?>" alt="<?= $category ?>">
		</div>
	<?php } ?>
	<div class="content-busqueda">
        <p class="tipe"><?= $category ?></p>
		<?= HTMLHelper::link(
					Route::_($this->result->cleanURL),
					$this->result->title,
					[
							'class' => 'head-busqueda'
					])
			 ?>
		<p class="des-busqueda"><?= $description ?></p>
		<?php if ($this->result->route) : ?>
			<?php echo HTMLHelper::link(
					Route::_($this->result->cleanURL),
					$this->baseUrl . Route::_($this->result->cleanURL),
					[
							'class' => 'more-info'
					]
			); ?>
		<?php endif; ?>

	</div>	
</div>
