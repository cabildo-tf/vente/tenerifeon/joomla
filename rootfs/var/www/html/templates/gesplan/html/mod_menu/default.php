<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   (C) 2009 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ModuleHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\Component\Content\Site\Helper\RouteHelper;


/** @var Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $app->getDocument()->getWebAssetManager();
$wa->registerAndUseScript('mod_menu', 'mod_menu/menu.min.js', [], ['type' => 'module']);
$wa->registerAndUseScript('mod_menu', 'mod_menu/menu-es5.min.js', [], ['nomodule' => true, 'defer' => true]);

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}

// The menu class is deprecated. Use mod-menu instead
?>
<?php
	$app = JFactory::getApplication();
	$menu = $app->getMenu();
	$user = JFactory::getUser();
?>
<div id="content-menu">
    <button id="closeMenu">
        <?= Text::_('TPL_GESPLAN_CLOSE') ?><i class="fa fa-times"></i>
    </button>
	<figure>
		<?= HTMLHelper::_('image','menu.jpg', Text::_("Imagen de menu"), null, true, 0) ?>
	</figure>
    <div class="content-login order-last order-lg-first">
	<?php   //Get association of article Login 
		$articleId = 1158;
		$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
		$articleLang = Factory::getLanguage()->getTag();
		if(count($associations) > 0) {
			$articleLang = Factory::getLanguage()->getTag();
			$idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
			$catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
		}
		else{
			$articleLang = 'es-ES';
			$idLang = $articleId;
			$catidLang = 69;
		}
	?>
        <a href="<?php echo Route::_(RouteHelper::getArticleRoute( $idLang, $catidLang, $articleLang ));?>" class="link-app 1">
		<i class="fa fa-mobile-alt"></i>	
		<!-- <?= HTMLHelper::_('image','icon-mobil-menu.svg', Text::_('TPL_GESPLAN_APP_DOWNLOAD'), null, true, 0) ?> -->
            <span class="desktop"><?= Text::_('TPL_GESPLAN_APP_DOWNLOAD') ?></span>
            <!-- <span class="mobil">App</span> -->
        </a>
		<div class="id-controler">
		<?php if($user->guest) : //Check if user is logged in And is not in front page as we dont't want voing here?>
			<a href="<?php echo Route::_('index.php?option=com_users&view=login'); ?>" class="link-app 2">
				<!-- <?= HTMLHelper::_('image','icon-login-menu.svg', Text::_('TPL_GESPLAN_LOGIN_REGISTER'), null, true, 0) ?> -->
				
				<i class="far fa-user"></i>
				<?= Text::_('TPL_GESPLAN_LOGIN_REGISTER') ?>
			</a>
		<?php else : ?>
			
			<a href="<?php echo Route::_('index.php?option=com_users&view=profile'); ?>" class="link-app 3">
				<!-- <?= HTMLHelper::_('image','icon-login-menu.svg', Text::_('TPL_GESPLAN_LOGIN_REGISTER'), null, true, 0) ?> -->
				<i class="far fa-user"></i>
				<?=Text::_('TPL_GESPLAN_GREETING_USER') ?>, <?= $user->name; ?>
			</a>
			
			<?= HTMLHelper::_('link', Route::_('index.php?option=com_users&view=login&layout=logout&task=user.menulogout'),'<i class="fa fa-times"></i>' , [ 'class' => 'link-app desconect']); ?>
			
		<?php endif; ?>
		</div>
    </div>
	<?php 
		$searchPageOriginalId = 2168;
		$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $searchPageOriginalId);
		$articleLang = Factory::getLanguage()->getTag();
		if(count($associations) > 0) {
			$articleLang = Factory::getLanguage()->getTag();
			$idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
			$catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
		}
		else{
			$articleLang = 'es-ES';
			$idLang = $searchPageOriginalId;
			$catidLang = 35; //Guía de actividades categoty id
		}
	?>
    <div class="d-block d-lg-none order-1">
        <form action="<?= Route::_(RouteHelper::getArticleRoute($idLang, $catidLang, $articleLang)) ?>">
            <label for="q"><?=Text::_('TPL_GESPLAN_SEARCH_WEB')?></label>
            <div class="search-mobil input-group">
                <input type="text" name="q" id="q" class="form-control">
                <button type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </div>
		
        </form>
    </div>
	
	<ul<?php echo $id; ?> class="order-2">
	<?php foreach ($list as $i => &$item)
	{
		$itemParams = $item->getParams();
		$class      = '' . $item->id;

		if ($item->id == $default_id)
		{
			$class .= '';
		}

		if ($item->id == $active_id || ($item->type === 'alias' && $itemParams->get('aliasoptions') == $active_id))
		{
			$class .= ' current';
		}

		if (in_array($item->id, $path))
		{
			$class .= ' active';
		}
		elseif ($item->type === 'alias')
		{
			$aliasToId = $itemParams->get('aliasoptions');

			if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
			{
				$class .= ' active';
			}
			elseif (in_array($aliasToId, $path))
			{
				$class .= ' alias-parent-active';
			}
		}

		if ($item->type === 'separator')
		{
			$class .= ' divider';
		}

		if ($item->deeper)
		{
			$class .= ' deeper';
		}

		if ($item->parent)
		{
			$class .= ' parent';
		}

		echo '<li class="' . $class . '">';

		switch ($item->type) :
			case 'separator':
			case 'component':
			case 'heading':
			case 'url':
				require ModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
				break;

			default:
				require ModuleHelper::getLayoutPath('mod_menu', 'default_url');
				break;
		endswitch;

		// The next item is deeper.
		if ($item->deeper)
		{
			echo '<ul class="mod-menu__sub list-unstyled small">';
		}
		// The next item is shallower.
		elseif ($item->shallower)
		{
			echo '</li>';
			echo str_repeat('</ul></li>', $item->level_diff);
		}
		// The next item is on the same level.
		else
		{
			echo '</li>';
		}
	}
	?>
	<!-- <li class="d-none d-lg-block">
        <a class="" href="<?= Route::_('index.php?option=com_finder&view=search') ?>"><i class="fa fa-search"></i></a>
    </li>	 -->
	</ul>
</div>
