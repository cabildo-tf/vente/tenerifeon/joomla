<?php

defined('_JEXEC') or die;

?>
<?php if ($displayData['params']->get('show_category')) : ?>
	<?php echo $this->sublayout('category', $displayData); ?>
<?php endif; ?>