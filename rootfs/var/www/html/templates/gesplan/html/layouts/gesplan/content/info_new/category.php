<?php
/**
 * @package     Gesplan.Site
 * @subpackage  Layout
 *
 * @copyright   (C) 2022 Studiogenesis <https://studiogenesis.es>
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;

$title = $this->escape($displayData['item']->category_title);
?>
<?php if (!empty($displayData['item']->catid)) : ?>
	<?php $url = '<a href="' . Route::_(
			RouteHelper::getCategoryRoute($displayData['item']->catid, $displayData['item']->category_language)
			)
			. '" itemprop="genre">' . $title . '</a>'; ?>
		<?php echo $url ?>
	<?php else : ?>
	<span itemprop="genre"><?= $title ?></span>
	<?php endif; ?>