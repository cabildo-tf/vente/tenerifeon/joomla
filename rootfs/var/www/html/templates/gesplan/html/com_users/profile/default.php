<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   (C) 2009 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\User\User;

?>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<section class="hero__section hero__section--personal-information">
    <figure>
        <?= HTMLHelper::_('image','head-user.jpg', 'Header profile', null, true, 0) ?>
    </figure>
</section>
<section class="intro intro--basic">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_DATA'); ?>
                </h1>

            </div>
        </div>
    </div>
</section>
<script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"
    integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ=="
    crossorigin=""></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<?php 	$user =   Factory::getUser();?>
<?php //dd($this->data) ?>
<section class="faq pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="col">

                    <div class="responsive-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <button class="nav-link active" id="faqtabs-profile" data-bs-toggle="tab"
                                    data-bs-target="#profile-profile" type="button" role="tab" aria-controls="home"
                                    aria-selected="true"><?= Text::_("TPL_GESPLAN_USERS_PROFILE_DATA");?></button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" id="faqtabs-ratings" data-bs-toggle="tab"
                                    data-bs-target="#profile-ratings" type="button" role="tab" aria-controls="home"
                                    aria-selected="false"
                                    data-userid="<?= $user->id?>"><?= Text::_("TPL_GESPLAN_USERS_PROFILE_RATINGS");?></button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" id="faqtabs-favourites" data-bs-toggle="tab"
                                    data-bs-target="#profile-favourites" type="button" role="tab" aria-controls="home"
                                    aria-selected="false"
                                    data-userid="<?= $user->id?>"><?= Text::_("TPL_GESPLAN_USERS_PROFILE_FAVOURITES");?></button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" id="faqtabs-routes" data-bs-toggle="tab"
                                    data-bs-target="#profile-routes" type="button" role="tab" aria-controls="home"
                                    aria-selected="false"
                                    data-userid="<?= $user->id?>"><?= Text::_("TPL_GESPLAN_USERS_PROFILE_ROUTES");?></button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" id="faqtabs-activities" data-bs-toggle="tab"
                                    data-bs-target="#profile-activities" type="button" role="tab" aria-controls="home"
                                    aria-selected="false"
                                    data-userid="<?= $user->id?>"><?= Text::_("TPL_GESPLAN_USERS_PROFILE_ACTIVITIES");?></button>
                            </li>
                        </ul>

                        <div id="content" class="tab-content" role="tablist">
                            <div id="profile-profile" class="card tab-pane fade show active" role="tabpanel"
                                aria-labelledby="category1-tab">
                                <section id="personal__information" class="personal__information">
                                    <div class="com-users-profile profile">

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <div class="user__photo">
                                                                <picture>
                                                                    <?php if(($this->data->jcfields[133]->rawvalue) != "") : ?>
                                                                    <?php $avatar = json_decode($this->data->jcfields[133]->rawvalue);?>
                                                                    <img src="<?= Uri::root() ?>/media/plg_fields_bffile/com_users.user/<?= $avatar->storedname ?>"
                                                                        alt="<?= Text::_('');?>"
                                                                        class="perfil__user img-fluid" />
                                                                    <?php else : ?>
                                                                    <?= HTMLHelper::_('image','user-avatar.png',  $this->escape($this->data->name), 'class="perfil__user img-fluid"', true, 0) ?>
                                                                    <?php endif; ?>
                                                                </picture>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <form>
                                                        <fieldset>
                                                            <legend><?= Text::_("TPL_GESPLAN_USERS_PROFILE_DATA");?>
                                                            </legend>
                                                            <div class="form-group">
                                                                <label
                                                                    for="user-name"><?php echo Text::_('COM_USERS_PROFILE_USERNAME_LABEL'); ?></label>
                                                                <p><strong><?php echo $this->escape($this->data->username); ?></strong>
                                                                    <p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="name"><?php echo Text::_('COM_USERS_PROFILE_NAME_LABEL'); ?></label>
                                                                <p><strong><?php echo $this->escape($this->data->name); ?></strong>
                                                                    <p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="surname"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_SURNAME_LABEL'); ?></label>
                                                                <p><strong><?php echo $this->escape($this->data->jcfields[126]->rawvalue); ?></strong>
                                                                    <p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="email"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_EMAIL_LABEL'); ?></label>
                                                                <p><strong><?php echo $this->escape($this->data->email); ?></strong>
                                                                    <p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="phone"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_PHONE_LABEL'); ?></label>
                                                                <p><strong><?php echo $this->escape($this->data->profile["phone"]);?></strong>
                                                                    <p>
                                                            </div>

                                                            <div class="form-group">
                                                                <label
                                                                    for="surname"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_ARE_YOU_LICENSED_LABEL'); ?></label>
                                                                <p><strong><?php if($this->data->jcfields[129]->rawvalue == '1'){echo Text::_('TPL_GESPLAN_USERS_PROFILE_YES');}else{echo Text::_('TPL_GESPLAN_USERS_PROFILE_NO');} ?></strong>
                                                                    <p>
                                                            </div>
                                                            <?php if(($this->data->jcfields[128]->rawvalue) != "") :?>
                                                            <div class="form-group">
                                                                <label
                                                                    for="surname"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_MOUNTAIN_LICENSE'); ?></label>
                                                                <p><strong><?php echo $this->escape($this->data->jcfields[128]->rawvalue); ?></strong>
                                                                    <p>
                                                            </div>
                                                            <?php endif; ?>
                                                            <?php if(($this->data->jcfields[132]->rawvalue) != "") : ?>
                                                            <div class="form-group">
                                                                <label
                                                                    for="surname"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_CYCLING_LICENSE'); ?></label>
                                                                <p><strong><?php echo $this->escape($this->data->jcfields[132]->rawvalue); ?></strong>
                                                                    <p>
                                                            </div>
                                                            <?php endif; ?>
                                                            <div class="card card-body--gray">
                                                                <div class="card-body">
                                                                    <fieldset>
                                                                        <?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_INTRESTED_IN_LABEL'); ?>
                                                                    </fieldset>
                                                                    <!--<p><?php echo Text::_('COM_USERS_PROFILE_INTRESTED_DESCRIPTION_LABEL'); ?></p>-->
                                                                    <div class="row">
                                                                        <?php 
                                                            $intrested = $this->data->jcfields[88]->fieldparams["options"];
                                                            (array)$chosenIntres = $this->data->jcfields[88]->rawvalue;
                                                        ?>
                                                                        <?php foreach($intrested as $option) : ?>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-check">
                                                                                <input class="form-check-input"
                                                                                    type="checkbox" disabled
                                                                                    <?php if(!empty($chosenIntres)){if(in_array($option->value, (array)$chosenIntres)){echo "checked";}} ?>
                                                                                    value="<?= $option->value ? $option->value : "";?>"
                                                                                    id="<?= $option->value;?>">
                                                                                <label class="form-check-label"
                                                                                    for="flexCheckDefault"><?= Text::_($option->name);?></label>
                                                                            </div>
                                                                        </div>
                                                                        <?php endforeach; ?>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </fieldset>


                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <?php if (Factory::getUser()->id == $this->data->id) : ?>
                                                                <a class="btn save__info mb-3"
                                                                    href="<?php echo Route::_('index.php?option=com_users&task=profile.edit&user_id=' . (int) $this->data->id); ?>">
                                                                    <span class="icon-user-edit"
                                                                        aria-hidden="true"></span>
                                                                    <?php echo Text::_('COM_USERS_EDIT_PROFILE'); ?>
                                                                </a>
                                                                <?php endif; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <a href="#" class="delete__info" data-bs-toggle="modal"
                                                                    data-bs-target="#modal-delete-user" onclick="deleteUser()"><?= Text::_("COM_USERS_DELETE_PROFILE");?></a>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <script>
                                                    function deleteUser(){
                                                        Swal.fire({
                                                            title: '<?= Text::_("TPL_GESPLAN_WANT_TO_DELETE_YOUR_ACCOUNT");?>',
                                                            text: '<?= Text::_("TPL_GESPLAN_THIS_ACTION_IS_UNREVERSIBLE");?>',
                                                            icon: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonColor: '#3085d6',
                                                            cancelButtonColor: '#d33',
                                                            confirmButtonText: '<?= Text::_("TPL_GESPLAN_YES_DELETE_ME");?>',
                                                            cancelButtonText: '<?= Text::_("TPL_GESPLAN_CANCEL");?>'
                                                            }).then((result) => {
                                                            if (result.isConfirmed) {
                                                                 window.location.href += "?deleteUser=<?= $user->id?>";
                                                            }
                                                        })
                                                    }

                                                </script>
                                                <!-- <div class="col-lg-3">
                                                        <div class="card-body">
                                                        <p class="mb-2"><strong>¿Quieres ser colaborador?</strong></p>
                                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque nisi possimus iure excepturi.</p>
                                                        <p class="mt-auto mb-0"><a href="" class="btn btn-primary">Más información</a></p>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <!-- <div class="card">

                                                </div> -->

                                            </div>

                                        </div>
                                </section>

                            </div>

                            <div id="profile-ratings" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="category2-tab">
                                <div class="container my-3">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <?= HTMLHelper::_('image','ilustración-soporte-1.svg', Text::_("Ilustración en la naturaleza"), 'class="img-fluid"', true, 0) ?>
                                        </div>
                                        <div class="col-md-7 d-flex">
                                            <div  class="content-noContent">
                                                <p class="noContentTitel"><?= Text::_("TPL_GESPLAN_NO_CONTENT_RATED");?></p>
                                                <!-- <a href="" class="custom-default"></a> -->
                                                <p class="noContentUser"><?= Text::_("TPL_GESPLAN_RATE_AND_HELP_USERS");?></p>
                                            </div >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="profile-favourites" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="category3-tab">
                                <div class="container my-3">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <?= HTMLHelper::_('image','ilustración-soporte-2.svg', Text::_("Ilustración en la naturaleza"), 'class="img-fluid"', true, 0) ?>
                                        </div>
                                        <div class="col-md-7 d-flex">
                                            <div class="content-noContent">
                                                <p class="noContentTitel"><?= Text::_("TPL_GESPLAN_HAVE_NO_FAVOURITE");?></p>
                                                <p class="noContentUser"><?= Text::_("TPL_GESPLAN_MARK_CONTENT_AS_FAVOURITE");?></p>
                                            </div>
                                            <!-- <a href="" class="custom-default"></a> -->
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div id="profile-routes" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="category4-tab">
                                <div class="row my-3">
                                    <div class="col-md-5">
                                        <?= HTMLHelper::_('image','ilustración-soporte-1.svg', Text::_("Ilustración en la naturaleza"), 'class="img-fluid"', true, 0) ?>
                                    </div>
                                    <div class="col-md-7 d-flex">
                                        <div class="content-noContent">
                                            <p class="noContentTitel"><?= Text::_("TPL_GESPLAN_HAVE_NO_SAVED_ROUTE");?></p>
                                            <p class="noContentUser"><?= Text::_("TPL_GESPLAN_PLAN_YOUR_FIRST_ROUTE");?></p>
                                        </div>
                                        <!-- <a href="" class="custom-default"></a> -->
                                    </div>
                                </div>
                            </div>
                            <div id="profile-activities" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="category5-tab">
                                <div class="row my-3">
                                    <div class="col-md-5">
                                        <?= HTMLHelper::_('image','ilustración-soporte-2.svg', Text::_("Ilustración en la naturaleza"), 'class="img-fluid"', true, 0) ?>
                                    </div>
                                    <div class="col-md-7 d-flex">
                                        <div class="content-noContent">
                                            <p class="noContentTitel"><?= Text::_("TPL_GESPLAN_ACTIVITY_DONE");?></p>
                                            <p class="noContentUser"><?= Text::_("TPL_GESPLAN_ACTIVITY_COMPLETED");?></p>
                                        </div>
                                        <!-- <a href="" class="custom-default"></a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>
<?php 
    if(isset($_GET['deleteUser'])){
        $deleteUser = $_GET['deleteUser'];
        if(!is_null($deleteUser)){
            $user   = User::getInstance($deleteUser);

            if (!$user->delete())
            {
                // Error occurred.
            }
            else{
                echo "<script>window.location.href='/';</script>";
            }
        }
    }

?>