<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   (C) 2009 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.formvalidator');

?>
    <!-- <section class="head-registro"></section> -->
    <section class="registro">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-flex flex-column">
					<?php if ($this->params->get('show_page_heading')) : ?>
						<div class="section-title">
							<h1>
								<?php echo $this->escape($this->params->get('page_heading')); ?>
							</h1>
						</div>
					<?php endif; ?>
                   
					<form id="user-registration" action="<?php echo Route::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="com-users-reset__form form-validate form-horizontal well">
						<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
							<fieldset>
						
								<div class="section-title" >
									<h1><?= JText::_("Recuperar contraseña");?></h1>
									<p><?php echo Text::_($fieldset->label); ?></p>
								</div>
								<?php echo $this->form->renderFieldset($fieldset->name); ?>
							</fieldset>
						<?php endforeach; ?>

								<button type="submit" class="custom-defoult">
									<?php echo Text::_('JSUBMIT'); ?>
								</button>

						<?php echo HTMLHelper::_('form.token'); ?>
					</form>
                </div>
              
            </div>
        </div>
    </section>




<?php
	$path = dirname(__FILE__);
	$path = dirname(dirname($path))."/com_content/article/partials/";

	require_once($path.'ayuda.php');
?>