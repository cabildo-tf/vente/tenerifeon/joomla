<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   (C) 2009 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.formvalidator');

?>
<!-- <section class="head-registro"></section> -->
    <section class="registro">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-flex flex-column">					
						<div class="section-title">
							<h1><?= Text::_('TPL_GESPLAN_LOGIN_REMIND') ?></h1>
						</div>
						<form id="user-registration" action="<?php echo Route::_('index.php?option=com_users&task=remind.remind'); ?>" method="post" class="com-users-remind__form form-validate form-horizontal well">
							<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
								<fieldset>
									<?php if (isset($fieldset->label)) : ?>
										<legend><?php echo Text::_($fieldset->label); ?></legend>
									<?php endif; ?>
									<?php echo $this->form->renderFieldset($fieldset->name); ?>
								</fieldset>
							<?php endforeach; ?>
							<div class="com-users-remind__submit control-group">
								<div class="controls">
									<button type="submit" class="custom-defoult validate">
										<?php echo Text::_('JSUBMIT'); ?>
									</button>
								</div>
							</div>
							<?php echo HTMLHelper::_('form.token'); ?>
						</form>
				</div>
				</div>
        </div>
</section>
