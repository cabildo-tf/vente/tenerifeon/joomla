<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   (C) 2009 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;

HTMLHelper::_('bootstrap.tooltip', '.hasTooltip');

// Load user_profile plugin language
$lang = Factory::getLanguage();
$lang->load('plg_user_profile', JPATH_ADMINISTRATOR);

/** @var Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $this->document->getWebAssetManager();
$wa->useScript('keepalive')
	->useScript('form.validate');
	//->useScript('com_users.two-factor-switcher');

?>
<section class="hero__section hero__section--personal-information">
	<figure>
	<?= HTMLHelper::_('image','head-user.jpg', 'Header profile', null, true, 0) ?><
	</figure>
</section>
<section class="intro intro--basic">
	<div class="container">
		<div class="row">
			<div class="col">
         
                    <h1>
                    <?php echo Text::_('TPL_GESPLAN_USERS_EDIT_PROFILE'); ?>
                    </h1>
          
			</div>
		</div>
	</div>
</section>
<?php   $coreFields = $this->form->getFieldset("core"); //Default user fields
        $fields_12Fields = $this->form->getFieldset("fields-12"); //Custom fields user fields
        $profileFields = $this->form->getFieldset("profile"); //Profile plugin fields
        $socialLoginFields= $this->form->getFieldset("sociallogin"); //
?>
<section id="personal__information" class="personal__information">
	<div class="com-users-profile profile">
    <form id="member-profile" action="<?php echo Route::_('index.php?option=com_users'); ?>" method="post" class="com-users-profile__edit-form form-validate form-horizontal well" enctype="multipart/form-data">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="user__photo">
                                    <picture>
                                        <?php if($fields_12Fields["jform_com_fields_avatar"]->value == "") : ?>                                     
                                            <?= HTMLHelper::_('image','user-avatar.png',  $this->escape($this->data->name), 'class="perfil__user img-fluid"', true, 0) ?> 
                                            <?php echo $fields_12Fields["jform_com_fields_avatar"]->renderField();?>
                                        <?php else : ?>
                                            <?php echo $fields_12Fields["jform_com_fields_avatar"]->renderField();?>
                                        <?php endif; ?>
                                    </picture>
                                    <!-- <div class="accions-user">
                                        <a class="edit" href="" title="Edita tu fotografía"><i class="fa fa-pencil-alt"></i></a>
                                        <a class="delete" href="" title="Elimina tu fotografía"><i class="fas fa-trash"></i></a>
                                    </div> -->
                                </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <?php   // Iterate through the form fieldsets and display each one. ?>
                      <?php

                                echo $coreFields["jform_id"]->renderField();
                                echo $coreFields["jform_name"]->renderField();
                                echo $fields_12Fields["jform_com_fields_apellidos"]->renderField();
                                echo $coreFields["jform_username"]->renderField();
                                echo $coreFields["jform_email1"]->renderField();
                                echo $profileFields["jform_profile_phone"]->renderField();
                                echo $fields_12Fields["jform_com_fields_federado_en_algun_deporte"]->renderField();
                               //echo $fields_12Fields["jform_com_fields_montanismo"]->renderField();
                                echo $fields_12Fields["jform_com_fields_licencia_montana"]->renderField();
                                //echo $fields_12Fields["jform_com_fields_ciclismo"]->renderField();
                                echo $fields_12Fields["jform_com_fields_licencia_ciclismo"]->renderField();
                                echo $fields_12Fields["jform_com_fields_intereses"]->renderField();
                                echo $coreFields["jform_password1"]->renderField();
                                echo $coreFields["jform_password2"]->renderField();?>
                      
                    <hr>
                    <?php foreach ($this->form->getFieldsets() as $group => $fieldset) : ?>
                        <?php if($group == "sociallogin") : ?>
                            <?php   $fields = $this->form->getFieldset($group); ?>
                            <?php if (count($fields)) : ?>
                                <fieldset>
                                    <?php // If the fieldset has a label set, display it as the legend. ?>
                                    <?php if (isset($fieldset->label)) : ?>
                                        <legend>
                                            <?php echo Text::_($fieldset->label); ?>
                                        </legend>
                                    <?php endif;?>
                                    <?php if (isset($fieldset->description) && trim($fieldset->description)) : ?>
                                        <p>
                                            <?php echo $this->escape(Text::_($fieldset->description)); ?>
                                        </p>
                                    <?php endif; ?>
                                    <?php // Iterate through the fields in the set and display them. ?>
                                    <?php foreach ($fields as $field) :  ?>
                                        <?php echo $field->renderField(); ?>
                                    <?php endforeach; ?>
                                </fieldset>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                 
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn save__info mb-3" name="task" value="profile.save">
                                    <span class="icon-check" aria-hidden="true"></span>
                                    <?php echo Text::_('JSAVE'); ?>
                                </button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="delete__info" name="task" value="profile.cancel" formnovalidate>
                                    <span class="icon-times" aria-hidden="true"></span>
                                    <?php echo Text::_('JCANCEL'); ?>
                                </button>
                                <input type="hidden" name="option" value="com_users">
                            </div>
                        </div>
                        <?php echo HTMLHelper::_('form.token'); ?>

                
                    </div>
                    <!-- <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                              <p class="mb-2"><strong>¿Quieres ser colaborador?</strong></p>
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque nisi possimus iure excepturi.</p>
                              <p class="mt-auto mb-0"><a href="" class="btn btn-primary">Más información</a></p>
                            </div>
                        </div>
                    </div> -->
                </div>
             
            </div>
    </form>
</div>
</section>
<div class="container com-users-profile__edit profile-edit">
	


</div>
