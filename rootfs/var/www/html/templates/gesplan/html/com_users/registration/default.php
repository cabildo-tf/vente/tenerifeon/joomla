<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   (C) 2009 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.formvalidator');

?>
    <!-- <section class="head-registro"></section> -->

    <section class="registro">
        <div class="container">
            <div class="row">
				<div class="col-md-6 d-flex flex-column">

					<?php if ($this->params->get('show_page_heading')) : ?>
						<div class="section-title">
							<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
						</div>
					<?php endif; ?>

					<form id="member-registration" action="<?php echo Route::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="com-users-registration__form form-validate" enctype="multipart/form-data">
						<?php // Iterate through the form fieldsets and display each one. ?>
						<?php foreach ($this->form->getFieldsets() as $index => $fieldset) : ?>
							<?php // Only echo default register fields ?>
							<?php if($fieldset->name == 'default') :?>
								<?php $fields = $this->form->getFieldset($fieldset->name); ?>
								<?php if (count($fields)) : ?>
									<fieldset>
										<?php // If the fieldset has a label set, display it as the legend. ?>
										<?php if (isset($fieldset->label) && $index != "privacyconsent") : ?>
											<div class="section-title">
												<h1><?php echo Text::_($fieldset->label); ?></h1>
											</div>
										<?php endif; ?>
										<?php echo $this->form->renderFieldset($fieldset->name); ?>
									</fieldset>
								<?php endif; ?>
							<?php else : ?>
								<?php // Echo privacy consent ?>
								<?php if (isset($fieldset->label) && $index == "privacyconsent") : ?>
									<?php echo $this->form->renderFieldset($fieldset->name); ?>
								<?php endif; ?>
							<?php endif; ?>

						<?php endforeach; ?>
					
							<button type="submit" class="custom-defoult">
									<?php echo Text::_('JREGISTER'); ?>
								</button>
								<input type="hidden" name="option" value="com_users">
								<input type="hidden" name="task" value="registration.register">
				
						<?php echo HTMLHelper::_('form.token'); ?>
					</form>
					</div>
				</div>			
			</div>
		</div>
	</section>
	<?php
		$path = dirname(__FILE__);
		$path = dirname(dirname($path))."/com_content/article/partials/";

		require_once($path.'ayuda.php');
	?>
