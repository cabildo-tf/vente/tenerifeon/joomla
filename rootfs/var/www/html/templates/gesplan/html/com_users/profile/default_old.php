<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   (C) 2009 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Uri\Uri;


?>
  <section class="hero__section hero__section--personal-information">
            <figure>
            <?= HTMLHelper::_('image','head-user.jpg', 'Header profile', null, true, 0) ?>
            </figure>
        </section>
		<section class="intro intro--basic">
            <div class="container">
                <div class="row">
                    <div class="col">
                            <h1>
                               <?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_DATA'); ?>
                            </h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque animi quos dolorem maiores excepturi.</p>
                    </div>
                </div>
            </div>
        </section>

<section id="personal__information" class="personal__information">
	<div class="com-users-profile profile">

            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="user__photo">
                                    <picture>
                                    <?php if(($this->data->jcfields[133]->rawvalue) != "") : ?>
                                        <?php $avatar = json_decode($this->data->jcfields[133]->rawvalue);?>
                                        <img src="<?= Uri::root() ?>/media/plg_fields_bffile/com_users.user/<?= $avatar->storedname ?>" alt="<?= Text::_('');?>" class="perfil__user img-fluid" />
                                    <?php else : ?>
                                    <?= HTMLHelper::_('image','user-avatar.png',  $this->escape($this->data->name), 'class="perfil__user img-fluid"', true, 0) ?> 
                                    <?php endif; ?>
                                    </picture>
                                </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <form>
                            <fieldset>
                                <legend>Datos personales</legend>
                                <div class="form-group">
                                    <label for="user-name"><?php echo Text::_('COM_USERS_PROFILE_USERNAME_LABEL'); ?></label>
                                    <p><strong><?php echo $this->escape($this->data->username); ?></strong><p>
                                </div>
                                <div class="form-group">
                                    <label for="name"><?php echo Text::_('COM_USERS_PROFILE_NAME_LABEL'); ?></label>
                                    <p><strong><?php echo $this->escape($this->data->name); ?></strong><p>
                                </div>
                                <div class="form-group">
                                    <label for="surname"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_SURNAME_LABEL'); ?></label>
                                    <p><strong><?php echo $this->escape($this->data->jcfields[126]->rawvalue); ?></strong><p>
                                </div>
                                <div class="form-group">
                                    <label for="email"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_EMAIL_LABEL'); ?></label>
                                    <p><strong><?php echo $this->escape($this->data->email); ?></strong><p>
                                </div>
                                <div class="form-group">
                                    <label for="phone"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_PHONE_LABEL'); ?></label>
                                    <p><strong><?php echo $this->escape($this->data->profile["phone"]);?></strong><p>
                                </div>
                                
                                <div class="form-group">
                                    <label for="surname"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_ARE_YOU_LICENSED_LABEL'); ?></label>
                                    <p><strong><?php if($this->data->jcfields[129]->rawvalue == '1'){echo Text::_('TPL_GESPLAN_USERS_PROFILE_YES');}else{echo Text::_('TPL_GESPLAN_USERS_PROFILE_NO');} ?></strong><p>
                                </div>
                                <?php if(($this->data->jcfields[128]->rawvalue) != "") :?>
                                <div class="form-group">
                                    <label for="surname"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_MOUNTAIN_LICENSE'); ?></label>
                                    <p><strong><?php echo $this->escape($this->data->jcfields[128]->rawvalue); ?></strong><p>
                                </div>
                                <?php endif; ?>
                                <?php if(($this->data->jcfields[132]->rawvalue) != "") : ?>
                                <div class="form-group">
                                    <label for="surname"><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_CYCLING_LICENSE'); ?></label>
                                    <p><strong><?php echo $this->escape($this->data->jcfields[132]->rawvalue); ?></strong><p>
                                </div>
                                <?php endif; ?>
                                <div class="card card-body--gray">
                                    <div class="card-body">
                                        <fieldset><?php echo Text::_('TPL_GESPLAN_USERS_PROFILE_INTRESTED_IN_LABEL'); ?></fieldset>
                                        <!--<p><?php echo Text::_('COM_USERS_PROFILE_INTRESTED_DESCRIPTION_LABEL'); ?></p>-->
                                        <div class="row">
                                            <?php 
                                                $intrested = $this->data->jcfields[88]->fieldparams["options"];
                                                $chosenIntres = $this->data->jcfields[88]->rawvalue;
                                            ?>
                                            <?php foreach($intrested as $option) : ?>
                                                <div class="col-sm-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" disabled <?php if(!empty($chosenIntres)){if(in_array($option->value, $chosenIntres)){echo "checked";}} ?> value="<?= $option->value ? $option->value : "";?>" id="<?= $option->value;?>">
                                                    <label class="form-check-label" for="flexCheckDefault"><?= $option->name;?></label>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                           
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            
                            
                            <hr>
                            <div class="row">
                                <div class="col-6">
                                    <?php if (Factory::getUser()->id == $this->data->id) : ?>
                                    <a class="btn save__info mb-3" href="<?php echo Route::_('index.php?option=com_users&task=profile.edit&user_id=' . (int) $this->data->id); ?>">
                                        <span class="icon-user-edit" aria-hidden="true"></span> <?php echo Text::_('COM_USERS_EDIT_PROFILE'); ?>
                                    </a>
                                    <?php endif; ?>
                                </div>
                                <div class="col-6">
                                    <a href="#" class="delete__info" data-bs-toggle="modal" data-bs-target="#modal-delete-user">Eliminar perfil</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                              <p class="mb-2"><strong>¿Quieres ser colaborador?</strong></p>
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque nisi possimus iure excepturi.</p>
                              <p class="mt-auto mb-0"><a href="" class="btn btn-primary">Más información</a></p>
                            </div>
                        </div>
                    </div>
                </div>
     
            </div>

</div>
</section>


