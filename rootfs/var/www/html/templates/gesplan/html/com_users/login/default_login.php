<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   (C) 2009 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Associations;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.formvalidator');

$usersConfig = ComponentHelper::getParams('com_users');

?>	
			
<!-- Get article página Inicia sesión (id 56) to print fields -->
<?php  
//Get association of article Login / inicia sesion 
$articleId = 56; //Guía de actividades al aire libre en Tenerife
$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
$articleLang = Factory::getLanguage()->getTag();
if(count($associations) > 0) {
	$articleId = intval(explode(":", $associations[$articleLang]->id)[0]);
}
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;

$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
$appParams = JFactory::getApplication()->getParams();  
$model->setState('params', $appParams);
$model->setState('filter.article_id', $articleId); //Página Inicia sesión

$paginaLogin =   $model->getItems();
$paginaLogin = $paginaLogin[0];
$fields = FieldsHelper::getFields('com_content.article', $paginaLogin, true);
// Adding the fields to the object
$paginaLogin->jcfields = array();

foreach ($fields as $key => $field)
{
	$paginaLogin->jcfields[$field->id] = $field;
}
?>
   <section class="hero__section hero__section--personal-information">
    <figure>
        <?= HTMLHelper::_('image','head-user.jpg', 'Header profile', null, true, 0) ?>
    </figure>
</section>

    <section class="registro">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-flex flex-column">
                    <div class="section-title" >
                        <h1><?= $paginaLogin->title; //Titulo?></h1>
                        <p><?= $paginaLogin->introtext; //Descripcion?></p>
                    </div>
					<form action="<?php echo Route::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="com-users-login__form form-validate form-horizontal well" id="com-users-login__form">

						<fieldset>
							<?php echo $this->form->renderFieldset('credentials', ['class' => 'com-users-login__input']); ?>

							<?php if ($this->tfa) : ?>
								<?php echo $this->form->renderField('secretkey', null, null, ['class' => 'com-users-login__secretkey']); ?>
							<?php endif; ?>

							<?php if (PluginHelper::isEnabled('system', 'remember')) : ?>
								<div class="com-users-login__remember">
									<div class="form-check">
										<input class="form-check-input" id="remember" type="checkbox" name="remember" value="yes">
										<label class="form-check-label" for="remember">
											<?php echo Text::_('COM_USERS_LOGIN_REMEMBER_ME'); ?>
										</label>
									</div>
								</div>
							<?php endif; ?>

							<div class="control-forget">
								<a class="com-users-login__reset list-group-item" href="<?php echo Route::_('index.php?option=com_users&view=reset'); ?>">
									<?php echo Text::_('COM_USERS_LOGIN_RESET'); ?>
								</a>
								<a class="com-users-login__remind list-group-item" href="<?php echo Route::_('index.php?option=com_users&view=remind'); ?>">
									<?php echo Text::_('COM_USERS_LOGIN_REMIND'); ?>
								</a>
							</div>
							
							<button type="submit" class="custom-defoult">
										<?php echo Text::_('JLOGIN'); ?>
							</button>

							<div class="col-md-12">
								<p><?= $paginaLogin->jcfields[86]->rawvalue; //Otras cuentas?></p>
								<div class="d-flex flex-column flex-lg-row">
												<?php foreach ($this->extraButtons as $button):
				$dataAttributeKeys = array_filter(array_keys($button), function ($key) {
					return substr($key, 0, 5) == 'data-';
				});
				?>
				<div class="com-users-login__submit control-group">
					<div class="controls">
						<button type="button"
								class="btn btn-secondary w-100 <?php echo $button['class'] ?? '' ?>"
								<?php foreach ($dataAttributeKeys as $key): ?>
								<?php echo $key ?>="<?php echo $button[$key] ?>"
								<?php endforeach; ?>
								<?php if ($button['onclick']): ?>
								onclick="<?php echo $button['onclick'] ?>"
								<?php endif; ?>
								title="<?php echo Text::_($button['label']) ?>"
								id="<?php echo $button['id'] ?>"
						>
							<?php if (!empty($button['icon'])): ?>
								<span class="<?php echo $button['icon'] ?>"></span>
							<?php elseif (!empty($button['image'])): ?>
								<?php echo HTMLHelper::_('image', $button['image'], Text::_($button['tooltip'] ?? ''), [
									'class' => 'icon',
								], true) ?>
							<?php elseif (!empty($button['svg'])): ?>
								<?php echo $button['svg']; ?>
							<?php endif; ?>
							<?php echo Text::_($button['label']) ?>
						</button>
					</div>
				</div>
			<?php endforeach; ?>

									<?php $return = $this->form->getValue('return', '', $this->params->get('login_redirect_url', $this->params->get('login_redirect_menuitem'))); ?>
									<input type="hidden" name="return" value="<?php echo base64_encode($return); ?>">
									<?php echo HTMLHelper::_('form.token'); ?>
								</div> 
							</div>
							
						</fieldset>
					</form>
                </div>
                <div class="col-md-6 d-flex flex-column">
                    <div class="section-title">
                        <h2><?= $paginaLogin->jcfields[78]->rawvalue; //Titulo registrate?></h2>
                        <p><?= $paginaLogin->jcfields[79]->rawvalue; //Descripcion registrate?></p>
                    </div>
                    <div class="title-ul-registro"><?= $paginaLogin->jcfields[85]->rawvalue; //titulo ventajas?></div>
                    <ul class="registro-ul">
						<?php if($paginaLogin->jcfields[80]->rawvalue != "") : ?>  
                        	<li><?= $paginaLogin->jcfields[80]->rawvalue; //Ventaja 1?></li>
						<?php endif; ?>
						<?php if($paginaLogin->jcfields[81]->rawvalue != "") : ?>  
                        	<li><?= $paginaLogin->jcfields[81]->rawvalue; //Ventaja 2?></li>
						<?php endif; ?>
						<?php if($paginaLogin->jcfields[82]->rawvalue != "") : ?>  
                        	<li><?= $paginaLogin->jcfields[82]->rawvalue; //Ventaja 3?></li>
						<?php endif; ?>
						<?php if($paginaLogin->jcfields[83]->rawvalue != "") : ?>  
                        	<li><?= $paginaLogin->jcfields[83]->rawvalue; //Ventaja 4?></li>
						<?php endif; ?>
						<?php if($paginaLogin->jcfields[84]->rawvalue != "") : ?>  
                        	<li><?= $paginaLogin->jcfields[84]->rawvalue; //Ventaja 5?></li>
						<?php endif; ?>
                    </ul>
                    <a href="<?php  echo Route::_("index.php?option=com_users&view=registration"); ?>" title="" class="custom-defoult"><?= $paginaLogin->jcfields[87]->rawvalue; //Enlace registrate?></a>
                </div>
               
            </div>
        </div>
    </section> 

    <?php
		$path = dirname(__FILE__);
		$path = dirname(dirname($path))."/com_content/article/partials/";

		require_once($path.'ayuda.php');
	?>