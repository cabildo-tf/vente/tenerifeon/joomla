<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;

$lang   = Factory::getLanguage();
$user   = Factory::getUser();
$groups = $user->getAuthorisedViewLevels();

if ($this->maxLevel != 0 && count($this->children[$this->category->id]) > 0) : ?>

	<?php foreach ($this->children[$this->category->id] as $id => $child) : ?>
		<?php // Check whether category access level allows access to subcategories. ?>
		<?php if (in_array($child->access, $groups)) : ?>
			<?php if ($this->params->get('show_empty_categories') || $child->numitems || count($child->getChildren())) : ?>
				<li class="nav-item" role="presentation">
                    <button class="nav-link <?= ($id ==0)?'active':'' ?>" id="faqtabs-category<?= $id ?>" data-bs-toggle="tab" data-bs-target="#category<?= $id ?>" type="button" role="tab" aria-controls="home" aria-selected="<?= ($id ==0)?'true':'false' ?>">
					<?php echo $this->escape($child->title); ?>
					</button>
                </li>
			<?php endif; ?>
		<?php endif; ?>
	<?php endforeach; ?>

<?php endif;
