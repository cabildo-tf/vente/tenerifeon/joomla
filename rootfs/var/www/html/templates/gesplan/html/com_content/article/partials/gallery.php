<?php 
    use Joomla\CMS\Language\Text;
?>
<?php if(isset($this->item->jcfields[107]->subform_rows)) : ?>
<?php $images = ($this->item->jcfields[107]->subform_rows) ?>
<?php if(isset($images)) :?>
    <div class="project__view__article-gallery" id="gallery" data-aos="fade-up" data-aos-duration="750">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3><?php echo Text::_( 'TPL_GESPLAN_GALLERY' );?></h3>
                    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <?php foreach($images as $key => $image) : ?>
                                <div class="carousel-item <?= (($key == 0)?'active':'') ?>">
                                    <?php  $imgUrl = $image["imagen"]->rawvalue["imagefile"]; ?>
                                    <?php if(!empty($imgUrl)) : ?>
                                        <figure>
                                            <img src="<?php echo $imgUrl;?>" alt="<?php  echo($image["imagen"]->rawvalue["alt_text"]); ?>" loading="lazy">
                                        </figure>
                                        <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden"><?= Text::_('TPL_GESPLAN_PREVIOUS')?></span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden"><?= Text::_('TPL_GESPLAN_NEXT∫')?></span>
                        </button>
                    </div>
                </div>			
            </div>
        </div>
    </div>
<?php endif; ?>
<?php endif; ?>