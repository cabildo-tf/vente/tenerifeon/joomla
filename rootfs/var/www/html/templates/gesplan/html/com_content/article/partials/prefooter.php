<?php
use Joomla\CMS\HTML\HTMLHelper;
?>
<section id="prefooter">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="bg-prefooter">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="content-rotat-img">
                                <figure>
                                <?php echo $this->item->jcfields[23]->value ; ?> 
                            </div>
                        </div>
                        <div class="col-md-6 d-flex flex-column">
                            <h2><?php echo $this->item->jcfields[24]->value ; ?> </h2>
                            <p> <?php echo $this->item->jcfields[25]->value ; ?></p>
                            <div class="row mt-auto mb-0">
                                <div class="col-6"><a href="<?= $this->item->jcfields[259]->rawvalue;//Enlace android?>"><?= HTMLHelper::_('image', 'btn-Store-android-orange.svg', 'Android Play', "class=btn-store", true, 0) ?></a></div>
                                <div class="col-6"><a href="<?= $this->item->jcfields[260]->rawvalue; //Enlace apple?>"><?= HTMLHelper::_('image', 'btn-Store-ios-orange.svg', 'IOS Store', "class=btn-store", true, 0) ?></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>