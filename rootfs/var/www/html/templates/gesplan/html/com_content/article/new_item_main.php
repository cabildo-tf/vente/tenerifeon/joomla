<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Language\Text;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;

// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);


?>
<div class="col-md-6">
    <div class="information__category__featured" data-aos="fade-up">
        <article class="information__category__featured-in">
			<a class="hyper-link" href="<?php $link = Route::_(RouteHelper::getArticleRoute($this->item->id, $this->item->catid, $this->item->language));													
														echo $link?>" title="<?= Text::_('TPL_GESPLAN_NEW_LINK');?>: <?= $this->item->title; ?>">
														
													</a>
			<figure class="information__category__featured-image">
				<?php
                            $images         = json_decode($this->item->images); 
                        ?>
                            <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy">
				</figure>
				<div class="information__category__featured-text">
					<p class="information__category__featured-category"><?php echo $this->item->category_title; ?></p>
					<h2><?php echo $this->item->title;?></h2>
					<?php 
							$fields = FieldsHelper::getFields('com_content.article',  $this->item, true);
                            // Adding the fields to the object
							$this->item->jcfields = array();
    
                            foreach ($fields as $key => $field)
                            {
								$this->item->jcfields[$field->id] = $field;
                            }   
					?>
					<?php   	$textoLargo =  strip_tags($this->item->jcfields[14]->rawvalue);  //entradilla
								$textoCorto = substr($textoLargo, 0, 150);
								$resumen = substr($textoCorto, 0, strrpos($textoCorto, ' '));
								echo '<p>'.$resumen.' ...</p>';
						?>
				</div>
				<div class="information__category__featured-button mt-auto">
					<p class="link-primary">
						<?= Text::_('TPL_GESPLAN_NEW_LINK');?>								
					</p>
				</div>
			</div>
		</div>
	</article>
