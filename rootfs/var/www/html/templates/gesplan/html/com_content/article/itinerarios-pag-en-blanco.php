<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;

$app = Factory::getApplication();

// $this->category->text = $this->category->description;
// $app->triggerEvent('onContentPrepare', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
// $this->category->description = $this->category->text;

// $results = $app->triggerEvent('onContentAfterTitle', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
// $afterDisplayTitle = trim(implode("\n", $results));

// $results = $app->triggerEvent('onContentBeforeDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
// $beforeDisplayContent = trim(implode("\n", $results));

// $results = $app->triggerEvent('onContentAfterDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
// $afterDisplayContent = trim(implode("\n", $results));

// $htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';

?>

	<!-- Si hay articulos en la categoría/subcategoria pinta listado. 
	En la categoria padre(information) no hay articulos, entonces solamente pintará cuando entramos en una subcategoría -->
	<?php //if (!empty($this->items)) : ?>
		
		<section class="filtros-itinerarios  view_rezizer">
        <div class="map-mobil">
            <?php echo $this->loadTemplate('offcanvas');?>
            <figure>
                <button class="open-rout-btn">1</button>
                <?= HTMLHelper::_('image', 'fake-map-filtros.png', 'Logo del cabildo', null, true, 0) ?>
            </figure>

        </div>
        <div class="filtros-mobil">
            
            <form>
                <div class="cont-btn-mobile">
                    <button class="show-filters delete-filters">
                        Borrar
                    </button>
                    <button class="show-filters">
                        Aplicar
                    </button>
                </div>
                <ul class="general-ul">
                    
                   
                    <li>
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_TYPE_OF_ROUTE");?></legend>
                            <ul>
                                <?php       

                                    $categories = \Joomla\CMS\Categories\Categories::getInstance('com_content');
                                    $cat        = $categories->get(38);
                                    $routeType = $cat->getChildren();
                                    
                                ?>
                                <?php foreach($routeType as $route) : ?>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            <?= $route->title; ?>
                                        </label>
                                      </div>
                                </li>
                                <?php endforeach; ?>
                              
                            </ul>
                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_ROUTE_SHAPE")?></legend>
                            <ul>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Circular
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Lineal de punto a punto
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Lineal ida y vuelta
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend> <?= Text::_("TPL_GESPLAN_ROUTE_DISTANCE")?></legend>
                            <ul>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            0-2 KM
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            2-5 KM
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            5-10 KM
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            10-30 KM
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Más de 30 KM
                                        </label>
                                    </div>
                                </li>
                            </ul>

                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_ROUTE_DIFFICULTY")?></legend>
                            <ul>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Muy baja
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Baja
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Media
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Alta
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Muy alta
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_LANDSCAPE_ZONES")?></legend>
                            <ul>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                        Macizo de Anaga
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                        Macizo de Teno
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                        Valles del Norte
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Abona
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Isona
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </fieldset>
                    </li>             
                </ul>
            </form>
        </div>
       <div class="cont-btn-mobile">
            <button class="show-filters">
                Mostrar filtros
            </button>
            <button class="show-map">
                Mostrar mapa
            </button>
            
       </div>
        <div class="container-fluid">
            <div class="row m-0">
                <div class="col-md-12">
                    <form class="m-0">
                        <div class="filter-row">
                            <div class="item-filters">
                            <?php 

                                //Get num published rutas
                                $db = Factory::getDbo();

                                $query = $db->getQuery(true);
                                $query
                                ->select(array('COUNT(catid)'))
                                ->from($db->quoteName("#__content","c"))
                                ->join('INNER', $db->quoteName('#__categories', 'b') . ' ON ' . $db->quoteName('c.catid') . ' = ' . $db->quoteName('b.id'))
                                ->where($db->quoteName('b.parent_id') . ' = ' . $db->quote('38'))
                                ->where($db->quoteName('c.state') . ' = ' . $db->quote('1'));
                                //->group($db->quoteName("c.catid"));

                                // Reset the query using our newly populated query object.
                                $db->setQuery($query);

                                // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                $results = $db->loadObjectList();
                                $numberOfroutes = $results[0]->count;
                                $numberPages = $numberOfroutes / 20;
                            ?>
                                <button>Mostrar los <?=  $numberOfroutes ?> itinerarios </button>
                                <!-- //Text::_("TPL_GESPLAN_SHOW_ALL")." ".count($this->items)." ".Text::_("TPL_GESPLAN_ROUTES");-->
                            </div>
                            <div class="item-filters">
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_TYPE_OF_ROUTE")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <?php foreach($routeType as $route) : ?>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    <?= $route->title; ?>
                                                </label>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                        <div class="submits-container">
                                            <button type="submit" class="delete">Borrar</button>
                                            <button type="submit">Guardar</button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-filters">
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_ROUTE_SHAPE")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Circular
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                Lineal de punto a punto
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                Lineal ida y vuelta
                                                </label>
                                            </div>
                                        </li>
                                        <div class="submits-container">
                                            <button type="submit" class="delete">Borrar</button>
                                            <button type="submit">Guardar</button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-filters">
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_ROUTE_DISTANCE")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    0-2 KM
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    2-5 KM
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    5-10 KM
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    10-30 KM
                                                </label>
                                            </div>
                                        </li>                                   
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Más de 30 KM
                                                </label>
                                            </div>
                                        </li>
                                        <div class="submits-container">
                                            <button type="submit" class="delete">Borrar</button>
                                            <button type="submit">Guardar</button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-filters">
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_ROUTE_DIFFICULTY")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Muy baja
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Baja
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Media
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Alta
                                                </label>
                                            </div>
                                        </li>                                   
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Muy alta
                                                </label>
                                            </div>
                                        </li>
                                        <div class="submits-container">
                                            <button type="submit" class="delete">Borrar</button>
                                            <button type="submit">Guardar</button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-filters">
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_LANDSCAPE_ZONES")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                Macizo de Anaga
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                Macizo de Teno
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                Valles del Norte
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Abona
                                                </label>
                                            </div>
                                        </li>                                   
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Isona
                                                </label>
                                            </div>
                                        </li>
                                        <div class="submits-container">
                                            <button type="submit" class="delete">Borrar</button>
                                            <button type="submit">Guardar</button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class=" col-lg-5 p-0">
                    <div id="itinerarios-column" class="itinerarios-column">
					
				
                    </div>
                </div>
                <div class="col-lg-7 p-0 d-none d-md-flex">

                    <div class="content-map-desktop">
					<?php echo $this->loadTemplate('offcanvas');?>

                        <!-- <a href="" title="enalce planificador del itienerario X" class="btn-link-itinerario">
						<?= HTMLHelper::_('image', 'route-icon-btn.svg', Text::_("TPL_GESPLAN_ROUTE_ICON"), null, true, 0) ?>
                            Planifica tu itinerario
                        </a> -->
                        <button class="open-rout-btn">1</button>
                        <div id="map-equipamiento" style="height: 100%;"></div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="/templates/gesplan/js/gesplan/pagination.js?3e2be082abda46f8aa924a4a3c5ca486" data-asset-name="template.gesplan.pagination"></script>
    <div>
        <samp>
            <span>Current page:</span>
            <span id="page-number-1">1</span>
        </samp>

        <div id="pagination-1"></div>
    </div>
    <script>

        //New var to get all routes latlon to fit in map
        var routeMarkers = [];
    function pageClick1(pageNumber) {

        $("#page-number-1").text(pageNumber);
        getAjaxContent(pageNumber);
    }

    function pageClickAll(pageNumber) {
        $("#page-number-all").text(pageNumber);
    }

    $(document).ready(function () {

        var itemsCount = <?= $numberOfroutes?>;
        var itemsOnPage = 20;

        var pagination1 = new Pagination({
            container: $("#pagination-1"),
            pageClickCallback: pageClick1,
            maxVisibleElements: 3
    });


    pagination1.make(itemsCount, itemsOnPage);

        var paginationAll = new Pagination({
            container: $("#pagination-all"),
            pageClickCallback: pageClickAll,
            callPageClickCallbackOnInit: true,
            showInput: true,
            showSlider: true,
            enhancedMode: true,
            maxVisibleElements: 20,
            inputTitle: "Go to page"
        });
        paginationAll.make(itemsCount, itemsOnPage);

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

       
    });


    /* Get first routes page */
    $(document).ready(function () {
        getAjaxContent(1);
    })
    const routesForMap = [];
    //Get content by Ajax
    function getAjaxContent(pageNum){
        var routesCategory = 38;
        var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + routesCategory + "&page=" + pageNum;

        Joomla.request({
            url: baseUrl +  "&plugin=Listajaxcontent",
            method: 'GET',
            perform: true,
            onSuccess: function onSuccess(resp) {
                // Remove loader image

                var response = {};

                try {
                    response = JSON.parse(resp);
                } catch (e) {
                Joomla.renderMessages({
                    error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
                }, ".sampledata-steps-" + type + "-" + step);
                SampleData.inProgress = false;
                return;
                }
                var routesList = response.data[0];
                /* PINTAMOS LO EQUIPAMIENTOS */
                if(routesList.length){
                    const obj =(routesList);
                    $('#itinerarios-column').empty()
                    routesList.forEach(printDivContent);

                    function printDivContent(item, index) {
                        routesForMap[item.id] = {
                            title:item.title, 
                            matricula:item.jcfields[140].rawvalue, 
                            link: item.link,
                            text: item.introtext.substring(0, 80),
                            imageSrc: item.imageItem['src'],
                            imageAlt:item.imageItem['alt'],
                            polyline: item.jcfields[145].rawvalue,
                            distance: item.jcfields[60].rawvalue,
                            gainAlt: item.jcfields[63].rawvalue,
                            lostAlt: item.jcfields[64].rawvalue,
                            gallery: item.gallery}
                        var route = `
                                    <div class="item-itinerario">
                                        <div class="title-item">
                                            <div class="dropdown">
                                                <button type="button" class="badge" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                                                    '<li><a class="dropdown-item" href="#"><?= Text::_("TPL_GESPLAN_SAVE_ROUTE");?></a></li>
                                                    <li><a class="dropdown-item" href="#"><?= Text::_("TPL_GESPLAN_VIEW_FULL_DETAIL");?></a></li>
                                                    <li><a class="dropdown-item" href="#"><?= Text::_("TPL_GESPLAN_SHARE_IT");?></a></li>
                                                    <li><a class="dropdown-item" href="#"><?= Text::_("TPL_GESPLAN_INCIDENCE_NOTIFICATE");?></a></li>
                                                </ul>
                                            </div>
                                            <p><span class="badge">${routesForMap[item.id].matricula}</span><a href="${routesForMap[item.id].link.substring(1)}" title="${routesForMap[item.id].title}>" style="color:#4a503b">${routesForMap[item.id].title}</a></p>

                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <figure class="sizeingImg">
                                                <img src="/${routesForMap[item.id].imageSrc}" alt="${routesForMap[item.id].imageAlt}" loading="lazy">
                                            </figure>
                                            </div>
                                            <div class="col-9">
                                                <div class="content-values">
                                                <div class="item-value">
                                                    <i class="fa fa-long-arrow-alt-right"></i>
                                                    <p class="value">${(routesForMap[item.id].distance/1000).toFixed(2)}km</p>
                                                </div>
                                                <div class="item-value">
                                                    <i class="far fa-clock"></i>
                                                    <p class="value">1h 30m</p>
                                                </div>
                                                
                                                <div class="item-value">
                                                    <i class="fas fa-level-up-alt"></i>
                                                    <p class="value">${(routesForMap[item.id].gainAlt*1).toFixed(2)}m</p>
                                                </div>
                                                <div class="item-value">
                                                    <i class="fas fa-level-down-alt"></i>
                                                    <p class="value">${(routesForMap[item.id].lostAlt*1).toFixed(2)}m</p>
                                                </div>
                                                <div class="item-value alert">
                                                    <i class="far fa-bell"></i>
                                                    <p class="value">Alerta</p>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        `;
                        

                        $("#itinerarios-column").append(route);

                    }
                    //If first page load, print routes in map
                    //if(pageNum == 1){
                        printInMap(routesForMap);
                    //}

                }	
                    /*    */ 
            },
            onError: function onError() {
                Joomla.renderMessages({
                error: ['Something went wrong! Please close and reopen the browser and try again!']
                });
            }
        });

    }
    function generatePagination(){

        // Foreach page

        // Generate a button that on click calls to setPage
    }

    function printInMap(routesForMap){


        let opts = {
            map: {
                center: [28.291565, -16.629129],
                zoom: 13,
                zoomControl: true,
                fullscreenControl: true,
                attributionControl: true,
                resizerControl: true,
                preferCanvas: false,
                rotate: false,
                collapsed: true,
                // bearing: 45,
                rotateControl: {
                    closeOnZeroBearing: true
                },
            },
            layersControl: {
                options: {
                    collapsed: false,
                },
            },
        };

        let map = L.map('map-equipamiento', opts.map);

        let controlLayer = L.control.layers(null, null, opts.layersControl.options);
        

        
        routesForMap.forEach(fillMarkersObject);

        function fillMarkersObject(item,id){

            routeMarkers[id] = JSON.parse(item.polyline);       
               
        }
        // function onEachFeature(feature, layer) {
        //     //bind click
        //     layer.on('click', function (e) {
        //     // e = event
        //     console.log(e);
        //     // You can make your ajax call declaration here
        //     //$.ajax(... 
        //     });

        // }

        // L.geoJSON(routeMarkers, {
        //     style: function (feature) {
        //         return {color: feature.properties.color};
        //     }
        // }).bindPopup(function (layer) {
        //     return layer.feature.properties.description;
        // }).addTo(map);

    }

    // Called when a page link is clicked
    function setPage(numberOfPage){
        // Do ajax for get next page routes

        // Add to routes array

        // Add to markers

        // Clean html paginated list

        // Add html paginated list

    }

    // Called when changed map viewing area
    function onChangeMapCameraPosition(latitude, longitude){
        // Do ajax for get routes on specific area where not in the routes

        // Add to routes array

        // Add to markers
    }


    </script>
	<?php //endif; ?>

<!-- </div> -->
