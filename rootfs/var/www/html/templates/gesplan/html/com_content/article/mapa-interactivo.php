<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$status = $user->guest; //Cehck if user is not logued in ( == guest)
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
$category_title   = $this->escape($this->item->category_title);

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
  
  <?php 

//Get num published equipamientos
$db = Factory::getDbo();

$query = $db->getQuery(true);
$query
->select(array('COUNT(catid)'))
->from($db->quoteName("#__content","c"))
->join('INNER', $db->quoteName('#__categories', 'b') . ' ON ' . $db->quoteName('c.catid') . ' = ' . $db->quoteName('b.id'))
->where($db->quoteName('b.parent_id') . ' = ' . $db->quote('39'))
->where($db->quoteName('c.state') . ' = ' . $db->quote('1'));
//->group($db->quoteName("c.catid"));

// Reset the query using our newly populated query object.
$db->setQuery($query);

// Load the results as a list of stdClass objects (see later for more options on retrieving data).
$results = $db->loadObjectList();
$numberOfEquipments = $results[0]->count;
?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<?php   if(substr( $_SERVER['SERVER_NAME'], strpos($_SERVER['SERVER_NAME'], ".") + 1) == 'test'){
            $protocol = 'http';
        }
        else{
            $protocol = 'https';
        }
$siteUrl = $protocol.'://'.$_SERVER['SERVER_NAME'];?>

<section class="filtros-itinerarios mapaInteractivo">
    <div class="offCanvascontainer">
        <div class="offCanvas-item">
        <button class="open-rout-btn">
            <i class="fa fa-times"></i>
        </button>
        <p class="title"><span class="badge"></span><span class="title"></span></p>
        <!-- <p class="description"></p> -->
        <div class="content-actions">
            <div id="content-valoration">
                <img src="/img/icon-full-star.svg" alt="">
                <img src="/img/icon-full-star.svg" alt="">
                <img src="/img/icon-full-star.svg" alt="">
                <img src="/img/icon-full-star.svg" alt="">
                <img src="/img/icon-half-star.svg" alt="">
            </div>
            <!-- <a href="">
                <i class="far fa-comment"></i>
                3
            </a> -->
        </div>
        <div id="carouselOfCanvas" class="carousel slide my-2" data-bs-ride="carousel">
            <div class="carousel-inner">
            
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselOfCanvas" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselOfCanvas" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
            </button>
        </div>
        <div class="content-values">
            <div class="item-value description">
                <!-- <i class="fa fa-long-arrow-alt-right"></i> -->
                <p class="description value"></p>
            </div>
            <div class="item-value distance">
                <i class="fa fa-long-arrow-alt-right"></i>
                <p class="value">2,7km</p>
            </div>
            <div class="item-value time">
                <i class="far fa-clock"></i>
                <p class="value">1h 30m</p>
            </div>
            
            <div class="item-value gainAlt">
                <i class="fas fa-level-up-alt"></i>
                <p class="value">300m</p>
            </div>
            <div class="item-value lostAlt">
                <i class="fas fa-level-down-alt"></i>
                <p class="value">54m</p>
            </div>
            <div class="item-value alert" style="display:none">
            <?= HTMLHelper::_('image', 'icon-warning.svg', Text::_("TPL_GESPLAN_INCIDENCE"), 'class="icon-warning"', true, 0) ?>
                <p class="value"><?= Text::_("TPL_GESPLAN_INCIDENCE")?></p>
            </div>
        </div>
        <div class="more-actions">
          <div id="addToFavouritesModal"></div>
          <div id="downloadGpxLink"></div>
          <div id="downloadKmlLink"></div>
        </div>
        <div class="point-description"></div>
        <div class="lugares-interes"></div>
        <a href="" alt="ir a la ficha del itinerario" class="btn-ficha-completa"><?= Text::_("TPL_GESPLAN_VIEW_FULL_DETAIL")?></a>
        </div>
    </div>
    <button id="openData" class="niceRotate"><i class="fa fa-chevron-left"></i></button>
    <button id="openLegend"><i class="fa fa-info"></i></button>
    <div id="customLegend">
        <ul>
            <li><span class="red-itinerario"></span> <?= Text::_("TPL_GESPLAN_HIKE_ROADS")?></li>
            <li><span class="orange-itinerario"></span> <?= Text::_("TPL_GESPLAN_BICA_ROADS")?></li>
            <li><span class="blue-itinerario"></span> <?= Text::_("TPL_GESPLAN_VM_ROADS")?></li>
        </ul>
        <ul>
            <li><img src="<?= $siteUrl ?>/templates/gesplan/images/icono-campamentos-revisado.png" alt="<?= Text::_("TPL_GESPLAN_REST_AREA")?>"><?= Text::_("TPL_GESPLAN_REST_AREA")?></li>
            <li><img src="<?= $siteUrl ?>/templates/gesplan/images/i-parking.png" alt="<?= Text::_("TPL_GESPLAN_PARKING")?>"><?= Text::_("TPL_GESPLAN_PARKING")?></li>
            <li><img src="<?= $siteUrl ?>/templates/gesplan/images/i-acampada.png" alt="<?= Text::_("TPL_GESPLAN_CAMPING_AREA")?>"><?= Text::_("TPL_GESPLAN_CAMPING_AREA")?></li>
            <li><img src="<?= $siteUrl ?>/templates/gesplan/images/i-recreativo.png" alt="<?= Text::_("TPL_GESPLAN_RECREATIONAL_AREA")?>"><?= Text::_("TPL_GESPLAN_RECREATIONAL_AREA")?></li>
            <li><img src="<?= $siteUrl ?>/templates/gesplan/images/i-campamentos.png" alt="<?= Text::_("TPL_GESPLAN_CAMP")?>"><?= Text::_("TPL_GESPLAN_CAMP")?></li>
            <li><img src="<?= $siteUrl ?>/templates/gesplan/images/i-publico.png" alt="<?= Text::_("TPL_GESPLAN_VISITORS_CENTER")?>"><?= Text::_("TPL_GESPLAN_VISITORS_CENTER")?></li>
            <hr>
            <li><img src="<?= $siteUrl ?>/templates/gesplan/images/i-natural.png" alt="<?= Text::_("TPL_GESPLAN_NATURAL")?>"><?= Text::_("TPL_GESPLAN_NATURAL")?></li>
            <li><img src="<?= $siteUrl ?>/templates/gesplan/images/icono-cultural-revisado.png" alt="<?= Text::_("TPL_GESPLAN_CULTURAL_SERVICES")?>"><?= Text::_("TPL_GESPLAN_CULTURAL_SERVICES")?></li>
        </ul>

    </div>
    
    <div id="map-equipamiento" style="height: 80vh;">
        
    </div>
</section>
<?php
    $path = dirname(__FILE__) . "/partials/";
    require_once($path.'inspirador-part.php');
?>

<?php
    $config = Factory::getConfig();
    $mapbox_token = $config->get('mapbox_token');
?>
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="/templates/gesplan/js/gesplan/pagination.js?3e2be082abda46f8aa924a4a3c5ca486" data-asset-name="template.gesplan.pagination"></script>
<script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
<script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>
<link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.css" />
<link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.Default.css" />
<script src="/templates/gesplan/js/gesplan/leaflet.groupedlayercontrol.js"></script>
<script>

var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<?= $mapbox_token ?>';

var tenerifeon = L.tileLayer(mbUrl, {id: 'tenerifeon/cl2pyxrtp001b15lfuq3x23z5', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
var satellite = L.tileLayer(mbUrl, {id: 'tenerifeon/cl3rgf7dn000s14nyiiyuw8qm', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
var senderos = L.layerGroup();
var incidences = L.layerGroup();

var  scrollWheelZoom = true;
var disableDragIfMobile = function() {
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  if(check){
    //map.dragging.disable();
    map.scrollWheelZoom.disable();
  }  
};

var map = L.map('map-equipamiento', {
    center: [28.291564, -16.629130],
    zoom: 10,
    minZoom: 10,
    scrollWheelZoom: scrollWheelZoom,
    detectRetina: true,
    layers: [tenerifeon, senderos,incidences]
});

disableDragIfMobile();

/*Itinerarios layers*/ 
var bica = L.layerGroup();
var vm = L.layerGroup();
var services = L.layerGroup();

/*Equipments layers*/
var acampada = L.layerGroup();
var recreativo = L.layerGroup();
var campamento = L.layerGroup();
var visitantes = L.layerGroup();
/*Points intrest*/ 
var cultural = L.layerGroup();
var info = L.layerGroup();
var natural = L.layerGroup();
var publico = L.layerGroup();
var general = L.layerGroup();
var interestpoints = L.layerGroup();
var espacios = L.layerGroup();
var monumento = L.layerGroup();
var cientifico = L.layerGroup();
var protegido = L.layerGroup();
var rural = L.layerGroup();
var especial = L.layerGroup();
var integral = L.layerGroup();
var nacional = L.layerGroup();
var parque = L.layerGroup();
/*Servicios*/
var descanso = L.layerGroup();
var aparcamiento = L.layerGroup();
/*Municipios*/
var municipios = L.layerGroup();


var baseMaps = {
    'Tenerifeon': tenerifeon,
    '<?= Text::_("TPL_GESPLAN_SATELLITE")?>': satellite
};


var groupedOverlays = {
"<strong><?= Text::_("TPL_GESPLAN_ROUTES")?></strong>": {
    "<?= Text::_("TPL_GESPLAN_HIKE_ROADS")?>": senderos,
    "<?= Text::_("TPL_GESPLAN_BICA_ROADS")?>": bica,
    "<?= Text::_("TPL_GESPLAN_VM_ROADS")?>": vm 
},
"<hr><strong><?= Text::_("TPL_GESPLAN_EQUIPMENTS") ?></strong>":{
    "<?= Text::_("TPL_GESPLAN_CAMPING_AREA");?>": acampada,
    "<?= Text::_("TPL_GESPLAN_RECREATIONAL_AREA")?>": recreativo,
    "<?= Text::_("TPL_GESPLAN_CAMP") ?>": campamento,
    "<?= Text::_("TPL_GESPLAN_VISITORS_CENTER") ?>": visitantes,
},
"<hr><strong><?= Text::_("TPL_GESPLAN_SERVICES") ?></strong>":{
    "<?= Text::_("TPL_GESPLAN_REST_AREA") ?>": descanso,
    "<?= Text::_("TPL_GESPLAN_PARKING")?>": aparcamiento
},
"<hr><strong><?= Text::_("TPL_GESPLAN_INTRESTING_POINTS")?></strong>": {
    "<?= Text::_("TPL_GESPLAN_NATURAL")?>": natural,
    "<?= Text::_("TPL_GESPLAN_CULTURAL_SERVICES")?>": cultural
},
"<hr><strong><?= Text::_("TPL_GESPLAN_INCIDENCES") ?></strong>": {
    "<?= Text::_("TPL_GESPLAN_INCIDENCES") ?>": incidences
}
,"<hr><strong><?= Text::_("TPL_GESPLAN_NATURAL_AREAS") ?></strong>": {
    "<?= Text::_("TPL_GESPLAN_NATURAL_AREAS") ?>": espacios
},
"<hr><span id='municipios'><strong><?= Text::_("TPL_GESPLAN_TOWNS") ?></strong></span>":{
    "<?= Text::_("TPL_GESPLAN_TOWNS") ?>": municipios
}

};
var layerControl = L.control.groupedLayers(baseMaps, groupedOverlays,{collapsed:false}).addTo(map);
var alrGivId = [];

function getRanges(array) {
    var ranges = [],
        rstart, rend;
    for (var i = 0; i < array.length; i++) {
        rstart = array[i];
        rend = rstart;
        while (array[i + 1] - array[i] == 1) {
            rend = array[i + 1]; // increment the index if the numbers sequential
            i++;
        }
        ranges.push(rstart == rend ? rstart + '' : rstart + '-' + rend);
    }
    return ranges;
}


    var _isProcessing = false;
    var _limit = false;


    async function getData(northEast, southWest) {
        if (_isProcessing == false && _limit == false) {
            _isProcessing = true;
            var itineraryLayer = [];


            // Ajax to get content
            var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=&getdata=1";
            var url = baseUrl +  "&plugin=Listajaxcontent";
            try {
                await $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        "northEast": {
                            "latitude": northEast.lat,
                            "longitude": northEast.lng
                        },
                        "southWest": {
                            "latitude": southWest.lat,
                            "longitude": southWest.lng
                        },
                        "given": getRanges(alrGivId).join('_')
                    },
                    dataType: 'json',
                    success: function(response) {
                        response.data[0].data.forEach(function (itinerary){
                            
                            // If the element not exists
                            if(alrGivId.indexOf(itinerary.properties
                            .geom_id) === -1 ){
                                alrGivId.push(itinerary.properties.geom_id);
                      
                                if(itinerary.properties.type == "1"){
                                    L.geoJSON(itinerary, {color: '#FF0000',  onEachFeature: onEachFeature}).addTo(senderos);
                                }
                                else if(itinerary.properties.type == "2"){
                                    L.geoJSON(itinerary, {color: '#FF8F0B',  onEachFeature: onEachFeature}).addTo(bica);
                                }
                                else if(itinerary.properties.type == "3"){
                                    L.geoJSON(itinerary, {color: '#0B7CFF',  onEachFeature: onEachFeature}).addTo(vm);
                                }
                                

                            } 
                        });

                        if (alrGivId.length >= response.data[0].total_count) {
                            _limit = true;
                        }

                        function onEachFeature(feature, layer) {
                            //bind click
                            layer.on('click', function (e) {

                            var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&item=" + layer.feature.id;

                            Joomla.request({
                                url: baseUrl +  "&plugin=Listajaxcontent",
                                method: 'GET',
                                perform: true,
                                onSuccess: function onSuccess(resp) {
                                    // Remove loader image

                                    try {
                                        response = JSON.parse(resp);
                                        item = response.data[0];
                                        $('.offCanvas-item .title .title').text(item.title);
                                        $('.offCanvas-item .badge').text(item.jcfields[140].rawvalue).show();
                                        $('.offCanvas-item .item-value .description').empty();
                                        $('.offCanvas-item .distance ').show();
                                        $('.offCanvas-item .time').show();
                                        $('.offCanvas-item .gainAlt').show();
                                        $('.offCanvas-item .lostAlt').show();
                                        $('.offCanvas-item .distance .value').text((item.jcfields[60].rawvalue/1000).toFixed(2)+'km').show();
                                        $('.offCanvas-item .time .value').text(item.duration).show();
                                        $('.offCanvas-item .gainAlt .value').text(parseInt(item.jcfields[63].rawvalue).toFixed(0)+'m').show();
                                        $('.offCanvas-item .lostAlt .value').text(parseInt(item.jcfields[64].rawvalue).toFixed(0)+'m').show();
                                        $('.offCanvas-item .btn-ficha-completa').attr("href", item.link).show();   
                                        if(item.incidence != ""){
                                            $(".item-value.alert").show();
                                        }
                                        else{
                                            $(".item-value.alert").hide();
                                        }  
                                        $('.carousel-inner').empty();                             
                                        if(item.gallery.length > 0){
                                            item.gallery.forEach(printFigure);
                                            function printFigure(image, index){
                                               let imageDiv = "";
                                                imageDiv = '<div class="carousel-item';
                                                if(index == 0){
                                                    imageDiv += " active";
                                                }
                                                imageDiv += '"><img src="/'+image.src+'" class="d-block w-100" alt="'+image.alt+'" loading="lazy"></div>';
                                                $('.carousel-inner').append(imageDiv);
                                            }
                                        }
                                        
                                        $('.more-actions').show();
                                        $('.lugares-interes').empty();

                                        var addToFavButton = `<a class="dropdown-item`;
                                        if(item.favourite){
                                            addToFavButton += ` favorito-marked`;
                                        }
                                        addToFavButton += `"`;
                                        <?php if($status): ?>
                                            addToFavButton += ` data-bs-toggle="modal" data-bs-target="#mustlog"`;
                                        <?php endif; ?>
                                        addToFavButton += `href="#" data-itemId="${item.id}" title="añadir a mis rutas" onclick="setAddToFavouritesLink(this)">
                                                                    <i class="far fa-heart"></i>`;
                                        if(item.favourite){
                                            addToFavButton += `<span><?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?></span>`;
                                        }
                                        else{
                                            addToFavButton += `<span><?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?></span>`;
                                        }
                                        addToFavButton += `</a>`;
                                        $('#addToFavouritesModal').html(addToFavButton).show();
                                        var downloadGpxLink = `<a  class="dropdown-item" href="#" title="descargar itinerario gpx" onclick="downloadGpx(${item.id}, '${item.title}')"><i class="fa fa-cloud-download-alt"></i> <span><?= Text::_("TPL_GESPLAN_DOWNLOAD_GPX");?></span></a>`;
                                        $('#downloadGpxLink').html(downloadGpxLink).show();
                                        var downloadKmlLink = `<a  class="dropdown-item" href="#" title="descargar itinerario kml" onclick="downloadKml(${item.id}, '${item.title}')"><i class="fa fa-cloud-download-alt"></i> <span><?= Text::_("TPL_GESPLAN_DOWNLOAD_KML")?></span></a>`;
                                        $('#downloadKmlLink').html(downloadKmlLink).show();
                                        
                                        $('.offCanvas-item .point-description').hide();
                                        $('.offCanvas-item .tabs-map').hide();
                                        $('.offCanvascontainer').addClass('active');
                                    } catch (e) {
                                    Joomla.renderMessages({
                                        error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
                                    }, ".sampledata-steps-");
                                    return;
                                    }

                                        /*    */ 
                                },
                                onError: function onError() {
                                    Joomla.renderMessages({
                                    error: ['Something went wrong! Please close and reopen the browser and try again!']
                                    });
                                }
                            });
                            });
                            layer.on('mouseover', function () {
                                        // if(this.feature.properties.type == "1"){
                                        //     this.setStyle({
                                        //         //color: '#0d6efd',
                                        //         weight:6
                                        //     })
                                        // }
                                        this.setStyle({
                                                //color: '#0d6efd',
                                                weight:9
                                            })
                                    });
                                    layer.on('mouseout', function () {
                                        // if(this.feature.properties.type == "1"){
                                        //     this.setStyle({
                                        //         //color: '#ff0000',
                                        //         weight: 3
                                        //     })
                                        // }
                                        this.setStyle({
                                                //color: '#ff0000',
                                                weight: 3
                                            })
                                    });

                        }

                    },
                });
            } catch (e) {}

            _isProcessing = false;
        }

    }

        map.on('moveend', function(ev) {
            let bounds = map.getBounds();
            getData(bounds._northEast, bounds._southWest);
        });

        map.on('zoomend', function(ev) {
            let bounds = map.getBounds();
            getData(bounds._northEast, bounds._southWest);
        });


        map.on("overlayadd", function (event) { //when active or desactive a layer
            //If senderos, bica or vm layers are active, then push them to layers top to be clickable
            if(event.layer == municipios || event.layer == espacios) {
                if(senderos._leaflet_id in map._layers){
                    map.removeLayer(senderos).addLayer(senderos)
                }
                if(vm._leaflet_id in map._layers){
                    map.removeLayer(vm).addLayer(vm)
                }
                if(bica._leaflet_id in map._layers){
                    map.removeLayer(bica).addLayer(bica)
                }

            }
            //Push incidences layer always on top
            if(event.layer != incidences){
                map.removeLayer(incidences);
                map.addLayer(incidences);
            }

        });  
        if (typeof incidences !== 'undefined'){
                setTimeout(incidencesOnTop,3000);
        }
        function incidencesOnTop(){
            map.removeLayer(incidences);
            map.addLayer(incidences);
        }
   
</script>
<script>

//New var to get all equipments latlon to fit in map
var equipmentMarkers = [];

/* Get first equipments page */
$(document).ready(function () {
    let bounds = map.getBounds();
    getData(bounds._northEast, bounds._southWest);
    getAjaxEquipments(0, map, layerControl);
    getAjaxPuntos(0,map, layerControl);
    getAjaxServices(0, map, layerControl);
    printMunicipiosInMap(map, layerControl);
    printEspaciosInMap(map, layerControl);
    getEquipmentIncidences();
    getRoutesIncidences(); 
    $(".leaflet-touch .leaflet-control-layers").addClass("opened");
    $("#openData").addClass("opened");
    $("#openLegend").addClass("opened");
    $("#customLegend").addClass("opened");
    if ($(window).width() >= 768) {
        $(".leaflet-control-zoom.leaflet-bar.leaflet-control").css("left", "300px");
        $(".leaflet-control-zoom.leaflet-bar.leaflet-control").css("transition", "0.3s ease-in-out");
    }
    //not allow to navigate out of tenerife bounds
    map.setMaxBounds([
        [28.748396571187406, -15.671997070312502],
        [27.831789750079267, -17.586364746093754]
    ]);
})
const equipmentsForMap = [];

//Get content by Ajax
function getAjaxEquipments(pageNum){
var equipmentsCategory = 39;
var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + equipmentsCategory + "&page="+pageNum+"";

Joomla.request({
    url: baseUrl +  "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,
    onSuccess: function onSuccess(resp) {
        // Remove loader image

        var response = {};

        try {
            response = JSON.parse(resp);
        } catch (e) {
        Joomla.renderMessages({
            error: [Joomla.Text._('Respuesta invalida')]
        }, "");
        SampleData.inProgress = false;
        return;
        }
        var equipmentsList = response.data[0].data;
        /* PINTAMOS LO EQUIPAMIENTOS */
        if(equipmentsList.length){
            const obj =(equipmentsList);
            $('#itinerarios-column').empty()
            equipmentsList.forEach(printDivContent);
            function printDivContent(item, index) {
                equipmentsForMap[item.id] = {
                    title:item.title, 
                    itemId:item.id,
                    favourite: item.favourite,
                    typeEquipment: item.type_equipment,
                    codigo:item.jcfields[146].rawvalue, 
                    link: item.link,
                    incidence: item.incidence,
                    text: item.introtext.substring(0, 80),
                    imageSrc: item.imageItem['src'],
                    imageAlt:item.imageItem['alt'],
                    latitude: item.jcfields[177].rawvalue,
                    longitude:item.jcfields[178].rawvalue,
                    gallery: item.gallery}         
            }
            //console.log(equipmentsForMap);

            printEquipInMap(equipmentsForMap);

        }	
            /*    */ 
    },
    onError: function onError() {
        Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
        });
    }
});

}
function printEquipInMap(equipmentsForMap){



    var markersEquip = L.markerClusterGroup();

 
        var marker = [];

        equipmentsForMap.forEach(fitInMap);

        function fitInMap(item,id){

            let lat_lng = [item.latitude, item.longitude];

             if(item.typeEquipment == 3){
                marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.acampada}).addTo(acampada);
                //markersEquip.addLayer( marker[id]).addTo(acampada);
            }
            else if(item.typeEquipment == 4){
                marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.recreativo}).addTo(recreativo);
               // markersEquip.addLayer( marker[id]).addTo(recreativo);
            }
            else if(item.typeEquipment == 5){
                marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.campamento}).addTo(campamento);
                //markersEquip.addLayer( marker[id]).addTo(campamento);
            }
            else if(item.typeEquipment == 6){
                marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.visitantes}).addTo(visitantes).bindPopup(item.title);
                //markersEquip.addLayer( marker[id]).addTo(visitantes);
            }            
                //map.addLayer(markersEquip);
               // bounds.extend(lat_lng) ;
               if( (item.typeEquipment == 3) || (item.typeEquipment == 4) || (item.typeEquipment == 5) || (item.typeEquipment == 6)) {
                marker[id].addEventListener('click',
                    function () {
                        $('.offCanvas-item .title .title').text(item.title);
                        $('.offCanvas-item .badge').empty();
                        $('.offCanvas-item .item-value .description').show();
                        $('.offCanvas-item .item-value .description').text(item.text);
                        $('.offCanvas-item .item-value.distance').hide();
                        $('.offCanvas-item .item-value.gainAlt').hide();
                        $('.offCanvas-item .item-value.time').hide();
                        $('.offCanvas-item .item-value.lostAlt').hide();
                        if(item.incidence != ""){
                            $(".item-value.alert").show();
                        }
                        else{
                            $(".item-value.alert").hide();
                        }
                        $('.offCanvas-item .btn-ficha-completa').attr("href", item.link).show();
                        $('.carousel-inner').empty();                             

                        if(item.gallery.length > 0){
                            item.gallery.forEach(printFigure);
                            function printFigure(image, index){
                                let imageDiv = "";
                                imageDiv = '<div class="carousel-item';
                                if(index == 0){
                                    imageDiv += " active";
                                }
                                imageDiv += '"><img src="/'+image.src+'" class="d-block w-100" alt="'+image.alt+'" loading="lazy"></div>';
                                $('.carousel-inner').append(imageDiv);
                            }
                        }
                        else{
                            $('#carouselOfCanvas').empty();
                        }
                        $('.more-actions').show();
                        $('.lugares-interes').empty();

                        var addToFavButton = `<a class="dropdown-item`;
                        if(item.favourite){
                            addToFavButton += ` favorito-marked`;
                        }
                        addToFavButton += `"`;
                        <?php if($status): ?>
                            addToFavButton += ` data-bs-toggle="modal" data-bs-target="#mustlog"`;
                        <?php endif; ?>
                        addToFavButton += `href="#" data-itemId="${item.itemId}" title="añadir a mis rutas" onclick="setAddToFavouritesLink(this)">
                                                    <i class="far fa-heart"></i>`;
                        if(item.favourite){
                            addToFavButton += `<span><?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?></span>`;
                        }
                        else{
                            addToFavButton += `<span><?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?></span>`;
                        }
                        addToFavButton += `</a>`;
                        $('#addToFavouritesModal').html(addToFavButton);
                        $('#downloadGpxLink').empty();
                        $('#downloadKmlLink').empty();
                            
                        $('.offCanvas-item .point-description').empty();
                        $('.offCanvas-item .tabs-map').empty();
                        $('.offCanvascontainer').addClass('active');
                        
                });
            }
        }   

        //layerControl.addOverlay(equipments, '<?= Text::_("TPL_GESPLAN_EQUIPMENTS");?>');

        //map.fitBounds(bounds);
        //load itinerarios

}
   




//Print puntos de interés
//New var to get all puntos de interés latlon to fit in map
var puntosMarkers = [];
const puntosForMap = [];
//Get content by Ajax
function getAjaxPuntos(pageNum, map, layerControl){

        var rpuntosInteresCategory = 66;
        var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + rpuntosInteresCategory + "&page=" + pageNum;

        Joomla.request({
            url: baseUrl +  "&plugin=Listajaxcontent",
            method: 'GET',
            perform: true,
            onSuccess: function onSuccess(resp) {
                // Remove loader image

                var response = {};

                try {
                    response = JSON.parse(resp);
                } catch (e) {
                Joomla.renderMessages({
                    error: [Joomla.Text._('Respuesta invalida')]
                }, "");
                SampleData.inProgress = false;
                return;
                }
                var puntosList = response.data[0].data;
                /* PINTAMOS LO EQUIPAMIENTOS */
                if(puntosList.length){
                    const obj =(puntosList);
                    $('#itinerarios-column').empty()
                    puntosList.forEach(printDivContent);

                    function printDivContent(item, index) {
                        puntosForMap[item.id] = {
                            title:item.title, 
                            description:item.introtext,
                            typeIntrestingPoint: item.type_intresting_point,
                            imageSrc: item.imageItem['src'],
                            imageAlt:item.imageItem['alt'],
                            latitude: item.jcfields[202].rawvalue,
                            longitude:item.jcfields[203].rawvalue,
                            email: item.jcfields[168].rawvalue,
                            phone:item.jcfields[167].rawvalue
                        }

                    }
                    printPuntosInMap(puntosForMap, map, layerControl);

                }	
                    /*    */ 
            },
            onError: function onError() {
                Joomla.renderMessages({
                error: ['Something went wrong! Please close and reopen the browser and try again!']
                });
            }
        });

    }
i = 0;
function printPuntosInMap(puntosForMap, map, layerControl){


    var bounds = L.latLngBounds();
    var markersPoints = L.markerClusterGroup();
    var marker = [];

    puntosForMap.forEach(fitInMap);

    function fitInMap(item,id){

    //Object.values(typeNatural).includes(2)
        let lat_lng = [item.latitude, item.longitude];
        marker[id] = new L.marker(lat_lng);
        // debugger;
        if(Object.values(typeNatural).includes( Number(item.typeIntrestingPoint) )){
            marker[id] = new L.marker(lat_lng, {icon: pointsIcons.natural}).addTo(natural);
        }
        else if(Object.values(typeCultural).includes( Number(item.typeIntrestingPoint) )){
            marker[id] = new L.marker(lat_lng, {icon: pointsIcons.cultural}).addTo(cultural);
        }
        else if(Object.values(typeGeneral).includes( Number(item.typeIntrestingPoint) )){
            marker[id] = new L.marker(lat_lng, {icon: pointsIcons.general}).addTo(general);
        }

            //markersPoints.addLayer( marker[id]).addTo(interestpoints);
            // map.addLayer(markersPoints);
            //bounds.extend(lat_lng) ;
            //marker[id].bindPopup(item.title);
            marker[id].addEventListener('click',
                function () {
                    $('.offCanvas-item .title .title').text(item.title);
                    $('.offCanvas-item .badge').hide();
                    $('.offCanvas-item .item-value.distance').hide();
                    $('.offCanvas-item .item-value.time').hide();
                    $('.offCanvas-item .item-value.gainAlt').hide();
                    $('.offCanvas-item .item-value.lostAlt').hide();
                    $('.offCanvas-item .item-value .description').empty();
                    $('.offCanvas-item .btn-ficha-completa').hide();
                    $('.more-actions').hide();
                    $('.carousel-inner').empty();                             
                    if(item.imageSrc != ""){
                        
                       let imageDiv = "";
                        
                        imageDiv += '<img src="/'+item.imageSrc+'" class="d-block w-100" alt="'+item.imageAlt+'" loading="lazy">';
                        $('.carousel-inner').append(imageDiv);
                    }

                    $('.offCanvas-item .point-description').empty();
                    $('.offCanvas-item .lugares-interes').empty();
                    $('.offCanvas-item .point-description').text(item.description).show();
                    if(item.email != ""){
                        
                        let email = "";                      
                        email += '<br/><a href="mailto:'+item.email+'">'+item.email+'</a>';
                        $('.offCanvas-item .lugares-interes').append(email);
                        $('.offCanvas-item .lugares-interes').show();
                    }
                    if(item.phone != ""){
                        
                        let phone = "";                      
                        phone += '<br/><a href="mailto:'+item.phone+'">'+item.phone+'</a>';
                        $('.offCanvas-item .lugares-interes').append(phone);
                        $('.offCanvas-item .lugares-interes').show();
                    }
                    $('.offCanvascontainer').addClass('active');
                    
            });
        }   
        //layerControl.addOverlay(interestpoints, 'Puntos interés');
        //map.fitBounds(bounds);
}
function printEspaciosInMap(map, layerControl){


    // var espacios_naturales = new L.geoJson();
    // espacios_naturales.addTo(espaciosnaturales);

    $.ajax({
    dataType: "json",
    url: "<?= $siteUrl ?>/templates/gesplan/images/EENNPP_TF.geojson",
    success: function(data) {

        $(data.features).each(function(key, data) {
                if( data.properties.categoria == 'Monumento Natural'){
                    L.geoJSON(data, {
                                    style: {"color": "#004776"}}).addTo(espacios).on('click',onMapClick);
                }
                else if(data.properties.categoria == 'Sitio de Interés Científico'){
                    L.geoJSON(data, {
                                    style: {"color": "#555555"}}).addTo(espacios).on('click',onMapClick);
                }
                else if(data.properties.categoria == 'Paisaje Protegido'){
                    L.geoJSON(data, {
                                    style: {"color": "#7c91b6"}}).addTo(espacios).on('click',onMapClick);
                }
                else if(data.properties.categoria =='Parque Rural' ){
                    L.geoJSON(data, {
                                    style: {"color": "#cc3333"}}).addTo(espacios).on('click',onMapClick);
                }
                else if(data.properties.categoria == 'Reserva Natural Especial'){
                    L.geoJSON(data, {
                                    style: {"color": "#6e9480"}}).addTo(espacios).on('click',onMapClick);
                }
                else if(data.properties.categoria == 'Parque Natural'){
                    L.geoJSON(data, {
                                    style: {"color": "#c87f12"}}).addTo(espacios).on('click',onMapClick);
                }
                else if(data.properties.categoria == 'Reserva Natural Integral'){
                    L.geoJSON(data, {
                                    style: {"color": "#004030"}}).addTo(espacios).on('click',onMapClick);
                }
                else if(data.properties.categoria == 'Parque Nacional'){
                    L.geoJSON(data, {
                                    style: {"color": "#e68200"}}).addTo(espacios).on('click',onMapClick);
                }
           
            })
             
    }//colores
    });
 
}
const servicesForMap = [];

//Get content by Ajax
function getAjaxServices(pageNum, map, layerControl){
var servicesCategory = 99;
var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + servicesCategory + "&page="+pageNum+"";

Joomla.request({
    url: baseUrl +  "&plugin=Listajaxcontent",
    method: 'GET',
    perform: true,
    onSuccess: function onSuccess(resp) {
        // Remove loader image

        var response = {};

        try {
            response = JSON.parse(resp);
        } catch (e) {
        Joomla.renderMessages({
            error: [Joomla.Text._('Respuesta invalida')]
        }, "");
        return;
        }
        var servicesList = response.data[0].data;
        /* PINTAMOS LO EQUIPAMIENTOS */
        if(servicesList.length){
            const obj =(servicesList);
            $('#itinerarios-column').empty()
            servicesList.forEach(printDivContent);

            function printDivContent(item, index) {
                servicesForMap[item.id] = {
                    title:item.title, 
                    itemId:item.id,
                    favourite: item.favourite,
                    typeEquipment: item.type_equipment,
                   // codigo:item.jcfields[146].rawvalue, 
                    link: item.link,
                    //incidence: item.jcfields[254].rawvalue,
                    text: item.introtext.substring(0, 80),
                    imageSrc: item.imageItem['src'],
                    imageAlt:item.imageItem['alt'],
                    latitude: item.jcfields[177].rawvalue,
                    longitude:item.jcfields[178].rawvalue,
                    gallery: item.gallery}         
            }
            //console.log(equipmentsForMap);

            printServicesInMap(servicesForMap);

        }	
            /*    */ 
    },
    onError: function onError() {
        Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
        });
    }
});

}

function printServicesInMap(servicesForMap){



    var markersServices = L.markerClusterGroup();




        var marker = [];

        servicesForMap.forEach(fitInMap);

        function fitInMap(item,id){

            let lat_lng = [item.latitude, item.longitude];

            if(item.typeEquipment == 1){
                marker[id] = new L.marker(lat_lng, {icon: servicesIcons.descanso}).addTo(descanso).bindPopup(item.title);
               // markersServices.addLayer( marker[id]).addTo(descanso);
            }
            else if(item.typeEquipment == 2){
                marker[id] = new L.marker(lat_lng, {icon: servicesIcons.aparcamiento}).addTo(aparcamiento).bindPopup(item.title);
                //markersServices.addLayer( marker[id]).addTo(aparcamiento);
            }
        }   
        
        //layerControl.addOverlay(equipments, '<?= Text::_("TPL_GESPLAN_EQUIPMENTS");?>');

        //map.fitBounds(bounds);
        //load itinerarios

}
function printMunicipiosInMap(map, layerControl){


    $.ajax({
    dataType: "json",
    url: "<?= $siteUrl ?>/templates/gesplan/images/new_municipios.geojson",
    success: function(data) {
        $(data.features).each(function(key, data) {
                
                    L.geoJSON(data, {
                                    style: {"color": "#a16249"}}).addTo(municipios).on('click', onMapClick); 
            })            
        }
    });

}
</script>

<script>
    var container = document.getElementsByClassName("leaflet-control-layers")[0];
    if (!L.Browser.touch) {
        L.DomEvent
            .disableClickPropagation(container)
            .disableScrollPropagation(container);
    } else {
        L.DomEvent.disableClickPropagation(container);
    }
</script>
<?php
            $path = dirname(__FILE__) . "/partials/";
            require_once($path.'banner-planificador.php');
            require_once($path.'modal-favorito.php');
        ?>
                                        <script>
    
    function setAddToFavouritesLink(el){
        console.log(el);
        item_id = $(el).data("itemid");
        action = $(el).data("action");
        addToFavourites(action,item_id);
    }
  
    //Get content by Ajax
    function addToFavourites(user_id,item_id){

        var baseUrl = "/index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&item=" + item_id;
        console.log(baseUrl);

        Joomla.request({
            url: baseUrl +  "&plugin=Addtofavourites",
            method: 'GET',
            perform: true,
            onSuccess: function onSuccess(resp) {
                const obj = JSON.parse(resp);
                if(obj.data[0].message == "ADDED"){
                    $(".dropdown-item[data-itemid="+obj.data[0].item+"]").addClass('favorito-marked');
                    $(".dropdown-item[data-itemid="+obj.data[0].item+"]").html('<?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?>');
                    Swal.fire({
                            html: '<p>¡Guardado en favoritos!</p>',
                            confirmButtonText: 'Aceptar'
                    })
                }
                else if(obj.data[0].message == "REMOVED"){
                    $(".dropdown-item[data-itemid="+obj.data[0].item+"]").removeClass('favorito-marked');
                    $(".dropdown-item[data-itemid="+obj.data[0].item+"]").html('<?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?>');
                    Swal.fire({
                            html: '<p>¡Eliminado de favoritos!</p>',
                            confirmButtonText: 'Aceptar'
                    })
        }
            },
            onError: function onError() {
                Joomla.renderMessages({
                error: ['Something went wrong! Please close and reopen the browser and try again!']
                });
            }
        });


    }


</script>

<?php // Valoraciones ?>
<script>
    $(document).ready(function () {

        $('#leaflet-control-layers-group-6 input').on('click', function(){
            municipios.eachLayer(function (layer) {
                layer.bringToBack();
            });
        });   
        $('#leaflet-control-layers-group-5 input').on('click', function(){
            espacios.eachLayer(function (layer) {
                layer.bringToBack();
            });
        });

    })
</script>
<script>
    function onMapClick(e) {
        var espaciosNaturales = espacios.getLayers();
        var municipiosLayer = municipios.getLayers();

        var espaciosHtml = [];
        var municipiosHtml = [];
        var html = "";

        espaciosNaturales.forEach(function(layer){
            layer.eachLayer(function(memberLayer) {
                if (memberLayer.contains(e.latlng)) {
                    espaciosHtml.push(memberLayer.feature.properties)
                    console.log(memberLayer.feature.properties);
                }
            });
        });
        if(espaciosHtml.length > 0){
            html += "<b><?= Text::_("TPL_GESPLAN_NATURAL_AREAS");?>: </b><br/>" + espaciosHtml.map(function(o) {
                 
                        return o.categoria+" "+o.nombre
                    
                
            }).join('<br/>');
            html += '<br/>';
        }
        municipiosLayer.forEach(function(layer){
            layer.eachLayer(function(memberLayer) {
                if (memberLayer.contains(e.latlng)) {
                    municipiosHtml.push(memberLayer.feature.properties)
                    console.log(memberLayer.feature.properties);
                }
            });
        });
        if(municipiosHtml.length > 0){
            html += "<b><?= Text::_("TPL_GESPLAN_TOWNS");?>:</b> <br/>" + municipiosHtml.map(function(o) {
                 
                        return o.nombre
                    
                
            }).join('<br/>');
        }
        map.openPopup(html,e.latlng);

    }
</script>
