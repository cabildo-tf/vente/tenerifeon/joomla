<?php

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;

            $municipios_catid = 88;
            //Get municipio id by cfield 73, first occurence
            $query = $db
            ->getQuery(true)
            ->select('title')
            ->from($db->quoteName('#__content'))
            ->where($db->quoteName('id') . " = " . $db->quote($this->item->jcfields[256]->rawvalue))
            ->where($db->quoteName('catid') . " = " . $db->quote($municipios_catid)); 
     
            $db->setQuery($query);
             $municipio_title = $db->loadResult();

             $meteoJSON_field = 222;
             $query = $db
             ->getQuery(true)
             ->select('value')
             ->from($db->quoteName('#__fields_values'))
             ->where($db->quoteName('field_id') . " = " . $db->quote($meteoJSON_field)) //id cf codigo ruta
             ->where($db->quoteName('item_id') . " = " . $db->quote($this->item->jcfields[256]->rawvalue)); //value codigo ruta
     
             // Reset the query using our newly populated query object.
             $db->setQuery($query);
     
             // Load the results as a list of stdClass objects (see later for more options on retrieving data).
             $meteoJSON = $db->loadResult();
    ?>
    <?php $weather = (json_decode($meteoJSON));?>
    <section class="meteo ficha">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title">
                        <h2>
                            <?= Text::_("TPL_GESPLAN_METEO_INFO");?>
                        </h2>
                    </div>
                </div>
            </div>

            <?php //Current weather
                    $weather_conditions = [
                        'Clouds' => Text::_("TPL_GESPLAN_CLOUDS"),
                        'Rain' => Text::_("TPL_GESPLAN_RAIN"),
                        'Thunderstorm'  => Text::_("TPL_GESPLAN_THUNDERSTORM"),
                        'Drizzle' => Text::_("TPL_GESPLAN_DRIZZLE"),
                        'Snow' => Text::_("TPL_GESPLAN_SNOW"),
                        'Atmosphere' => Text::_("TPL_GESPLAN_ATMOSPHERE"),
                        'Clear' => Text::_("TPL_GESPLAN_CLEAR"),
                        'Fog' => Text::_("TPL_GESPLAN_FOG"),
                        'Mist' => Text::_("TPL_GESPLAN_MIST"),
                        'Smoke' => Text::_("TPL_GESPLAN_SMOKE"),
                        'Haze' => Text::_("TPL_GESPLAN_HAZE"),
                        'Dust' => Text::_("TPL_GESPLAN_DUST"),
                        'Arena' => Text::_("TPL_GESPLAN_SAND"),
                        'Ash' => Text::_("TPL_GESPLAN_ASH"),
                        'Squall' => Text::_("TPL_GESPLAN_SQUALL"),
                        'Tornado' => Text::_("TPL_GESPLAN_TORNADO")
                    ];
                    $days_dias = array(
                        'Monday'=>Text::_("TPL_GESPLAN_MONDAY"),
                        'Tuesday'=>Text::_("TPL_GESPLAN_TUESDAY"),
                        'Wednesday'=>Text::_("TPL_GESPLAN_WEDNESDAY"),
                        'Thursday'=>Text::_("TPL_GESPLAN_THURSDAY"),
                        'Friday'=>Text::_("TPL_GESPLAN_FRIDAY"),
                        'Saturday'=>Text::_("TPL_GESPLAN_SATURDAY"),
                        'Sunday'=>Text::_("TPL_GESPLAN_SUNDAY"),
                    );
                    $temperature = $weather->current->temp;
                    $today = $weather->current->dt;
                    $wind_speed =  $weather->current->wind_speed;
                    $sky = $weather->current->weather[0]->main;
                    $min = $weather->daily[0]->temp->min;
                    $max = $weather->daily[0]->temp->max;
                    if(isset($weather->daily[0]->rain)){
                        $rain = $weather->daily[0]->rain;
                    }
                    else{
                        $rain = 0;
                    }
                    $icon = "https://openweathermap.org/img/wn/".$weather->current->weather[0]->icon."@4x.png";
                    $date = new DateTime();
                    $date->setTimestamp($today);
                ?>
                <?php 
                    setlocale( LC_ALL,"es_ES@euro","es_ES","esp" );
                ?>
            <div class="row">
                <div class="col-lg-6">
                    <div class="title-today">
                        <h3><?= Text::_("TPL_GESPLAN_TODAY")?>, <?= $days_dias[date('l', $today)]?>, <?= Text::_("TPL_GESPLAN_IN");?> <?= $municipio_title;?></h3>
                    </div>
                    <div class="row row-today">
                        <div class="col-6 col-md-3 border-right border-bottom">
                            <div class="col-today">
                                <img src="<?=	$icon?>" />
                                <hr>
                                <?=   $weather_conditions[$sky];?>

                            </div>
                        </div>
                        <div class="col-6 col-md-3 border-bottom">
                            <div class="col-today">
                                <?= HTMLHelper::_('image','meteo_wind_icon.png',"Wind", null, true, 0) ?>
                                <hr>
                                <p><strong><?= round($wind_speed)?></strong> km/h</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 border-right">
                            <div class="col-today">
                                <?= HTMLHelper::_('image','meteo_temperatura_icon.png',"Temperature", null, true, 0) ?>
                                <hr>
                                <p><strong><?= round($max)?>ºC</strong> <?= Text::_("TPL_GESPLAN_MAX")?></p>
                                <p><strong><?= round($min)?>ºC</strong> <?= Text::_("TPL_GESPLAN_MIN")?></p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="col-today">
                                <?= HTMLHelper::_('image','meteo_rain_icon.png',"Rain", null, true, 0) ?>
                                <hr>
                                <p><strong><?= $rain * 100;?>%</strong> </p>
                                <p><?php if($rain > 50){echo "Probabilidad de lluvia";}?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <div class="title-prev">
                        <h3><?= Text::_("TPL_GESPLAN_NEXT_3_DAYS_FORECAST")?></h3>
                    </div>
                    <div class="row seven-cols">
                        <?php //Forecast
                            $days = $weather->daily;
                            $date = new DateTime();
                            //Next day
                            $d = 1;
                            while($d < 4){ //1 = tomorrow, 2 = next tomorrow, 3 = next after next
                                $date->setTimestamp($days[$d]->dt);?>

                        <div class="col-sm-4">
                            <div class="day-prev">
                                <img
                                    src="<?=	$next_icon = "https://openweathermap.org/img/wn/".$days[$d]->weather[0]->icon."@2x.png"?>" />
                                <p class="order-1 order-md-2">  <?= $days_dias[date('l', $days[$d]->dt)]?></p>
                                <p class="order-3"><strong><?= round($days[$d]->temp->max);?>ºC</strong> <?= Text::_("TPL_GESPLAN_MAX")?></p>
                                <p class="order-4"><strong><?= round($days[$d]->temp->min);?>ºC</strong> <?= Text::_("TPL_GESPLAN_MIN")?></p>
                            </div>
                        </div>
                        <?php
                                $d++;
                            }
                        ?>


                    </div>
                </div>
            </div>
        </div>
    </section>