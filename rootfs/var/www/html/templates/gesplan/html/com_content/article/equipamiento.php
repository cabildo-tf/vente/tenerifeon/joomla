<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
<script>
    var itineraries = [];
    var otherEquipments = [];
</script>
<?php $db = Factory::getDbo(); ?>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<div class="com-content-article item-page<?php echo $this->pageclass_sfx; ?>" itemscope
    itemtype="https://schema.org/Article">
    <meta itemprop="inLanguage"
        content="<?php echo ($this->item->language === '*') ? Factory::getApplication()->get('language') : $this->item->language; ?>">

    <section class="itinerarios">
        <div class="container mb-3">
            <div class="row">
                <div class="col-lg-8">
                    <h1><?php echo $this->escape($this->item->title); ?></h1>
                    <div class="subtitle">
                        <p class="region"><?= $this->escape($this->item->category_title)?>,
                        <?php echo($this->item->jcfields[76]->value) //Ubicación / Situación?>,
                            <?php echo($this->item->jcfields[90]->value) //municipio?></p>
                    </div>

                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-md-6 col-lg-12">
                            <div class="container-actions">
                                <!-- <a><i class="far fa-comment"></i><span>3</span></a> -->
                                <?php   $user =   Factory::getUser();
                                        $status = $user->guest;
                                ?>
                                <?php   //Check if user has already masrked as favourite
                                        $query = $db
                                        ->getQuery(true)
                                        ->select(array('COUNT(id)'))
                                        ->from($db->quoteName('#__sg_favourites'))
                                        ->where($db->quoteName('user_id') . " = " . $db->quote($user->id))
                                        ->where($db->quoteName('item_id') . " = " . $db->quote($this->item->id));
                                        // Reset the query using our newly populated query object.
                                        $db->setQuery($query);
                                        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                        $isFavourite = $db->loadObjectList();
                                ?>
                                <button <?php if(!$status):?><?= 'id="addToFavourites"'?>
                                    data-itemId="<?= $this->item->id;?>" <?php else:?> data-bs-toggle="modal"
                                    data-bs-target="#mustlog"
                                    <?php endif; ?><?php if($isFavourite[0]->count):?><?= 'class="favorito-marked"'?>
                                    data-action="remove" <?php else:?>data-action="add" <?php endif; ?>><i
                                        class="far fa-heart"></i><span><?= Text::_("TPL_GESPLAN_FAVOURITES");?></span></button>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                                <script>
                                    //if not logged in

                                    //trigger popup

                                    //if logged in

                                    $(document).ready(function () {
                                        $("#addToFavourites").click(function () {
                                            item_id = $(this).data("itemid");
                                            action = $(this).data("action");
                                            addToFavourites(action, item_id);
                                        });
                                    });
                                    //Get content by Ajax
                                    function addToFavourites(user_id, item_id) {

                                        var baseUrl = "/index.php?option=com_ajax&format=json&group=ajax&" + Joomla
                                            .getOptions('csrf.token') + "=1&item=" + item_id + "&action=" + action;
                                        console.log(baseUrl);

                                        Joomla.request({
                                            url: baseUrl + "&plugin=Addtofavourites",
                                            method: 'GET',
                                            perform: true,
                                            onSuccess: function onSuccess(resp) {
                                                const obj = JSON.parse(resp);
                                                if (obj.data[0].message == "ADDED") {
                                                    $("#addToFavourites").addClass('favorito-marked').data(
                                                        "action", "remove");
                                                } else if (obj.data[0].message == "REMOVED") {
                                                    $("#addToFavourites").removeClass('favorito-marked')
                                                        .data("action", "add");
                                                }
                                            },
                                            onError: function onError() {
                                                Joomla.renderMessages({
                                                    error: [
                                                        'Something went wrong! Please close and reopen the browser and try again!'
                                                    ]
                                                });
                                            }
                                        });


                                    }
                                    //call ajax with user id and article id

                                    //insert in _sg_favourites 

                                    //if insert ok

                                    //mark heart 
                                </script>
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown"
                                        aria-expanded="false"><i class="fa fa-share-alt"></i></button>
                                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                                        <div class="addthis_inline_share_toolbox">
                                            <script type="text/javascript"
                                                src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-621f488e60708602">
                                            </script>
                                        </div>
                                    </div>
                                </div>

                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown"
                                        aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton2">
                                        <li><a class="dropdown-item" href="#" onclick="window.print();"><?= Text::_("TPL_GESPLAN_PRINT")?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-12 d-flex">
                            <div id="content-valoration" class="head-ficha">
                                <?php echo $this->item->event->beforeDisplayContent; //Displays ratings ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Alertas -->
        <script>
            var incidence = [];
        </script>
        <?php 
    //Check if this equipamiento has incidencias
        $field_incidencias = 254;//Field to save the incidencias in equipamiento
        $query = $db
        ->getQuery(true)
        ->select('value')
        ->from($db->quoteName('#__fields_values'))
        ->where($db->quoteName('field_id') . " = " . $db->quote($field_incidencias)) //id cf field incidencias en equipamiento
        ->where($db->quoteName('item_id') . " = " . $db->quote($this->item->id)); //id equipamiento
        // Reset the query using our newly populated query object.
        $db->setQuery($query);
        $incidencias_in_equipamiento = $db->loadResult();
        //Chech
        if(!is_null($incidencias_in_equipamiento)){
				
                $db = Factory::getDbo();
                $query = $db
                ->getQuery(true)
                ->select('state')
                ->from($db->quoteName('#__content'))
                ->where($db->quoteName('id') . " = " . $db->quote($incidencias_in_equipamiento));
                // Reset the query using our newly populated query object.
                $db->setQuery($query);
                // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                $indicendeIsActive = $db->loadResult();

                if($indicendeIsActive == 1): ?>

                    <?php
                        $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                        $appParams = JFactory::getApplication()->getParams();  
                        $model->setState('params', $appParams);
                        $model->setState('filter.article_id', $incidencias_in_equipamiento); //Banner home category ID
                        
                        $helpArticle =   $model->getItems();
                        $incidencia = $helpArticle[0];
                        $fields = FieldsHelper::getFields('com_content.article', $incidencia, true);
                        // Adding the fields to the object
                        $incidencia->jcfields = array();
                
                        foreach ($fields as $key => $field)
                        {
                            $incidencia->jcfields[$field->id] = $field;
                        }
                    //dd($incidencia);
                    ?>
                    <script> //Get incidence linestring

                        incidence.push({lat:'<?=  $incidencia->jcfields[249]->rawvalue?>', lon:'<?=  $incidencia->jcfields[250]->rawvalue?>',name:'<?= $incidencia->title?>'});

                    </script>
                    <div class="alertas">
                        <div class="container">
                            <div class="row">
                                <div class="col-3 col-md-1">
                                    <div class="icon-alerta">
                                        <?= HTMLHelper::_('image', 'icon-warning.png', Text::_("TPL_GESPLAN_INCIDENCE"), 'class="icon-warning"', true, 0) ?>
                                        <span class="alerta"><?= Text::_("TPL_GESPLAN_INCIDENCE");?></span>
                                    </div>
                                </div>
                                <div class="col-9 col-md-11 col-alert">
                                    <p><?= $incidencia->title;?></p>
                                    <p><?= $incidencia->jcfields[236]->rawvalue;?></p>
                                    <?php if(!empty($incidencia->jcfields[247]) && !is_null($incidencia->jcfields[247])): ?>
                                    <a title="Enlace a la alerta x" href=""><?=$incidencia->jcfields[247]->rawvalue ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>

        <?php } ?>
 
 



        <div class="container-itinerario">
            <?php if(!empty($this->item->jcfields[107]->value)) :?>
            <?php $images = ($this->item->jcfields[107]->subform_rows) ?>
            <?php if(!empty($images)) :?>
            <?php //Checf if at least there is one image to echo ?>
            <?php if(!empty($images[0]["imagen"]->rawvalue["imagefile"])) :?>

            <div class="slick-itinerarios">
                <?php foreach($images as $image) : ?>
                <div>
                    <?php  $imgUrl = $image["imagen"]->rawvalue["imagefile"]; ?>
                    <?php if(!empty($imgUrl)) :?>
                    <figure><img src="<?php echo $imgUrl;?>"
                            alt="<?php  echo($image["imagen"]->rawvalue["alt_text"]); ?>" loading="lazy"></figure>
                    <?php endif; ?>
                </div>
                <?php endforeach; ?>
                
            </div>

            <?php endif; ?>
            <?php endif; ?>
            <?php endif; ?>
             
        </div>

        <div class="container-tags-itinerarios">
            <!--  -->
            <div class="tag-equipamiento disabled">
                <?php if(!empty($this->item->jcfields[231]->rawvalue)):?>
                    <?php if( $this->item->jcfields[209]->rawvalue[0]== 1){
                        $accesClass = "accesible";
                    }elseif( $this->item->jcfields[209]->rawvalue[0]== 3){
                        $accesClass = "parcialmente-accesible";
                    }
                    elseif( $this->item->jcfields[209]->rawvalue[0]== 2){
                        $accesClass = "inaccesible";
                    } else{
                        $accesClass = "grey";  
                    }
                    ?>
                    <button type="button" class="<?php echo $accesClass?>" data-bs-toggle="modal" data-bs-target="#accesibilidadModal">
                    <i class="fas fa-wheelchair"></i><?= Text::_("TPL_GESPLAN_ACCESIBILITY_".$this->item->jcfields[209]->rawvalue[0].""); ?>
                    </button> 
                <?php else : ?>
                    <i class="fas fa-wheelchair"></i><p><?= Text::_("TPL_GESPLAN_ACCESIBILITY_".$this->item->jcfields[209]->rawvalue[0].""); ?></p>
                <?php endif; ?>
            </div>
            <!--  -->
            <div class="container">
                        <div class="row">
                            <?php if ($this->item->catid != 51 && $this->item->catid != 115 && $this->item->catid != 116 ) : //Show aforo if not centro de visitante ?>
                                <div class="col-md-3  extra-pad">
                                    <p class="head-equipamiento"><?= Text::_('TPL_GESPLAN_MAX_CAPACITY')?></p>
                                    <?php if(!empty($this->item->jcfields[95]->rawvalue)) : ?>
                                    <p><?php echo($this->item->jcfields[95]->rawvalue) //Aforo?>
                                        <?= Text::_('TPL_GESPLAN_SEATS_NUMBER')?></p>
                                    <?php else : ?>
                                    <p><?= Text::_('TPL_GESPLAN_NO_DATA_AVAILABLE')?></p>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <?php if(!empty($this->item->jcfields[98]->rawvalue) || (!empty($this->item->jcfields[276]->rawvalue))) : //horario?>
                            <div class="col-md-3  extra-pad">
                                <p class="head-equipamiento">
                                    <?= Text::_('TPL_GESPLAN_CONTACT')?>
                                </p>
                                <p><?php echo($this->item->jcfields[98]->rawvalue) //Teléfono?></p>
                                <?php if($this->item->jcfields[276]->value != "") : ?>
                                    <a href="mailto:><?php echo($this->item->jcfields[276]->value) //Email?>"></a>
                                <?php endif; ?>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($this->item->jcfields[282]->rawvalue) ) : //periodo-de-uso-equipamiento?>
                            <div class="col-md-6  extra-pad">
                                <p class="head-equipamiento">
                                    <?= Text::_('TPL_GESPLAN_USE_PERIOD')?>
                                </p>
                                <?php echo($this->item->jcfields[282]->rawvalue) //periodo-de-uso-equipamiento?>
                            </div>
                            <?php endif; ?>

                            <?php if(!empty($this->item->jcfields[97]->rawvalue)) : //horario?>
                                <div class="col-md-6 extra-pad">
                                    <div class="head-equipamiento">
                                        <p class="head-equipamiento"><?= Text::_('TPL_GESPLAN_TIME_SCHEDULE')?></p>
                                        <p><?php echo($this->item->jcfields[97]->rawvalue) //Horario?></p>
                                        </p>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
            </div>
        </div>


            <div class="modal fade" id="accesibilidadModal" tabindex="-1" aria-labelledby="accesibilidadModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <p class="modal-title"><?= Text::_("TPL_GESPLAN_ACCESIBILITY_".$this->item->jcfields[209]->rawvalue[0].""); ?></p>
                        </div>
                        <div class="modal-body">
                            <p><?= $this->item->jcfields[231]->rawvalue?></p>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="close-modal" data-bs-dismiss="modal"><?= text::_('TPL_GESPLAN_CLOSE_MODAL');?></button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container-itinerario">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="responsive-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                            <?php if(!empty($this->item->jcfields[199]->value)) : //consejos?>

                                <li class="nav-item">
                                    <button class="nav-link active" id="faqtabs-category1" data-bs-toggle="tab"
                                        data-bs-target="#category1" type="button" role="tab" aria-controls="home"
                                        aria-selected="true"> <?= Text::_('TPL_GESPLAN_DESCRIPTION')?></button>
                                </li>

                                <li class="nav-item">
                                    <button class="nav-link" id="faqtabs-category2" data-bs-toggle="tab"
                                        data-bs-target="#category2" type="button" role="tab" aria-controls="home"
                                        aria-selected="false"> <?= Text::_('TPL_GESPLAN_TIPS')?> </button>
                                </li>
                                <!-- <li class="nav-item">
                                    <button class="nav-link" id="faqtabs-category3" data-bs-toggle="tab"
                                        data-bs-target="#category3" type="button" role="tab" aria-controls="home"
                                        aria-selected="false"> <?= Text::_('TPL_GESPLAN_GUIDES')?> </button>
                                </li> -->

                                <?php endif; ?>
                            </ul>

                            <div id="content" class="tab-content" role="tablist">
                                <div id="category1" class="card tab-pane fade show active flex-column" role="tabpanel"
                                    aria-labelledby="category1-tab">

                                    <h2> <?= Text::_('TPL_GESPLAN_DESCRIPTION')?></h2>
                                    <?php   
                                                if(!empty($this->item->fulltext)){
                                                    echo $this->item->fulltext;
                                                }else{
                                                    echo $this->item->text;
                                                }
                                        ?>


                                </div>
                                <?php if(!empty($this->item->jcfields[199]->value)) : //consejos?>

                                <div id="category2" class="card tab-pane fade" role="tabpanel"
                                    aria-labelledby="category2-tab">
                                    <h2> <?= Text::_('TPL_GESPLAN_TIPS')?></h2>

                                    <p><?php echo($this->item->jcfields[199]->value) //consejos?></p>
                                <?php

                                $articleId = 1192; //Guia de actividades spanish article ID
                                ($associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId));
                                $articleLang = Factory::getLanguage()->getTag();
                                if(count($associations) > 0) {
                                    $articleLang = Factory::getLanguage()->getTag();
                                    $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                                    $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                                }
                                else{
                                    $articleLang = 'es-ES';
                                    $idLang = $articleId;
                                    $catidLang = 69; //Guía de actividades categoty id
                                }

                                ?>

                                <a href="<?php echo Route::_(RouteHelper::getArticleRoute($idLang, $catidLang, $articleLang));?>"><?= Text::_("TPL_GESPLAN_GUIDE");?></a>

                                </div>


                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php if(!empty($this->item->jcfields[197]->value)) : //autorizaciones?>                    
                        <div class="col-lg-5">
                            <div class="permisos">
                                <h2><?= Text::_('TPL_GESPLAN_SEATS_PERMISSIONS')?></h2>

                                <?php echo($this->item->jcfields[197]->value) //autorizaciones?>
                                
                                <?php if ($this->item->catid != 50 && $this->item->catid != 113 && $this->item->catid != 114 ) : //Show mas informacion if not campamento ?>
                                
                                <?php

                                    $articleId = 580;
                                    $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
                                    $articleLang = Factory::getLanguage()->getTag();
                                    if(count($associations) > 0) {
                                        $articleLang = Factory::getLanguage()->getTag();
                                        $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                                        $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                                    }
                                    else{
                                        $articleLang = 'es-ES';
                                        $idLang = $articleId;
                                        $catidLang = 69;
                                    }
                                ?>
                                
                                <a href="<?php echo Route::_(RouteHelper::getArticleRoute( $idLang, $catidLang, $articleLang ));?>"
                                    alt="<?= Text::_('TPL_GESPLAN_LINK_TO_PERMISSION')?>"><?= Text::_('TPL_GESPLAN_MORE_INFO')?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php elseif(!empty($this->item->jcfields[275]->value)):?>
                            <!-- Grupos -->
                            <div class="col-lg-5">
                                <div class="permisos">
                                    <h2><?= Text::_("TPL_GESPLAN_VISITORS");?></h2>
                                    <?= $this->item->jcfields[275]->value; ?>  
                                </div>
                            </div>
                        <?php endif;?>
                </div>
            </div>

        </div>

    </section>
    <?php  if(!empty($this->item->jcfields[232]->rawvalue)) : ?>
    <section class="servicios-equipamiento">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <!-- Servicios -->
                    <h2><?= Text::_("TPL_GESPLAN_SERVICES");?></h2>

                    <!-- DIV que sea *-->
                    <?= $this->item->jcfields[232]->rawvalue; ?>
                    <!-- /div -->
                    
                </div>
                <div class="col-md-5 d-flex">
                    <?= HTMLHelper::_('image','img-apoyo-servicios.jpg', Text::_("TPL_GESPLAN_ROUTE_ICON"), 'class="img-suport"', true, 0) ?>

                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <section class="map-equipamientos">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2><?= Text::_('TPL_GESPLAN_EQUIPMENT_LOCATION')?></h2>

                    <div id="map-equipamiento" class="map-equipamiento-container">
                    <button id="openData" class="niceRotate" style="z-index: 9999999;"><i class="fa fa-chevron-left"></i></button>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    $config = Factory::getConfig();
    $mapbox_token = $config->get('mapbox_token');
    if($this->item->parent_id == 39 || $this->item->parent_id == 99 || $this->item->parent_id == 102 || $this->item->parent_id == 103 || $this->item->parent_id == 117 || $this->item->parent_id == 118 ){
        $categoryCustomIdFeield = 257;
        $db = Factory::getDbo();
        $query = $db
            ->getQuery(true)
            ->select(array('value'))
            ->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('field_id') . " = " . $db->quote($categoryCustomIdFeield))
            ->where($db->quoteName('item_id') . " = " . $db->quote($this->item->catid));
            // Reset the query using our newly populated query object.
            $db->setQuery($query);
            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
            $typeEquipment = $db->loadResult();
            $this->item->type_equipment = $typeEquipment;
    }
?>

    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"
        integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ=="
        crossorigin=""></script>

    <script src="/templates/gesplan/js/gesplan/leaflet.groupedlayercontrol.js"></script>

    <script>
        window.onload = function() {
            setTimeout(loadMap, 500)
            if (typeof incidences !== 'undefined'){
                setTimeout(loadIncidences,3000);
            }
        }; 

        function loadMap(){
            var lat = <?= $this->item->jcfields[177]->rawvalue ?> ;
            var lon = <?= $this->item->jcfields[178]->rawvalue ?> ;
            var mbAttr =
                'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
            var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<?= $mapbox_token ?>';
            var satellite = L.tileLayer(mbUrl, {id: 'tenerifeon/cl3rgf7dn000s14nyiiyuw8qm', tileSize: 512, zoomOffset: -1, attribution: mbAttr});

            var tenerifeon = L.tileLayer(mbUrl, {
                id: 'tenerifeon/cl2pyxrtp001b15lfuq3x23z5',
                tileSize: 512,
                zoomOffset: -1,
                attribution: mbAttr
            });
            var incidences = L.layerGroup();
            
            var map = L.map('map-equipamiento', {
                center: [lat, lon],
                zoom: 14,
                minZoom: 14,
                scrollWheelZoom: false,
                detectRetina: true,
                layers: [tenerifeon,incidences]
            });

            var itemTypeEquipment = <?=  $typeEquipment;?>;
            var popupTitle = '<b><?= $this->item->title."</b>, ".$this->item->category_title?>';
            if(itemTypeEquipment == 3){
                var marker = L.marker([lat, lon], {icon: equipmentsIcons.acampada}).addTo(map)
                .bindPopup().openPopup();
            }
            else if(itemTypeEquipment == 4){
                var marker = L.marker([lat, lon], {icon: equipmentsIcons.recreativo}).addTo(map)
                .bindPopup(popupTitle).openPopup();
            }
            else if(itemTypeEquipment == 5){
                var marker = L.marker([lat, lon], {icon: equipmentsIcons.campamento}).addTo(map)
                .bindPopup(popupTitle).openPopup();
            }
            else if(itemTypeEquipment == 6){
                var marker = L.marker([lat, lon], {icon: equipmentsIcons.visitantes}).addTo(map)
                .bindPopup(popupTitle).openPopup();
            } 

            map.setMaxBounds([
                [28.748396571187406, -15.671997070312502],
                [27.831789750079267, -17.586364746093754]
            ]);
            var baseMaps = {
                'Tenerifeon': tenerifeon,
                '<?= Text::_("TPL_GESPLAN_SATELLITE");?>': satellite
            };
            var groupedOverlays = {
            "<strong>Incidencias</strong>": {
                "<?= Text::_("TPL_GESPLAN_INCIDENCES")?>": incidences
            }
        };

        var layerControl = L.control.groupedLayers(baseMaps, groupedOverlays,{collapsed:false}).addTo(map);

            itineraries.forEach(function(route){
                var color = "";
                if(route[1] == 1){
                    color = 'red'
                }
                else if(route[1] == 2){
                    color = 'orange';
                }
                else if(route[1] == 3){
                    color = 'blue';
                }
                L.geoJSON(route, {color: color, onEachFeature: onEachFeature}).bindPopup(route[2]).addTo(map);
            })
            intrestingPoints.forEach(function(point){
                let lat_lng = [point.lat, point.lon];             
                    marker = new L.marker(lat_lng, {icon: pointsIcons.general}).addTo(map).bindPopup(point.title);
            })
            function onEachFeature(feature, layer) {
                layer.on('mouseover', function () {

                    this.setStyle({
                            //color: '#0d6efd',
                            weight:9
                        })
                });
                layer.on('mouseout', function () {

                    this.setStyle({
                            //color: '#ff0000',
                            weight: 3
                        })
                });

            }
            if (typeof incidence !== 'undefined'){
                let lat_lng = [incidence[0].lat, incidence[0].lon];
                L.marker(lat_lng, {icon: incidencesIcons.routeWarning}).addTo(incidences).bindPopup(incidence[0].name); 
            }
            //debugger;

        }

       

    </script>
   
    <section class="tab-maps">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="responsive-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#categoryMap1"
                                    type="button" role="tab" aria-controls="home"
                                    aria-selected="true"><?= Text::_('TPL_GESPLAN_ITINERARIES_CONNECTION')?></button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#categoryMap2"
                                    type="button" role="tab" aria-controls="home"
                                    aria-selected="false"><?= Text::_('TPL_GESPLAN_ACCESS')?> </button>
                            </li>
                            <!-- <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#categoryMap3"
                                    type="button" role="tab" aria-controls="home" aria-selected="false"><?php //echo Text::_('TPL_GESPLAN_TOWNS')?>  </button>                             </button>
                            </li> -->
                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#categoryMap4"
                                    type="button" role="tab" aria-controls="home"
                                    aria-selected="false"><?= Text::_('TPL_GESPLAN_RULES')?></button>
                            </li>
                        </ul>

                        <div id="content" class="tab-content" role="tablist">
                            <div id="categoryMap1" class="card tab-pane fade show active flex-column" role="tabpanel"
                                aria-labelledby="categoryMap1">
                                <h2><?= Text::_('TPL_GESPLAN_ITINERARIES_CONNECTION')?></h2>
                                <div class="row">
                                    <div class="col-md-8">
                                        <?php if(!empty($this->item->jcfields[68]->rawvalue)) ://Conexiones con otros campos ?>
                                            <?php       $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                                                        $appParams = JFactory::getApplication()->getParams();  
                                                        $model->setState('params', $appParams);
                                                        $itinerariesMap = [];
                                            ?>
                                        <ul class="conexiones">
                                            <?php $idRutas = explode(",",$this->item->jcfields[68]->rawvalue); ?>
                                            <?php foreach($idRutas as $idRuta) :?>
                                            <?php 
                                                        //Get associations for each article
                                                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $idRuta );
                                                        $articleLang = Factory::getLanguage()->getTag();
                                                        if(count($associations) > 0) { //If has associations, get id to show in current language
                                                            $idRuta = intval(explode(":", $associations[$articleLang]->id)[0]);
                                                        }
                                                        else{
                                                            $idRuta = $idRuta; //If not association, spanish article id
                                                        }
                                                        ?>
                                                        <?php  
                
                                                            //Get the article model
                                                            $model->setState('filter.article_id', $idRuta); //Ayuda article id
                                                
                                                            $helpArticle =   $model->getItems();
                                                            $item = $helpArticle[0];

                                                            //Get route type, print route by color
                                                            $db = Factory::getDbo(); 
                                                            $query = $db
                                                            ->getQuery(true)
                                                            ->select('value')
                                                            ->from($db->quoteName('#__fields_values'))
                                                            ->where($db->quoteName('field_id') . " = " . $db->quote(139))
                                                            ->where($db->quoteName('item_id') . " = " . $db->quote($item->catid)); 
                                                        
                                                            $db->setQuery($query);
                                                            $itineraryType = $db->loadResult();

                                                            //Fill article custom fields
                                                            $fields = FieldsHelper::getFields('com_content.article', $item, true);
                                                            // Adding the fields to the object
                                                            $item->jcfields = array();
                                                            foreach ($fields as $key => $field)
                                                            {
                                                                $item->jcfields[$field->id] = $field;
                                                                if($field->id == 145){
                                                                    array_push($itinerariesMap,["route"=>$field->rawvalue,"type"=>$itineraryType,"title"=>$item->title]);
                                                                }
                                                            }

                                                    ?>
                                                                <li><div class="col-2 col-sm-1 connection-type">
                                                                        <?php     if($item->catid == 61 || $item->catid == 123 || $item->catid == 124 ): ?>
                                                                            <i class="fa fa-walking"></i>
                                                                            <!-- <?= HTMLHelper::_('image','icon-route.svg', Text::_("TPL_GESPLAN_ROUTE_ICON"), null, true, 0) ?> -->
                                                                        <?php elseif($item->catid == 62 || $item->catid == 131 || $item->catid == 132 ): ?>
                                                                            <i class="fa fa-bicycle"></i> / <i class="fa fa-horse"></i>
                                                                        <?php elseif($item->catid == 63 || $item->catid == 133 || $item->catid == 134 ): ?>
                                                                            <i class="fa fa-truck-pickup"></i>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <div class="col-10 col-sm-11">
                                                                        <div class="link-routes">

                                                                            <p class="conex"><?= $item->title ?>
                                                                            </p>
                                                                            <?php 
                                                                                    $link = (RouteHelper::getArticleRoute($item->id, $item->catid, $item->language ));
                                                                                    $link = Route::_($link);
                                                                            ?>
                                                                            <a href="<?= $link?>"><?= Text::_('TPL_GESPLAN_VIEW_ROUTE')?></a>
                                                                        </div>
                                                                    </div>
                                                                </li>

                                            <?php endforeach;?>
                                            <?php foreach($itinerariesMap as $route): ?>

                                                <script>
                                                    itineraries.push([<?= $route["route"].",".$route["type"].",'".$route["title"]."'"?>]);
                                                </script>
                                                    
                                            <?php endforeach; ?>
                                        </ul>
                                        <?php else : ?>
                                        <p><?= Text::_('TPL_GESPLAN_NO_DATA_AVAILABLE')?></p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="link-plan">
                                            <p class="head-plan"><?= Text::_("TPL_GESPLAN_KNOW_OUR_PLANNER")?></p>
                                            <p><?= Text::_("TPL_GESPLAN_FIND_OUT_OUR_ROUTE_PLANNER_PLAN")?></p>
                                            <?php 

                                                $planificadorItemId = 1156;
                                                $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $planificadorItemId);
                                                $articleLang = Factory::getLanguage()->getTag();
                                                if(count($associations) > 0) {
                                                    $articleLang = Factory::getLanguage()->getTag();
                                                    $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                                                    $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                                                }
                                                else{
                                                    $articleLang = 'es-ES';
                                                    $idLang = $planificadorItemId;
                                                    $catidLang = 35; //Guía de actividades categoty id
                                                }
                                            ?>
                                            <a href="<?php  echo Route::_(RouteHelper::getArticleRoute($idLang, $catidLang, $articleLang)); ?>"><?= Text::_("TPL_GESPLAN_GOTO_PLANNER")?></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div id="categoryMap2" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="categoryMap2">
                                <h2><?= Text::_('TPL_GESPLAN_ACCESS')?></h2>
                                <?php if(!empty($this->item->jcfields[227]->rawvalue)) : //Acceso transporte privado?>
                                <div class="acceso-ruta">
                                    <i class="fa fa-car"></i>
                                    <?= $this->item->jcfields[227]->rawvalue //Acceso transporte privado?>
                                </div>
                                <?php else : ?>
                                <p><?= Text::_('TPL_GESPLAN_NO_DATA_AVAILABLE')?></p>
                                <?php endif; ?>

                                <?php if(!empty($this->item->jcfields[226]->rawvalue)) : //Acceso transporte público?>
                                <div class="acceso-ruta">
                                    <i class="fa fa-bus"></i>
                                    <?= $this->item->jcfields[226]->rawvalue //Acceso transporte público?>
                                </div>
                                <?php endif; ?>



                            </div>

                            <div id="categoryMap4" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="categoryMap4">
                                <h2><?= Text::_("TPL_GESPLAN_RULES")?></h2>
                                <?php if(!empty($this->item->jcfields[200]->rawvalue)) :?>
                                <?php echo ($this->item->jcfields[200]->rawvalue); ?>

                                <?php else : ?>
                                <p><?= Text::_('TPL_GESPLAN_NO_DATA_AVAILABLE')?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    //Prepare model to get articles to tpint (espacios naturales, putos de interés)

        $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
        $appParams = JFactory::getApplication()->getParams();  
        $model->setState('params', $appParams);
    ?>
    <?php  
    //Get all Espacio natural by that contains cfield[91] espacio-natural-protegido
    if(!empty($this->item->jcfields[91]->rawvalue) && !is_null(($this->item->jcfields[91]->rawvalue))): 
    ?>
    <section class="enp">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2><?= Text::_("TPL_GESPLAN_NATURAL_AREAS");?></h2>
                    <!-- <p>Es parte de 2 espacios naturales y esta relacionado con 1 más:</p> -->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="slick-enp">
                        <?php   
                                $espacios_naturales = explode(",",$this->item->jcfields[91]->rawvalue);
                                foreach($espacios_naturales as $key => $enp){
                                    if(!is_null($enp) && (!empty($enp))){
                                        $articleId = $enp;
                                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
                                        $articleLang = Factory::getLanguage()->getTag();
                                        if(count($associations) > 0) {
                                            $articleId = intval(explode(":", $associations[$articleLang]->id)[0]);
                                        }
                                        $model->setState('filter.article_id', $articleId); //Ayuda article id
                            
                                        $helpArticle =   $model->getItems();
                                        $item = $helpArticle[0];
                                        $fields = FieldsHelper::getFields('com_content.article', $item, true);
                                        // Adding the fields to the object
                                        $item->jcfields = array();
                                        foreach ($fields as $key => $field)
                                        {
                                            $item->jcfields[$field->id] = $field;
                                        }
                                        ?>

                        <div>
                            <figure>
                                <div class="caption">
                                    <p class="transita"><?= Text::_("TPL_GESPLAN_TRANSITS")?></p>
                                    <h3><?= $item->title?> </h3>
                                    <!-- <p class="enp">Espacio natural protegido</p>
                                    <p class="reserva"><?= $item->jcfields[186]->rawvalue?></p> -->
                                    <p><?= preg_replace('/\s+?(\S+)?$/', '', substr(strip_tags($item->introtext), 0, 200));?> ...</p>
                                    <a
                                        href="<?php echo Route::_(RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));?>"><?= Text::_("TPL_GESPLAN_DISCOVER_NATURAL_AREA");?></a>
                                </div>
                                <?php  $images = json_decode($item->images); ?>
                                <?php if(!empty($images->image_intro)) : ?>
                                <img src="<?php echo $images->image_intro ; ?>"
                                    alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy">
                                <?php else: ?>
                                <img src="templates/gesplan/images/neutra-900.jpg" alt="Tenerife ON" loading="lazy">
                                <?php endif; ?>
                            </figure>
                        </div>

                        <?php
                                        
                                    }
                                }

                    ?>

                    </div>
                </div>
            </div>
        </div>

    </section>
    <?php endif; ?>
   
   <?php 
        //If has municipio meteo
    
        if(!empty($this->item->jcfields[256]->rawvalue)) {

        $path = dirname(__FILE__) . "/partials/";
        require_once($path.'meteorologia.php');

         } 
    ?>

    <script>
    var intrestingPoints = [];
   </script>
    <?php   if(!empty($this->item->jcfields[100]->rawvalue) && !is_null(($this->item->jcfields[100]->rawvalue))): ?>
        <?php $all_intresting_points = explode(",",$this->item->jcfields[100]->rawvalue); 
                $cleaned_intesting_points = [];
                //If field contains empty fields due to a updating isseu, clean them
                foreach($all_intresting_points as $point){
                    if($point != ""){
                        $cleaned_intesting_points[] = $point;
                    }
                }
            //Get all puntos de interes that are active
            $db = Factory::getDbo();
            $query = "select sc.id from sooeg_content sc left join sooeg_categories cc 
                on cc.id = sc.catid where cc.parent_id = '66' and sc.state = '1'
                and sc.id in (".implode(",", $cleaned_intesting_points).")";

            $db->setQuery($query);
            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
            $puntos_interes = $db->loadColumn();

        ?>
        <?php if(sizeOf($puntos_interes) > 0) : ?>
        <section class="lugares-interes">
        <?php
                $path = dirname(__FILE__) . "/partials/";
                require_once($path.'punto-de-interes.php');
        ?>
 

                    <?php foreach($points_for_modal as $key => $point) :?>
            
                        <script>
                                var lat = '<?= $point['lat'];?>';
                                var lon = '<?= $point['lon'];?>';
                                var title = '<?= $point['title']?>';

                                intrestingPoints.push({
                                    title: title,
                                    lat : lat,
                                    lon : lon
                                });

                        </script>
                    <?php endforeach; ?>

    <?php endif; ?>
<?php endif; ?>
   


    <?php
            $path = dirname(__FILE__) . "/partials/";
            require_once($path.'banner-planificador.php');
            require_once($path.'modal-favorito.php');
        ?>



</div>


<?php 
	if (!empty($this->item->pagination) && !$this->item->paginationposition && $this->item->paginationrelative)
	{
		echo $this->item->pagination;
	}
	?>

<?php if ($canEdit) : ?>
<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
<?php endif; ?>

<?php // Content is generated by content plugin event "onContentAfterTitle" ?>
<?php //echo $this->item->event->afterDisplayTitle; ?>





<?php // Content is generated by content plugin event "onContentBeforeDisplay" ?>
<?php //echo $this->item->event->beforeDisplayContent; ?>


<?php if ($params->get('access-view')) : ?>
<?php //echo LayoutHelper::render('joomla.content.full_image', $this->item); ?>
<?php
	if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && !$this->item->paginationrelative) :
		echo $this->item->pagination;
	endif;
	?>
<?php if (isset ($this->item->toc)) :
		echo $this->item->toc;
	endif; ?>


<?php if ($info == 1 || $info == 2) : ?>
<?php if ($useDefList) : ?>
<?php echo LayoutHelper::render('joomla.content.info_block', array('item' => $this->item, 'params' => $params, 'position' => 'below')); ?>
<?php endif; ?>

<?php endif; ?>

<?php
	//if (!empty($this->item->pagination) && $this->item->paginationposition && !$this->item->paginationrelative) :
		//echo $this->item->pagination;
	?>
<?php //endif; ?>
<?php if ((int) $params->get('urls_position', 0) === 1) : ?>
<?php echo $this->loadTemplate('links'); ?>
<?php endif; ?>
<?php // Optional teaser intro text for guests ?>
<?php elseif ($params->get('show_noauth') == true && $user->get('guest')) : ?>
<?php echo LayoutHelper::render('joomla.content.intro_image', $this->item); ?>
<?php echo HTMLHelper::_('content.prepare', $this->item->introtext); ?>
<?php // Optional link to let them register to see the whole article. ?>
<?php if ($params->get('show_readmore') && $this->item->fulltext != null) : ?>
<?php $menu = Factory::getApplication()->getMenu(); ?>
<?php $active = $menu->getActive(); ?>
<?php $itemId = $active->id; ?>
<?php $link = new Uri(Route::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false)); ?>
<?php $link->setVar('return', base64_encode(RouteHelper::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language))); ?>
<?php echo LayoutHelper::render('joomla.content.readmore', array('item' => $this->item, 'params' => $params, 'link' => $link)); ?>
<?php endif; ?>
<?php endif; ?>
<?php
	//if (!empty($this->item->pagination) && $this->item->paginationposition && $this->item->paginationrelative) :
		//echo $this->item->pagination;
	?>
<?php // endif; ?>
<?php // Content is generated by content plugin event "onContentAfterDisplay" ?>
<?php //echo $this->item->event->afterDisplayContent; ?>