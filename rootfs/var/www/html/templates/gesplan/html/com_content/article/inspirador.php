<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
$category_title   = $this->escape($this->item->category_title);

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
<section class="head-inspirador-article">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1><?= $this->item->title?></h1>
                    <h2><?= $this->item->jcfields[274]->rawvalue;?></h2>    
                    <?= $this->item->text; ?>
                </div>
            </div>
        </div>
</section>
<section class="body-inspirador-article">
    <div class="container">
            <div class="row">  
                
            <?php if(isset($this->item->jcfields[273]->subform_rows)) : ?>

            <?php   $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                    $appParams = Factory::getApplication()->getParams();  
                    $model->setState('params', $appParams); ?>

                <?php $itineraries = ($this->item->jcfields[273]->subform_rows) ?>
                <?php if(isset($itineraries)) :?>
                    <?php foreach($itineraries as $key => $route) : ?>
                        <?php  $routeId = $route["itinerario-form"]->rawvalue; ?>
                        <?php 
                            $articleId = $routeId; //Id ruta in spanish
                            $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
                            $articleLang = Factory::getLanguage()->getTag();
                            if(count($associations) > 0) {
                                //$articleLang = Factory::getLanguage()->getTag();
                                $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                                //$catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                            }
                            else{
                                //$articleLang = 'es-ES';
                                $idLang = $articleId;
                                //$catidLang = 95; //Inspirador categoty id
                            }
                            $model->setState('filter.article_id', $idLang); 
                                                                
                            $helpArticle =   $model->getItems();
                            $item = $helpArticle[0];
                            $fields = FieldsHelper::getFields('com_content.article', $item, true);
                            // Adding the fields to the object
                            $item->jcfields = array();

                            foreach ($fields as $key => $field){
                                $item->jcfields[$field->id] = $field;
                            }
                        ?>
                        <div class="col-md-6 col-lg-6 col-xl-4">
                            <div class="card-default position-relative">
                                <a class="hyper-link" href="<?= Route::_(RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));?>">
                                </a>
                                <figure>
                                    <div class="gradient"></div>
                                    <?php
                                        $images = json_decode($item->images); 
                                    ?>
                                    <?php if(empty($images->image_intro)) : ?>
                                        <?php dd($images->image_intro)?>
                                        <?= HTMLHelper::_('image','neutra-300.jpg', "Tenerife ON", null, true, 0) ?>
                                    <?php else: ?>
                                        <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" class="img-fluid" loading="lazy">
                                    <?php endif; ?>                                    </figure>
                                <div class="caption">
                                    <div class="head-caption">
                                        <span><?php $item->jcfields[57]->rawvalue //Situacion?></span>
                                        <p><?= $item->title?></p>
                                    </div>
                                    <div class="actions-enp">
                                        <div class="content_rating head-ficha">

                                    <?php   $valoracion = null;
                                            $db = Factory::getDbo();
                                            $query = $db
                                            ->getQuery(true)
                                            ->select(array('SUM(rating) as rating, COUNT(id) as count'))
                                            ->from($db->quoteName('#__sg_valoraciones'))
                                            ->where($db->quoteName('item_id') . " = " . $db->quote($item->id));
                                            // Reset the query using our newly populated query object.
                                            $db->setQuery($query);
                                            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                            if($hasRating = $db->loadObjectList()){
                                                if(!is_null($hasRating[0]->rating)){
                                                    $rating = $hasRating[0]->rating;
                                                    $count = $hasRating[0]->count;
                                                    $item->valoracion = (float)($rating/$count);
                                                    
                                                }
                                                else{
                                                    $item->valoracion = 0;
                                                } 
                                            }
                                    ?>
                                    <?php 
                                                
                                                for ($i = 0; $i < $item->valoracion; $i++) {
                                                    echo '<li class="vote-star"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                                                                        </li>';
                                                };
                                                for ($i = 0; $i < (5 - $item->valoracion); $i++) {
                                                    echo '<li class="vote-star-empty"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                                                                        </li>';
                                                }
                                    ?>
                
                                        </div>
                                    </div>
                                    <div class="link-card">
                                        <p><?= Text::_("TPL_GESPLAN_MORE_INFO")?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    <?php endforeach; ?>
                <?php endif;?>
            <?php endif;?>
        </div>
    </div>               
</section>

   

  
    <?php
    $path = dirname(__FILE__) . "/partials/";
   // require_once($path.'prefooter.php');
    require_once($path.'banner-planificador.php');
    require_once($path.'ayuda.php');
    //require_once($path.'colaboradores.php');
?>

