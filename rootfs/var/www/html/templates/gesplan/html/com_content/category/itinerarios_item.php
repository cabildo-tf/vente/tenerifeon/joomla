<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Language\Text;


// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);

?>


    <?php if ($canEdit) : ?>
		<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
	<?php endif; ?>
    <div class="item-itinerario">
        <div class="title-item">
            <div class="dropdown">
                <button type="button" class="badge" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-ellipsis-v"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="#"><?= Text::_('TPL_GESPLAN_SAVE_ROUTE');?></a></li>
                    <li><a class="dropdown-item" href="#"><?= Text::_('TPL_GESPLAN_VIEW_FULL_DETAIL');?></a></li>
                    <li><a class="dropdown-item" href="#"><?= Text::_("TPL_GESPLAN_SHARE_IT");?></a></li>
                    <li><a class="dropdown-item" href="#"><?= Text::_("TPL_GESPLAN_INCIDENCE_NOTIFICATE");?></a></li>
                </ul>
                </div>
                <p><span class="badge"><?= $this->key + 1;?></span>
                
                <?php 
                        $link = (RouteHelper::getArticleRoute($this->item->id, $this->item->catid, $this->item->language));
                        $link = Route::_($link);
        
                ?>
                <a href="<?php echo $link;?>" title="<?php echo $this->item->title;?>"  style="color:#4a503b"> <?php echo $this->item->title;?></a>         
                </p>
            
        </div>
        <div class="row">
            <div class="col-3">
                <figure class="sizeingImg">
                <?php
                        $images = json_decode($this->item->images); 
                        if(!is_null($images->image_intro)) :
                ?>
                        <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy">
                <?php else: ?>

                <?= HTMLHelper::_('image','itinerario.jpg', Text::_("Equipamiento"), null, true, 0) ?>

                <?php endif; ?>
                </figure>
            </div>
            <div class="col-9">
                <div class="content-values">
                    <div class="item-value">
                        <i class="fa fa-long-arrow-alt-right"></i>
                        <p class="value"><?php echo(number_format($this->item->jcfields[60]->value,2)) //longitud?>km</p>
                    </div>
                    <div class="item-value">
                        <i class="far fa-clock"></i>
                        <p class="value">1h 30m</p>
                    </div>
                    
                    <div class="item-value">
                        <i class="fas fa-level-up-alt"></i>
                        <p class="value"><?php echo(number_format($this->item->jcfields[63]->value,2)) //desnivel ascenso?>m</p>
                    </div>
                    <div class="item-value">
                        <i class="fas fa-level-down-alt"></i>
                        <p class="value"><?php echo(number_format($this->item->jcfields[64]->value,2)); //desnivel descenso?>m</p>
                    </div>
                    <div class="item-value alert">
                        <?= HTMLHelper::_('image', 'icon-warning.svg', Text::_("TPL_GESPLAN_INCIDENCE"), 'class="icon-warning"', true, 0) ?>
                        <p class="value"><?= Text::_('TPL_GESPLAN_INCIDENCE')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
