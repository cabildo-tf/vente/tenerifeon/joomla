<?php
use Joomla\CMS\HTML\HTMLHelper;
?>
<section class="insta">
    <div class="container my-3">
        <div class="row">
            <div class="col-md-12">
                <h2><?= HTMLHelper::_('image', 'instagram-img-section.svg', 'Icono de Instagram', null, true, 0) ?>
                    /ventetenerife</h2>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-2">
        <?= HTMLHelper::_('image', 'fake-img.png', 'alt', null, true, 0) ?>
        </div>
        <div class="col-md-2">
        <?= HTMLHelper::_('image', 'fake-img.png', 'alt', null, true, 0) ?>
        </div>
        <div class="col-md-2">
        <?= HTMLHelper::_('image', 'fake-img.png', 'alt', null, true, 0) ?>
        </div>
        <div class="col-md-2">
        <?= HTMLHelper::_('image', 'fake-img.png', 'alt', null, true, 0) ?>
        </div>
        <div class="col-md-2">
        <?= HTMLHelper::_('image', 'fake-img.png', 'alt', null, true, 0) ?>
        </div>
        <div class="col-md-2">
        <?= HTMLHelper::_('image', 'fake-img.png', 'alt', null, true, 0) ?>
        </div>
    </div>
</section>