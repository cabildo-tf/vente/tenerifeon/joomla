<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;

?>
<div class="com-content-article item-page<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="https://schema.org/Article">
	<meta itemprop="inLanguage" content="<?php echo ($this->item->language === '*') ? Factory::getApplication()->get('language') : $this->item->language; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1> <?php echo $this->escape($this->params->get('page_heading')); ?> </h1>
	</div>
	<?php endif;
	if (!empty($this->item->pagination) && !$this->item->paginationposition && $this->item->paginationrelative)
	{
		echo $this->item->pagination;
	}
	?>
	<!-- Template -->

	<article class="news__view">
	<?php if ($canEdit) : ?>
		<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
	<?php endif; ?>
		<div class="header__article">
			<div class="container">
				<div class="row">
					<div class="col-12 news__view-content">
						<div class="news__view-content-info">
							<header>
								<p class="news__view-category"><a href="/index.php/<?= substr($this->item->parent_language,0 ,2)?>/<?= $this->item->parent_route;?>/<?= $this->item->category_alias;?>" 
								title="<?php echo JText::_( 'Ir a la categoría Nombre de la Categoría' );?>"><?php echo $this->item->category_title; ?></a></p>

								<h1><?php echo $this->escape($this->item->title); ?></h1>
								<h2><?php echo($this->item->jcfields[14]->value)?></h2>
							</header>
						</div>
					</div>
			
				</div>
			</div>
		</div>

		<div class="fullHeadArticle">
			<?php
				$images         = json_decode($this->item->images); 
			?>
			<img src="<?= $images->image_fulltext;?>" alt="<?= $images->image_fulltext_alt;?>" class="news__view-image" loading="lazy">
		</div>
			
		<div class="container">
                <div class="row">
                    <div class="col-12 news__view-content"> 


						<div class="news__view-content-text">
							
							<?php echo $this->item->text; ?>
							<div class="share-social">
                                <p class="head"><?php echo JText::_( 'TPL_GESPLAN_SHARE' );?></p>
								<div class="addthis_inline_share_toolbox"><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-621f488e60708602"></script></div>                        
                            </div>
							
						</div>
						
				</div>
			</div>
		</div>
		<!-- Gallery section -->

		<?php
			$path = dirname(__FILE__) . "/partials/";
			require_once($path.'gallery.php');
		?>

		<!-- End Gallery -->
			
		<?php $tags = $this->item->tags->itemTags;?>

		<?php if($tags) : ?>

		<div class="container">
			<div class="row">
				<div class="col offset-leg-2">
						<footer>
							<div class="row">
								<div class="col-sm-8">
									
									<div class="news__view-content-tags">
									<?php $this->item->tagLayout = new FileLayout('joomla.content.tags'); ?>
										<?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
									</div>
								</div>
							</div>
						</footer>
				</div>
			</div>
		</div>
		

		<?php endif; ?>

	</article>

	<!-- /own template -->
	<?php 
						//Use JModelLegacy to get all articles within $categoryID
						//var_dump($this->item);
						$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
						$appParams = JFactory::getApplication()->getParams();
						$model->setState('params', $appParams);
						$model->setState('filter.category_id', $this->item->catid); //change that to your Category ID
						$model->setState('list.ordering', Factory::getDbo()->getQuery(true)->rand());
						$model->setState('list.limit', 3);
						$model->setState('filter.article_id', $this->item->id);
						$model->setState('filter.article_id.include', false);

						$articlesCategory =   $model->getItems();
					?>
					<?php
					if(sizeof($articlesCategory) > 0){
						?>
						<section class="general__information__related">
									<div class="container">
										<div class="row">
											<div class="col offset-lg-2">
												<h3><?php echo JText::_( 'Otros artículos que te pueden interesar' );?></h3>
											</div>
										</div>
										<div class="row">
											<div class="col">
										<?php

						foreach ($articlesCategory as &$item) {
							$this->item = & $item;
							echo $this->loadTemplate('item');
						}
					?>
					</div>
				</div>
			</div>
	</section>
	<?php
					}
					?>

	<?php if ((int) $params->get('urls_position', 0) === 0) : ?>
	<?php echo $this->loadTemplate('links'); ?>
	<?php endif; ?>
	<?php if ($params->get('access-view')) : ?>
	<?php //echo LayoutHelper::render('joomla.content.full_image', $this->item); ?>
	<?php
	if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && !$this->item->paginationrelative) :
		echo $this->item->pagination;
	endif;
	?>
	<?php if (isset ($this->item->toc)) :
		echo $this->item->toc;
	endif; ?>

	<?php
	if (!empty($this->item->pagination) && $this->item->paginationposition && !$this->item->paginationrelative) :
		echo $this->item->pagination;
	?>
	<?php endif; ?>
	<?php if ((int) $params->get('urls_position', 0) === 1) : ?>
	<?php echo $this->loadTemplate('links'); ?>
	<?php endif; ?>
	<?php // Optional teaser intro text for guests ?>
	<?php elseif ($params->get('show_noauth') == true && $user->get('guest')) : ?>
	<?php echo LayoutHelper::render('joomla.content.intro_image', $this->item); ?>
	<?php echo HTMLHelper::_('content.prepare', $this->item->introtext); ?>
	<?php // Optional link to let them register to see the whole article. ?>
	<?php if ($params->get('show_readmore') && $this->item->fulltext != null) : ?>
	<?php $menu = Factory::getApplication()->getMenu(); ?>
	<?php $active = $menu->getActive(); ?>
	<?php $itemId = $active->id; ?>
	<?php $link = new Uri(Route::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false)); ?>
	<?php //$link->setVar('return', base64_encode(RouteHelper::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language))); ?>
	<?php echo LayoutHelper::render('joomla.content.readmore', array('item' => $this->item, 'params' => $params, 'link' => $link)); ?>
	<?php endif; ?>
	<?php endif; ?>
	<?php
	if (!empty($this->item->pagination) && $this->item->paginationposition && $this->item->paginationrelative) :
		echo $this->item->pagination;
	?>
	<?php endif; ?>
	<?php // Content is generated by content plugin event "onContentAfterDisplay" ?>
	<?php echo $this->item->event->afterDisplayContent; ?>
</div>

<?php
    $path = dirname(__FILE__) . "/partials/";
    require_once($path.'banner-planificador.php');
?>
