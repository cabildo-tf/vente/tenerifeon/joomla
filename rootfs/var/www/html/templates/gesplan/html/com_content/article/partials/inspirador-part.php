        <?php

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Utilities\ArrayHelper;

            $catID = 95; //Inspirador category ID  
            $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($catID));
            $catLang =   Factory::getLanguage()->getTag() ;
            $catId = $associations[ $catLang]; 
            $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
            $appParams = Factory::getApplication()->getParams(); 
            $model->setState('params', $appParams);
            $model->setState('filter.category_id', $catId); //change that to your Category ID
            $model->setState('list.ordering', 'created');
            $model->setState('list.limit', 4);
            $model->setState('list.direction', 'DESC');
            $model->setState('filter.published', '1');
            $articlesCategory =   $model->getItems();

           
            $inicio_page_id = 32;  
            $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $inicio_page_id );
            $articleLang = Factory::getLanguage()->getTag();
            $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
            $catID =  intval(explode(":", $associations[$articleLang]->catid)[0]);
            $model->setState('filter.article_id', $idLang); //Inicio page id
            $model->setState('filter.category_id', $catID); //change that to your Category ID                                   
            $helpArticle =   $model->getItems();
            $item = $helpArticle[0];
            $fields = FieldsHelper::getFields('com_content.article', $item, true);
            // Adding the fields to the object
            $item->jcfields = array();

            foreach ($fields as $key => $field){
                $item->jcfields[$field->id] = $field;
            }
        ?>
        <section id="inspirador_home">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <div class="section-title">
                            <h2>
                                <?= $item->jcfields[17]->rawvalue;?>
                                <strong><span style="text-transform: uppercase;"><?= Text::_("TPL_GESPLAN_INSPIRATOR");?></span></strong>
                            </h2>
                            <p><?= $item->jcfields[18]->rawvalue;?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                <?php   
                   
                ?>
                <?php   if($articlesCategory) : //If articles in noticias ?>

                    <?php   foreach ($articlesCategory as $item) : //start the loop

                                $fields = FieldsHelper::getFields('com_content.article', $item, true);
                                // Adding the fields to the object
                                $item->jcfields = array();

                                foreach ($fields as $key => $field)
                                {
                                    $item->jcfields[$field->id] = $field;
                                }                       
                    ?>   
                        <div class="col-md-6 col-lg-6 col-xl-3 my-4">
                            <div class="card-inspirador">
                                
                                    <figure>
                                    <?php  $images = json_decode($item->images); ?>
                                            <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy" >
                                    </figure>
                                
                                <div class="body-inspirator">
                                    <h3><?= $item->title ?></h3>
                                    <p><?= $item->jcfields[274]->rawvalue //subtitulo-inspirador?></p>
                                    <div class="link-card">
                                        <a href="<?php echo Route::_(RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));?>"><?= Text::_("TPL_GESPLAN_MORE_INFO");?></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>

                <?php endif; ?>
                </div>
                <!-- <div class="row">
                    <div class="col-12">
                        <a class="more-options" href="<?php  echo Route::_("index.php?option=com_content&view=article&id=1157"); ?>">Ver todas las opciones</a>
                    </div>
                </div>  -->
            </div>
        </section>
              