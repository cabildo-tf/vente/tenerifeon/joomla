<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Utilities\ArrayHelper;

$app = Factory::getApplication();

// $this->category->text = $this->category->description;
// $app->triggerEvent('onContentPrepare', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
// $this->category->description = $this->category->text;

// $results = $app->triggerEvent('onContentAfterTitle', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
// $afterDisplayTitle = trim(implode("\n", $results));

// $results = $app->triggerEvent('onContentBeforeDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
// $beforeDisplayContent = trim(implode("\n", $results));

// $results = $app->triggerEvent('onContentAfterDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
// $afterDisplayContent = trim(implode("\n", $results));

// $htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';

?>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="/templates/gesplan/js/gesplan/pagination.js?3e2be082abda46f8aa924a4a3c5ca486" data-asset-name="template.gesplan.pagination"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<!-- Si hay articulos en la categoría/subcategoria pinta listado. 
	En la categoria padre(information) no hay articulos, entonces solamente pintará cuando entramos en una subcategoría -->
		
		<section class="filtros-itinerarios  view_rezizer">
        <div class="map-mobil">
            <?php //echo $this->loadTemplate('offcanvas');?>
            <figure>
                <button class="open-rout-btn">1</button>
                <?= HTMLHelper::_('image', 'fake-map-filtros.png', 'Logo del cabildo', null, true, 0) ?>
            </figure>

        </div>
        <div class="filtros-mobil">
            
            <form>
                <div class="cont-btn-mobile">
                    <button class="show-filters delete-filters" onclick="cleanFilter(this)"><?= Text::_("TPL_GESPLAN_DELETE") ?>
                    </button>
                    <button class="show-filters"  id="filter-button-mobile" onclick="filterAjaxContent(this)" ><?= Text::_("TPL_GESPLAN_FILTER") ?>
                    </button>
                </div>
                <ul class="general-ul">
                    
                   
                    <li>
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_TYPE_OF_EQUIPMENT");?></legend>
                            <ul>
                                <?php       

                                    $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations(39)); //Category for Itinerarios
                                    $catLang =   Factory::getLanguage()->getTag() ;
                                    $catId = $associations[ $catLang];               
                                    $categories = \Joomla\CMS\Categories\Categories::getInstance('com_content');
                                    $cat        = $categories->get($catId);
                                    $equipmentType = $cat->getChildren();
                                    
                                ?>
                                <?php foreach($equipmentType as $equipment) : ?>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  name="equipmentCategory" value="<?= $equipment->id; ?>" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            <?= $equipment->title; ?>
                                        </label>
                                      </div>
                                </li>
                                <?php endforeach; ?>
                              
                            </ul>
                        </fieldset>
                    </li>
                   
                
                </ul>
            </form>
        </div>
       <div class="cont-btn-mobile">
            <button class="show-filters">
                <?= Text::_("TPL_GESPLAN_SHOW_FILTERS")?>
            </button>
            <button class="show-map" >
                <?= Text::_("TPL_GESPLAN_SHOW_MAP")?>
            </button>
            
       </div>
        <div class="container-fluid">
            <div class="row m-0">
                <div class="col-md-12">
                    <form class="m-0">
                        <div class="filter-row">
                            <div class="item-filters">
                            <?php 

                                $categoryId = 39; //Equipamientos category original
                                $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($categoryId)); 
                                $catLang =   Factory::getLanguage()->getTag() ;
                                $catId = $associations[ $catLang];
                                //Get num published equipamientos
                                $db = Factory::getDbo();

                                $query = $db->getQuery(true);
                                $query
                                ->select(array('COUNT(catid)'))
                                ->from($db->quoteName("#__content","c"))
                                ->join('INNER', $db->quoteName('#__categories', 'b') . ' ON ' . $db->quoteName('c.catid') . ' = ' . $db->quoteName('b.id'))
                                ->where($db->quoteName('b.parent_id') . ' = ' . $db->quote($catId))
                                ->where($db->quoteName('c.state') . ' = ' . $db->quote('1'));
                                //->group($db->quoteName("c.catid"));

                                // Reset the query using our newly populated query object.
                                $db->setQuery($query);

                                // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                $results = $db->loadObjectList();
                                $numberOfEquipments = $results[0]->count;
                                $numberPages = $numberOfEquipments / 20;
                            ?>
                                <button><?= Text::_("TPL_GESPLAN_SHOW_ALL") ?> <?=  $numberOfEquipments ?> <?= Text::_("TPL_GESPLAN_EQUIPMENTS")?> </button>
                                <!-- //Text::_("TPL_GESPLAN_SHOW_ALL")." ".count($this->items)." ".Text::_("TPL_GESPLAN_ROUTES");-->
                            </div>
                            <script>
                                var  equipmentsCategory = [];
                                equipmentsCategory[0] = 39;
    
                                $(document).ready(function () {
                                    $('.submits-container button').bind('click', function(event) {
                                        event.preventDefault()
                                    });
                                })
                                function filterAjaxContent(el){
                                    var categories = [];
                                    $.each($("input[name='equipmentCategory']:checked"), function(){
                                        categories.push($(this).val());
                                    });
                                    $.each(categories,(function(index, value) {
                                        equipmentsCategory[index] = value;
                                    }));

                                    pagination.goToPage(1);
                                }
                                function cleanFilter(el){
                                    $.each($("input[name='equipmentCategory']:checked"), function(){
                                         $(this).prop("checked", false);
                                    });
                                    $('#dropdownMenuButton1').removeClass('active');
                                    equipmentsCategory = [];
                                    equipmentsCategory[0] = 39;
                                    pagination.goToPage(1);
                                }
                               
                            </script>
                            <div class="item-filters">
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_TYPE_OF_EQUIPMENT")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <?php foreach($equipmentType as $equipment) : ?>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input category" type="checkbox" name="equipmentCategory" value="<?= $equipment->id; ?>" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    <?= $equipment->title; ?>
                                                </label>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                        <div class="submits-container">
                                            <button type="submit" class="delete" onclick="cleanFilter(this)"><?= Text::_("TPL_GESPLAN_DELETE") ?></button>
                                            <button type="submit" id="filter-button" onclick="filterAjaxContent(this)" ><?= Text::_("TPL_GESPLAN_FILTER") ?></button>
                                        </div>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class=" col-lg-5 p-0">
                    <div id="itinerarios-column" class="itinerarios-column">
					
				
                    </div>
                </div>
                <div class="col-lg-7 p-0 d-md-flex">
                
                    <div class="content-map-desktop">
                    <!-- <button id="openData"><i class="fa fa-chevron-left"></i></button> -->
					<?php echo $this->loadTemplate('offcanvas');?>

                        <!-- <a href="" title="enalce planificador del itienerario X" class="btn-link-itinerario">
						<?= HTMLHelper::_('image', 'route-icon-btn.svg', Text::_("TPL_GESPLAN_ROUTE_ICON"), null, true, 0) ?>
                            Planifica tu itinerario
                        </a> -->
                        <button id="openData" class="niceRotate"><i class="fa fa-chevron-left"></i></button>
                        <div id="map-equipamiento" style="height: 100%;"></div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php
    //Get mapbox
    $config = Factory::getConfig();
        $mapbox_token = $config->get('mapbox_token');
    ?>
<script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>

    <div class="container control-paginator">
       
        <div id="pagination"></div>
    </div>
    <?php   $user =   Factory::getUser();
            $status = $user->guest;
            $user_id = 0;
            if(!$status){
               $user_id = $user->id;
            }
    ?>
    <script src="/templates/gesplan/js/gesplan/leaflet.groupedlayercontrol.js"></script>
	<script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>
    <link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.css" />
	<link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.Default.css" />
    <script>

var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
        var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<?= $mapbox_token ?>';

        var tenerifeon = L.tileLayer(mbUrl, {id: 'tenerifeon/cl2pyxrtp001b15lfuq3x23z5', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
        var satellite = L.tileLayer(mbUrl, {id: 'tenerifeon/cl3rgf7dn000s14nyiiyuw8qm', tileSize: 512, zoomOffset: -1, attribution: mbAttr});

        var incidences = L.layerGroup();

        var  scrollWheelZoom = true;
        //disable drag and wheel zoom in mobile
        var disableDragIfMobile = function() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        if(check){
            //map.dragging.disable();
            map.scrollWheelZoom.disable();
        }  
        };
        var map = L.map('map-equipamiento', {
            center: [28.291564, -16.629130],
            zoom: 10,
            minZoom: 10,
            scrollWheelZoom: scrollWheelZoom,
            detectRetina: true,
            layers: [tenerifeon,incidences]
        });

        disableDragIfMobile();

        //not allow to navigate out of tenerife bounds
        map.setMaxBounds([
            [28.748396571187406, -15.671997070312502],
            [27.831789750079267, -17.586364746093754]
        ]);
        var baseMaps = {
            'Vista Tenerifeon': tenerifeon,
            'Vista Satélite': satellite
        };
        var groupedOverlays = {
            "<strong>Incidencias</strong>": {
                "<?= Text::_("TPL_GESPLAN_INCIDENCES")?>": incidences
            }
        };
        var markers = L.markerClusterGroup(); 
        var bounds = L.latLngBounds();
        var marker = [];
        var layerControl = L.control.groupedLayers(baseMaps, groupedOverlays,{collapsed:false}).addTo(map);
        
        var equipmentMarkers = [];
        var pagination;

    function pageClick1(pageNumber) {

        $('.itinerarios-column').animate({
        scrollTop: 0 //#DIV_ID is an example. Use the id of your destination on the page
        }, 'slow');
        getAjaxContent(pageNumber, equipmentsCategory);

    }

    function pageClickAll(pageNumber) {
        $("#page-number-all").text(pageNumber);
    }

    $(document).ready(function () {

        var itemsCount = <?= $numberOfEquipments?>;
        var itemsOnPage = 20;

        pagination = new Pagination({
            container: $("#pagination"),
            pageClickCallback: pageClick1,
            maxVisibleElements: 3
    });


    pagination.make(itemsCount, itemsOnPage);


        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

       
    });


    /* Get first equipments page */
    $(document).ready(function () {
        getAjaxContent(1, equipmentsCategory);
            //Get markers and start map
    getEquipmentsForMap();

    getEquipmentIncidences();
    
    })
    const equipmentsForMap = [];
    

    //Get content by Ajax
    async function getAjaxContent(pageNum, categories) {
        var filterCategories = categories.join();

        var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + filterCategories + "&page=" + pageNum + "&user=" + <?= $user_id?>;
        
        $("#itinerarios-column").html('<div class="loader-cont"><div class="buble"></div><i class="fa fa-spin fa-circle-notch"></i><p>Obteniendo datos</p></div>');
        await Joomla.request({
            url: baseUrl +  "&plugin=Listajaxcontent",
            method: 'GET',
            perform: true,
            onSuccess: function onSuccess(resp) {
                // Remove loader image

                var response = {};

                try {
                    response = JSON.parse(resp);
                } catch (e) {
                Joomla.renderMessages({
                    error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
                }, ".sampledata-steps");
                return;
                }
                var itemsCount = response.data[0].total;

                //$('span#totalLength').html(totalLength);
                $('#itinerarios-column').empty()
                $('#itinerarios-column').append(' <h1 class="totalItemsSearch"><span id="totalLength">'+itemsCount+'</span> <?= Text::_('TPL_GESPLAN_EQUIPMENTS');?> </h1>')

                var equipmentsList = response.data[0].data;
                /* PINTAMOS LO EQUIPAMIENTOS */
                if(equipmentsList.length){
                    const obj =(equipmentsList);
                    equipmentsList.forEach(printDivContent);

                    function printDivContent(item, index) {
                        console.log(equipmentsForMap[item.id] = {
                            itemId:item.id,
                            userLogged:item.user_logged,
                            title:item.title, 
                            codigo:item.jcfields[146].rawvalue, 
                            link: item.link,
                            text: item.introtext.substring(0, 80),
                            imageSrc: item.imageItem['src'],
                            imageAlt:item.imageItem['alt'],
                            latitude: item.jcfields[177].rawvalue,
                            longitude:item.jcfields[178].rawvalue,
                            favourite: item.favourite,
                            incidence: item.incidence,
                            gallery: item.gallery});
                        var equipment = `
                                    <div class="item-itinerario">
                                      
                                        <div class="row">
                                            <div class="col-3">
                                                <figure class="sizeingImg">
                                                <img src="/${equipmentsForMap[item.id].imageSrc}" alt="${equipmentsForMap[item.id].imageAlt}" loading="lazy">
                                            </figure>
                                            </div>
                                            <div class="col-9">
                                            <div class="title-item">
                                            <div class="dropdown">
                                                <button type="button" class="badge" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                                                    <li><a id="addToFavourites" onclick="setAddToFavouritesLink(this)"`;
                                                    <?php if($status): ?>
                                                            equipment += `data-bs-toggle="modal" data-bs-target="#mustlog"`;
                                                    <?php endif; ?>
                                                    equipment += ` data-itemId="${equipmentsForMap[item.id].itemId}"`;
                                                    equipment += `class="dropdown-item`;

                                                    if(equipmentsForMap[item.id].favourite){
                                                        equipment += ` favorito-marked`;
                                                    }
                                                        equipment += `" href="#">`;
                                                    if(equipmentsForMap[item.id].favourite){
                                                        equipment += `<?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?>`;
                                                    }
                                                    else{
                                                        equipment += `<?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?>`;
                                                    }
                                                    equipment += `</a></li>
                                                    <li><a class="dropdown-item" href="${equipmentsForMap[item.id].link.substring(1)}"><?= Text::_("TPL_GESPLAN_VIEW_FULL_DETAIL");?></a></li>
                                                </ul>
                                            </div>
                                            <p><a href="${equipmentsForMap[item.id].link.substring(1)}" title="${equipmentsForMap[item.id].title}>" style="color:#4a503b">${equipmentsForMap[item.id].title}</a></p>

                                        </div>
                                                <div class="content-values">
                                                    <div class="item-value">
                                                        <p class="value">${equipmentsForMap[item.id].text}...</p>
                                                    </div>`;
                                                    if(equipmentsForMap[item.id].incidence != ""){                    
                equipment +=                        `<div class="item-value">
                                                        <?= HTMLHelper::_('image', 'icon-warning.svg', Text::_("TPL_GESPLAN_INCIDENCE"), 'class="icon-warning"', true, 0) ?>
                                                        <p class="value"><?= Text::_('TPL_GESPLAN_INCIDENCE')?></p>
                                                    </div>`;
                                                }
                 equipment +=                   `</div>
                                            </div>
                                        </div>
                                    </div>
                        `;
                        

                        $("#itinerarios-column").append(equipment);

                    }

                }	
                else{
        
                    $('#itinerarios-column').append('<p>No hemos encotrado equipamientos para tus filtros de búsqueda</p>');
                }

                if(pageNum == 1) pagination.make(itemsCount, 20);                
                    /*    */ 
            },
            onError: function onError() {
                Joomla.renderMessages({
                error: ['Something went wrong! Please close and reopen the browser and try again!']
                });
            },
        });

  

    }
    function generatePagination(){

        // Foreach page

        // Generate a button that on click calls to setPage
    }

    async function getEquipmentsForMap() {
        var pageNum = 0; //get all equipamientos
        var equipamientosCat = 39;

        var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + equipamientosCat + "&page=" + pageNum;
        
        await Joomla.request({
            url: baseUrl +  "&plugin=Listajaxcontent",
            method: 'GET',
            perform: true,
            onSuccess: function onSuccess(resp) {
                // Remove loader image

                var response = {};

                try {
                    response = JSON.parse(resp);
                } catch (e) {
                Joomla.renderMessages({
                    error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
                }, ".sampledata-steps");
                return;
                }
                var equipmentsList = response.data[0].data;
                /* PINTAMOS LO EQUIPAMIENTOS */
                if(equipmentsList.length){
                    const obj =(equipmentsList);
                    equipmentsList.forEach(printDivContent);

                    function printDivContent(item, index) {
                        console.log(equipmentsForMap[item.id] = {
                            itemId:item.id,
                            catId:item.catid,
                            category_title:item.category_title,
                            typeEquipment: item.type_equipment,
                            userLogged:item.user_logged,
                            title:item.title, 
                            codigo:item.jcfields[146].rawvalue, 
                            link: item.link,
                            text: item.introtext.substring(0, 80),
                            imageSrc: item.imageItem['src'],
                            imageAlt:item.imageItem['alt'],
                            latitude: item.jcfields[177].rawvalue,
                            longitude:item.jcfields[178].rawvalue,
                            favourite: item.favourite,
                            incidence: item.incidence,
                            gallery: item.gallery});
                    }
                    printInMap(equipmentsForMap);
                }	
                    /*    */ 
            },
            onError: function onError() {
                Joomla.renderMessages({
                error: ['Something went wrong! Please close and reopen the browser and try again!']
                });
            },
        });

  

    }
    function printInMap(equipmentsForMap){

        equipmentsForMap.forEach(fitInMap);

        function fitInMap(item,id){
                let lat_lng = [item.latitude, item.longitude];
                if(item.typeEquipment == 3){
                marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.acampada})
                //markersEquip.addLayer( marker[id]).addTo(acampada);
                }
                else if(item.typeEquipment == 4){
                    marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.recreativo});
                // markersEquip.addLayer( marker[id]).addTo(recreativo);
                }
                else if(item.typeEquipment == 5){
                    marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.campamento});
                    //markersEquip.addLayer( marker[id]).addTo(campamento);
                }
                else if(item.typeEquipment == 6){
                    marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.visitantes});
                    //markersEquip.addLayer( marker[id]).addTo(visitantes);
                }      
                //marker[id] = new L.marker(lat_lng);
                markers.addLayer( marker[id]);
                map.addLayer( markers);
                //    .addTo(map);
                bounds.extend(lat_lng) ;

                marker[id].addEventListener('click',
                    function () {
                        $('.offCanvas-item .title .title').text(item.title);
                        $('.offCanvas-item .category').text(item.category_title);
                        //$('.offCanvas-item .badge').text(item.codigo);
                        $('.offCanvas-item .description').text(item.text);
                        $('.offCanvas-item .btn-ficha-completa').attr("href", item.link);
                        if(item.incidence != ""){
                            $(".offCanvas-item .item-value.alert").show();
                        }
                        else{
                            $(".offCanvas-item .item-value.alert").hide();
                        }
                        if(item.gallery.length > 0){
                            $('.carousel-inner').empty();

                            item.gallery.forEach(printFigure);
                            function printFigure(image, index){
                                imageDiv = '<div class="carousel-item';
                                if(index == 0){
                                    imageDiv += " active";
                                }
                                imageDiv += '"><img src="/'+image.src+'" class="d-block w-100" alt="'+image.alt+'" loading="lazy"></div>';
                                $('.carousel-inner').append(imageDiv);
                            }
                        }
                        else{
                            $('.carousel-inner').empty();
                        }
                        
                         

                    var addToFavButton = `<a class="dropdown-item`;
                    if(item.favourite){
                        addToFavButton += ` favorito-marked`;
                    }
                    addToFavButton += `"`;
                    <?php if($status): ?>
                        addToFavButton += ` data-bs-toggle="modal" data-bs-target="#mustlog"`;
                    <?php endif; ?>
                    addToFavButton += `href="#" data-itemId="${item.itemId}" title="añadir a mis rutas" onclick="setAddToFavouritesLink(this)">
                                                <i class="far fa-heart"></i>`;
                    if(item.favourite){
                        addToFavButton += `<span><?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?></span>`;
                    }
                    else{
                        addToFavButton += `<span><?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?></span>`;
                    }
                    addToFavButton += `</a>`;
                    $('#addToFavouritesModal').html(addToFavButton);
                    $('.offCanvascontainer').addClass('active');
                });
        }   
           // map.fitBounds(bounds);
    }

    </script>






<!-- </div> -->
                                <script>
                                        //if not logged in

                                            //trigger popup

                                        //if logged in
                                        function setAddToFavouritesLink(el){
                                            console.log(el);
                                            item_id = $(el).data("itemid");
                                            action = $(el).data("action");
                                            addToFavourites(action,item_id);
                                        }
                                      
                                        //Get content by Ajax
                                        function addToFavourites(user_id,item_id){

                                            var baseUrl = "/index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&item=" + item_id;
                                            console.log(baseUrl);

                                            Joomla.request({
                                                url: baseUrl +  "&plugin=Addtofavourites",
                                                method: 'GET',
                                                perform: true,
                                                onSuccess: function onSuccess(resp) {
                                                    const obj = JSON.parse(resp);
                                                    if(obj.data[0].message == "ADDED"){
                                                        $(".dropdown-item[data-itemid="+obj.data[0].item+"]").addClass('favorito-marked');
                                                        $(".dropdown-item[data-itemid="+obj.data[0].item+"]").html('<?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?>');
                                                        Swal.fire({
                                                                html: '<p>¡Guardado en favoritos!</p>',
                                                                confirmButtonText: 'Aceptar'
                                                        })
                                                    }
                                                    else if(obj.data[0].message == "REMOVED"){
                                                        $(".dropdown-item[data-itemid="+obj.data[0].item+"]").removeClass('favorito-marked');
                                                        $(".dropdown-item[data-itemid="+obj.data[0].item+"]").html('<?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?>');
                                                        Swal.fire({
                                                                html: '<p>¡Eliminado de favoritos!</p>',
                                                                confirmButtonText: 'Aceptar'
                                                        })
                                                    }
                                                },
                                                onError: function onError() {
                                                    Joomla.renderMessages({
                                                    error: ['Something went wrong! Please close and reopen the browser and try again!']
                                                    });
                                                }
                                            });


                                        }
                                         //call ajax with user id and article id

                                            //insert in _sg_favourites 

                                            //if insert ok

                                                //mark heart 



                                </script>

<?php // Valoraciones ?>

<?php 
$categoryId = 39; //Equipamientos category original
$associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($categoryId)); 
$catLang =   Factory::getLanguage()->getTag() ;
$catId = $associations[ $catLang];
$db = Factory::getDbo();
$query ="
select
ratings.item_id, AVG(ratings.rating) as avg
from
   sooeg_content as article
   
left join sooeg_categories as category on
   article.catid = category.id
   
left join sooeg_sg_valoraciones as ratings on
   ratings.item_id = article.id
where
   state = 1
   and category.parent_id = ".$catId."
   and ratings.item_id is not null
GROUP BY ratings.item_id
ORDER BY avg desc LIMIT 8";
$db->setQuery($query);
if(count($mostRated = $db->loadObjectList())) :
?>
<?php if( $mostRated[0]->avg > 0) : ?>
    <section class="mas-valorados">
        <div class="container">
            <div class="row content-horizontal-scrol">
                <div class="col-md-12">
                    <h2 class="head-scroll"><?= Text::_("TPL_GESPLAN_OTHER_EQUIPMENTS")?></h2>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="multiple-items">
                        <?php foreach($mostRated as $item) : ?>
                            <?php

                            //Create new model for article
                                $model = JModelLegacy::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
                                $appParams = Factory::getApplication()->getParams();
                                $model->setState('params', $appParams);
                                $model->setState('filter.article_id', $item->item_id); //change that to your Category ID
                                //$model->setState('filter.published', '1');

                                $rated =   $model->getItems();
                                // There is only one article in the array, the this is the article
                                $item = $rated[0];    

                                $fields = FieldsHelper::getFields('com_content.article', $item, true);
                                //create  article link
                                $articleLink = (RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));
                                $link = Route::_($articleLink);
                                // Adding the fields to the object
                                $item->jcfields = array();

                                foreach ($fields as $key => $field)
                                {
                                    if($field->id == 76){//ubicacion
                                        $item->jcfields[$field->id] = $field;
                                    }
                                }
                            ?>

                            <div class="card-default">
                                <button class="like-post"><i class="far fa-heart"></i></button>
                                <a class="hyper-link" href="<?php echo $link; ?>"></a>

                                <figure>
                                    <div class="gradient"></div>
                                    <?php
                                   $images = json_decode($item->images); 
                                    if(!empty($images->image_intro)) :
                                        $listImage['src'] = $images->image_intro ;
                                        $listImage['alt'] = $images->image_intro_alt;
                                    else :
                                        $listImage['src'] = "templates/gesplan/images/neutra-300.jpg";
                                        $listImage['alt'] = "Tenerife ON";
                                    endif;
                                    ?>
                                    <img src="<?php echo $listImage['src'] ; ?>" alt="<?php echo $listImage['alt'] ; ?>" loading="lazy">
                                </figure>
                                <div class="caption">
                                    <div class="head-caption">
                                        <span><?= $item->jcfields[76]->rawvalue; // ubicación?></span>
                                    </div>
                                    <div class="body-caption">
                                        <h3><?php echo $item->title; ?></h3>
                                    </div>
                                    <div class="icons-caption">
                                        <!-- Rating on article list view -->
                                        <?php 
                                        //Prepare icons for rating on articles
                                        $iconStar     = HTMLHelper::_('image', 'plg_content_vote/vote-star.svg', '', '', true, true);
                                        $iconHalfstar = HTMLHelper::_('image', 'plg_content_vote/vote-star-half.svg', '', '', true, true);
                                        
                                        // If you can't find the icons then skip it
                                        if ($iconStar === null || $iconHalfstar === null)
                                        {
                                            return;
                                        }
                                        
                                        // Get paths to icons
                                        $pathStar     = JPATH_ROOT . substr($iconStar, strlen(Uri::root(true)));
                                        $pathHalfstar = JPATH_ROOT . substr($iconHalfstar, strlen(Uri::root(true)));
                                        
                                        // Write inline '<svg>' elements
                                        $star     = file_exists($pathStar) ? file_get_contents($pathStar) : '';
                                        $halfstar = file_exists($pathHalfstar) ? file_get_contents($pathHalfstar) : '';

                                        // Get rating
                                        $db = Factory::getDbo();
                                        $query = $db
                                        ->getQuery(true)
                                        ->select(array('SUM(rating) as rating, COUNT(id) as count'))
                                        ->from($db->quoteName('#__sg_valoraciones'))
                                        ->where($db->quoteName('item_id') . " = " . $db->quote($item->id));
                                        // Reset the query using our newly populated query object.
                                        $db->setQuery($query);
                                        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                        $itemRating = $db->loadObjectList();

                                        $rating = (float) $itemRating[0]->rating;
                                        if($rating){
                                            $rating = $rating / (int) $itemRating[0]->count;
                                        }
                                        $rcount = (int) $itemRating[0]->count;

                                        // Round to 0.5
                                        $rating = round($rating / 0.5) * 0.5;
                                            // Determine number of stars
                                            $stars = $rating;
                                            $img   = '';

                                            for ($i = 0; $i < floor($stars); $i++)
                                            {
                                                $img .= '<li class="vote-star">' . $star . '</li>';
                                            }

                                            if (($stars - floor($stars)) >= 0.5)
                                            {
                                                $img .= '<li class="vote-star-empty">' . $star . '</li>';
                                                $img .= '<li class="vote-star-half">' . $halfstar . '</li>';

                                                ++$stars;
                                            }

                                            for ($i = $stars; $i < 5; $i++)
                                            {
                                                $img .= '<li class="vote-star-empty">' . $star . '</li>';
                                            }

                                            ?>
                                            <div class="content_rating" role="img" aria-label="<?php echo Text::sprintf('PLG_VOTE_STAR_RATING', $rating); ?>">
                                                <?php if ($rcount) : ?>
                                                    <p class="visually-hidden" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
                                                        <?php echo Text::sprintf('PLG_VOTE_USER_RATING', '<span itemprop="ratingValue">' . $rating . '</span>', '<span itemprop="bestRating">5</span>'); ?>
                                                        <meta itemprop="ratingCount" content="<?php echo $rcount; ?>">
                                                        <meta itemprop="worstRating" content="1">
                                                    </p>
                                                <?php endif; ?>
                                                <ul>
                                                    <?php echo $img; ?>
                                                </ul>
                                            </div> 

                                            <!-- /Rating -->
                                        
                                    </div>
                                </div>
                                <div class="link-card">
                                    <p><?= Text::_("TPL_GESPLAN_MORE_INFO")?></p>
                                </div>
                            </div> 
                        <?php endforeach; ?>
             
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <?php endif; ?>
<?php endif; ?> 
<?php
    $path = dirname(__FILE__, 2) . "/article/partials/";
    require_once($path.'modal-favorito.php');
?>