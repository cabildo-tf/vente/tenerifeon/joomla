<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);

?>


    <?php if ($canEdit) : ?>
		<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
	<?php endif; ?>

            <article class="information__category__featured-in">
                <div class="row">
                    <div class="col-sm-7">
                        <figure class="information__category__featured-image">
                        <?php
                            $images         = json_decode($this->item->images); 
                        ?>
                            <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" class="img-fluid" loading="lazy">

                        </figure>
                    </div>
                    <div class="col-sm-5 d-flex align-items-start flex-column">
                        <div class="information__category__container__text">
                            <div class="information__category__featured-text">
                                <p class="information__category__featured-category"><?= $this->item->jcfields[9]->rawvalue;?></p>
                                <h2><?php echo $this->item->title;?></h2>
                                <p><?php echo($this->item->jcfields[14]->value); //cf entradila?></p>
                            </div>
                            <?php 
                                    $link = (RouteHelper::getArticleRoute($this->item->id, $this->item->catid, $this->item->language));
                                    $link = Route::_($link);
                    
                            ?>
                            <div class="information__category__featured-button mt-auto">
                                <a href="<?php echo $link;?>" title="<?php echo JText::_( 'Lee el artículo' );?> <?php echo $this->item->title;?>"><?php echo JText::_( 'Lee el artículo' );?></a>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </article>
