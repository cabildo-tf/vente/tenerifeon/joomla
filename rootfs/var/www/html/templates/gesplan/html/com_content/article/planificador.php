<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
$category_title   = $this->escape($this->item->category_title);

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
    <?php
    //Get mapbox
    $config = Factory::getConfig();
        $mapbox_token = $config->get('mapbox_token');
    ?>
        <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>

   <section class="planificador view_rezizer">
            <button id="openPlnificador" class="opened niceRotate"><i class="fa fa-chevron-right"></i></button>
            <div id="planificador-column" class="planificador-column opened">
                <div id="planificador-options">
                    <div class="planificador-column-justify">
                    <?php
                        $user = JFactory::getUser();
                        $status = $user->guest;
                        if($status) :
                    ?>
                    <div class="container aviso-login">
                        <i class="fa fa-exclamation"></i>
                        <p><?= Text::_("TPL_GESPLAN_NEED_TO_LOGIN_TUSAVE_YOUR_ROUTE");?></p>
                    </div>
                    <?php endif; ?>
                    <div class="head-planificador">
    
                        <h1><?= Text::_("TPL_GESPLAN_PLAN_ROUTE");?></h1>
                       
                        <a href="#" id="reset-planificador" class="openClose-planificador">
                            <?= Text::_("TPL_GESPLAN_CLEAN_ROUTE");?>
                        </a> 
                    </div>
                    <form>


                        <div class="accordion" id="acordionFilters">
                            <div class="accordion-item">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#container__deportes" aria-expanded="true"
                                    aria-controls="container__deportes">
                                    <!-- pasale el icono correcto en funcion del que seleccionas dentro del dropdown -->
                                    <div id="sportIcon">
                                        <i class="fa fa-walking"></i>
                                    </div>
                                    <?= Text::_("TPL_GESPLAN_SPORT")?> -&nbsp;
                                    <span id="sport"><?= Text::_("TPL_GESPLAN_HIKING");?></span>
                                </button>

                                <div id="container__deportes" class="container__deportes accordion-collapse collapse show"
                                    data-bs-parent="#accordionExample">
                                    <div class="item form-check">
                                        <input id="walking" type="checkbox" value="1" id="">
                                        <label for="walking"><i class="fa fa-walking"></i></label>
                                    </div>
                                    <div class="item form-check">
                                        <input id="biking" type="checkbox" value="2" id="">
                                        <label for="biking"><i class="fa fa-biking"></i></label>
                                    </div>
                                    <div class="item form-check">
                                        <input id="horse" type="checkbox" value="2" id="">
                                        <label for="horse"><i class="fa fa-horse"></i></label>
                                    </div>
                                    <div class="item form-check">
                                        <input id="motorcycle" type="checkbox" value="3" id="">
                                        <label for="motorcycle"><i class="fa fa-motorcycle"></i></label>
                                    </div>
                                </div>


                            </div>

                        </div>

                        <div>
                        <div class="input__buscador">
                            <label for=""> <i class="fa fa-map-marker" style="color:#4127ee!important"></i> <span ></span></label>
                            <input type="search" id="inputText-1" placeholder="<?= Text::_("TPL_GESPLAN_PLAN_START_SEARCH");?>" onkeyup="showResult(this.value,1,1)" onclick="showResult(this.value,1,1)" >
                        </div>
                        <div id="livesearch-1" class="live-search"></div>
                            <!-- <div  id="start-point" class="route-point">
                                <a href="#" title="Selecciona punto <?= Text::_('TPL_GESPLAN_PLAN_ROUTE_START') ?>" onclick="startPoint()">
                                    <i class="fa fa-map-marker"></i><?= Text::_("TPL_GESPLAN_PLAN_END_SEARCH");?>                             
                                <span></span>
                                </a>
                            
                            </div> -->
                            <ul class="draggables planificador_ruta">
                            
        
                            </ul>
                            <div class="new-point" id="new-point" style="display:none">
                                <a href="#" class="new-point-link" onclick="addNewPoint(event)">
                                    <i class="fa fa-plus"></i> <?= Text::_("TPL_GESPLAN_PLAN_ADD_POINT");?>
                                </a>
                            </div>
                            <div class="input__buscador">
                                <label for=""> <i class="fa fa-map-marker" style="color:#d6282e!important"></i> <span ></span></label>
                                <input type="search" id="inputText-2" placeholder="<?= Text::_("TPL_GESPLAN_PLAN_END_SEARCH");?>" onkeyup="showResult(this.value,2,2)" onclick="showResult(this.value,2,2)" >
                            </div>
                            <div id="livesearch-2" class="live-search"></div>
                            <!-- <div  id="end-point" class="route-point">
                                <a href="#" title="End point" onclick="endPoint()">
                                    <i class="fa fa-flag-checkered"></i><?= Text::_("TPL_GESPLAN_PLAN_ROUTE_END");?>
                                    <span></span>
                                </a>
                            </div> -->
                            <div class="container capas_equipamientos" style="display:none">
                                <p><?= Text::_("TPL_GESPLAN_VIEWS_ON_THE_WAY")?></p>
                                <div id="capas_equipamientos" >
                                    <div class="item form-check">
                                        <input name="capas_equipamientos"  id="equipamientos" type="checkbox" value="equipamientosMarkers" id="equipamientosMarkers">
                                        <label for="equipamientos"><i class="fas fa-campground"></i></label>
                                    </div>
                                    <div class="item form-check">
                                        <input name="capas_equipamientos"  id="puntos-interes" type="checkbox" value="puntosMarkers" id="puntosMarkers">
                                        <label for="puntos-interes"><i class="far fa-image"></i></label>
                                    </div>
                                </div>
                            </div>


                            <div class="form-check contol-circular">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    <?= Text::_("TPL_GESPLAN_START_SAME_END")?>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <?php //planificador-options ?>
            <div id="loader-icon"></div>
            <div class="route-path" style="display:none">
                <div id="route-title"></div>
                <h2><?= Text::_("TPL_GESPLAN_ROUTE_INFO")?></h2>
                <div class="route-datas">             
                    <p><i class="fa fa-level-up-alt"></i><strong><?= Text::_("TPL_GESPLAN_ALTITUDE_GAIN")?>: </strong><span id="altitudeGain"></span> m</p>
                    <p><i class="fa fa-level-down-alt"></i><strong><?= Text::_("TPL_GESPLAN_ALTITUDE_LOST")?>:</strong> <span id="altitudeLost"></span> m</p>
                    <p><i class="fa fa-sort-up"></i><strong><?= Text::_("TPL_GESPLAN_MAX_ALTITUDE")?>:</strong> <span id="altitudeMax"></span> m </p>
                    <p><i class="fa fa-sort-down"></i><strong><?= Text::_("TPL_GESPLAN_MIN_ALTITUDE")?>:</strong> <span id="altitudeMin"></span>m</p>
                    <!-- <p><i class="fa fa-tachometer-alt"></i><strong>Velocidad estimada:</strong> <span id="distanceRoute"></span>km/h</p>  -->
                    <p><i class="fa fa-arrow-right"></i><strong><?= Text::_("TPL_GESPLAN_ROUTE_DISTANCE")?>: </strong><span id="distanceRoute"></span> km</p>
                    <h3><?= Text::_("TPL_GESPLAN_ELEVATION_PROFILE")?></h3>
                    <?php
                        $config = Factory::getConfig();
                        $mapbox_token = $config->get('mapbox_token');
                    ?>
                    <div class="map" id="map-itinerario"></div>
                    <div id="elevation-div"></div>
                    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
                    <script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>
                    <link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.css" />
                    <link rel="stylesheet" href="https://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.Default.css" />
                    <script src="/templates/gesplan/js/gesplan/leaflet.groupedlayercontrol.js"></script>
                    <!-- leaflet-elevation -->
                    <link rel="stylesheet" href="https://unpkg.com/@raruto/leaflet-elevation@2.2.5/dist/leaflet-elevation.min.css" />
                    <script src="https://unpkg.com/@raruto/leaflet-elevation@2.2.5/dist/leaflet-elevation.min.js"></script>

                    <style> 
                        #elevation-div { height: 100%; width: 100%; padding: 0; margin: 0; } 
                        #elevation-div {	height: 25%; font: 12px/1.5 "Helvetica Neue", Arial, Helvetica, sans-serif; } 
                    </style>
                </div>
                <div id="alertas"></div>
                <!-- <div class="alertas">
                    <h3>Alerta</h3>
                    <p>12/12/2021 a las 17:35h</p>
                    <p><strong>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                        ut .</strong></p>
                    <a href="">MÁS INFORMACIÓN</a>
                </div> -->
                <div class="consejos">
                <h3><?= Text::_("TPL_GESPLAN_ROUTES");?></h3>
                    <p><strong><?= Text::_("TPL_GESPLAN_ITINERARIES_BY_YOUR_ROUTE")?></strong></br>
                        <?= Text::_("TPL_GESPLAN_CHECK_ITINERARIES_INFO_AND_FIND")?></p>
                        <div id="itinerariesBy"></div>

                </div>

                <div class="posicion">

                </div>

                <h3><?= Text::_("TPL_GESPLAN_CHECK_WEATHER_FORECAST")?></h3>
            </div>
            <?php   if($status) {
                        $modal = 'mustlog';
                    }else{
                        $modal = 'saveRouteModal';
                    } 
            ?>
            <a href="#"  data-bs-toggle="modal" data-bs-target="#<?= $modal ?>" class="save-route disabled" >
                <?= Text::_("TPL_GESPLAN_SAVE_ROUTE");?>
            </a>
        </div>
        <div class="content-map-desktop planificator" style="z-index:1"id="content-map-desktop">
        </div>

    </section>
 <!-- Modal Save route-->
<div class="modal fade" id="saveRouteModal" tabindex="-1" aria-labelledby="saveRouteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title" id="saveRouteModalLabel"><?= Text::_("TPL_GESPLAN_SAVE_ROUTE");?></p>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
            <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label"><?= Text::_("TPL_GESPLAN_ROUTE_NAME");?></label>
            <input type="text" class="form-control" id="routeName" placeholder="Ruta personalizada" value="Ruta personalizada">
            </div>
            <div class="modal-footer">
        <button type="button" class="btn-closes" data-bs-dismiss="modal"><?= Text::_("TPL_GESPLAN_CLOSE_MODAL");?></button>
        <button type="button" class="btn-save" onclick="saveRoute()"><?= Text::_("TPL_GESPLAN_SAVE");?></button>
      </div>
        </form>
    </div>

      
    </div>
  </div>
</div>
     <script>
        var firstMarker = new L.marker;
        var type = 1; //Default value is sendero
        var myRoute = "";
        var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
        var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<?= $mapbox_token ?>';

        var tenerifeon = L.tileLayer(mbUrl, {id: 'tenerifeon/cl2pyxrtp001b15lfuq3x23z5', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
        var senderos = L.layerGroup();
        var  scrollWheelZoom = true;

        var disableDragIfMobile = function() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        if(check){
            //map.dragging.disable();
            map.scrollWheelZoom.disable();
        }  
        };

        var map = L.map('content-map-desktop', {
            center: [28.307706, -16.595535],
            zoom: 11,
            minZoom:10,
            scrollWheelZoom: false,
            layers: [tenerifeon],
            detectRetina: true,
            scrollWheelZoom: scrollWheelZoom
        });

        disableDragIfMobile();
        var bica = L.layerGroup();
        var vm = L.layerGroup();
        var baseMaps = {
            'Tenerifeon': tenerifeon
        };
        var overlayMaps = {
            "Senderos": senderos,
            "Bica": bica,
            "VM": vm 
        };
        <?php $siteUrl = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://'.$_SERVER['SERVER_NAME']; ?>

        var alrGivId = [];

        function getRanges(array) {
            var ranges = [],
                rstart, rend;
            for (var i = 0; i < array.length; i++) {
                rstart = array[i];
                rend = rstart;
                while (array[i + 1] - array[i] == 1) {
                    rend = array[i + 1]; // increment the index if the numbers sequential
                    i++;
                }
                ranges.push(rstart == rend ? rstart + '' : rstart + '-' + rend);
            }
            return ranges;
        }


            var _isProcessing = false;
            var _limit = false;
            var distanceToStart;

            async function getData(northEast, southWest) {

                if (_isProcessing == false && _limit == false) {
                    _isProcessing = true;
                    var itineraryLayer = [];


                    // Ajax to get content
                    var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=&getdata=1";
                    var url = baseUrl +  "&plugin=Listajaxcontent";
                    try {
                        await $.ajax({
                            type: 'POST',
                            url: url,
                            data: {
                                "northEast": {
                                    "latitude": northEast.lat,
                                    "longitude": northEast.lng
                                },
                                "southWest": {
                                    "latitude": southWest.lat,
                                    "longitude": southWest.lng
                                },
                                "given": getRanges(alrGivId).join('_')
                            },
                            dataType: 'json',
                            success: function(response) {
                                response.data[0].data.forEach(function (itinerary){
                                    // If the element not exists
                                    if(alrGivId.indexOf(itinerary.properties
                                    .geom_id) === -1 ){
                                        alrGivId.push(itinerary.properties.geom_id);
                                        if(itinerary.properties.type == "1"){
                                            L.geoJSON(itinerary, {color: '#FF0000',  onEachFeature: onEachFeature}).addTo(senderos);
                                        }
                                        else if(itinerary.properties.type == "2"){
                                            L.geoJSON(itinerary, {color: '#FF8F0B',  onEachFeature: onEachFeature}).addTo(bica);
                                        }
                                        else if(itinerary.properties.type == "3"){
                                            L.geoJSON(itinerary, {color: '#0B7CFF',  onEachFeature: onEachFeature}).addTo(vm);
                                        }
                                    } 
                                });

                                if (alrGivId.length >= response.data[0].total_count) {
                                    _limit = true;
                                }
    
                                function onEachFeature(feature, layer) {
                                    //bind click
                                    layer.on('click', function (e) {

                                    var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&item=" + layer.feature.id;

                                    Joomla.request({
                                        url: baseUrl +  "&plugin=Listajaxcontent",
                                        method: 'GET',
                                        perform: true,
                                        onSuccess: function onSuccess(resp) {
                                          
                                                /*    */ 
                                        },
                                        onError: function onError() {
                                            Joomla.renderMessages({
                                            error: ['Something went wrong! Please close and reopen the browser and try again!']
                                            });
                                        }
                                    });
                                    });

                                }

                            },
                        });
                    } catch (e) {}

                    _isProcessing = false;
                }
            }

        map.on('moveend', function(ev) {
            let bounds = map.getBounds();
            getData(bounds._northEast, bounds._southWest);
        });

        map.on('zoomend', function(ev) {
            let bounds = map.getBounds();
            getData(bounds._northEast, bounds._southWest);
        });

        map.clicked = 0;                                                                      
        map.on('click', function(e){
            map.clicked = map.clicked + 1;
            setTimeout(function(){
                if(map.clicked == 1){
                    lat_lng = [e.latlng.lat, e.latlng.lng];
                    var startMarker;
                    marker = new L.marker(lat_lng);
                    //map.off('click');
                    if(pointsList.length == 0){
                        setMarker(marker, null, 1);                
                    }
                    else if(pointsList.length == 1){
                        setMarker(marker, null, 2);   
                    }
                    map.clicked = 0;
                }
            }, 300);
        });
        map.on('dblclick', function(event){
            map.clicked = 0;
            map.zoomIn();
        });
        //Prevent map to navigate out of Tenerife limits
        map.setMaxBounds([
                [28.748396571187406, -15.671997070312502],
                [27.831789750079267, -17.586364746093754]
            ]);
        function checkName(title, marker){
            if(title != null){
                pointTitle = title;
            }
            else{
                pointTitle = marker.getLatLng().lat.toFixed(6)+', '+marker.getLatLng().lng.toFixed(6);
            }
            return pointTitle;
        }
        //Create start an end markers
        function setMarker(marker, title, point){
            $("#livesearch-1").empty(); 
            $("#livesearch-2").empty();
            marker = getNearestPointMarker(marker);
            pointTitle = checkName(title, marker);
            if(point == 1){
                marker.setIcon(markersIcons.start)
                map.removeLayer(jsonLayer);
                markers.clearLayers();
                pointsList[0] = ({title: "<b><?= Text::_('TPL_GESPLAN_PLAN_ROUTE_START') ?>:</b> "+pointTitle, marker: marker});
                pointsList[0].marker.addTo(markers).bindPopup(pointsList[0].title).openPopup().dragging.enable();
                pointsList[0].marker.on("dragend", function(e) {
                    markers.clearLayers();
                    newPosition = getNearestPointMarker( new L.marker (L.latLng( this.getLatLng().lat, this.getLatLng().lng )) );
                    this.setLatLng( L.latLng( newPosition.getLatLng().lat, newPosition.getLatLng().lng)  );
                    $("#inputText-1").val("<?= Text::_('TPL_GESPLAN_PLAN_ROUTE_START') ?>: "+this.getLatLng().lat.toFixed(6)+', '+marker.getLatLng().lng.toFixed(6));
                    pointsList[0].title = "<b><?= Text::_('TPL_GESPLAN_PLAN_ROUTE_START') ?>:</b> "+this.getLatLng().lat.toFixed(6)+', '+marker.getLatLng().lng.toFixed(6);
                    updateMarkers();                    
                }); 
                $("#inputText-1").val("<?= Text::_('TPL_GESPLAN_PLAN_ROUTE_START') ?>: " + pointTitle);
                if($('#flexCheckDefault').is(':checked')){
                        makeCircularRoute();
                }
              
                      
            }
            else if(point == 2){
                marker.setIcon(markersIcons.end)
                pointsList[1] = ({title: "<b><?= Text::_('TPL_GESPLAN_PLAN_ROUTE_END') ?>: </b>" + pointTitle, marker: marker});
                pointsList[1].marker.addTo(markers).bindPopup(pointsList[1].title).openPopup().dragging.enable(); 
                pointsList[1].marker.on("dragend", function(e) { 
                    newPosition = getNearestPointMarker( new L.marker (L.latLng( this.getLatLng().lat, this.getLatLng().lng )) );
                    this.setLatLng( L.latLng( newPosition.getLatLng().lat, newPosition.getLatLng().lng)  );
                    $("#inputText-2").val("<?= Text::_('TPL_GESPLAN_PLAN_ROUTE_END') ?>: " +this.getLatLng().lat.toFixed(6)+', '+marker.getLatLng().lng.toFixed(6));
                    pointsList[1].title = "<b><?= Text::_('TPL_GESPLAN_PLAN_ROUTE_END') ?>: </b>" + this.getLatLng().lat.toFixed(6)+', '+marker.getLatLng().lng.toFixed(6);
                    updateMarkers();                   
                }); 
                $("#inputText-2").val("<?= Text::_('TPL_GESPLAN_PLAN_ROUTE_END') ?>: " + pointTitle);
                $('.new-point').show();
                //map.off('click');
            }
            if(connectedItineraries.getLayers().length == 0){
                getConnectedItineraries(marker);
            }
            if(firstMarker.getLatLng() == undefined){
                firstMarker.setLatLng( L.latLng( marker.getLatLng().lat, marker.getLatLng().lng)  );     
            }
   
            // else{
            //     markers.clearLayers();
            //     pointsList[1] = ({title: pointTitle, marker: marker});
            //     pointsList[1].marker.addTo(markers).bindPopup(pointsList[1].title).openPopup().dragging.enable(); 
            //     pointsList[0].marker.addTo(markers);
            //     $("#inputText-2").val(pointTitle);
            //     $('.new-point').show();
            //     map.off('click');
            //     getPlanificador(pointsList);
            // }
            updateMarkers();
            if(pointsList.length > 2 ){
                map.removeLayer(jsonLayer);
                //getPlanificador(pointsList);
            } 
            aux = [...pointsList];

        }

        function updateMarkers(){
            markers.clearLayers();;
            pointsList.forEach(function(marker){
                marker.marker.addTo(markers).bindPopup(marker.title).openPopup().dragging.enable();
            })
            if(pointsList.length > 1){
                    getPlanificador(pointsList);
                } 
        }
        function getNearestPointMarker(marker){

            var coordinates_array = [];

            var routesLayer;//Will set the type of itineraries to search for
            if(type == 1){
                routesLayer = senderos
            }
            else if(type == 2){
                routesLayer = bica
            }
            else if(type == 3){
                routesLayer = vm
            }
            var layers_array = routesLayer.getLayers(); //Gett coordianetes of itineraries by current type
            layers_array.forEach(function(layer){
                coordinates_array.push(layer.getLayers().map(l => l.feature.geometry.coordinates));
            });  
            //Gets the nearest point in a itinerary of current type
            let closest_latlng = L.GeometryUtil.closest(map, coordinates_array, [marker.getLatLng().lng, marker.getLatLng().lat], true)
            lat_lng = [closest_latlng.lng.toFixed(12), closest_latlng.lat.toFixed(12)];
            //Moves the selected position to the nearest point
            marker = new L.marker(lat_lng);

            return marker;
        }

        function makeCircularRoute() {
            var lastPosition = pointsList.length - 1;
            lat_lng = [pointsList[0].marker.getLatLng()];
            var endMarker;
            endMarker = new L.marker([lat_lng[0].lat, lat_lng[0].lng]);

            if(pointsList.length == 1){  
                pointsList[1] = ({title: '<?= Text::_("TPL_GESPLAN_PLAN_ROUTE_END");?>', marker: endMarker});
                pointsList[1].marker.addTo(markers).bindPopup(pointsList[1].title).openPopup().dragging.enable(); 
             } else{
                pointsList[lastPosition] = ({title: '<?= Text::_("TPL_GESPLAN_PLAN_ROUTE_END");?>', marker: endMarker});
                pointsList[lastPosition].marker.addTo(markers).bindPopup(pointsList[lastPosition].title).openPopup().dragging.enable();  
                $('.new-point').show();
            }
            $("#inputText-2").val("<?= Text::_("TPL_GESPLAN_PLAN_ROUTE_END");?>");
            //map.off('click');
        
            pointsList.forEach(function(marker){
                marker.marker.on("dragend", function(e) {
                if(pointsList.length > 1){
                    getPlanificador(pointsList);
                }  
            }); 
            })
            aux = [...pointsList];

            // if(pointsList.length > 2 ){
            //     map.removeLayer(jsonLayer);
            //     getPlanificador(pointsList);
            // } 
            
           
        }
        var plannedRoute = "";
        //Planificador
        function getPlanificador(pointsList) {
                $('.route-path').hide();
                $("#loader-icon").html('<div class="loader-cont"><div class="buble"></div><i class="fa fa-spin fa-circle-notch"></i><p>Obteniendo datos</p> </div>');
                $("#livesearch-1").empty(); 
                $("#livesearch-2").empty();         

                // Ajax to get content
                var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token')+ "&type="+type;
                var url = baseUrl +  "&plugin=Planificador";
                pointsList = pointsList.map(function(point) {
                    return {'latitude': point.marker.getLatLng().lat, 'longitude': point.marker.getLatLng().lng };
                });   
                console.log(pointsList);             
                try {
                     $.ajax({   
                        type: 'POST',
                        url: url,
                        data: {'points': pointsList},
                        dataType: 'json',
                        success: function(response) {
                            if (plannedRoute != "") {
                                map.removeLayer(plannedRoute);
                            }
                            if(response.data[0] == "KO"){
                                $('#loader-icon').empty();
                                $('#loader-icon').html('<div class="container"><p>No hemos encontrado rutas posibles para los puntos seleccionados.</p></div>');
                            }
                            else if(response.data[0] == "MULTI"){
                               // alert("Ruta multilinestring");
                            }
                            else{
                                var itineraryColor = '#637bf9';
                                if(type == 3){
                                    itineraryColor = 'red';
                                }
                                plannedRoute = L.geoJSON(response.data[0], {color: itineraryColor});
                                                                    
                                plannedRoute.addTo(map);
                                //map.fitBounds(jsonLayer);
                                myRoute = response.data[0];
                                getRouteData(pointsList);
                               
                                itinerariesBy = response.data[0].properties.itineraries;
                                if(itinerariesBy != ""){
                                    getItinerariesBy(itinerariesBy);
                                    getInterestPointsBy(itinerariesBy);
                                }
                                map.fitBounds(plannedRoute.getBounds());
                            }
 

                        }, onError: function onError() {
                            Joomla.renderMessages({
                            error: ['Something went wrong! Please close and reopen the browser and try again!']
                            });
                        }

                    });
                } catch (e) {
                    throw(e);
                }


        }
        var connectedItineraries = L.layerGroup();

        function getConnectedItineraries(marker) {
                // Ajax to get content
                var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "&type="+type;
                var url = baseUrl +  "&plugin=Planificador";
                marker =  {'latitude': marker.getLatLng().lat, 'longitude': marker.getLatLng().lng };
                try {
                     $.ajax({   
                        type: 'POST',
                        url: url,
                        data: {'firstLastlon': marker},
                        dataType: 'json',
                        success: function(response) {
                            if(response.data[0] == "KO"){
                                //alert("Ruta no encontrada para los puntos seleccionados");
                            }
                            else if(response.data[0] == "MULTI"){
                               // alert("Ruta multilinestring");
                            }
                            else{
                                if(connectedItineraries.getLayers().length > 0){
                                    connectedItineraries.eachLayer(function (layer) {
                                        if (layer._leaflet_id === id){
                                            connectedItineraries.removeLayer(layer)
                                        }
                                    });	
                                }

                                map.removeLayer(senderos).removeLayer(bica).removeLayer(vm);
                                if(type == "1"){
                                    color ='#FF0000';
                                    jsonLayer = senderos;
                                }
                                else if(type == "2"){
                                    color = '#FF8F0B';
                                    jsonLayer = bica;
                                }
                                else if(type == "3"){
                                    color = '#0B7CFF';
                                    jsonLayer = vm;
                                }
                               
                                layer = L.geoJSON(response.data[0], {color: color});

                                connectedItineraries.addLayer(layer).addTo(map);

                                map.fitBounds(layer.getBounds());
                            }
 

                        }, onError: function onError() {
                            Joomla.renderMessages({
                            error: ['Something went wrong! Please close and reopen the browser and try again!']
                            });
                        }

                    });
                } catch (e) {
                    throw(e);
                }


        }
        //Planificador get itineraries passed by

    // .planificador+
    var markers = L.layerGroup();
    var puntosMarkers = L.layerGroup();
    var equipamientosMarkers = L.layerGroup();

    map.addLayer(markers);
    // map.addLayer(puntosMarkers);
    // map.addLayer(equipamientosMarkers);
    var pointsList = [];
    var  aux = [];

    function setMarkerFromSearch(el){
        $("#livesearch-"+parseInt(el.dataset.point)+"").empty();
        if(($(window).width() < 768)) {
            if ($("#openPlnificador").hasClass("opened")) {
                $("#openPlnificador").removeClass("opened");
                $("#planificador-column").removeClass("opened");
            } else {
                $("#openPlnificador").addClass("opened");
                $("#planificador-column").addClass("opened");
            }
        }

        var startLat = el.getAttribute('data-latitude');
        var startLng = el.getAttribute('data-longitude');
        lat_lng = [startLat, startLng];
        let marker;
        marker = new L.marker(lat_lng);
        var point = el.dataset.point;
        var title = el.title;
        setMarker(marker, title, point);
        
    }

   
  
    var itemNumber =1;
    function addNewPoint(event){
        event.preventDefault();
        var newItem = ` <li class="route-point new" id="newItem-${itemNumber}">
                                <i class="fa fa-bars"></i>
                                <p><a href="#" class="open-modal">Punto de paso ${itemNumber}</a></p>
                                <a href="#"  onclick='removeNewItem(${itemNumber})' class="delete__route-point" title="Elimina este punto intermedio">
                                    <i class="far fa-trash-alt"></i>
                                </a>
                                <span></span>
                            </li>`;
        $('.draggables.planificador_ruta').append(newItem);

        var newPosition;
        //alert("Selecciona punto en el mapa")
        //Let add marker only once
        if(($(window).width() < 768)) {
            if ($("#openPlnificador").hasClass("opened")) {
                $("#openPlnificador").removeClass("opened");
                $("#planificador-column").removeClass("opened");
            } else {
                $("#openPlnificador").addClass("opened");
                $("#planificador-column").addClass("opened");
            }
        }
            var coord = map.getCenter();
            newPosition = getNearestPointMarker (new L.marker(coord));
            var currentIndex = $('.route-point.new').length;
            if(pointsList.length > currentIndex){
                //if new index will be create, need to move the end point to the new array length
                pointsList[pointsList.length] = pointsList[currentIndex]
            }
            pointsList[currentIndex] = ({ title:'Punto '+currentIndex, marker: newPosition});
            pointsList[currentIndex].marker.addTo(markers).bindPopup("Punto "+itemNumber).openPopup();
            pointsList[currentIndex].marker.dragging.enable();
            pointsList[currentIndex].marker.on("dragend", function(e) { 
                newPosition = getNearestPointMarker( new L.marker (L.latLng( this.getLatLng().lat, this.getLatLng().lng )) );
                this.setLatLng( L.latLng( newPosition.getLatLng().lat, newPosition.getLatLng().lng)  );
                pointsList[currentIndex].title = "<b>Punto " + currentIndex + ":</b> " + this.getLatLng().lat.toFixed(6) +', '+ marker.getLatLng().lng.toFixed(6);
                updateMarkers(); 
            }); 
            itemNumber ++;
            aux.splice((pointsList.length-2), 0, pointsList[currentIndex])


       
    }


    function removeNewItem(el){
        //$("#newItem-"+el).remove();
        markers.clearLayers();
        //Last position (lenght-1) moves one position before
        // pointsList[pointsList.length-2] = pointsList[pointsList.length-1];
        //Remove last position (length - 1)
        pointsList.splice(el, 1);
        itemNumber --;
        var newItem = "";
        $('.draggables.planificador_ruta').empty();
        pointsList.forEach(function(marker, index){
            if( index > 0 && index < pointsList.length-1 ){
                marker.title = "Punto "+(index);
                newItem += ` <li class="route-point new" id="newItem-${index}">
                                <i class="fa fa-bars"></i>
                                <p><a href="#" class="open-modal">Punto de paso ${index}</a></p>
                                <a href="#"  onclick='removeNewItem(${index})' class="delete__route-point" title="Elimina este punto intermedio">
                                    <i class="fa fa-trash"></i>
                                </a>
                                <span></span>
                            </li>`;
            }
            marker.marker.addTo(markers).bindPopup(marker.title).openPopup().dragging.enable();
            marker.marker.on("dragend", function(e) {      
                getPlanificador(pointsList);             
            })
        })
        aux = [...pointsList];
        $('.draggables.planificador_ruta').append(newItem);
        getPlanificador(pointsList);

    }

    
    function saveRoute(){
        
                var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "&saveRoute=1";
                var url = baseUrl +  "&plugin=Planificador";
                var type_checked = $('#container__deportes input:checked').val();
                var route_type = parseInt(type_checked);
                try {
                     $.ajax({   
                        type: 'POST',
                        url: url,
                        data: { 'linestring': JSON.stringify(myRoute),
                                'route_name': $('#routeName').val(),
                                'route_type': route_type,
                                'altitudeMin': altitudeMin,
                                'altitudeMax':  altitudeMax,
                                'altitudeGain': altitudeGain,
                                'altitudeLost': altitudeLost,
                                },
                        dataType: 'json',
                        success: function(response) {
                            console.log(response);
                            if(response.data[0] == "KO"){
                                alert("Ruta no encontrada para los puntos seleccionados");
                            }
                            else if(response.data[0] == "MULTI"){
                                alert("Ruta multilinestring");
                            }
                            else{
                                $("#saveRouteModal").modal('hide');
                            }
 

                        }, onError: function onError() {
                            Joomla.renderMessages({
                            error: ['Something went wrong! Please close and reopen the browser and try again!']
                            });
                        }

                    });
                } catch (e) {
                    throw(e);
                }
    }
    </script>
    <script>
            function getItinerariesBy(itinerariesBy) {
            // Ajax to get content
            var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&itinerariesBy="+itinerariesBy + "&language=<?=  Factory::getLanguage()->getTag()?>";
            var url = baseUrl +  "&plugin=Planificador";

            try {
                    $.ajax({   
                    type: 'GET',
                    url: url,
                    dataType: 'json',
                    success: function(response) {  
                        itinerariesBy = response.data[0];
                        var itinerariesLayout = `<div class="tab-maps"><ul class="conexiones">`;
                        
                        itinerariesBy.forEach(function(itinerary){

                            
                            itinerariesLayout += ` <li><div class="col-2 col-sm-1 connection-type">`;

                            if(itinerary.catid == 61 || itinerary.catid == 123 || itinerary.catid == 124 ){
                                itinerariesLayout +=  `<i class="fa fa-walking"></i>`;
                            }
                            else if(itinerary.catid == 62 || itinerary.catid == 131 || itinerary.catid == 132 ){
                                itinerariesLayout +=  ` <i class="fa fa-bicycle"></i> / <i class="fa fa-horse"></i>`;
                            }
                            else if(itinerary.catid == 63 || itinerary.catid == 133 || itinerary.catid == 134 ){
                                itinerariesLayout +=  `  <i class="fa fa-truck-pickup"></i>`;
                            }

                            itinerariesLayout += `  </div>
                                                    <div class="col-10 col-sm-11">
                                                        <div class="link-routes">

                                                    <p class="conex">${itinerary.title}`;
                            if(itinerary.incidence != ""){
                                itinerariesLayout += ` <?= HTMLHelper::_('image', 'icon-warning.svg', Text::_("TPL_GESPLAN_INCIDENCE"), 'class="icon-warning"', true, 0) ?>`;
                            }
                            itinerariesLayout += `</p>
                                                    <a href="`;
                            itinerariesLayout += itinerary.link.substring(1);
                            itinerariesLayout +=    `"target="_blank" ><?= Text::_('TPL_GESPLAN_VIEW_ROUTE')?></a>
                                                        </div>
                                                    </div>
                                                </li>`;
                        })   
                        itinerariesLayout += `</ul></div>`;  
                        $('#itinerariesBy').empty();       
                        $('#itinerariesBy').append(itinerariesLayout);
                        distanceToStart = getDistance();
                        if(distanceToStart != -1){
                            $('.posicion').empty();
                            var distanceLayout = `<p><i class="fa fa-map-marker"></i> <?= Text::_("TPL_GESPLAN_YOU_ARE_AT")?> ${(distanceToStart/1000).toFixed(2)}km <?= Text::_("TPL_GESPLAN_FROM_START_POINT")?></p>
                                    <a href="https://www.google.com/maps/search/?api=1&query=${pointsList[0].marker.getLatLng().lat}%2C${pointsList[0].marker.getLatLng().lng}" target="_blank"><?= Text::_("TPL_GESPLAN_HOW_TO_GET_THERE")?></a> `;
                                    $('.posicion').append(distanceLayout);   
                                    ;   
                        }

                    }, onError: function onError() {
                        Joomla.renderMessages({
                        error: ['Something went wrong! Please close and reopen the browser and try again!']
                        });
                    }

                });
            } catch (e) {
                throw(e);
            }


        }

        function getInterestPointsBy(itinerariesBy) {
            // Ajax to get content
            var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&interestPointsBy="+itinerariesBy + "&language=<?=  Factory::getLanguage()->getTag()?>";
            var url = baseUrl +  "&plugin=Planificador";
            // var puntosInteres = [];
            try {
                $.ajax({   
                    type: 'GET',
                    url: url,
                    dataType: 'json',
                    success: function(response) {  
                        interestPoints = response.data[0];
                        puntosMarkers.clearLayers();
                        equipamientosMarkers.clearLayers();
                        var bounds = L.latLngBounds();

                        var marker = [];
                        if(interestPoints.length > 0){
                            $('.container.capas_equipamientos').show();
                        }
                        else{
                            $('.container.capas_equipamientos').hide();
                        }
                        interestPoints.forEach(fitInMap);

                        function fitInMap(item,id){
                        //Object.values(typeNatural).includes(2)
                            let lat_lng = [item.latitude, item.longitude];
    
                            marker[id] = new L.marker(lat_lng);
                            //Create puntos de interés markers with custom icon
                            if(item.typeIntrestingPoint != null){
                                if(Object.values(typeNatural).includes( Number(item.typeIntrestingPoint) )){
                                    marker[id] = new L.marker(lat_lng, {icon: pointsIcons.natural}).addTo(puntosMarkers);
                                }
                                else if(Object.values(typeCultural).includes( Number(item.typeIntrestingPoint) )){
                                    marker[id] = new L.marker(lat_lng, {icon: pointsIcons.cultural}).addTo(puntosMarkers);
                                }
                                else if(Object.values(typeGeneral).includes( Number(item.typeIntrestingPoint) )){
                                    marker[id] = new L.marker(lat_lng, {icon: pointsIcons.general}).addTo(puntosMarkers);
                                }
                            }
                            
                            //Create equipamientos markes with custom icons
                            if(item.typeEquipment != null){
                                if(item.typeEquipment == 1){
                                    marker[id] = new L.marker(lat_lng, {icon: servicesIcons.descanso}).addTo(equipamientosMarkers);
                                // markersServices.addLayer( marker[id]).addTo(descanso);
                                }
                                else if(item.typeEquipment == 2){
                                    marker[id] = new L.marker(lat_lng, {icon: servicesIcons.aparcamiento}).addTo(equipamientosMarkers);
                                    //markersServices.addLayer( marker[id]).addTo(aparcamiento);
                                }
                                else  if(item.typeEquipment == 3){
                                    marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.acampada}).addTo(equipamientosMarkers);
                                //markersEquip.addLayer( marker[id]).addTo(acampada);
                                }
                                else if(item.typeEquipment == 4){
                                    marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.recreativo}).addTo(equipamientosMarkers);
                                // markersEquip.addLayer( marker[id]).addTo(recreativo);
                                }
                                else if(item.typeEquipment == 5){
                                    marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.campamento}).addTo(equipamientosMarkers);
                                    //markersEquip.addLayer( marker[id]).addTo(campamento);
                                }
                                else if(item.typeEquipment == 6){
                                    marker[id] = new L.marker(lat_lng, {icon: equipmentsIcons.visitantes}).addTo(equipamientosMarkers);
                                    //markersEquip.addLayer( marker[id]).addTo(visitantes);
                                }  
                            }



                            marker[id].bindPopup(item.titleResult);


                        }   

                    }, onError: function onError() {
                        Joomla.renderMessages({
                        error: ['Something went wrong! Please close and reopen the browser and try again!']
                        });
                    }

                 });
            } catch (e) {
                throw(e);
            }


        }
        var controlElevation = undefined;
        var altitudeGain = 0;
        var altitudeLost = 0;
        var altitudeMax = 0;
        var altitudeMin = 0;
        function getRouteData(pointsList) {
            // Ajax to get content
            var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&myroutedata=1&type= "+type+"";
            var url = baseUrl +  "&plugin=Planificador";
            try {
                    $.ajax({   
                    type: 'POST',
                    url: url,
                    data: {'points': pointsList},
                    success: function(response) {  
                        linestring = response.data[0];
                        properties = myRoute.properties;
                        linestring.properties = properties
                        myRoute = linestring;
                        $('#distanceRoute').html( (myRoute.properties.distance/1000).toFixed(2) );
                       
                        echoRouteData(linestring);

 
                        $("#loader-icon").empty();
                        $('.route-path').show();
                        $('.save-route').removeClass('disabled');                             

                        
                    }, onError: function onError() {
                        Joomla.renderMessages({
                        error: ['Something went wrong! Please close and reopen the browser and try again!']
                        });
                    }

                });
            } catch (e) {
                throw(e);
            }


        }
        function echoRouteData(linestring){
            altitudeMin = 0;
            altitudeMax = 0;
            altitudeGain = 0;
            altitudeLost = 0;
            var geoJsonAltitude = linestring.features[0].geometry.coordinates;
            var altitudeZ = [];
            var startAltitude = geoJsonAltitude[0][2];
            $(geoJsonAltitude).each(function(key, coord) {

                altitudeZ.push(coord[2]);
                if(key > 0){
                    if(coord[2] > startAltitude){
                        altitudeGain += (coord[2] - startAltitude);
                    }
                    else if(coord[2] < startAltitude){
                        altitudeLost += (startAltitude - coord[2]);
                    }
                    startAltitude = coord[2]
                }
            });
            altitudeMin = Math.min(...altitudeZ).toFixed(2);
            altitudeMax = Math.max(...altitudeZ).toFixed(2);
            altitudeGain = altitudeGain.toFixed(2);
            altitudeLost = altitudeLost.toFixed(2);
            // put values in textbox
            $('#altitudeMax').html(altitudeMax);
            $('#altitudeMin').html(altitudeMin)
            $('#altitudeGain').html(altitudeGain);
            $('#altitudeLost').html(altitudeLost);
            //Prepare map and elevation profile
            var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
            var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<?= $mapbox_token ?>';
            //If elevation-map is already inizialized, we null it and empty
            var container = L.DomUtil.get('map-itinerario');
            $('#elevation-div').empty()
                if(container != null){
                    container._leaflet_id = null;
                }

            let maxHeigthAxis,minHeigthAxis;
            minHeigthAxis = parseInt(altitudeMin) - 50;
            maxHeigthAxis = parseInt(altitudeMax) + 200;
            let geoJson = JSON.stringify(linestring);
            let opts = {
                map: {
                    center: [28, -16],
                    zoom: 12,
                    scrollWheelZoom: scrollWheelZoom,
                    dragging: !L.Browser.mobile, 
                    tap: !L.Browser.mobile,
                },
                elevationControl: {
                    data: geoJson,
                    options: {
                        position: "bottomleft",
                        detached: true,
                        summary: false,
                        imperial: false,
                        altitude: true,
                        slope: false,
                        speed: false,
                        acceleration: false,
                        time: "summary",
                        legend: true,
                        followMarker: true,
                        almostOver: true,
                        distanceMarkers: true,
                        hotline: false,
                        closeBtn: false,
                        yAxisMax: maxHeigthAxis,
                        yAxisMin: minHeigthAxis,
                    },
                }
            };

            let elevMap = L.map('map-itinerario', opts.map);

            L.Control.Elevation.include({

                _customClear: function() {
                    this._clearChart();
                    this._clearMap();
                    this.clear();
                },

                _clearChart: function() {
                if (this._events && this._events.elechart_updated) {
                    this._events.elechart_updated.forEach(e => controlElevation.off('elechart_updated', e.fn, e.ctx));
                }
                if (this._chart && this._chart._container) {
                    this._chart._container.selectAll('g.point .point').remove();
                }
                },

                _clearMap: function() {
                    if (this._layers && this._layers.eachLayer) {
                        this._layers.eachLayer(l => l.remove())
                    }
                }

            });

            // Instantiate elevation control and then add to map.
            if (typeof controlElevation === 'undefined') {
                controlElevation = L.control.elevation(opts.elevationControl.options).addTo(elevMap);
                controlElevation.load(opts.elevationControl.data);
            }
            else{
            // Invoke your custom function
                controlElevation._customClear();
                controlElevation.load(opts.elevationControl.data);                        
                controlElevation.addTo(elevMap);
            }
        }
        function getDistance() {
            if(sessionStorage.getItem('userLat') && sessionStorage.getItem('userLon'))
            // return distance in meters
            var origin = new L.LatLng(sessionStorage.getItem('userLat'), sessionStorage.getItem('userLon'));
            var destination = pointsList[0].marker.getLatLng();
            var lon1 = toRadian(origin.lng),
                lat1 = toRadian(origin.lat),
                lon2 = toRadian(destination.lng),
                lat2 = toRadian(destination.lat);

            if (origin.distanceTo(destination)) {
                var deltaLat = lat2 - lat1;
                var deltaLon = lon2 - lon1;

                var a = Math.pow(Math.sin(deltaLat/2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon/2), 2);
                var c = 2 * Math.asin(Math.sqrt(a));
                var EARTH_RADIUS = 6371;
                return c * EARTH_RADIUS * 1000;
            }
            else{
                return -1;
            }
          
        }
        function toRadian(degree) {
            return degree*Math.PI/180;
        }
</script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
<script>
    //Route has to end at start point
    $("#flexCheckDefault").change(function() {
        if(this.checked) {
            if(pointsList.length > 0) {
                //if start is set, make it back to start
                makeCircularRoute();
            }       
        }     
    });
    $('#reset-planificador').on('click', function(){
        $('.draggables.planificador_ruta').empty();
        itemNumber = 1;
        cleanPreviousRoute();
        // map.setView(new L.LatLng(28.307706, -16.595535), 11);
        // let bounds = map.getBounds();
        // getData(bounds._northEast, bounds._southWest);
        if(type == 1){
            senderos.addTo(map);
        }
        else if(type == 2){
            bica.addTo(map);
        }
        else if(type == 3){
            vm.addTo(map);
        }
        map.setView(new L.LatLng(28.307706, -16.595535), 11);
    });
    var jsonLayer = L.geoJSON();

    $(document).ready(function () {
    <?php
        if(!isset($_POST['myRoute'])){
            echo "let bounds = map.getBounds();
            getData(bounds._northEast, bounds._southWest);
            map.addLayer(senderos);
            $('#walking').prop('checked', true);";
        };
    ?>
       
    })
    $("#motorcycle").click(function() {
        //Remove other layers and show current
        map.addLayer(vm)
        map.removeLayer(senderos).removeLayer(bica)
        $('.draggables.planificador_ruta').empty();
        itemNumber = 1;
        $('.new-point').hide();
        $('#walking, #horse, #biking').each(function(){
            $(this).prop('checked', false);
        });
        $('#sport').html('<?= Text::_("TPL_GESPLAN_MOTOR_VEHICLE")?>');
        $('#sportIcon').html('<i class="fa fa-motorcycle"></i>');
        cleanPreviousRoute();
        map.setView(new L.LatLng(28.307706, -16.595535), 11);

        //Set category type
        type = 3;

    });
    $("#horse").click(function() {
         //Remove other layers and show current
        map.addLayer(bica)
        map.removeLayer(senderos).removeLayer(vm)
        $('.draggables.planificador_ruta').empty();
        itemNumber = 1;
        $('.new-point').hide();
        $('#walking, #motorcycle, #biking').each(function(){
            $(this).prop('checked', false);
        });
        $('#sport').html('<?= Text::_("TPL_GESPLAN_HORSEING")?>');
        $('#sportIcon').html('<i class="fa fa-horse"></i>');
        cleanPreviousRoute();
        map.setView(new L.LatLng(28.307706, -16.595535), 11);

        //Set category type
        type = 2;
    });
    $("#biking").click(function() {
        //Remove other layers and show current
        map.addLayer(bica)
        map.removeLayer(senderos).removeLayer(vm)
        $('.draggables.planificador_ruta').empty();
        itemNumber = 1;
        $('.new-point').hide();
        $('#walking, #horse, #motorcycle').each(function(){
            $(this).prop('checked', false);
        });
        $('#sport').html('<?= Text::_("TPL_GESPLAN_BICKING")?>');
        $('#sportIcon').html('<i class="fa fa-biking"></i>');
        cleanPreviousRoute();
        map.setView(new L.LatLng(28.307706, -16.595535), 11);

        //Set category type
        type = 2;
    });
    $("#walking").click(function() {
        //Remove other layers and show current
        map.addLayer(senderos)
        map.removeLayer(bica).removeLayer(vm)
        $('.draggables.planificador_ruta').empty();
        itemNumber = 1;
        $('.new-point').hide();
        $('#motorcycle, #horse, #biking').each(function(){
            $(this).prop('checked', false);
        });
        $('#sport').html('<?= Text::_("TPL_GESPLAN_HIKING")?>');
        $('#sportIcon').html('<i class="fa fa-walking"></i>');
        cleanPreviousRoute();
        map.setView(new L.LatLng(28.307706, -16.595535), 11);

        //Set category type
        type = 1;
    });

    //Show / hide equipamientos layers
    $('input[name="capas_equipamientos"]').on('click', function(){

        if($(this).prop('checked') == false){
            map.removeLayer(eval(this.value));
        }else{
            map.addLayer(eval(this.value));
        }
    })
    function cleanPreviousRoute(){
        $("#livesearch-1").empty();
        $("#livesearch-2").empty();
        $("#inputText-1").val('');
        $("#inputText-2").val('');
        //remove all markers already set
        markers.clearLayers();
        equipamientosMarkers.clearLayers();
        puntosMarkers.clearLayers();
        //if first marker was set, we clean i in order to get new connected itineraries after the next marker is set
        if(firstMarker.getLatLng() != undefined) {
            firstMarker = new L.marker();
        }
        //remove connected itineraries
        if(connectedItineraries.getLayers().length > 0){
            map.removeLayer(connectedItineraries);  
            connectedItineraries = L.layerGroup();
        }
      
        //jsonLayer.addTo(map);
        //Remove previous planned routes
        if(plannedRoute != ""){
            map.removeLayer(plannedRoute);
        }
        pointsList = [];
        //Rebind onclick events
        // $("#start-point a").on('click', startPoint); 
        // $("#end-point a").on('click', endPoint); 
        //Empty roiute data
        $('#itinerariesBy').empty();  
        $('.posicion').empty();
        $("#end-point").show();
        $('.route-path').hide();
        $("#inputText-1").val('');
        $("#inputText-2").val(''); 

    }

    </script>
    <script>

        var xhr = $.ajax({});
        //Search equipments and interest points fotr search
        function showResult(str, num, input){
            var firstLat,firstLng;
            xhr.abort();
            xhr.readyState = 4;
           //if(input == 2){
            //If is first search we get all points, else we pass latlon to filter by connected itineraries
            if(firstMarker.getLatLng() != undefined){
                firstLat = firstMarker.getLatLng().lat;
                firstLng = firstMarker.getLatLng().lng;
            }
            else{ 
                firstLat = "";
                firstLng = "";
            }
            //}
            if (str.length == 0) {
                $("#livesearch-"+num+"").html();
                $("#livesearch-"+num+"").css('border', '0px');
                var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "&search=allsearchablefromhere&firstLat="+firstLat + "&firstLng="+firstLng + "&input="+input + "&type="+type + "&language=<?=  Factory::getLanguage()->getTag()?>";
                var url = baseUrl +  "&plugin=Planificador";

                try {
                    xhr =  $.ajax({   
                        type: 'GET',
                        url: url,
                        dataType: 'json',
                        success: function(response) {
                            resp = response.data[0];
                            $("#livesearch-"+num+"").empty();
                            if(resp.length > 0){
                               
                                resp.forEach(function(item){
                                    if(item.latitude){
                                        let image = JSON.parse(item.images).image_intro;
                                        if(image == null){
                                            image = "templates/gesplan/images/neutra-300.jpg";
                                        }
                                        option = `<a href="#" title="${item.titleResult}" data-point="${num}"
                                        data-latitude="${item.latitude}" data-longitude="${item.longitude}" onclick="setMarkerFromSearch(this)">
                                        <div class="row">
                                        <div class="col-3"><img src="${image}" class="imageSearch" style="width:60px"/> </div>
                                        <div class="col-9">${item.titleResult}</div>
                                        </div></a>`;
                                        $("#livesearch-"+num+"").append(option);
                                        if(item.titleResultFin){
                                            if(image == null){
                                                image = "templates/gesplan/images/neutra-300.jpg";
                                            }
                                            option = `<a href="#" title="${item.titleResultFin}" data-point="${num}"
                                            data-latitude="${item.latitudeFin}" data-longitude="${item.longitudeFin}" onclick="setMarkerFromSearch(this)">
                                            <div class="row">
                                            <div class="col-3"><img src="${image}" class="imageSearch" style="width:60px"/> </div>
                                            <div class="col-9">${item.titleResultFin}</div>
                                            </div></a>`;
                                            $("#livesearch-"+num+"").append(option);          
                                        }
                                    }
                                })
                                
                            }

                        }, onError: function onError() {
                            Joomla.renderMessages({
                            error: ['Something went wrong! Please close and reopen the browser and try again!']
                            });
                        }

                    });
                } catch (e) {
                    throw(e);
                }
                return;
            }
            if (str.length > 0) {
            var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "&search="+str + "&firstLat="+firstLat + "&firstLng="+firstLng + "&input="+input + "&type="+type + "&language=<?=  Factory::getLanguage()->getTag()?>";
                var url = baseUrl +  "&plugin=Planificador";

                try {
                    xhr = $.ajax({   
                        type: 'GET',
                        url: url,
                        dataType: 'json',
                        success: function(response) {
                            resp = response.data[0];
                            $("#livesearch-"+num+"").empty();
                            if(resp.length > 0){
                               
                                resp.forEach(function(item){
                                    if(item.latitude){
                                        let image = JSON.parse(item.images).image_intro;
                                        if(image == null){
                                            image = "templates/gesplan/images/neutra-300.jpg";
                                        }
                                        option = `<a href="#" title="${item.titleResult}" data-point="${num}"
                                        data-latitude="${item.latitude}" data-longitude="${item.longitude}" onclick="setMarkerFromSearch(this)">
                                        <div class="row">
                                        <div class="col-3"><img src="${image}" class="imageSearch" style="width:60px"/> </div>
                                        <div class="col-9">${item.titleResult}</div>
                                        </div></a>`;
                                        $("#livesearch-"+num+"").append(option);
                                        if(item.titleResultFin){
                                            if(image == null){
                                                image = "templates/gesplan/images/neutra-300.jpg";
                                            }
                                            option = `<a href="#" title="${item.titleResultFin}" data-point="${num}"
                                            data-latitude="${item.latitudeFin}" data-longitude="${item.longitudeFin}" onclick="setMarkerFromSearch(this)">
                                            <div class="row">
                                            <div class="col-3"><img src="${image}" class="imageSearch" style="width:60px"/> </div>
                                            <div class="col-9">${item.titleResultFin}</div>
                                            </div></a>`;
                                            $("#livesearch-"+num+"").append(option);          
                                        }
                                    }
                                })
                                
                            }

                        }, onError: function onError() {
                            Joomla.renderMessages({
                            error: ['Something went wrong! Please close and reopen the browser and try again!']
                            });
                        }

                    });
                } catch (e) {
                    throw(e);
                }
            }

        }
    </script>


    <?php
    $path = dirname(__FILE__) . "/partials/";
    //require_once($path.'prefooter.php');
    //require_once($path.'instagram.php');
    //require_once($path.'ayuda.php');
    //require_once($path.'colaboradores.php');
?>

<script>

    function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(savePosition);
    } else { 
        console.log("Geolocation is not supported by this browser");
    }
    }

    function savePosition(position) {
        sessionStorage.setItem('userLat', position.coords.latitude);
        sessionStorage.setItem('userLon', position.coords.longitude);
    }
    getLocation();

</script>
<?php
    $path = dirname(__FILE__) . "/partials/";
    require_once($path.'inspirador-part.php');
?>
<?php
    //If user not logged in display modal on save route click
    if($status){
        $path = dirname(__FILE__) . "/partials/";
        require_once($path.'modal-favorito.php');
    }
?>
<?php //View planned route when open from profile saved routes
    if(isset($_POST['myRoute'])){
        $myRoute = (json_decode($_POST['myRoute']));
($coords = json_decode($myRoute->route_geom)->features[0]->geometry->coordinates);
        if(!is_null($myRoute)){
            echo "<script>
            map.off('click');
            var siteUrl =  window.location.protocol+'//'+window.location.host;
            var markersIcons = {

                start : L.icon({ //3
                    iconUrl: siteUrl+'/templates/gesplan/images/i-start.png',
                    iconSize:     [26, 34], // size of the icon
                    iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                    popupAnchor: [0, -34]
                }),
                end : L.icon({
                    iconUrl: siteUrl+'/templates/gesplan/images/i-end.png',
                    iconSize:     [26, 34], // size of the icon
                    iconAnchor:   [13, 34], // point of the icon which will correspond to marker's location
                    popupAnchor: [0, -34]
                })
            };
            $('#planificador-options').hide();
            $('.save-route').hide();map.removeLayer(senderos);
            jsonLayer = L.geoJSON(".$myRoute->route_geom.");
            jsonLayer.addTo(map);$('.route-path').show();$('#distanceRoute').html('".($myRoute->route_distance)."');
            $('#route-title').append('<h1>".$myRoute->route_name."</h1>');
            itinerariesBy = '".($myRoute->route_itineraries)."';
            if(itinerariesBy != ''){
                getItinerariesBy(itinerariesBy);
            }
            echoRouteData(".$myRoute->route_geom.");
            var myRouteStart = new L.marker([".$coords[0][1].",".$coords[0][0]."], {icon: markersIcons.start}).bindPopup('".Text::_('TPL_GESPLAN_PLAN_ROUTE_START')."').openPopup().addTo(map);
            var myRouteEnd = new L.marker([".$coords[ count($coords)-1 ][1].",".$coords[count($coords)-1 ][0]."], {icon: markersIcons.end}).bindPopup('".Text::_('TPL_GESPLAN_PLAN_ROUTE_END')."').openPopup().addTo(map);
            map.fitBounds(jsonLayer.getBounds());</script>";
        }
    };
?>