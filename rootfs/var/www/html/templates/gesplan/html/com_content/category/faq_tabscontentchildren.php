<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;

$lang   = Factory::getLanguage();
$user   = Factory::getUser();
$groups = $user->getAuthorisedViewLevels();

if ($this->maxLevel != 0 && count($this->children[$this->category->id]) > 0) : ?>

	<?php foreach ($this->children[$this->category->id] as $id => $child) : ?>
		<?php // Check whether category access level allows access to subcategories. ?>
		<?php if (in_array($child->access, $groups)) : ?>
			<?php if ($this->params->get('show_empty_categories') || $child->numitems || count($child->getChildren())) : ?>
				<div id="category<?= $id ?>" class="card tab-pane fade <?= ($id ==0)?'show active':'' ?>" role="tabpanel" aria-labelledby="category<?= $id ?>-tab">
                    <div class="card-header" role="tab" id="heading-category<?= $id ?>">
                        <div class="mb-0">
                            <button data-bs-toggle="collapse" href="#collapse-category<?= $id ?>" aria-expanded="true" aria-controls="collapse-<?= $id ?>"  type="button" role="tab"  aria-selected="true" class="btn accordion-button"><?php echo $this->escape($child->title); ?></button>
                        </div>
                    </div>
                    <div id="collapse-category<?= $id ?>" class="collapse collapse__level <?= ($id ==0)?'show':'' ?>" data-bs-parent="#content" role="tabpanel" aria-labelledby="heading-collapse-category<?= $id ?>">
                        <div class="card-body">
							<div class="accordion" id="accordion-faq-<?= $id ?>">
								<?php 
									//Use JModelLegacy to get all articles within $categoryID
									$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
									$appParams = JFactory::getApplication()->getParams();
									$model->setState('params', $appParams);
									$model->setState('filter.category_id', $child->id); //change that to your Category ID
									$model->setState('list.ordering', 'created');
									$model->setState('list.direction', 'DESC');

									$articlesCategory =   $model->getItems();
										foreach ($articlesCategory as $item) {
										$this->item = & $item;
										echo $this->loadTemplate('item');
									}

								?>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endforeach; ?>

<?php endif;
