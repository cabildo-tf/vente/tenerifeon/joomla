<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;



// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);

?>


<?php if ($canEdit) : ?>
<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
<?php endif; ?>
<?php
    $user = JFactory::getUser();
    $status = $user->guest;
   ?>
<!-- loop start -->
<div class="col-md-6 col-lg-4 mb-5">
    <div class="card-enp-list">
        <a class="hyper-link"href="<?= Route::_(RouteHelper::getArticleRoute($this->item->id, $this->item->catid, $this->item->language));?>" ></a>
        <figure>
        <?php 
        //dd($this->item);
            $imageSet = [];
            $image = json_decode($this->item->images);
            //dd($image->image_intro);
            if(!empty($image->image_intro)):
                $imageSet['src'] = $image->image_intro;
                $imageSet['alt'] = $image->image_intro_alt;
            else :
                $imageSet['src'] = "templates/gesplan/images/neutra-900.jpg";
                $imageSet['alt'] = "Tenerife ON";
            endif;
        ?>
            <img src="<?= $imageSet['src'];?>" alt="<?= $imageSet['alt'];?>">
        </figure>
        <div class="enp-text-content">
            <p class="tipo-enp"><?= $this->item->jcfields[186]->rawvalue;?></p>
            <h2><?= $this->item->title?></h2>
            <div class="descripcion-enp-list"><?= !empty($this->item->introtext) && strlen($this->item->introtext) > 150 ? strstr(wordwrap($this->item->introtext, 150), "\n", true)."..." : $this->item->introtext;?></div>
        </div>
      
        <p class="link-enp-list"><?= Text::_("TPL_GESPLAN_MORE_INFO");?></p>
        
        
    </div>
</div>

<!-- end loop -->