<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
$category_title   = $this->escape($this->item->category_title);

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>

<div class="com-content-article item-page<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="https://schema.org/Article">
	<meta itemprop="inLanguage" content="<?php echo ($this->item->language === '*') ? Factory::getApplication()->get('language') : $this->item->language; ?>">
	<article class="news__view">
		<div class="container">
			<div class="row">
				<div class="col-lg-1">
                    <div class="news__view-date">
						<p>	
							<time datetime="<?php echo HTMLHelper::_('date', $this->item->publish_up, 'c'); ?>" itemprop="datePublished">
							<strong><?php echo HTMLHelper::_('date', $this->item->publish_up, 'j'); ?></strong><br />
							<?php echo HTMLHelper::_('date', $this->item->publish_up, 'M'); ?><br />
							<small><?php echo HTMLHelper::_('date', $this->item->publish_up, 'Y'); ?></small>
							</time></p>
					</div>
				</div>
				<div class="col-lg-10 news__view-content">
					<div class="news__view-content-info">
						<header>
							<p class="news__view-category"><a href="/index.php/<?= substr($this->item->parent_language,0 ,2)?>/<?= $this->item->parent_route;?>/<?= $this->item->category_alias;?>" title="Ir a la categoría Nombre de la Categoría"><?php echo $this->item->category_alias; ?></a></p>
							<h1><?php echo $this->escape($this->item->title); ?></h1>
							<h2><?php echo($this->item->jcfields[8]->value)?></h2>
						</header>
						
						<?php
                            $images         = json_decode($this->item->images); 
                        ?>
						<img src="<?= $images->image_fulltext;?>" alt="<?= $images->image_fulltext_alt;?>" class="img-fluid news__view-image" loading="lazy">

						<div class="news__view-text-featured">
							<?php echo $this->item->introtext; ?>
                        </div>
					</div>


					<!-- Gallery section -->
					<?php
						$path = dirname(__FILE__) . "/partials/";
						require_once($path.'gallery.php');
					?>

					<!-- End Gallery -->

					<div class="news__view-content-text">
						<?php echo $this->item->fulltext; ?>

						<div class="share-social">
                            <p class="head"><?= Text::_( 'TPL_GESPLAN_SHARE' );?></p>
							<div class="addthis_inline_share_toolbox"><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-621f488e60708602"></script></div>
                        </div>
					</div>
					<?php echo $this->item->event->beforeDisplayContent; ?>					
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="offset-lg-1 col-lg-8">
					<div class="news__view-content-tags">
						<?php $this->item->tagLayout = new FileLayout('joomla.content.tags'); ?>
						<?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
					</div>
				</div>
			</div>
		</div>
					
						<?php $url = '<a href="' . Route::_(
							RouteHelper::getCategoryRoute($this->item->catid, $this->item->category_language)
							)
							. '" itemprop="genre">' . $category_title . '</a>'; ?>
						<?php //echo $url ?>
						
			
					<?php if ((int) $params->get('urls_position', 0) === 0) : ?>
					<?php echo $this->loadTemplate('links'); ?>
					<?php endif; ?>
					<?php if ($params->get('access-view')) : ?>
						<?php
						if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && !$this->item->paginationrelative) :
							echo $this->item->pagination;
						endif;
						?>
						<?php if (isset ($this->item->toc)) :
							echo $this->item->toc;
						endif; ?>
						

						<?php if ($info == 1 || $info == 2) : ?>
							<!-- <?php if ($useDefList) : ?>
								<?php //echo LayoutHelper::render('joomla.content.info_block', array('item' => $this->item, 'params' => $params, 'position' => 'below')); ?>
							<?php endif; ?> -->
							<?php if ($params->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
								<?php $this->item->tagLayout = new FileLayout('joomla.content.tags'); ?>
								<?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
							<?php endif; ?>
						<?php endif; ?>

						<?php
						if (!empty($this->item->pagination) && $this->item->paginationposition && !$this->item->paginationrelative) :
							echo $this->item->pagination;
						?>
						<?php endif; ?>
						<?php if ((int) $params->get('urls_position', 0) === 1) : ?>
						<?php echo $this->loadTemplate('links'); ?>
						<?php endif; ?>
						<?php // Optional teaser intro text for guests ?>
						<?php elseif ($params->get('show_noauth') == true && $user->get('guest')) : ?>
						<?php //echo LayoutHelper::render('joomla.content.intro_image', $this->item); ?>
						<?php echo HTMLHelper::_('content.prepare', $this->item->introtext); ?>
						<?php // Optional link to let them register to see the whole article. ?>
						<?php if ($params->get('show_readmore') && $this->item->fulltext != null) : ?>
						<?php $menu = Factory::getApplication()->getMenu(); ?>
						<?php $active = $menu->getActive(); ?>
						<?php $itemId = $active->id; ?>
						<?php $link = new Uri(Route::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false)); ?>
						<?php $link->setVar('return', base64_encode(RouteHelper::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language))); ?>
						<?php echo LayoutHelper::render('joomla.content.readmore', array('item' => $this->item, 'params' => $params, 'link' => $link)); ?>
						<?php endif; ?>
					<?php endif; ?>
					<?php
					if (!empty($this->item->pagination) && $this->item->paginationposition && $this->item->paginationrelative) :
						echo $this->item->pagination;
					?>
					<?php endif; ?>
					<?php // Content is generated by content plugin event "onContentAfterDisplay" ?>
					
	</article>

	<!-- Related noticias -->

	<?php 
		//Use JModelLegacy to get all articles within $categoryID
		//var_dump($this->item);
		$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
		$app = JFactory::getApplication();
		$appParams = $app->getParams();
		$model->setState('params', $appParams);
		$model->setState('filter.category_id', $this->item->catid); //change that to your Category ID
		$model->setState('list.ordering', 'created');
		$model->setState('list.direction', 'DESC');
		$model->setState('filter.article_id', $app->input->get('id'));
		$model->setState('filter.article_id.include', false);
		$model->setState('filter.published', '1');
		$model->setState('list.limit', 2);


		$articlesCategory =   $model->getItems();
	?>
	<?php if(count($articlesCategory) ) : ?>
	<section class="general__information__related news">
			<div class="container">
				<div class="row">
					<div class="col offset-lg-1">
						<h3><?= Text::_( 'TPL_GESPLAN_RELATED' );?></h3>
					</div>
				</div>
				<div class="row">
					<?php 
						foreach ($articlesCategory as $item) {
							$this->item = & $item;
							echo $this->loadTemplate('item_main');
						}
					?>

				</div>
			</div>
	</section>
	<?php endif; ?>

</div>
