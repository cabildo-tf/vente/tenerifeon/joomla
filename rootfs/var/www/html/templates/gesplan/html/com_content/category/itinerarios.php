<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Categories\Categories;
use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Language\Associations;

?>
<?php
    $category = JTable::getInstance('category');
    //Set vehiculo motor alias to compare
    $category->load('63');
    $vm_es = $category->alias;
    $vm_es_title = $category->title;
    $category->load('133');
    $vm_de = $category->alias;
    $vm_de_title = $category->title;
    $category->load('134');
    $vm_en = $category->alias;
    $vm_en_title = $category->title;

    //Set bica alias to compare
    $category->load('62');
    $bica_es = $category->alias;
    $bica_es_title = $category->title;
    $category->load('131');
    $bica_de = $category->alias;
    $bica_de_title = $category->title;
    $category->load('132');
    $bica_en = $category->alias;
    $bica_en_title = $category->title;

    //Set bica alias to compare
    $category->load('61');
    $sendero_es = $category->alias;
    $sendero_es_title = $category->title;
    $category->load('124');
    $sendero_de = $category->alias;
    $sendero_de_title = $category->title;
    $category->load('123');
    $sendero_en = $category->alias;
    $sendero_en_title = $category->title;
    echo    "<script>   const vm_es = '".$vm_es."';
                        const vm_de = '".$vm_de."';
                        const vm_en = '".$vm_en."';

                        const bica_es = '".$bica_es."';
                        const bica_de = '".$bica_de."';
                        const bica_en = '".$bica_en."';

                        const sendero_es = '".$sendero_es."';
                        const sendero_de = '".$sendero_de."';
                        const sendero_en = '".$sendero_en."';

                        const vm_es_title = '".$vm_es_title."';
                        const vm_de_title = '".$vm_de_title."';
                        const vm_en_title = '".$vm_en_title."';

                        const bica_es_title = '".$bica_es_title."';
                        const bica_de_title = '".$bica_de_title."';
                        const bica_en_title = '".$bica_en_title."';

                        const sendero_es_title = '".$sendero_es_title."';
                        const sendero_de_title = '".$sendero_de_title."';
                        const sendero_en_title = '".$sendero_en_title."';</script>";

    if(isset($_GET['cat'])){
       $filteredCategory = $_GET["cat"];
        echo "<script>const filteredCategory = '".$filteredCategory."';var queryResults = ''; </script>";
        if( ($filteredCategory == $vm_es) || ($filteredCategory == $vm_de) || ($filteredCategory == $vm_en) ){
            echo "<script>const  itinerariosCategory = []; itinerariosCategory[0] = 63;</script>";
        }elseif( ($filteredCategory == $bica_es) || ($filteredCategory == $bica_de) || ($filteredCategory == $bica_en) ){
            echo "<script>const  itinerariosCategory = []; itinerariosCategory[0] = 62;</script>";
        }elseif( ($filteredCategory == $sendero_es) || ($filteredCategory == $sendero_de) || ($filteredCategory == $sendero_en) ){
            echo "<script>const  itinerariosCategory = []; itinerariosCategory[0] = 61;</script>";
        } 

    }else{
        echo "<script>const  itinerariosCategory = []; itinerariosCategory[0] = 38;var queryResults = '';</script>";
    }

?>
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="/templates/gesplan/js/gesplan/pagination.js?3e2be082abda46f8aa924a4a3c5ca486" data-asset-name="template.gesplan.pagination"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<?php   
    $user =   Factory::getUser();
    $status = $user->guest;
    $user_id = 0;
    if(!$status){
        $user_id = $user->id;
    }
?>
<script>

function setAddToFavouritesLink(el){
    console.log(el);
    item_id = $(el).data("itemid");
    action = $(el).data("action");
    addToFavourites(action,item_id);
}
    
//Get content by Ajax
function addToFavourites(user_id,item_id){

var baseUrl = "/index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&item=" + item_id;
console.log(baseUrl);

Joomla.request({
    url: baseUrl +  "&plugin=Addtofavourites",
    method: 'GET',
    perform: true,
    onSuccess: function onSuccess(resp) {
        const obj = JSON.parse(resp);
        if(obj.data[0].message == "ADDED"){
            $(".dropdown-item[data-itemid="+obj.data[0].item+"]").addClass('favorito-marked');
            $(".dropdown-item[data-itemid="+obj.data[0].item+"]").html('<?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?>');
            Swal.fire({
                    html: '<p>¡Guardado en favoritos!</p>',
                    confirmButtonText: 'Aceptar'
            })
        }
        else if(obj.data[0].message == "REMOVED"){
            $(".dropdown-item[data-itemid="+obj.data[0].item+"]").removeClass('favorito-marked');
            $(".dropdown-item[data-itemid="+obj.data[0].item+"]").html('<?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?>');
            Swal.fire({
                    html: '<p>¡Eliminado de favoritos!</p>',
                    confirmButtonText: 'Aceptar'
            })
        }
    },
    onError: function onError() {
        Joomla.renderMessages({
        error: ['Something went wrong! Please close and reopen the browser and try again!']
        });
    }
});


}
</script>
	<!-- Si hay articulos en la categoría/subcategoria pinta listado. 
	En la categoria padre(information) no hay articulos, entonces solamente pintará cuando entramos en una subcategoría -->
	<?php //if (!empty($this->items)) : ?>
		
		<section class="filtros-itinerarios  view_rezizer">
        <div class="map-mobil">
            <?php //echo $this->loadTemplate('offcanvas');?>
            <!-- <figure>
                <button class="open-rout-btn">1</button>
                <?= HTMLHelper::_('image', 'fake-map-filtros.png', 'Logo del cabildo', null, true, 0) ?>
            </figure> -->

        </div>
        <div class="filtros-mobil">
            
            <form>
                <div class="cont-btn-mobile">
                    <button class="show-filters delete-filters" onclick="cleanMobileFilters()" >
                    <?= Text::_("TPL_GESPLAN_DELETE") ?>
                    </button>
                    <button class="show-filters" onclick="getMobileContent(event)" id="searchMobile">
                    <?= Text::_("TPL_GESPLAN_APPLY") ?>
                    </button>
                </div>
                <ul class="general-ul">
                    
                   
                    <li>
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_TYPE_OF_ROUTE");?></legend>
                            <ul>
                                <?php       
                                    $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations(38)); //Category for Itinerarios
                                    $catLang =   Factory::getLanguage()->getTag() ;
                                    $catId = $associations[ $catLang];               
                                    $categories = \Joomla\CMS\Categories\Categories::getInstance('com_content');
                                    $cat        = $categories->get($catId);
                                    $routeType = $cat->getChildren();
                                    
                                ?>
                                <?php foreach($routeType as $index=>$route) : ?>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="mitinerariosCategory" value="<?= $route->id?>" id="mitinerariosCategory<?=($index+1)?>" data-title="<?=$route->title?>"data-alias="<?= $route->alias; ?>">
                                        <label class="form-check-label" for="mitinerariosCategory<?=($index+1)?>">
                                            <?= $route->title; ?>
                                        </label>
                                      </div>
                                </li>
                                <?php endforeach; ?>
                              
                            </ul>
                        </fieldset>
                    </li>
                    <script>
                        $('input[type=radio][name=mitinerariosCategory]').change(function() {
                            cleanMobileFilters();
                            alias = this.dataset.alias;
                            //window.location.search += '?cat=sendero';
                            window.history.pushState(null, null, "?cat="+alias);
                            //Show zones filter in mobile
                            $('.zones').show();

                            //Show filters depending on category and show/hide routes layers on map
                            if( ($(this).val() == '61') || ($(this).val() == '124') || ($(this).val() == '123') ){
                                $('.bica-senderos').show();
                                $('.senderos-only').show();
                                $('li#m1205').show();
                                map.addLayer(senderos)
                                map.removeLayer(vm).removeLayer(bica)
                            }else if( ($(this).val() == '62') || ($(this).val() == '131') || ($(this).val() == '132')){
                                $('.bica-senderos').show();
                                $('.senderos-only').hide();  
                                $('li#m1205').hide(); 
                                map.addLayer(bica)
                                map.removeLayer(senderos).removeLayer(vm)  
                            }else if( ($(this).val() == '63') || ($(this).val() == '133') || ($(this).val() == '134') ){
                                $('.bica-senderos').hide();
                                $('.senderos-only').hide();  
                                $('li#m1205').hide();
                                map.addLayer(vm)
                                map.removeLayer(senderos).removeLayer(bica)  
                            }

                            itinerariosCategory[0] = $("input[name='mitinerariosCategory']:checked").val();
                        });
                        function fillMobileQuery(){

                            url = window.location.href.split('&')[0];
                            window.history.pushState(null, null, url);
                            var modalidad =  $('input[name=mmodalidad]');
                            filterQuery['modalidad'] = [];
                            modalidad.each(function(){

                                var queryParam = (this.name).substring(1)+'='+this.dataset.alias;  
                                
                                if(this.checked){
                                    var refresh = window.location.href + '&'+queryParam+'';    
                                    window.history.pushState({ path: refresh }, '', refresh);
                                    filterQuery['modalidad'].push( this.value );
                                }
                            })
                            var distance =  $('input[name=mdistance]');
                            filterQuery['distance'] = [];

                            distance.each(function(){
                                var queryParam = (this.name).substring(1)+'='+this.dataset.alias;
                                
                                if(this.checked){
                                    var refresh = window.location.href + '&'+queryParam+'';    
                                    window.history.pushState({ path: refresh }, '', refresh);
                                    filterQuery['distance'].push( this.value );

                                }
                            })
                            var difficulty =  $('input[name=mdifficulty]');
                            filterQuery['difficulty'] = [];
                            difficulty.each(function(){
                                var queryParam = (this.name).substring(1)+'='+this.dataset.alias; 
                                
                                if(this.checked){
                                    var refresh = window.location.href + '&'+queryParam+'';    
                                    window.history.pushState({ path: refresh }, '', refresh);
                                    filterQuery['difficulty'].push( this.value );

                                }
                            })
                            var zones =  $('input[name=mzones]');
                            filterQuery['zones'] = [];
                            zones.each(function(){
                                var queryParam = (this.name).substring(1)+'='+this.dataset.alias;
                                
                                if(this.checked){
                                    var refresh = window.location.href + '&'+queryParam+'';    
                                    window.history.pushState({ path: refresh }, '', refresh);
                                    filterQuery['zones'].push( this.value );
                                }
                            })

                        };
                    
                        function getMobileContent(event){
                            fillMobileQuery();
                            event.preventDefault();

                            readUrlParameters();
                        }
                        function cleanMobileFilters(){
                            event.preventDefault();
                            
                            var url = window.location.href;
                            var anyParameters = url.indexOf('&');
                            if(anyParameters != -1){
                                url = url.slice( 0, url.indexOf('&') );
                                window.history.pushState(null, null ,url); 
                            }
                            readUrlParameters();

                        }
                        function showAllItineraries(){
                            event.preventDefault();
                            
                            var url = window.location.href;
                            var anyParameters = url.indexOf('?');
                            if(anyParameters != -1){
                                url = url.slice( 0, url.indexOf('?') );
                                window.history.pushState(null, null ,url); 
                            }
                            readUrlParameters();

                        }

                    </script>
                    <li class="bica-senderos">
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_ROUTE_SHAPE")?></legend>
                            <ul>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  type="radio" value="1" name="mmodalidad" id="mmodalidad1" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_CIRCULAR")) ?>">
                                        <label class="form-check-label" for="mmodalidad1">
                                            <?= Text::_("TPL_GESPLAN_CIRCULAR") ?>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  type="radio" value="2" name="mmodalidad" id="mmodalidad2" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_LINEAR")) ?>">
                                        <label class="form-check-label" for="mmodalidad2">
                                            <?= Text::_("TPL_GESPLAN_LINEAR") ?>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  type="radio" value="3" name="mmodalidad" id="mmodalidad3" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_GO_N_BACK")) ?>">
                                        <label class="form-check-label" for="mmodalidad3">
                                            <?= Text::_("TPL_GESPLAN_GO_N_BACK") ?>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </fieldset>
                    </li>
                    <li class="bica-senderos">
                        <fieldset>
                            <legend> <?= Text::_("TPL_GESPLAN_ROUTE_DISTANCE")?></legend>
                            <ul>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="1" name="mdistance" id="mdistance1"  data-alias="<?= ApplicationHelper::stringURLSafe("0-2 KM") ?>">
                                        <label class="form-check-label" for="mdistance1">
                                            0-2 KM
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="2" name="mdistance" id="mdistance2"  data-alias="<?= ApplicationHelper::stringURLSafe("2-5 KM") ?>">
                                        <label class="form-check-label" for="mdistance2">
                                            2-5 KM
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="3" name="mdistance" id="mdistance3"  data-alias="<?= ApplicationHelper::stringURLSafe("5-10 KM") ?>">
                                        <label class="form-check-label" for="mdistance3">
                                            5-10 KM
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="4" name="mdistance" id="mdistance4"  data-alias="<?= ApplicationHelper::stringURLSafe("10-30 KM") ?>">
                                        <label class="form-check-label" for="mdistance4">
                                            10-30 KM
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="5" name="mdistance" id="mdistance5"  data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_MORE_THAN")." 30 KM") ?>">
                                        <label class="form-check-label" for="mdistance5">
                                            <?= Text::_('TPL_GESPLAN_MORE_THAN')?> 30 KM
                                        </label>
                                    </div>
                                </li>
                            </ul>

                        </fieldset>
                    </li>
                    <li class="senderos-only">
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_ROUTE_DIFFICULTY")?></legend>
                            <ul>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="1"  name="mdifficulty" id="mdifficulty1" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_1"))?>">
                                        <label class="form-check-label" for="mdifficulty1">
                                            <?= Text::_("TPL_GESPLAN_DIFFICULTY_1")?>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="2"  name="mdifficulty" id="mdifficulty2" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_2"))?>">
                                        <label class="form-check-label" for="mdifficulty2">
                                            <?= Text::_("TPL_GESPLAN_DIFFICULTY_2")?>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="3"  name="mdifficulty" id="mdifficulty3" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_3"))?>">
                                        <label class="form-check-label" for="mdifficulty3">
                                        <?= Text::_("TPL_GESPLAN_DIFFICULTY_3")?>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="4"  name="mdifficulty" id="mdifficulty4" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_4"))?>">
                                        <label class="form-check-label" for="mdifficulty4">
                                        <?= Text::_("TPL_GESPLAN_DIFFICULTY_4")?>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="5"name="mdifficulty"  id="mdifficulty5" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_5"))?>">
                                        <label class="form-check-label" for="mdifficulty5">
                                        <?= Text::_("TPL_GESPLAN_DIFFICULTY_5")?>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </fieldset>
                    </li>
                    <li class="zones">
                        <fieldset>
                            <legend><?= Text::_("TPL_GESPLAN_LANDSCAPE_ZONES")?></legend>
                            <ul>
                            <?php     
                                    $categoryId = 98; //Zonas de tenerife category id
                                    $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                                    $appParams = JFactory::getApplication()->getParams();  
                                    $model->setState('params', $appParams);
                                    $model->setState('filter.category_id', $categoryId);
                                    $model->setState('filter.subcategories', true);
                                    $model->setState('filter.published', '1');
                                    $content =   $model->getItems();
                                        foreach($content  as $index => $item) : 
                                ?>
                                    <li id="m<?= $item->id?>">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="mzones" value="<?= $item->id?>" id="mzones<?= $index+1?>"  data-alias="<?= ApplicationHelper::stringURLSafe($item->alias)?>">
                                            <label class="form-check-label" for="mzones<?= $index+1?>">
                                           <?= $item->title ?>
                                            </label>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </fieldset>
                    </li>             
                </ul>
            </form>
        </div>
       <div class="cont-btn-mobile">
            <button class="show-filters">
                <?= Text::_("TPL_GESPLAN_SHOW_FILTERS")?>
            </button>
            <button class="show-map">
                <?= Text::_("TPL_GESPLAN_SHOW_MAP")?>
            </button>
            
       </div>
        <div class="container-fluid">
            <div class="row m-0">
                <div class="col-md-12">
                    <form class="m-0">
                        <div class="filter-row">
                            <div class="item-filters">
                            <?php
                                $categoryId = 38;
                                $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($categoryId)); 
                                $catLang =   Factory::getLanguage()->getTag() ;
                                $catId = $associations[ $catLang];
                                //Get num published rutas
                                $db = Factory::getDbo();
                                $query = $db->getQuery(true);
                                $query
                                ->select(array('COUNT(catid)'))
                                ->from($db->quoteName("#__content","c"))
                                ->join('INNER', $db->quoteName('#__categories', 'b') . ' ON ' . $db->quoteName('c.catid') . ' = ' . $db->quoteName('b.id'))
                                ->where($db->quoteName('b.parent_id') . ' = ' . $db->quote($catId))
                                ->where($db->quoteName('c.language') . ' = ' . $db->quote(Factory::getLanguage()->getTag()))
                                ->where($db->quoteName('c.state') . ' = ' . $db->quote('1'));
                                //->group($db->quoteName("c.catid"));

                                // Reset the query using our newly populated query object.
                                $db->setQuery($query);

                                // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                $results = $db->loadObjectList();
                                $numberOfroutes = $results[0]->count;
                                $numberPages = $numberOfroutes / 20;
                            ?>
                                <button onclick="showAllItineraries()" type="button"  ><?= Text::_("TPL_GESPLAN_SHOW_ALL")." ".$numberOfroutes." ".Text::_("TPL_GESPLAN_ROUTES")?></button>
                                <!-- //" ".count($this->items)." ";-->
                                
                            </div>
                            <script>
                                const language = '<?= $catLang?>';
                                var  filterQuery = {
                                    'modalidad': '',
                                    'distance': '',
                                    'difficulty': '',
                                    'zones': '',
                                };
    
                                $(document).ready(function () {
                                    $('.submits-container button').bind('click', function(event) {
                                        event.preventDefault()
                                    });
                                    $('.item-filters input[name="itinerariosCategory"]').on("change", function(){
                                        $('.zones').show();

                                        $('.item-filters ul[aria-labelledby="dropdownMenuButton1"]').removeClass("show");
                                        $('.item-filters button#dropdownMenuButton1').removeClass("show");
                                        
                                        alias = this.dataset.alias;
                                        window.history.pushState(null, null, "?cat="+alias);
                                        readUrlParameters();
                                    })
                                    $('.item-filters input[name="modalidad"]').on("change", function(){
                                        var queryParam = this.name+'='+this.dataset.alias;

                                       
                                        if(this.checked){
                                            var refresh = window.location.href + '&'+queryParam+'';    
                                            window.history.pushState({ path: refresh }, '', refresh);
                                        }else{
                                            var params = window.location.href.split('&');
                                            params.forEach(function(param){
                                                if(param == queryParam){
                                                    var re = new RegExp('&'+queryParam+'');
                                                    var newUrl= window.location.href;
                                                    window.history.pushState(null, null , newUrl.replace(re, ''));                                        

                                                }
                                            });
                                        } readUrlParameters();

                                    })
                                    $('.item-filters input[name="difficulty"]').on("change", function(){
                                        var queryParam = this.name+'='+this.dataset.alias;
                                       
                                        if(this.checked){
                                            var refresh = window.location.href + '&'+queryParam+'';    
                                            window.history.pushState({ path: refresh }, '', refresh);
                                        }else{
                                            var params = window.location.href.split('&');
                                            params.forEach(function(param){
                                                if(param == queryParam){
                                                    var re = new RegExp('&'+queryParam+'');
                                                    var newUrl= window.location.href;
                                                    window.history.pushState(null, null , newUrl.replace(re, '')); 
                                                }
                                            });
                                        } readUrlParameters();

                                    })
                                    $('.item-filters input[name="zones"]').on("change", function(){
                                        var queryParam = this.name+'='+this.dataset.alias;
                                       
                                        if(this.checked){
                                            var refresh = window.location.href + '&'+queryParam+'';    
                                            window.history.pushState({ path: refresh }, '', refresh);
                                        }else{
                                            var params = window.location.href.split('&');
                                            params.forEach(function(param){
                                                if(param == queryParam){
                                                    var re = new RegExp('&'+queryParam+'');
                                                    var newUrl= window.location.href;
                                                    window.history.pushState(null, null , newUrl.replace(re, '')); 
                                                }
                                            });
                                        } readUrlParameters();

                                    })
                                    $('.item-filters input[name="distance"]').on("change", function(){
                                        var queryParam = this.name+'='+this.dataset.alias;
                                       
                                        if(this.checked){
                                            var refresh = window.location.href + '&'+queryParam+'';    
                                            window.history.pushState({ path: refresh }, '', refresh);
                                        }else{
                                            var params = window.location.href.split('&');
                                            params.forEach(function(param){
                                                if(param == queryParam){
                                                    var re = new RegExp('&'+queryParam+'');
                                                    var newUrl= window.location.href;
                                                    window.history.pushState(null, null , newUrl.replace(re, '')); 
                                                }
                                            });
                                        } readUrlParameters();

                                    })
                                    $(window).on('popstate', function(event) {
                                        $('.item-filters .dropdown-menu').removeClass('show');
                                        readUrlParameters();

                                    });
                                })
                                function readUrlParameters(){
                                    //Search for query parameters
                                    var params = window.location.href.split('&');
                                    //If not query parameters, check if category is filtered
                                    if(params.length == 1){
                                        var params = window.location.href.split('?');
                                        //If not category filtered, print itinerarios from start
                                        if(params.length == 1){
                                            $.each($("input[name='itinerariosCategory']"), function(){
                                                this.checked = false;
                                            });
                                            filterQuery['modalidad'] = [];
                                            filterQuery['distance'] = [];
                                            filterQuery['difficulty'] = [];
                                            filterQuery['zones'] = [];
                                            $('.bica-senderos').hide();
                                            $('.senderos-only').hide();
                                            $('.zones').hide();
    ;
                                            itinerariosCategory[0] = 38;
                                            queryResults ="";
                                            map.addLayer(vm);  map.addLayer(bica);  map.addLayer(senderos);

                                        }//If category is filtered, query itineraries filteres
                                        else{
                                            queryResults = "";
                                            cat = params[1].substring(params[1].indexOf('=') + 1);
                                            if( (cat == vm_es) || (cat == vm_de) || (cat == vm_en) ){
                                                itinerariosCategory[0] = 63;
                                            }else if( (cat == bica_es) || (cat == bica_de) || (cat == bica_en) ){
                                                itinerariosCategory[0] = 62;
                                            }else if( (cat == sendero_es) || (cat == sendero_de) || (cat == sendero_en) ){
                                                itinerariosCategory[0] = 61;
                                            } 
                                            filterQuery['modalidad'] = [];
                                            filterQuery['distance'] = [];
                                            filterQuery['difficulty'] = [];
                                            filterQuery['zones'] = [];
                                            $('.form-check-input[type="checkbox"]').prop('checked', false);
                                            // setFiltersAndGetData(itinerariosCategory);

                                            queryResults = $('[data-alias="'+cat+'"]')[0].dataset.title + " | ";

                                        }
    
                                    }else if(params.length > 1){
                                        filterQuery['modalidad'] = [];
                                        filterQuery['distance'] = [];
                                        filterQuery['difficulty'] = [];
                                        filterQuery['zones'] = [];
                                        $('.zones').show();
                                        $('.bica-senderos .form-check-input').prop("checked",false);
                                        $('.senderos-only .form-check-input').prop("checked",false);
                                        $('.zones .form-check-input').prop("checked",false);
                                        queryResults = "";
                                        params.forEach(function(param){
                                            
                                            filterVariable = param.split('=');
                                            if(filterVariable[0] == 'modalidad'){
                                                modalidad = $('[data-alias="'+filterVariable[1]+'"]')[0].value;
                                                $('[data-alias="'+filterVariable[1]+'"]').prop("checked",true);
                                                filterQuery['modalidad'].push(modalidad);
                                            }
                                            else if(filterVariable[0] == 'distance'){
                                                distance = $('[data-alias="'+filterVariable[1]+'"]')[0].value;
                                                $('[data-alias="'+filterVariable[1]+'"]').prop("checked",true);
                                                filterQuery['distance'].push(distance);
                                            }
                                            else if(filterVariable[0] == 'difficulty'){
                                                difficulty = $('[data-alias="'+filterVariable[1]+'"]')[0].value;
                                                $('[data-alias="'+filterVariable[1]+'"]').prop("checked",true);
                                                filterQuery['difficulty'].push(difficulty);
                                            }
                                            else if(filterVariable[0] == 'zones'){

                                                zones = $('[data-alias="'+filterVariable[1]+'"]')[0].value;
                                                $('[data-alias="'+filterVariable[1]+'"]').prop("checked",true);
                                                filterQuery['zones'].push(zones);
                                            }
                                            let id_input = $('[data-alias="'+filterVariable[1]+'"]')[0].id;
                                            let text = $('label[for="'+id_input+'"]').html()
                                            queryResults += text + " | ";
                                            //queryResults += text.replace(/^\s+|\s+$/gm,''); + " | ";

                                        })

                                    }

                                    if($('[name="modalidad"]:checked').length == 0){
                                        $('.item-filters #modalidad').removeClass('active');
                                    }else{
                                        $('.item-filters #modalidad').addClass('active');
                                    };
                                    if($('[name="distance"]:checked').length == 0){
                                        $('.item-filters #distance').removeClass('active');
                                    }else{
                                        $('.item-filters #distance').addClass('active');
                                    };
                                    if($('[name="difficulty"]:checked').length == 0){
                                        $('.item-filters #difficulty').removeClass('active');
                                    }else{
                                        $('.item-filters #difficulty').addClass('active');
                                    };
                                    if($('[name="zones"]:checked').length == 0){
                                        $('.item-filters #zones').removeClass('active');
                                    }else{
                                        $('.item-filters #zones').addClass('active');
                                    };
                                    if($('[name="itinerariosCategory"]:checked').length == 0){
                                        $('.item-filters #dropdownMenuButton1').removeClass('active');
                                    }else{
                                        $('.item-filters #dropdownMenuButton1').addClass('active');
                                    }

                                    setFiltersAndGetData(itinerariosCategory);
                                  
 


                                }
                                function filterAjaxContent(el){
                                   
                                    var categories = [];
                                    $.each($("input[name='itinerariosCategory']:checked"), function(){
                                        categories.push($(this).val());
                                    });
                                    setFiltersAndGetData(categories);
                                  
                                }

                                function setFiltersAndGetData(categories){
                                        itinerariosCategory[0] = categories[0];

                                        //Set query to start filter again

                                        if( (itinerariosCategory[0] == 61) || (itinerariosCategory[0] == 123) || (itinerariosCategory[0] == 124)){
                                            $('.bica-senderos').show();
                                            $('.senderos-only').show();
                                            $('li#1205').show();
                                            map.addLayer(senderos)
                                            map.removeLayer(vm).removeLayer(bica)
                                        }
                                        else if( (itinerariosCategory[0] == 62) || (itinerariosCategory[0] == 131) || (itinerariosCategory[0] == 132) ){
                                            $('.bica-senderos').show();
                                            $('.senderos-only').hide();
                                            $('li#1205').hide();
                                            map.addLayer(bica)
                                            map.removeLayer(senderos).removeLayer(vm)
                                        }
                                        else if( (itinerariosCategory[0] == 63) || (itinerariosCategory[0] == 133) || (itinerariosCategory[0] == 134) ){
                                            $('.bica-senderos').hide();
                                            $('.senderos-only').hide();
                                            $('li#1205').hide();
                                            map.addLayer(vm)
                                            map.removeLayer(senderos).removeLayer(bica)
                                        }

                                    let bounds = map.getBounds();
                                    alrGivId = [];
                                    senderos.clearLayers();bica.clearLayers();vm.clearLayers();
                                    getData(bounds._northEast, bounds._southWest, itinerariosCategory);
                                    pagination.goToPage(1);
                                }
                                
                                function cleanCategory(el){
                                    $.each($("input[name='itinerariosCategory']:checked"), function(){
                                         $(this).prop("checked", false);

                                    });
                                    itinerariosCategory[0] = 38;
                                    readUrlParameters();
                                }
                                function cleanModalidad(el){
                                    $.each($("input[name='modalidad']:checked"), function(){
                                        queryParam = this.name+'='+this.dataset.alias;
                                        $(this).prop("checked", false);
                                        var params = window.location.href.split('&');
                                        params.forEach(function(param){
                                            if(param == queryParam){
                                                var re = new RegExp('&'+queryParam+'');
                                                var newUrl= window.location.href;
                                                window.history.pushState(null, null , newUrl.replace(re, ''));                                        
                                            }
                                        });

                                    });
                                    readUrlParameters();
                                } 
                                function cleanDistance(el){
                                    $.each($("input[name='distance']:checked"), function(){
                                        queryParam = this.name+'='+this.dataset.alias;
                                        $(this).prop("checked", false);
                                        var params = window.location.href.split('&');
                                        params.forEach(function(param){
                                            if(param == queryParam){
                                                var re = new RegExp('&'+queryParam+'');
                                                var newUrl= window.location.href;
                                                window.history.pushState(null, null , newUrl.replace(re, ''));                                        
                                            }
                                        });
                                    });
                                    readUrlParameters();
                                }  
                                function cleanDifficulty(el){
                                    $.each($("input[name='difficulty']:checked"), function(){
                                        queryParam = this.name+'='+this.dataset.alias;
                                        $(this).prop("checked", false);
                                        var params = window.location.href.split('&');
                                        params.forEach(function(param){
                                            if(param == queryParam){
                                                var re = new RegExp('&'+queryParam+'');
                                                var newUrl= window.location.href;
                                                window.history.pushState(null, null , newUrl.replace(re, ''));                                        
                                            }
                                        });
                                    });
                                    readUrlParameters();
                                }
                                function cleanZones(el){
                                    $.each($("input[name='zones']:checked"), function(){
                                        queryParam = this.name+'='+this.dataset.alias;
                                        $(this).prop("checked", false);
                                        var params = window.location.href.split('&');
                                        params.forEach(function(param){
                                            if(param == queryParam){
                                                var re = new RegExp('&'+queryParam+'');
                                                var newUrl= window.location.href;
                                                window.history.pushState(null, null , newUrl.replace(re, ''));                                        
                                            }
                                        });
                                    });
                                    readUrlParameters();
                                }      
                                  
                               
                            </script>
                            <div class="item-filters">
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_TYPE_OF_ROUTE")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <?php foreach($routeType as $index=>$route) : ?>
                                        <li>
                                        <div class="form-check">
                                                <input class="form-check-input category"  data-alias="<?= $route->alias; ?>" data-title="<?= $route->title; ?>"type="radio" name="itinerariosCategory" value="<?= $route->id; ?>" id="itinerariosCategory<?=($index+1)?>">
                                                <label class="form-check-label" for="itinerariosCategory<?=($index+1)?>">
                                                    <?= $route->title; ?>
                                                </label>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                        <div class="submits-container">
                                            <button type="submit" class="delete" onclick="cleanCategory(this)"><?= Text::_("TPL_GESPLAN_DELETE") ?></button>
                                            <!-- <button type="submit" id="filter-button" onclick="filterAjaxContent(this)" ><?= Text::_("TPL_GESPLAN_FILTER") ?></button> -->
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-filters bica-senderos">
                                <div class="dropdown">
                                    <button type="button" id="modalidad" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_ROUTE_SHAPE")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButtonModalidad">
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" name="modalidad" id="modalidad1" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_CIRCULAR")) ?>">
                                                <label class="form-check-label" for="modalidad1">
                                                    <?= Text::_("TPL_GESPLAN_CIRCULAR") ?>
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="2"  name="modalidad" id="modalidad2" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_LINEAR")) ?>">
                                                <label class="form-check-label" for="modalidad2">
                                                    <?= Text::_("TPL_GESPLAN_LINEAR") ?>
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="3" name="modalidad" id="modalidad3"  data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_GO_N_BACK")) ?>">
                                                <label class="form-check-label" for="modalidad3">
                                                    <?= Text::_("TPL_GESPLAN_GO_N_BACK") ?>
                                                </label>
                                            </div>
                                        </li>
                                        <div class="submits-container">
                                            <button type="submit" class="delete" onclick="cleanModalidad(this)"><?= Text::_("TPL_GESPLAN_DELETE") ?></button>
                                            <button><?= Text::_("TPL_GESPLAN_CLOSE") ?></button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-filters bica-senderos">
                                <div class="dropdown">
                                    <button type="button" id="distance" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_ROUTE_DISTANCE")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButtonModalidad">
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" name="distance" id="distance1"  data-alias="<?= ApplicationHelper::stringURLSafe("0-2 KM") ?>">
                                                <label class="form-check-label" for="distance1">
                                                    0-2 KM
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="2" name="distance" id="distance2"  data-alias="<?= ApplicationHelper::stringURLSafe("2-5 KM") ?>">
                                                <label class="form-check-label" for="distance2">
                                                    2-5 KM
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="3" name="distance" id="distance3"  data-alias="<?= ApplicationHelper::stringURLSafe("5-10 KM") ?>">
                                                <label class="form-check-label" for="distance3">
                                                    5-10 KM
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="4" name="distance" id="distance4"  data-alias="<?= ApplicationHelper::stringURLSafe("10-30 KM") ?>">
                                                <label class="form-check-label" for="distance4">
                                                    10-30 KM
                                                </label>
                                            </div>
                                        </li>                                   
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="5" name="distance" id="distance5"  data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_MORE_THAN")." 30 KM") ?>">
                                                <label class="form-check-label" for="distance5">
                                                    <?= Text::_('TPL_GESPLAN_MORE_THAN')?> 30 KM
                                                </label>
                                            </div>
                                        </li>
                                        <div class="submits-container">
                                            <button type="submit" class="delete" onclick="cleanDistance(this)"><?= Text::_("TPL_GESPLAN_DELETE") ?></button>
                                            <button><?= Text::_("TPL_GESPLAN_CLOSE") ?></button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-filters senderos-only">
                                <div class="dropdown">
                                    <button type="button" id="difficulty" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_ROUTE_DIFFICULTY")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"   name="difficulty" value="1" id="ddifficulty1" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_1"))?>">
                                                <label class="form-check-label" for="ddifficulty1">
                                                    <?= Text::_("TPL_GESPLAN_DIFFICULTY_1")?>
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"   name="difficulty" value="2" id="ddifficulty2" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_2"))?>">
                                                <label class="form-check-label" for="ddifficulty2">
                                                    <?= Text::_("TPL_GESPLAN_DIFFICULTY_2")?>
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"   name="difficulty" value="3" id="ddifficulty3" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_3"))?>">
                                                <label class="form-check-label" for="ddifficulty3">
                                                <?= Text::_("TPL_GESPLAN_DIFFICULTY_3")?>
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"   name="difficulty" value="4" id="ddifficulty4" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_4"))?>">
                                                <label class="form-check-label" for="ddifficulty4">
                                                <?= Text::_("TPL_GESPLAN_DIFFICULTY_4")?>
                                                </label>
                                            </div>
                                        </li>                                   
                                        <li>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"   name="difficulty" value="5" id="ddifficulty5" data-alias="<?= ApplicationHelper::stringURLSafe(Text::_("TPL_GESPLAN_DIFFICULTY_5"))?>">
                                                <label class="form-check-label" for="ddifficulty5">
                                                <?= Text::_("TPL_GESPLAN_DIFFICULTY_5")?>
                                                </label>
                                            </div>
                                        </li>
                                        <div class="submits-container">
                                            <button type="submit" class="delete" onclick="cleanDifficulty(this)"><?= Text::_("TPL_GESPLAN_DELETE") ?></button>
                                            <button><?= Text::_("TPL_GESPLAN_CLOSE") ?></button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-filters zones">
                                <div class="dropdown">
                                    <button type="button" id="zones" data-bs-toggle="dropdown" aria-expanded="false"><?= Text::_("TPL_GESPLAN_LANDSCAPE_ZONES")?></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <?php     
                                    $categoryId = 98; //Zonas de tenerife category id
                                    $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                                    $appParams = JFactory::getApplication()->getParams();  
                                    $model->setState('params', $appParams);
                                    $model->setState('filter.category_id', $categoryId);
                                    $model->setState('filter.subcategories', true);
                                    $model->setState('filter.published', '1');
                                    $content =   $model->getItems();
                                        foreach($content  as $index => $item) : 
                                ?>
                                    <li id="<?= $item->id?>">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="zones" value="<?= $item->id?>" id="dzonas<?= $index+1?>"  data-alias="<?= $item->alias?>">
                                            <label class="form-check-label" for="dzonas<?= $index+1?>">
                                           <?= $item->title ?>
                                            </label>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                                        <div class="submits-container">
                                        <button type="submit" class="delete" onclick="cleanZones(this)"><?= Text::_("TPL_GESPLAN_DELETE") ?></button>
                                        <button><?= Text::_("TPL_GESPLAN_CLOSE") ?></button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class=" col-lg-5 p-0 control-column">
                    <div id="itinerarios-column" class="itinerarios-column">
                    </div>
                </div>
                <!-- <div class="col-lg-7 p-0 d-none d-md-flex"> -->
                <div class="col-lg-7 p-0  d-md-flex control-column">
                    <div class="content-map-desktop">
					<?php echo $this->loadTemplate('offcanvas');?>
                    <?php
                        $articleId = 1156; //Guía de actividades al aire libre en Tenerife
                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
                        $articleLang = Factory::getLanguage()->getTag();
                        if(count($associations) > 0) {
                            $articleLang = Factory::getLanguage()->getTag();
                            $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                            $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                        }
                        else{
                            $articleLang = 'es-ES';
                            $idLang = $articleId;
                            $catidLang = 35; //Guía de actividades categoty id
                        }
                    ?>
                        <a href="<?php echo Route::_(RouteHelper::getArticleRoute( $idLang, $catidLang, $articleLang));?>" title="enalce planificador del itienerario X" class="btn-link-itinerario">
						<?= HTMLHelper::_('image', 'route-icon-btn.svg', Text::_("TPL_GESPLAN_ROUTE_ICON"), null, true, 0) ?>
                        <?= Text::_("TPL_GESPLAN_PLAN_YOUR_ROUTE")?>
                        </a>
                        <button id="openData" class="niceRotate"><i class="fa fa-chevron-left"></i></button>

                        <div id="map-equipamiento" style="height:100%;"></div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php
    //Get mapbox
    $config = Factory::getConfig();
        $mapbox_token = $config->get('mapbox_token');
    ?>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>

    <div class="container">
        <div id="pagination"></div>
    </div>
    <script>

        //New var to get all routes latlon to fit in map
        var routeMarkers = [];
        var pagination;

    function pageClick1(pageNumber) {

        $('.itinerarios-column').animate({
        scrollTop: 0 //#DIV_ID is an example. Use the id of your destination on the page
        }, 'slow');
        getAjaxContent(pageNumber, itinerariosCategory,filterQuery);

    }

    function pageClickAll(pageNumber) {
        $("#page-number-all").text(pageNumber);
    }

    $(document).ready(function () {

        var itemsCount = <?= $numberOfroutes?>;
        var itemsOnPage = 20;
        $('.bica-senderos').hide();
        $('.senderos-only').hide(); 
        $('.zones').hide(); 
        pagination = new Pagination({
            container: $("#pagination"),
            pageClickCallback: pageClick1,
            //maxVisibleElements: 3
    });


    pagination.make(itemsCount, itemsOnPage);



        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

       
    });


    /* Get first routes page */
    $(document).ready(function () {
        getAjaxContent(1, itinerariosCategory,filterQuery);
        if(itinerariosCategory[0] != 38){
            $('[data-alias="'+filteredCategory+'"]').prop('checked', true);
            $('.zones').show();

            //Show filters depending on category and show/hide routes layers on map
            filterQuery['modalidad'] ='';
            filterQuery['distance']= '';
            filterQuery['difficulty']= '';
            filterQuery['zones']= '';
            $('.zones').show();
            $('.bica-senderos .form-check-input').prop("checked",false);
            $('.senderos-only .form-check-input').prop("checked",false);
            $('.zones .form-check-input').prop("checked",false);
            readUrlParameters();
            //setFiltersAndGetData(itinerariosCategory);

        }
    })
    const routesForMap = [];
    //Get content by Ajax
    async function getAjaxContent(pageNum, categories, filterQuery){
        console.log(filterQuery);
        var filterCategories = categories.join();
        var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category=" + filterCategories + "&user=<?=$user->id?>" + "&page=" + pageNum+ "&modalidad=" + filterQuery['modalidad']+ "&distance=" + filterQuery['distance']+ "&difficulty=" + filterQuery['difficulty'] +"&zones=" +  filterQuery['zones'];
        $("#itinerarios-column").html('<div class="loader-cont"><div class="buble"></div><i class="fa fa-spin fa-circle-notch"></i><p>Obteniendo datos</p> </div>');
        await Joomla.request({
            url: baseUrl +  "&plugin=Listajaxcontent",
            method: 'GET',
            perform: true,
            onSuccess: function onSuccess(resp) {
                // Remove loader image
                var response = {};

                try {
                    response = JSON.parse(resp);

                } catch (e) {
                    return "No hay datos para mostrar";
                }

                var routesList = response.data[0].data;
                var itemsCount = response.data[0].total;

                //$('span#totalLength').html(totalLength);
                $('#itinerarios-column').empty()
                $('#itinerarios-column').append(' <h1 class="totalItemsSearch"><span id="totalLength">'+itemsCount+'</span> <?= Text::_('TPL_GESPLAN_ROUTES');?> </h1>')
                $('#itinerarios-column').append(' <p class="filter-results">'+ queryResults.slice(0, -2) +'</p> ')


                /* PINTAMOS LO EQUIPAMIENTOS */
                if(routesList.length){

                    const obj =(routesList);
                   
                    routesList.forEach(printDivContent);
                    function printDivContent(item, index) {
                        routesForMap[item.id] = {
                            title:item.title, 
                            itemid: item.id,
                            originalId: item.originalId,
                            favourite: item.favourite,
                            matricula:item.jcfields[140].rawvalue, 
                            showMatricula:item.jcfields[283].rawvalue, 
                            link: item.link,
                            text: item.introtext.substring(0, 80),
                            imageSrc: item.imageItem['src'],
                            imageAlt:item.imageItem['alt'],
                            incidence: item.incidence,
                            polyline: item.jcfields[145].rawvalue,
                            distance: item.jcfields[60].rawvalue,
                            duration: item.duration,
                            gainAlt: item.jcfields[63].rawvalue,
                            lostAlt: item.jcfields[64].rawvalue,
                            gallery: item.gallery}
                        var route = `
                                    
                                        <div id="${routesForMap[item.id].originalId}" class="row activeActions">
                                            <div class="col-3">
                                                <figure class="sizeingImg">
                                                <img src="/${routesForMap[item.id].imageSrc}" alt="${routesForMap[item.id].imageAlt}" loading="lazy">
                                            </figure>
                                            </div>
                                            <div class="col-9">
                                            <div class="item-itinerario">
                                                <div class="title-item">
                                                    <div class="dropdown">
                                                        <button type="button" class="badge" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                                                        <li><a id="addToFavourites" onclick="setAddToFavouritesLink(this)"`;
                                                        <?php if($status): ?>
                                                            route += `data-bs-toggle="modal" data-bs-target="#mustlog"`;
                                                    <?php endif; ?>
                                                            route += ` data-itemId="${routesForMap[item.id].itemid}"`;
                                                            route += `class="dropdown-item`;

                                                            if(routesForMap[item.id].favourite){
                                                                route += ` favorito-marked`;
                                                            }
                                                            route += `" href="#">`;
                                                            if(routesForMap[item.id].favourite){
                                                                route += `<?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?>`;
                                                            }
                                                            else{
                                                                route += `<?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?>`;
                                                            }
                                                            route += `</a></li>     
                                                            <!-- <li><a class="dropdown-item 
                                                            " href="#" data-title="${routesForMap[item.id].title}" data-itemid="${routesForMap[item.id].itemid}"><?= Text::_("TPL_GESPLAN_DOWNLOAD_GPX")?></a></li>-->
                                                            <li><a class="dropdown-item" href="${routesForMap[item.id].link.substring(1)}"><?= Text::_("TPL_GESPLAN_VIEW_FULL_DETAIL");?></a></li>
                                                        </ul>
                                                    </div>
                                                    <p><a href="${routesForMap[item.id].link.substring(1)}" title="${routesForMap[item.id].title}>" style="color:#4a503b">${routesForMap[item.id].title}</a>`;
                                if(routesForMap[item.id].showMatricula == 1){
                                    route += `<span class="badge">${routesForMap[item.id].matricula}</span>`;
                                }
                                route += `</p>
                                                </div>
                                                <div class="content-values">
                                                <div class="item-value">
                                                    <i class="fa fa-long-arrow-alt-right"></i>
                                                    <p class="value">${(routesForMap[item.id].distance/1000).toFixed(2)}km</p>
                                                </div>
                                                <div class="item-value">
                                                    <i class="far fa-clock"></i>
                                                    <p class="value">${routesForMap[item.id].duration}</p>
                                                </div>
                                                
                                                <div class="item-value">
                                                    <i class="fas fa-level-up-alt"></i>
                                                    <p class="value">${parseInt(routesForMap[item.id].gainAlt*1).toFixed(0)}m</p>
                                                </div>
                                                <div class="item-value">
                                                    <i class="fas fa-level-down-alt"></i>
                                                    <p class="value">${parseInt(routesForMap[item.id].lostAlt).toFixed(0)}m</p>
                                                    </div>`;
                                                if(item.incidence != ""){
                    
                                route +=            `<div class="item-value"><?= HTMLHelper::_('image', 'icon-warning.svg', Text::_("TPL_GESPLAN_INCIDENCE"), 'class="icon-warning"', true, 0) ?>
                                                        <p class="value"><?= Text::_('TPL_GESPLAN_INCIDENCE')?></p>
                                                    </div>`;
                                                }
                                route +=      `</div>
                                            </div>
                                        </div>
                                    </div>
                        `;
                        
                        $("#itinerarios-column").append(route);
                    }
                    $(".activeActions").hover(
                            function() {
                                var actualiIDs = $(this).attr("id");
                                senderos.getLayers().forEach(function(layer){
                                    layer.getLayers().forEach(function(layer2){
                                        if(layer2.feature.id == actualiIDs){
                                            layer2.setStyle({
                                                color: '#0d6efd',
                                                weight:6
                                            })
                                        }
                                    })
                                })
                                bica.getLayers().forEach(function(layer){
                                    layer.getLayers().forEach(function(layer2){
                                        if(layer2.feature.id == actualiIDs){
                                            layer2.setStyle({
                                                color: '#0d6efd',
                                                weight:6
                                            })
                                        }
                                    })
                                })
                                vm.getLayers().forEach(function(layer){
                                    layer.getLayers().forEach(function(layer2){
                                        if(layer2.feature.id == actualiIDs){
                                            layer2.setStyle({
                                                color: '#0d6efd',
                                                weight:6
                                            })
                                        }
                                    })
                                })
                            }, function() {
                                var actualiIDs = $(this).attr("id");
                                senderos.getLayers().forEach(function(layer){
                                    layer.getLayers().forEach(function(layer2){
                                        if(layer2.feature.id == actualiIDs){
                                            if(layer2.feature.properties.type == "1"){
                                                layer2.setStyle({
                                                    color: '#FF0000',
                                                    weight:3
                                                })
                                            }
                                            // else if(layer2.feature.properties.type == "2"){
                                            //     layer2.setStyle({
                                            //         color: '#FF8F0B',
                                            //         weight:3
                                            //     })
                                            // }
                                            // else if(layer2.feature.properties.type == "3"){
                                            //     layer2.setStyle({
                                            //         color: '#0B7CFF',
                                            //         weight:3
                                            //     })
                                            // }
                                        }
                                    })
                                })
                                bica.getLayers().forEach(function(layer){
                                    layer.getLayers().forEach(function(layer2){
                                        if(layer2.feature.id == actualiIDs){
                                            // if(layer2.feature.properties.type == "1"){
                                            //     layer2.setStyle({
                                            //         color: '#FF0000',
                                            //         weight:3
                                            //     })
                                            // }
                                            if(layer2.feature.properties.type == "2"){
                                                layer2.setStyle({
                                                    color: '#FF8F0B',
                                                    weight:3
                                                })
                                            }
                                            // else if(layer2.feature.properties.type == "3"){
                                            //     layer2.setStyle({
                                            //         color: '#0B7CFF',
                                            //         weight:3
                                            //     })
                                            // }
                                        }
                                    })
                                })
                                vm.getLayers().forEach(function(layer){
                                    layer.getLayers().forEach(function(layer2){
                                        if(layer2.feature.id == actualiIDs){
                                            // if(layer2.feature.properties.type == "1"){
                                            //     layer2.setStyle({
                                            //         color: '#FF0000'
                                            //     })
                                            // }
                                            // else if(layer2.feature.properties.type == "2"){
                                            //     layer2.setStyle({
                                            //         color: '#FF8F0B'
                                            //     })
                                            // }
                                            if(layer2.feature.properties.type == "3"){
                                                layer2.setStyle({
                                                    color: '#0B7CFF',
                                                    weight:3
                                                })
                                            }
                                        }
                                    })
                                })


                                
                            }
                        );
                }	
                else{
                    $('#itinerarios-column').append('<p>No hemos encotrado itinerarios para tus filtros de búsqueda</p>');
                }

                if(pageNum == 1) pagination.make(itemsCount, 20)
            },
            onError: function onError() {
                Joomla.renderMessages({
                error: ['Something went wrong! Please close and reopen the browser and try again!']
                });
            }
        });

    }
    </script>
    	<script src="/templates/gesplan/js/gesplan/leaflet.groupedlayercontrol.js"></script>

     <script>

        var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
        var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<?= $mapbox_token ?>';

        var tenerifeon = L.tileLayer(mbUrl, {id: 'tenerifeon/cl2pyxrtp001b15lfuq3x23z5', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
        var satellite = L.tileLayer(mbUrl, {id: 'tenerifeon/cl3rgf7dn000s14nyiiyuw8qm', tileSize: 512, zoomOffset: -1, attribution: mbAttr});

        var senderos = L.layerGroup();
        var bica = L.layerGroup();
        var vm = L.layerGroup();
        var incidences = L.layerGroup();


        var  scrollWheelZoom = true;
        //disable drag and wheel zoom in mobile
        var disableDragIfMobile = function() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        if(check){
            //map.dragging.disable();
            map.scrollWheelZoom.disable();
        }  
        };
        var map = L.map('map-equipamiento', {
            center: [28.291564, -16.629130],
            zoom: 10,
            minZoom: 10,
            scrollWheelZoom: scrollWheelZoom,
            detectRetina: true,
            layers: [tenerifeon, senderos,bica,vm,incidences]
        });
     
        var baseMaps = {
            'Vista Tenerifeon': tenerifeon,
            'Vista Satélite': satellite
        };
        var groupedOverlays = {
            "<strong>Incidencias</strong>": {
                "<?= Text::_("TPL_GESPLAN_INCIDENCES")?>": incidences
            }
        };
//        var layerControl = L.control.groupedLayers(baseMaps, groupedOverlays,{collapsed:false}).addTo(map);

        var layerControl = L.control.groupedLayers(baseMaps, groupedOverlays,{collapsed:false}).addTo(map);
        disableDragIfMobile();

        // map.on("overlayadd", function (event) {
        //     map.removeLayer(incidences);
        //     map.addLayer(incidences);

        // });

        var alrGivId = [];

        function getRanges(array) {
            var ranges = [],
                rstart, rend;
            for (var i = 0; i < array.length; i++) {
                rstart = array[i];
                rend = rstart;
                while (array[i + 1] - array[i] == 1) {
                    rend = array[i + 1]; // increment the index if the numbers sequential
                    i++;
                }
                ranges.push(rstart == rend ? rstart + '' : rstart + '-' + rend);
            }
            return ranges;
        }


            var _isProcessing = false;
            var _limit = false;


            async function getData(northEast, southWest, category = '') {
                if (1 == 1) {
                    _isProcessing = true;
                    var itineraryLayer = [];


                    // Ajax to get content
                    var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&category="+category[0]+"&getdata=1&modalidad=" + filterQuery['modalidad']+ "&distance=" + filterQuery['distance']+ "&difficulty=" + filterQuery['difficulty']+ "&zones=" + filterQuery['zones'];
                    var url = baseUrl +  "&plugin=Listajaxcontent";
                    try {
                        await $.ajax({
                            type: 'POST',
                            url: url,
                            data: {
                                "northEast": {
                                    "latitude": northEast.lat,
                                    "longitude": northEast.lng
                                },
                                "southWest": {
                                    "latitude": southWest.lat,
                                    "longitude": southWest.lng
                                },
                                "given": getRanges(alrGivId).join('_')
                            },
                            dataType: 'json',
                            success: function(response) {
                                response.data[0].data.forEach(function (itinerary){
                                    if(alrGivId.indexOf(itinerary.properties.geom_id) === -1 ){
                                        alrGivId.push(itinerary.properties.geom_id);
                                        if(itinerary.properties.type == "1"){
                                            L.geoJSON(itinerary, {color: '#FF0000',  onEachFeature: onEachFeature}).addTo(senderos);
                                        }
                                        else if(itinerary.properties.type == "2"){
                                            L.geoJSON(itinerary, {color: '#FF8F0B',  onEachFeature: onEachFeature}).addTo(bica);
                                        }
                                        else if(itinerary.properties.type == "3"){
                                            L.geoJSON(itinerary, {color: '#0B7CFF',  onEachFeature: onEachFeature}).addTo(vm);
                                        }
                                    } 
                                });

                                if (alrGivId.length >= response.data[0].total_count) {
                                    _limit = true;
                                }
    
                                function onEachFeature(feature, layer) {
                                    layer.on('mouseover', function () {
                                        // if(this.feature.properties.type == "1"){
                                        //     this.setStyle({
                                        //         //color: '#0d6efd',
                                        //         weight:6
                                        //     })
                                        // }
                                        this.setStyle({
                                                //color: '#0d6efd',
                                                weight:9
                                            })
                                    });
                                    layer.on('mouseout', function () {
                                        // if(this.feature.properties.type == "1"){
                                        //     this.setStyle({
                                        //         //color: '#ff0000',
                                        //         weight: 3
                                        //     })
                                        // }
                                        this.setStyle({
                                                //color: '#ff0000',
                                                weight: 3
                                            })
                                    });
                                    //bind click
                                    layer.on('click', function (e) {

                                    var baseUrl = "index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&item=" + layer.feature.id + "&language=" + language;

                                    Joomla.request({
                                        url: baseUrl +  "&plugin=Listajaxcontent",
                                        method: 'GET',
                                        perform: true,
                                        onSuccess: function onSuccess(resp) {
                                            // Remove loader image

                                            try {
                                                response = JSON.parse(resp);
                                                console.log(item = response.data[0]);
                                                $('.offCanvas-item .title .title').text(item.title);
                                                if(item.jcfields[283].rawvalue == 1){
                                                    $('.offCanvas-item .badge').text(item.jcfields[140].rawvalue);
                                                }else{
                                                    $('.offCanvas-item .badge').empty();
                                                }
                                                $('.offCanvas-item .description').text(item.text);
                                                $('.offCanvas-item .distance .value').text((item.jcfields[60].rawvalue/1000).toFixed(2)+'km');
                                                $('.offCanvas-item .time .value').text(item.duration); 
                                                $('.offCanvas-item .gainAlt .value').text(parseInt(item.jcfields[63].rawvalue).toFixed(0)+'m');
                                                $('.offCanvas-item .lostAlt .value').text(parseInt(item.jcfields[64].rawvalue).toFixed(0)+'m');
                                                $('.offCanvas-item .btn-ficha-completa').attr("href", item.link);
                                                if(item.gallery.length > 0){
                                                    $('.carousel-inner').empty();

                                                    item.gallery.forEach(printFigure);
                                                    function printFigure(image, index){
                                                        imageDiv = '<div class="carousel-item';
                                                        if(index == 0){
                                                            imageDiv += " active";
                                                        }
                                                        imageDiv += '"><img src="/'+image.src+'" class="d-block w-100" alt="'+image.alt+'" loading="lazy"></div>';
                                                        $('.carousel-inner').append(imageDiv);
                                                    }
                                                }
                                                else{
                                                    $('.carousel-inner').empty();
                                                }
                                                if(item.incidence != ""){
                                                    $(".offCanvas-item .item-value.alert").show();
                                                }
                                                else{
                                                    $(".offCanvas-item .item-value.alert").hide();
                                                }
                                                var addToFavButton = `<a class="dropdown-item`;
                                                if(item.favourite){
                                                    addToFavButton += ` favorito-marked`;
                                                }
                                                addToFavButton += `"`;
                                                <?php if($status): ?>
                                                    addToFavButton += ` data-bs-toggle="modal" data-bs-target="#mustlog"`;
                                                <?php endif; ?>
                                                addToFavButton += `href="#" data-itemId="${item.id}" title="añadir a mis rutas" onclick="setAddToFavouritesLink(this)">
                                                                            <i class="far fa-heart"></i>`;
                                                if(item.favourite){
                                                    addToFavButton += `<span><?= Text::_("TPL_GESPLAN_SAVED_IN_FAVOURITES");?></span>`;
                                                }
                                                else{
                                                    addToFavButton += `<span><?= Text::_("TPL_GESPLAN_SAVE_FAVOURITE");?></span>`;
                                                }
                                                addToFavButton += `</a>`;
                                                $('#addToFavouritesModal').html(addToFavButton);
                                                var downloadGpxLink = `<a class="dropdown-item" href="#" title="descargar itinerario gpx" onclick="downloadGpx(${item.id}, '${item.title}')"><i class="fa fa-cloud-download-alt"></i> <span><?= Text::_("TPL_GESPLAN_DOWNLOAD_GPX");?></span></a>`;
                                                $('#downloadGpxLink').html(downloadGpxLink);
                                                var downloadKmlLink = `<a class="dropdown-item" href="#" title="descargar itinerario kml" onclick="downloadKml(${item.id}, '${item.title}')"><i class="fa fa-cloud-download-alt"></i> <span><?= Text::_("TPL_GESPLAN_DOWNLOAD_KML")?></span></a>`;
                                                $('#downloadKmlLink').html(downloadKmlLink);
                                                $('.offCanvascontainer').addClass('active');
                                            } catch (e) {
                                            Joomla.renderMessages({
                                                error: [Joomla.Text._('MOD_SAMPLEDATA_INVALID_RESPONSE')]
                                            }, ".sampledata-steps-");
                                            return;
                                            }

                                                /*    */ 
                                        },
                                        onError: function onError() {
                                            Joomla.renderMessages({
                                            error: ['Something went wrong! Please close and reopen the browser and try again!']
                                            });
                                        }
                                    });
                                    });

                                }

                            },
                        });
                    } catch (e) {}

                    _isProcessing = false;
                }
            }

                map.on('moveend', function(ev) {
                    let bounds = map.getBounds();
                   getData(bounds._northEast, bounds._southWest, itinerariosCategory);
                });

                map.on('zoomend', function(ev) {
                    let bounds = map.getBounds();
                   getData(bounds._northEast, bounds._southWest, itinerariosCategory);
                });

                
    </script>
    <script>
$(document).ready(function () {
    let bounds = map.getBounds();
           getData(bounds._northEast, bounds._southWest, itinerariosCategory);
    getRoutesIncidences(); 
    //not allow to navigate out of tenerife bounds
    map.setMaxBounds([
        [28.748396571187406, -15.671997070312502],
        [27.831789750079267, -17.586364746093754]
    ]);

})
</script>

<section class="mas-valorados">
    <div class="container">
        <div class="row content-horizontal-scrol">
            <div class="col-md-12">
                <h2 class="head-scroll"><?= Text::_("TPL_GESPLAN_OTHER_ROUTES")?></h2>
            </div>
            <div class="col-md-12 padding-none">
                <div class="multiple-items">
	<?php 

        $categoryId = 38; //Irinerarios category id
        $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
        $appParams = Factory::getApplication()->getParams();  
        $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($categoryId)); 
        $catLang =   Factory::getLanguage()->getTag() ;
        $catId = $associations[ $catLang];               
        $model->setState('params', $appParams);
        $model->setState('filter.category_id', $catId); //change that to your Category ID
        $model->setState('list.ordering', Factory::getDbo()->getQuery(true)->rand());
        $model->setState('list.direction', 'ASC');
        $model->setState('filter.subcategories', true);
        $model->setState('filter.published', '1');
        $model->setState('list.limit', '8');
        $content =   $model->getItems();
        foreach($content as $item) : ?>
 
            <?php 

                    $fields = FieldsHelper::getFields('com_content.article', $item, true);
                    //create  article link
                    $articleLink = (RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));
                    $articleLink = Route::_($articleLink);
                    // Adding the fields to the object
                    $item->jcfields = array();

                    foreach ($fields as $key => $field)
                    {
                        if($field->id == 76){//ubicacion
                            $item->jcfields[$field->id] = $field;
                        }
                    }
                ?>

                <div class="card-default">
                    <button class="like-post"><i class="far fa-heart"></i></button>
                    <a class="hyper-link" href="<?php echo $articleLink; ?>"></a>
                    <figure>
                        <div class="gradient"></div>
                        <?php
                        $images = json_decode($item->images); 
                        if(!empty($images->image_intro)) {
                            $listImage['src'] = $images->image_intro ;
                            $listImage['alt'] = $images->image_intro_alt;
                        }else {
                            $listImage['src'] = "templates/gesplan/images/neutra-300.jpg";
                            $listImage['alt'] = "Tenerife ON";
                        };
                        ?>
                        <img src="<?php echo $listImage['src'] ; ?>" alt="<?php echo $listImage['alt'] ; ?>" loading="lazy">
                    </figure>
                    <div class="caption">
                        <?php if ($item->jcfields[76]->rawvalue): ?>
                            <div class="head-caption">
                                <span><?= $item->jcfields[76]->rawvalue; // ubicación?></span>
                            </div>
                        <?php endif; ?>
                        <div class="body-caption">
                            <h3><?php echo $item->title; ?></h3>
                        </div>
                        <div class="icons-caption">
                            <!-- Rating on article list view -->
                            <?php 
                            //Prepare icons for rating on articles
                            $iconStar     = HTMLHelper::_('image', 'plg_content_vote/vote-star.svg', '', '', true, true);
                            $iconHalfstar = HTMLHelper::_('image', 'plg_content_vote/vote-star-half.svg', '', '', true, true);
                            
                            // If you can't find the icons then skip it
                            if ($iconStar === null || $iconHalfstar === null)
                            {
                                return;
                            }
                            
                            // Get paths to icons
                            $pathStar     = JPATH_ROOT . substr($iconStar, strlen(Uri::root(true)));
                            $pathHalfstar = JPATH_ROOT . substr($iconHalfstar, strlen(Uri::root(true)));
                            
                            // Write inline '<svg>' elements
                            $star     = file_exists($pathStar) ? file_get_contents($pathStar) : '';
                            $halfstar = file_exists($pathHalfstar) ? file_get_contents($pathHalfstar) : '';

                            // Get rating
                            $db = Factory::getDbo();
                            $query = $db
                            ->getQuery(true)
                            ->select(array('SUM(rating) as rating, COUNT(id) as count'))
                            ->from($db->quoteName('#__sg_valoraciones'))
                            ->where($db->quoteName('item_id') . " = " . $db->quote($item->id));
                            // Reset the query using our newly populated query object.
                            $db->setQuery($query);
                            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                            $itemRating = $db->loadObjectList();

                            $rating = (float) $itemRating[0]->rating;
                            if($rating){
                                $rating = $rating / (int) $itemRating[0]->count;
                            }
                            $rcount = (int) $itemRating[0]->count;

                            // Round to 0.5
                            $rating = round($rating / 0.5) * 0.5;
                                // Determine number of stars
                                $stars = $rating;
                                $img   = '';

                                for ($i = 0; $i < floor($stars); $i++)
                                {
                                    $img .= '<li class="vote-star">' . $star . '</li>';
                                }

                                if (($stars - floor($stars)) >= 0.5)
                                {
                                    $img .= '<li class="vote-star-empty">' . $star . '</li>';
                                    $img .= '<li class="vote-star-half">' . $halfstar . '</li>';

                                    ++$stars;
                                }

                                for ($i = $stars; $i < 5; $i++)
                                {
                                    $img .= '<li class="vote-star-empty">' . $star . '</li>';
                                }

                                ?>
                                            <div class="content_rating" role="img" aria-label="<?php echo Text::sprintf('PLG_VOTE_STAR_RATING', $rating); ?>">
                                                <?php if ($rcount) : ?>
                                                    <p class="visually-hidden" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
                                                        <?php echo Text::sprintf('PLG_VOTE_USER_RATING', '<span itemprop="ratingValue">' . $rating . '</span>', '<span itemprop="bestRating">5</span>'); ?>
                                                        <meta itemprop="ratingCount" content="<?php echo $rcount; ?>">
                                                        <meta itemprop="worstRating" content="1">
                                                    </p>
                                                <?php endif; ?>
                                                <ul>
                                                    <?php echo $img; ?>
                                                </ul>
                                            </div> 

                                            <!-- /Rating -->
                                        
                                    </div>
                                </div>
                                <?php 
                                    $articleLink = (RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));
                                    $link = Route::_($articleLink);
                                ?>
                                <div class="link-card">
                                   <p> <?= Text::_("TPL_GESPLAN_MORE_INFO")?></p>
                                </div>
                            </div> 
        <?php endforeach; ?>
             
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- </div> -->
<?php
    $path = dirname(__FILE__, 2) . "/article/partials/";
    require_once($path.'modal-favorito.php');
?>