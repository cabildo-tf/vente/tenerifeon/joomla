<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Component\Menus\Administrator\Helper\MenusHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
<?php $db = Factory::getDbo(); ?>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<div class="com-content-article item-page<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="https://schema.org/Article">
	<meta itemprop="inLanguage" content="<?php echo ($this->item->language === '*') ? Factory::getApplication()->get('language') : $this->item->language; ?>">

	<section class="itinerarios">
        <div class="container mb-3">
            <div class="row">
                <div class="col-lg-8">
                    <h1><?php echo $this->escape($this->item->title); ?></h1>
                    <div class="subtitle">
                    <?php if($this->item->jcfields[283]->rawvalue) ://ocultar matricula?></p>
                        <p class="codigo-ruta"><?php echo($this->item->jcfields[140]->value) //Matricula?></p>
                    <?php endif; ?>
                        <p class="region"><?php echo($this->item->jcfields[76]->value) //Ubicación / Situación?></p>
                    </div>
                </div>
                <div class=" col-12 col-lg-4">
                    <div class="row">
                    <div class="container-actions">
                                <!-- <a><i class="far fa-comment"></i><span>3</span></a> -->
                                <?php   $user =   Factory::getUser();
                                        $status = $user->guest;
                                ?>
                                <?php   //Check if user has already masrked as favourite
                                        $query = $db
                                        ->getQuery(true)
                                        ->select(array('COUNT(id)'))
                                        ->from($db->quoteName('#__sg_favourites'))
                                        ->where($db->quoteName('user_id') . " = " . $db->quote($user->id))
                                        ->where($db->quoteName('item_id') . " = " . $db->quote($this->item->id));
                                        // Reset the query using our newly populated query object.
                                        $db->setQuery($query);
                                        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                        $isFavourite = $db->loadObjectList();
                                ?>
                                <button <?php if(!$status):?><?= 'id="addToFavourites"'?> data-itemId="<?= $this->item->id;?>" <?php else: ?> data-bs-toggle="modal" data-bs-target="#mustlog"<?php endif; ?><?php if($isFavourite[0]->count):?><?= 'class="favorito-marked"'?> data-action="remove"<?php else:?>data-action="add"<?php endif; ?>><i class="far fa-heart"></i><span><?= Text::_("TPL_GESPLAN_FAVOURITES");?></span></button>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                                <script>

                                        //if logged in
                                      
                                        $(document).ready(function () {
                                            $("#addToFavourites").click(function () {
                                                item_id = $(this).data("itemid");
                                                action = $(this).data("action");
                                                addToFavourites(action,item_id);
                                            });
                                        });
                                        //Get content by Ajax
                                        function addToFavourites(user_id,item_id){

                                            var baseUrl = "/index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&item=" + item_id + "&action=" + action;
                                            console.log(baseUrl);

                                            Joomla.request({
                                                url: baseUrl +  "&plugin=Addtofavourites",
                                                method: 'GET',
                                                perform: true,
                                                onSuccess: function onSuccess(resp) {
                                                    const obj = JSON.parse(resp);
                                                    if(obj.data[0].message == "ADDED"){
                                                        $("#addToFavourites").addClass('favorito-marked').data("action","remove");
                                                    }
                                                    else if(obj.data[0].message == "REMOVED"){
                                                        $("#addToFavourites").removeClass('favorito-marked').data("action","add");
                                                    }
                                                },
                                                onError: function onError() {
                                                    Joomla.renderMessages({
                                                    error: ['Something went wrong! Please close and reopen the browser and try again!']
                                                    });
                                                }
                                            });


                                        }
       


                                </script>
                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown"
                                        aria-expanded="false"><i class="fa fa-share-alt"></i></button>
                                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                                        <div class="addthis_inline_share_toolbox"><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-621f488e60708602"></script></div>
                                    </div>
                                </div>

                                <div class="dropdown">
                                    <button type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown"
                                        aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton2">
                                        <li><a class="dropdown-item downloadGpx" href="#"  onclick="downloadGpx(<?= $this->item->id?>, '<?= $this->item->title?>')" data-title="<?= $this->item->title?>" data-itemid="<?= $this->item->id?>"><?= Text::_("TPL_GESPLAN_DOWNLOAD_GPX")?></a></li>
                                        <li><a class="dropdown-item downloadKml" href="#"  onclick="downloadKml(<?= $this->item->id?>, '<?= $this->item->title?>')" data-title="<?= $this->item->title?>" data-itemid="<?= $this->item->id?>"><?= Text::_("TPL_GESPLAN_DOWNLOAD_KML")?></a></li>
                                        <li><a class="dropdown-item" href="#" onclick="window.print();"><?= Text::_("TPL_GESPLAN_PRINT")?></a></li>
                                    </ul>
                                </div>
                            </div>
                     </div>
                     <div id="content-valoration" class="head-ficha">
                        <?php echo $this->item->event->beforeDisplayContent; //Displays ratings ?>
                    </div>
                </div>
               
            </div>
        </div>
        <!-- Alertas -->
       <!-- Alertas -->
        <script>
            var incidences = [];
        </script>
       <?php 
    //Check if this itinerario has incidencias

        $incidencias_in_itinerario =  explode(',',$this->item->jcfields[253]->rawvalue);

        foreach ($incidencias_in_itinerario as $incidencia) : ?>

            <?php

            if(!is_null($incidencia) && $incidencia != '') : 
                $db = Factory::getDbo();
                $query = $db
                ->getQuery(true)
                ->select('state')
                ->from($db->quoteName('#__content'))
                ->where($db->quoteName('id') . " = " . $db->quote($incidencia));
                // Reset the query using our newly populated query object.
                $db->setQuery($query);
                // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                $indicendeIsActive = $db->loadResult();

                if($indicendeIsActive == 1):?>
                    <?php
                        $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                        $appParams = JFactory::getApplication()->getParams();  
                        $model->setState('params', $appParams);
                        $model->setState('filter.article_id', $incidencia); //Banner home category ID
                        
                        $helpArticle =   $model->getItems();
                        $incidencia = $helpArticle[0];
                        $fields = FieldsHelper::getFields('com_content.article', $incidencia, true);
                        // Adding the fields to the object
                        $incidencia->jcfields = array();
                
                        foreach ($fields as $key => $field)
                        {
                            $incidencia->jcfields[$field->id] = $field;
                        }
                    //dd($incidencia);
                    ?>
                    <script> //Get incidence linestring

                        incidences.push(JSON.parse('<?= $incidencia->jcfields[251]->rawvalue;?>'));


                    </script>
                    <div class="alertas">
                        <div class="container">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <div class="icon-alerta">
                                        <?= HTMLHelper::_('image', 'icon-warning.svg', Text::_("TPL_GESPLAN_INCIDENCE"), 'class="icon-warning"', true, 0) ?>
                                        <span class="alerta"><?= Text::_("TPL_GESPLAN_INCIDENCE");?></span>
                                    </div>
                                </div>
                                <div class="col-9 col-md-9 col-alert">
                                    <p><?= $incidencia->title;?></p>
                                    <p><?= $incidencia->jcfields[236]->rawvalue; //descripcion-incidencia field?></p>
                                    <?php if(!empty($incidencia->jcfields[247]) && !is_null($incidencia->jcfields[247])): ?>
                                    <a title="Enlace a la alerta x" href=""><?=$incidencia->jcfields[247]->rawvalue ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <!-- //Alertas -->
        <div class="container-itinerario">
        <!-- Get images from galeria (cf 107) -->
            <?php if(!empty($this->item->jcfields[107]->value)) :?>
            <?php $images = ($this->item->jcfields[107]->subform_rows) ?>

            <?php if(isset($images)) :?>
                <div class="slick-itinerarios">
                    <?php foreach($images as $image) : ?>
                        <div>
                            <?php  $imgUrl = $image["imagen"]->rawvalue["imagefile"]; ?>
                            <?php if(!empty($imgUrl)) : ?>
                                <figure><img src="<?php echo $imgUrl;?>" alt="<?php  echo($image["imagen"]->rawvalue["alt_text"]); ?>" loading="lazy"></figure>
                            <?php endif; ?>                               
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="modal fade" id="accesibilidadModal" tabindex="-1" aria-labelledby="accesibilidadModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <p class="modal-title"><?= Text::_("TPL_GESPLAN_ACCESIBILITY_".$this->item->jcfields[209]->rawvalue[0].""); ?></p>
                        </div>
                        <div class="modal-body">
                            <p> <?= $this->item->jcfields[231]->rawvalue; ?></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="close-modal" data-bs-dismiss="modal"><?= text::_('TPL_GESPLAN_CLOSE_MODAL');?></button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container-tags-itinerarios">
            <div class="tag-equipamiento disabled">
                <?php if( ($this->item->jcfields[231]->rawvalue != "")) : //Accesibilidad?>
                        <?php if( $this->item->jcfields[209]->rawvalue[0]== 1){
                            $accesClass = "accesible";
                        }elseif( $this->item->jcfields[209]->rawvalue[0]== 3){
                            $accesClass = "parcialmente-accesible";
                        }
                        elseif( $this->item->jcfields[209]->rawvalue[0]== 2){
                            $accesClass = "inaccesible";
                        } else{
                            $accesClass = "grey";  
                        }
                        ?>
                        <?php if($this->item->jcfields[209]->rawvalue[0] != 0) : ?>
                            <button type="button" class="<?php echo $accesClass?>" data-bs-toggle="modal" data-bs-target="#accesibilidadModal">
                            <i class="fas fa-wheelchair"></i><?= Text::_("TPL_GESPLAN_ACCESIBILITY_".$this->item->jcfields[209]->rawvalue[0].""); ?>
                            </button> 
                        <?php endif; ?>
                <?php elseif($this->item->jcfields[209]->rawvalue[0] != 0) : ?>
                    <i class="fas fa-wheelchair"></i><p><?= Text::_("TPL_GESPLAN_ACCESIBILITY_".$this->item->jcfields[209]->rawvalue[0].""); ?></p>
                <?php endif; ?>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-bubles">
                        <?php 
                                //Check type of itinerary
                                $clase_recorrido_field = 139;//Clase de recorrido field
                                $query = $db
                                ->getQuery(true)
                                ->select('value')
                                ->from($db->quoteName('#__fields_values'))
                                ->where($db->quoteName('field_id') . " = " . $db->quote($clase_recorrido_field)) 
                                ->where($db->quoteName('item_id') . " = " . $db->quote($this->item->catid)); //id category
                                // Reset the query using our newly populated query object.
                                $db->setQuery($query);
                                $clase_recorrido = $db->loadResult();
                        ?>
                            <div data-bs-toggle="tooltip" data-bs-placement="top" title="Itinerario a pie" class="buble-tags <?= $clase_recorrido == "1" ? "active" : ""?>">
                                <i class="fa fa-walking"></i>
                            </div>
                            <div class="buble-tags <?= $clase_recorrido == "2" ? "active" : ""?>"data-bs-toggle="tooltip" data-bs-placement="top" title="Itinerario en bicicleta">
                                <i class="fa fa-bicycle"></i>
                            </div>
                            <div class="buble-tags <?= $clase_recorrido == "2" ? "active" : ""?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Itinerario a caballo">
                                <i class="fa fa-horse"></i>
                            </div>
                            <div class="buble-tags <?= $clase_recorrido == "3" ? "active" : ""?>"data-bs-toggle="tooltip" data-bs-placement="top" title="Itinerario en vehículo a motor">
                                <i class="fa fa-truck-pickup"></i>
                            </div>
                        </div>
                        <div class="container-tags">
                            <!-- <div class="tag-itinerarios">
                                <i class="fas fa-child"> </i>
                                <p>Apta para niños</p>
                            </div>
                            <div class="tag-itinerarios">
                                <i class="fa fa-mountain"> </i>
                                <p>Vistas increíbles</p>
                            </div> -->
                            <?php if($this->item->jcfields[206]->rawvalue[0] != 0) ://Dificultad ?>
                            <div class="tag-itinerarios">
                                <i class="fas fa-hiking"> </i>
                                <p> <?= Text::_("TPL_GESPLAN_DIFFICULTY").": ". Text::_("TPL_GESPLAN_DIFFICULTY_".$this->item->jcfields[206]->rawvalue[0].""); ?></p>
                            </div>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-itinerario">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="responsive-tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <button class="nav-link active" id="faqtabs-category1" data-bs-toggle="tab"
                                            data-bs-target="#category1" type="button" role="tab" aria-controls="home"
                                            aria-selected="true"><?= Text::_("TPL_GESPLAN_DESCRIPTION");?></button>
                                    </li>
                                    <li class="nav-item">
                                        <button class="nav-link" id="faqtabs-category2" data-bs-toggle="tab"
                                            data-bs-target="#category2" type="button" role="tab" aria-controls="home"
                                            aria-selected="false"><?= Text::_("TPL_GESPLAN_TIPS");?> </button>
                                    </li>
                                    <li class="nav-item">
                                        <button class="nav-link" id="faqtabs-category3" data-bs-toggle="tab"
                                            data-bs-target="#category3" type="button" role="tab" aria-controls="home"
                                            aria-selected="false"><?= Text::_("TPL_GESPLAN_GUIDES");?> </button>
                                    </li>
                                </ul>

                                <div id="content" class="tab-content" role="tablist">
                                    <div id="category1" class="card tab-pane fade show active flex-column" role="tabpanel"
                                        aria-labelledby="category1-tab">

                                        <h2><?= Text::_("TPL_GESPLAN_DESCRIPTION");?></h2>
                                        <?php   
                                                if(!empty($this->item->fulltext)){
                                                    echo $this->item->fulltext;
                                                }else{
                                                    echo $this->item->text;
                                                }
                                        ?>

                                        <!-- <a href="">Más información</a> -->

                                    </div>

                                    <div id="category2" class="card tab-pane fade" role="tabpanel"
                                        aria-labelledby="category2-tab">
                                        <h2><?= Text::_("TPL_GESPLAN_TIPS");?></h2>
                                       <?= $this->item->jcfields[199]->value; //Consejos?>
                                       <?php   //Get association of article Guia 
                                            if( ($this->item->catid == 61) || ($this->item->catid == 123) || ($this->item->catid == 124) ){
                                                $articleId = 1197; //Recomendaciones para caminantes
                                            }else if( ($this->item->catid == 62) || ($this->item->catid == 131) || ($this->item->catid == 132) ){
                                                $articleId = 1194; //Recomendaciones para ciclistas y jinetes
                                            }else if( ($this->item->catid == 63) || ($this->item->catid == 133) || ($this->item->catid == 134) ){
                                                $articleId = 1248; //Buenas prácticas para vehículos a motor en Tenerife
                                            } 
                                            ($associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId));
                                            $articleLang = Factory::getLanguage()->getTag();
                                            if(count($associations) > 0) {
                                                $articleLang = Factory::getLanguage()->getTag();
                                                $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                                                $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                                            }
                                            else{
                                                $articleLang = 'es-ES';
                                                $idLang = $articleId;
                                                $catidLang = 69; //Guía de actividades categoty id
                                            }
                                        ?>
                                        <a href="<?php echo Route::_(RouteHelper::getArticleRoute($idLang, $catidLang, $articleLang));?>"><?= Text::_("TPL_GESPLAN_GUIDE");?></a>
                                    </div>

                                    <div id="category3" class="card tab-pane fade" role="tabpanel"
                                        aria-labelledby="category3-tab">
                                        <h2><?= Text::_("TPL_GESPLAN_GUIDES");?></h2>
                                        <?php if(!empty($this->item->jcfields[278]->value) ):?>
                                            <?= $this->item->jcfields[278]->value;?>
                                        <?php else : ?>
                                            <p><?= Text::_('TPL_GESPLAN_NO_DATA_AVAILABLE')?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="permisos">
                                <h2><?= Text::_("TPL_GESPLAN__PERMISSIONS");?></h2>
                               <?= $this->item->jcfields[198]->value; //Autorizaciones?>
                               <?php 
                                    //Display more info link if not BICA category
                                    if( ($this->item->catid != 62) && ($this->item->catid != 131) && ($this->item->catid != 132) ):
                                    //Get association of article Guia 
                                        $articleId = 580;
                                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
                                        $articleLang = Factory::getLanguage()->getTag();
                                        if(count($associations) > 0) {
                                            $articleLang = Factory::getLanguage()->getTag();
                                            $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                                            $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                                        }
                                        else{
                                            $articleLang = 'es-ES';
                                            $idLang = $articleId;
                                            $catidLang = 69;
                                        }
                                ?>
                                <a href="<?php echo Route::_(RouteHelper::getArticleRoute( $idLang, $catidLang, $articleLang ));?>"><?= Text::_("TPL_GESPLAN_MORE_INFO");?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
                <div class="container container-itinerario mt-5">
                    <div class="row">
                        <div class="col-md-7">
                            <div>
                                <h2><?= Text::_("TPL_GESPLAN_TECHNICAL_INFO") ?></h2>
                                <!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    invidunt ut labore et dolore magna aliquyam:</p> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-7 mb-4">
                            <div class="ficha-tecnica-itinerario">
                                <h3><?= Text::_("TPL_GESPLAN_ROUTE_INFO") ?></h3>
                                <!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    invidunt ut labore et dolore magna aliquyam:</p> -->

                                <div class="header">
                                    <?php if($this->item->catid == "61" || $this->item->catid == "123" || $this->item->catid == "124"):?>
                                        <div class="buble-tags ficha active">
                                            <i class="fa fa-walking"></i>
                                        </div>
                                        <h4><?= Text::_("TPL_GESPLAN_WALKING") ?></h4>
                                    <?php elseif($this->item->catid == "62" || $this->item->catid == "131" || $this->item->catid == "132") : ?>
                                        <div class="buble-tags <?= $this->item->category_alias == "bicicleta-caballo" ? "active" : ""?>">
                                            <i class="fa fa-biking"></i>
                                        </div>
                                        <div class="buble-tags <?= $this->item->category_alias == "bicicleta-caballo" ? "active" : ""?>">
                                            <i class="fa fa-horse"></i>
                                        </div>
                                        <h4><?= Text::_("TPL_GESPLAN_BIKING_HORSE") ?></h4>

                                    <?php elseif($this->item->catid == "63" || $this->item->catid == "133" || $this->item->catid == "134"):?>
                                        <div class="buble-tags <?= $this->item->category_alias == "vehiculo-motor" ? "active" : ""?>">
                                            <i class="fa fa-motorcycle"></i>
                                        </div>               
                                        <h4><?= Text::_("TPL_GESPLAN_MOTOR_VEHICLE") ?></h4>
                                    <?php endif; ?>
                                </div>
                                <?php  if($this->item->jcfields[255]->rawvalue[0] != 0) : //Modalidad cf, if it has value echo icon?>
                                    <div class="modalidad">
                                        <div class="graphic-ruta">
                                            <div class="item">
                                                <img src="templates/gesplan/images/modalidad-<?php echo $this->item->jcfields[255]->rawvalue[0]?>.png" alt="Tenerife ON" loading="lazy" >
                                            </div>
                                            <div class="item modalidad">
                                                <p class="value"><?= Text::_("TPL_GESPLAN_MODALIDAD_".$this->item->jcfields[255]->rawvalue[0]."")?></p>
                                            </div>
                                        </div>  
                                    </div>
                                <?php endif; ?>

                                <div class="graphic-ruta">
                                    <?php if($long=$this->item->jcfields[60]->value != '') : ?>
                                    <div class="item">
                                        <i class="fas fa-long-arrow-alt-right"></i>
                                        <p class="value"><?php echo(number_format( ( ($this->item->jcfields[60]->value) / 1000), 2,",",".")) //longitud?> km</p>
                                    </div>
                                    <?php endif; ?>
                                    <?php if($duration=$this->item->jcfields[207]->value != '') : ?>
                                    <div class="item">
                                        <i class="far fa-clock"></i>
                                        <?php // get hours and minutes
                                            $route_time = $this->item->jcfields[207]->rawvalue;
                                            $hours = intval($route_time / 60);
                                            $minutes = intval($route_time - ($hours * 60));
                                        ?> 
                                        <p class="value"><?= $hours. " h ".$minutes. " m "; //Duración estimada?>
                                        </p>
                                    </div>
                                    <?php endif; ?>
                                    <?php if($gain=$this->item->jcfields[63]->value != '') : ?>
                                    <div class="item">
                                        <i class="fas fa-level-up-alt"></i>
                                        <p class="value"><?php echo(number_format($this->item->jcfields[63]->value,2,",",".")) //desnivel ascenso?> m</p>
                                    </div>
                                    <?php endif; ?>
                                    <?php if($lost=$this->item->jcfields[64]->value != '') : ?>
                                    <div class="item">
                                        <i class="fas fa-level-down-alt"></i>
                                        <p class="value"><?php echo(number_format($this->item->jcfields[64]->value,2,",",".")); //desnivel descenso?> m</p>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <p class="mediumSpeed"><?= $this->item->jcfields[208]->rawvalue;//duracion observaciones;?></p>
                                <ul class="ul-itinerario-value">
                                    <?php if($gain != '') : ?>
                                    <li>
                                        <i class="fas fa-level-up-alt"></i>
                                        <p><strong><?= Text::_("TPL_GESPLAN_ALTITUDE_GAIN") ?>:</strong><?php echo(number_format($this->item->jcfields[63]->value,2,",",".")) //desnivel ascenso?> m</p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if($lost != '') : ?>
                                    <li>
                                        <i class="fas fa-level-down-alt"></i>
                                        <p><strong><?= Text::_("TPL_GESPLAN_ALTITUDE_LOST") ?>:</strong><?php echo(number_format($this->item->jcfields[64]->value,2,",",".")); //desnivel descenso?> m</p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if($this->item->jcfields[61]->value != '') : ?>
                                    <li>
                                        <i class="fas fa-arrow-up"></i>
                                        <p><strong><?= Text::_("TPL_GESPLAN_MAX_ALTITUDE") ?>:</strong><?php echo(number_format($this->item->jcfields[61]->value,2,",",".")) //altitud máxima?> m</p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if($this->item->jcfields[62]->value != '') : ?>
                                    <li>
                                        <i class="fas fa-arrow-down"></i>
                                        <p><strong><?= Text::_("TPL_GESPLAN_MIN_ALTITUDE") ?>:</strong><?php echo(number_format($this->item->jcfields[62]->value,2,",",".")) //altitud minima?> m</p>
                                    </li>
                                    <?php endif; ?>
                                </ul>


                            </div>
                        </div>
                        <div class="col-lg-5  mb-4">
                            <h3><?= Text::_("TPL_GESPLAN_SIGNALING") ?></h3>
                            <!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                invidunt ut labore et dolore magna aliquyam:</p>-->
                            <div class="row"> 
                                <div class="col-2">
                                    <figure class="señalizacion">
                                    <?php   $color_field_value = $this->item->jcfields[230]->rawvalue[0];
                                            if($color_field_value == ""){
                                                //dd($color_field_value);
                                                $color_field_value = '#ffffff';
                                            }
                                            $matricula = $this->item->jcfields[140]->rawvalue;
                                            $color = [];
                                            //VM abd BICA fill only one color
                                            if ( ($this->item->catid != 61) && ($this->item->catid != 123) && ($this->item->catid != 124) ) {
                                                $color = ['top'=>$color_field_value, 'bottom' => $color_field_value];
                                                
                                            }
                                            else{
                                                if(str_contains($this->item->jcfields[140]->value, 'PNT')){
                                
                                                    $color = ['top'=>'#006400', 'bottom' => '#006400'];
                                
                                                }
                                                elseif( (str_contains($matricula, 'PR')) || (str_contains($matricula, 'GR')) || (str_contains($matricula, 'SL')) ){
                                
                                                    $color = ['top'=>'#ffffff', 'bottom' => $color_field_value];
                                
                                                }
                                                else{
                                
                                                    $color = ['top'=> $color_field_value, 'bottom' => '#ffffff'];
                                
                                                }
                                
                                            }?>
                                         <div class="top-color" style="background-color:<?= $color['top']?>"></div>
                                         <div class="bottom-color"  style="background-color:<?= $color['bottom']?>"></div>

 
                                    </figure>
                                </div>
                               
                                    <?php if($this->item->jcfields[283]->rawvalue) ://ocultar matricula?>
                                        <div class="col-10">
                                            <p class="m-0"><?= Text::_("TPL_GESPLAN_ROUTE_CODE") ?></p>
                                            <p class="id-recorrido"><?php echo($this->item->jcfields[140]->value) //Matricula?></p>
                                        </div>
                                    <?php endif; ?>
                                <div class="col-md-10 <?= $this->item->jcfields[283]->rawvalue ? 'offset-md-2' : ''; ?>">
                                    <?= $this->item->jcfields[228]->value;//Señalización?>
                                    <?php   //Get association of article
                                        if( ($this->item->catid == 61) || ($this->item->catid == 123) || ($this->item->catid == 124) ){
                                            $articleId = 1162; //Señalética de senderos
                                            $signing = "TPL_GESPLAN_ROUTES_SIGNALING_61";
                                        }else if( ($this->item->catid == 62) || ($this->item->catid == 131) || ($this->item->catid == 132) ){
                                            $articleId = 1161; //Señalética de bica
                                            $signing = "TPL_GESPLAN_ROUTES_SIGNALING_62";

                                        }else if( ($this->item->catid == 63) || ($this->item->catid == 133) || ($this->item->catid == 134) ){
                                            $articleId = 1163; //Señalética de motor
                                            $signing = "TPL_GESPLAN_ROUTES_SIGNALING_63";

                                        } 
                                            $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
                                            
                                            $articleLang = 'es-ES';
                                            $idLang = $articleId;
                                            $catidLang = 34;

                                            if(count($associations) > 0) {
                                                $currentLang = Factory::getLanguage()->getTag();
                                                foreach($associations as $assoc){
                                                    if($assoc->language == $currentLang){
                                                        $articleLang = $currentLang;
                                                        $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                                                        $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                                                    }
                                                }
                                            }

                                            ?>
                                    <a href="<?php echo Route::_(RouteHelper::getArticleRoute( $idLang, $catidLang, $articleLang ));?>"><?= Text::_($signing)?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
    <section class="mapaItinerario">
        <div class="mapbox">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="colmap">
                            <h2><?= Text::_("TPL_GESPLAN_ROUTE_MAP") ?></h2>
                            <h3><?= Text::_("TPL_GESPLAN_ELEVATION_PROFILE") ?></h3>
                            <div class="perfil-head">           
                                <?php if($this->item->jcfields[206]->rawvalue[0] != 0 ) :?>
                                <p><?= Text::_("TPL_GESPLAN_DIFFICULTY") ?></p>
                                <p><strong>
                                    <?= Text::_("TPL_GESPLAN_DIFFICULTY_".$this->item->jcfields[206]->rawvalue[0].""); ?> *<?= Text::_("TPL_GESPLAN_SAFE_MOUNTAIN");?>
                                    </strong>
                                </p>
                                <?php endif; ?>
                            </div>
                            <!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam:</p> -->
                            <div id="elevation-div"></div>

                            <ul class="ul-itinerario-value">
                                <li>
                                    <i class="fas fa-arrow-up"></i>
                                    <p><strong><?= Text::_("TPL_GESPLAN_MAX_ALTITUDE") ?>:</strong><?php echo(number_format($this->item->jcfields[61]->value,2,",",".")) //desnivel ascenso?> m</p>
                                </li>
                                <li>
                                    <i class="fas fa-arrow-down"></i>
                                    <p><strong><?= Text::_("TPL_GESPLAN_MIN_ALTITUDE") ?>:</strong><?php echo(number_format($this->item->jcfields[62]->value,2,",",".")) //desnivel descenso?> m</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $config = Factory::getConfig();
                $mapbox_token = $config->get('mapbox_token');
            ?>
            <figure class="map" id="map-itinerario">
                <button id="openData" class="niceRotate" style="z-index: 9999999;"><i class="fa fa-chevron-left"></i></button>
            </figure>
            <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
            <script src="/templates/gesplan/js/gesplan/leaflet.groupedlayercontrol.js"></script>

            <!-- leaflet-elevation -->
            <link rel="stylesheet" href="https://unpkg.com/@raruto/leaflet-elevation@2.2.5/dist/leaflet-elevation.min.css" />
            <script src="https://unpkg.com/@raruto/leaflet-elevation@2.2.5/dist/leaflet-elevation.min.js"></script>

            <!-- <script src="https://unpkg.com/leaflet.fullscreen@latest/Control.FullScreen.js"></script> -->
            <script>

            var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
            var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<?= $mapbox_token ?>';

            var tenerifeon = L.tileLayer(mbUrl, {id: 'tenerifeon/cl2pyxrtp001b15lfuq3x23z5', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
            var satellite = L.tileLayer(mbUrl, {id: 'tenerifeon/cl3rgf7dn000s14nyiiyuw8qm', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
                const geoJson = JSON.stringify(<?= $this->item->jcfields[145]->rawvalue?>);
                let maxHeigth = <?= $this->item->jcfields[61]->value;?>;
                let minHeigth = <?= $this->item->jcfields[62]->value;?>;
                let maxHeigthAxis,minHeigthAxis;
                minHeigthAxis = minHeigth - 50;
                maxHeigthAxis = maxHeigth + 200;
                //debugger;
                var  scrollWheelZoom = true;

                var disableDragIfMobile = function() {
                let check = false;
                (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
                if(check){
                    //map.dragging.disable();
                    map.scrollWheelZoom.disable();
                }  
                };

            var incidencesLayer = L.layerGroup();

            let opts = {
                map: {
                    center: [28, -16],
                    zoom: 12,
                    minZoom: 10,
                    scrollWheelZoom: scrollWheelZoom,
                    dragging: !L.Browser.mobile, 
                    tap: !L.Browser.mobile,
                    layers: [tenerifeon,incidencesLayer],
                    detectRetina: true
                },
                elevationControl: {
                    data: geoJson,
                    options: {
                        position: "bottomleft",
                        detached: true,
                        summary: false,
                        imperial: false,
                        altitude: true,
                        slope: false,
                        speed: false,
                        acceleration: false,
                        time: "summary",
                        legend: true,
                        followMarker: true,
                        almostOver: true,
                        distanceMarkers: true,
                        hotline: false,
                        closeBtn: false,
                        height: 300,
	                    width:  600,	
                        yAxisMax: maxHeigthAxis,
                        yAxisMin: minHeigthAxis,
                    },
                }
            };
		let map = L.map('map-itinerario', opts.map);
        disableDragIfMobile();

        map.setMaxBounds([
            [28.748396571187406, -15.671997070312502],
            [27.831789750079267, -17.586364746093754]
        ]);

        var baseLayers = {
                'Tenerifeon': tenerifeon,
                '<?= Text::_("TPL_GESPLAN_SATELLITE");?>': satellite
        };
        var groupedOverlays = {
            "<strong>Incidencias</strong>": {
                "<?= Text::_("TPL_GESPLAN_INCIDENCES")?>": incidencesLayer
            }
        };
        var layerControl = L.control.groupedLayers(baseLayers, groupedOverlays,{collapsed:false}).addTo(map);

		let controlElevation = L.control.elevation(opts.elevationControl.options).addTo(map);

		//controlElevation.on('eledata_loaded', ({layer, name}) => controlLayer.addTo(map) && layer.eachLayer((trkseg) => trkseg.feature.geometry.type != "Point" && controlLayer.addOverlay(trkseg, trkseg.feature && trkseg.feature.properties && trkseg.feature.properties.name || name)));

        function loadItinerary(){
            
            controlElevation.load(opts.elevationControl.data);

            if (typeof incidences !== 'undefined'){
                setTimeout(loadIncidences,3000);
            }
  
        }
        function loadIncidences(){
            incidences.forEach(function(incidence){
                let coords = incidence.features[0].geometry.coordinates;

                let geometryCenter = coords[parseInt(coords.length / 2)];

                if(geometryCenter.length > 0){
                    if((incidence.features[0].geometry.type != "MultiLineString")){
                        let lat_lng = [geometryCenter[1], geometryCenter[0]];
                        L.marker(lat_lng, {icon: incidencesIcons.routeWarning}).addTo(incidencesLayer).bindPopup(incidence.features[0].properties.nombre);  
                    }
                    else{
                        let geometryCenter = coords[0][parseInt( coords[0].length / 2)];
                        let lat_lng = [geometryCenter[1], geometryCenter[0]];
                        L.marker(lat_lng, {icon: incidencesIcons.routeWarning}).addTo(incidencesLayer).bindPopup(incidence.features[0].properties.nombre);  
                    }
                    var incidenceTrack = L.geoJSON(incidence.features[0], {style: {"color": "#ffeb3b", "className": "incidences", "weight": "2"}});
                    incidencesLayer.addLayer(incidenceTrack);
                    incidenceTrack.bringToFront();
                }
            })



        }
        loadItinerary();


  

    </script>
    <style> 
            #elevation-div { height: 100%; width: 100%; padding: 0; margin: 0; } 
            #elevation-div {	height: 25%; font: 12px/1.5 "Helvetica Neue", Arial, Helvetica, sans-serif; } 
    </style>

        </div>
    </section>
    <section class="tab-maps">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="responsive-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#categoryMap1"
                                    type="button" role="tab" aria-controls="home" aria-selected="true"><?= Text::_("TPL_GESPLAN_ROUTES_CONNECTION") ?></button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#categoryMap2"
                                    type="button" role="tab" aria-controls="home" aria-selected="false"><?= Text::_("TPL_GESPLAN_ROUTE_START");?></button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#categoryMap3"
                                    type="button" role="tab" aria-controls="home" aria-selected="false"><?= Text::_("TPL_GESPLAN_ROUTE_END");?></button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#categoryMap4"
                                    type="button" role="tab" aria-controls="home" aria-selected="false"><?= Text::_("TPL_GESPLAN_TOWNS");?>
                                </button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#categoryMap5" type="button"
                                    role="tab" aria-controls="home" aria-selected="false"><?= Text::_('TPL_GESPLAN_RULES')?> </button>
                            </li>
                        </ul>

                        <div id="content" class="tab-content" role="tablist">
                            <div id="categoryMap1" class="card tab-pane fade show active flex-column" role="tabpanel"
                                aria-labelledby="categoryMap1">
                                <h2><?= Text::_("TPL_GESPLAN_ROUTES_CONNECTION") ?></h2>
                                <div class="row">
                                    <div class="col-md-8">
                                    <?php if(!empty($this->item->jcfields[68]->rawvalue)) ://Conexiones con otros campos ?>
                                        <ul class="conexiones">
                                            <?php $idRutas = explode(",",$this->item->jcfields[68]->rawvalue); ?>
                                            <?php foreach($idRutas as $idRuta) :?>
                                                <?php if($idRuta != "") : ?>
                                                <?php 
                                                        //Get associations for each article
                                                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $idRuta );
                                                        if(count($associations) > 0) { //If has associations, get id to show in current language
                                                            $currentLang = Factory::getLanguage()->getTag();
                                                            foreach($associations as $assoc){
                                                                if($assoc->language == $currentLang){
                                                                    $articleLang = $currentLang;
                                                                    $articleId = intval(explode(":", $associations[$articleLang]->id)[0]);
                                                                }
                                                            }
                                                        }
                                                        else{
                                                            $articleId = $idRuta; //If not association, spanish article id
                                                        }
                                                            $query = $db
                                                            ->getQuery(true)
                                                            ->select($db->quoteName(array('catid', 'title', 'language')))
                                                            ->from($db->quoteName('#__content'))
                                                            ->where($db->quoteName('id') . " = " . $db->quote($articleId))
                                                            ->where($db->quoteName('state') . " = " . $db->quote(1));
                                                            // Reset the query using our newly populated query object.
                                                            $db->setQuery($query);
                                                            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                                            $rutaData = $db->loadObjectList(); ?>
                                                            <?php if(count($rutaData) > 0 ) : ?>
                                                                <li><div class="col-2 col-sm-1 connection-type">
                                                                        <?php     if($rutaData[0]->catid == 61 || $rutaData[0]->catid == 123 || $rutaData[0]->catid == 124 ): ?>
                                                                            <i class="fa fa-walking"></i>
                                                                            <!-- <?= HTMLHelper::_('image','icon-route.svg', Text::_("TPL_GESPLAN_ROUTE_ICON"), null, true, 0) ?> -->
                                                                        <?php elseif($rutaData[0]->catid == 62 || $rutaData[0]->catid == 131 || $rutaData[0]->catid == 132 ): ?>
                                                                            <i class="fa fa-bicycle"></i> / <i class="fa fa-horse"></i>
                                                                        <?php elseif($rutaData[0]->catid == 63 || $rutaData[0]->catid == 133 || $rutaData[0]->catid == 134 ): ?>
                                                                            <i class="fa fa-truck-pickup"></i>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <div class="col-10 col-sm-11">
                                                                        <div class="link-routes">

                                                                            <p class="conex"><?= $rutaData[0]->title ?>
                                                                            </p>
                                                                            <?php 
                                                                                    $link = (RouteHelper::getArticleRoute($articleId, $rutaData[0]->catid, $rutaData[0]->language ));
                                                                                    $link = Route::_($link);
                                                                            ?>
                                                                            <a href="<?= $link?>"><?= Text::_('TPL_GESPLAN_VIEW_ROUTE')?></a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <?php endif; ?>

                                                        
                                                <?php endif; ?>
                                            <?php endforeach; ?>       
                                        </ul>
                                        <?php else : ?>
                                            <?= Text::_('TPL_GESPLAN_NO_DATA_AVAILABLE')?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="link-plan">
                                            <p class="head-plan"><?= Text::_("TPL_GESPLAN_KNOW_OUR_PLANNER")?></p>
                                            <p><?= Text::_("TPL_GESPLAN_FIND_OUT_OUR_ROUTE_PLANNER_PLAN")?></p>
                                            <?php 

                                                $planificadorItemId = 1156;
                                                $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $planificadorItemId);
                                                $articleLang = Factory::getLanguage()->getTag();
                                                if(count($associations) > 0) {
                                                    $articleLang = Factory::getLanguage()->getTag();
                                                    $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                                                    $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                                                }
                                                else{
                                                    $articleLang = 'es-ES';
                                                    $idLang = $planificadorItemId;
                                                    $catidLang = 35; //Guía de actividades categoty id
                                                }
                                            ?>
                                            <a href="<?php  echo Route::_(RouteHelper::getArticleRoute($idLang, $catidLang, $articleLang)); ?>"><?= Text::_("TPL_GESPLAN_GOTO_PLANNER")?></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div id="categoryMap2" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="categoryMap2">
                                <h2><?= Text::_("TPL_GESPLAN_GET_TO_ROUTE_START")?></h2>
                                <?php if($this->item->jcfields[58]->value != "") ://Inicio??>
                                <?php             
                                        $tags_extripped = [];                            
                                                preg_match( "=^<p>(.*)</p>$=i", $this->item->jcfields[58]->value, $tags_extripped). "<br />" ;
                                                if(sizeOf($tags_extripped) > 0){
                                                    echo "<p>".$tags_extripped[1]."</p>";
                                                }else{
                                                    echo "<p>".$this->item->jcfields[58]->value."</p>";
                                                }
                                        ?> 
                               <?php endif; ?>                                <?php if($this->item->jcfields[210]->value != "") :?>
                               <div class="acceso-ruta">
                                   <i class="fa fa-bus"></i>
                                   <?php             
                                        $tags_extripped = [];                            
                                                preg_match( "=^<p>(.*)</p>$=i", $this->item->jcfields[210]->value, $tags_extripped). "<br />" ;
                                                if(sizeOf($tags_extripped) > 0){
                                                    echo "<p>".$tags_extripped[1]."</p>";
                                                }else{
                                                    echo "<p>".$this->item->jcfields[210]->value."</p>";
                                                }
                                        ?> 
                                
                               </div>
                               <?php endif; ?>
                               <?php if($this->item->jcfields[212]->value != "") :?>
                                    <div class="acceso-ruta">
                                        <i class="fa fa-car"></i>
                                        <?php             
                                        $tags_extripped = [];                            
                                                preg_match( "=^<p>(.*)</p>$=i", $this->item->jcfields[212]->value, $tags_extripped). "<br />" ;
                                                if(sizeOf($tags_extripped) > 0){
                                                    echo "<p>".$tags_extripped[1]."</p>";
                                                }else{
                                                    echo "<p>".$this->item->jcfields[212]->value."</p>";
                                                }
                                        ?>                              
                                   </div>
                               <?php endif; ?>
                            </div>
                            <div id="categoryMap3" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="categoryMap3">
                                <h2><?= Text::_("TPL_GESPLAN_GET_TO_ROUTE_END")?></h2>
                                <?php if($this->item->jcfields[59]->value != "") ://Fin??>
                                <?php             
                                        $tags_extripped = [];                            
                                                preg_match( "=^<p>(.*)</p>$=i", $this->item->jcfields[59]->value, $tags_extripped). "<br />" ;
                                                if(sizeOf($tags_extripped) > 0){
                                                    echo "<p>".$tags_extripped[1]."</p>";
                                                }else{
                                                    echo "<p>".$this->item->jcfields[59]->value."</p>";
                                                }
                                        ?> 
                               <?php endif; ?>
                                <?php if($this->item->jcfields[211]->value != "") ://Acceso rodado al final de la ruta publico?>
                                <div class="acceso-ruta">
                                    <i class="fa fa-bus"></i>
                                    <?php             
                                        $tags_extripped = [];                            
                                                preg_match( "=^<p>(.*)</p>$=i", $this->item->jcfields[211]->value, $tags_extripped). "<br />" ;
                                                if(sizeOf($tags_extripped) > 0){
                                                    echo "<p>".$tags_extripped[1]."</p>";
                                                }else{
                                                    echo "<p>".$this->item->jcfields[211]->value."</p>";
                                                }
                                        ?>                            
                                   </div>
                                <?php endif;?>
                                <?php if($this->item->jcfields[225]->value != "") ://Acceso rodado al final de la ruta publico?>
                                <div class="acceso-ruta">
                                    <i class="fa fa-car"></i>
                                    <?php             
                                        $tags_extripped = [];                            
                                                preg_match( "=^<p>(.*)</p>$=i", $this->item->jcfields[225]->value, $tags_extripped). "<br />" ;
                                                if(sizeOf($tags_extripped) > 0){
                                                    echo "<p>".$tags_extripped[1]."</p>";
                                                }else{
                                                    echo "<p>".$this->item->jcfields[225]->value."</p>";
                                                }
                                        ?>                              
                                   </div>
                                <?php endif;?>
                            </div>
                            <div id="categoryMap4" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="categoryMap4">
                                <h2><?= Text::_("TPL_GESPLAN_TOWNS_CROSSED");?></h2>
                                <?php  $municipios = str_replace('"','',explode(",", trim($this->item->jcfields[73]->rawvalue,"{}") )); ?>
                                <ul class="conexiones">
                                    <?php foreach($municipios as $municipio) : ?>
                                    <li>
                                        <?= $municipio;?>
                                    </li>
                                  <?php endforeach; ?>
                                </ul>
                            </div>
                            <div id="categoryMap5" class="card tab-pane fade" role="tabpanel"
                                aria-labelledby="categoryMap4">
                                <h2><?= Text::_('TPL_GESPLAN_RULES')?></h2>
                                <?= $this->item->jcfields[200]->value;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php 
    //Prepare model to get articles to tpint (espacios naturales, putos de interés)

        $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
        $appParams = JFactory::getApplication()->getParams();  
        $model->setState('params', $appParams);
    ?>
    <?php                       
    //Get all Espacio natural by that contains cfield[91] espacio-natural-protegido
    if(!empty($this->item->jcfields[91]->rawvalue) && !is_null(($this->item->jcfields[91]->rawvalue))): 
    ?>
    <section class="enp">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2><?= Text::_("TPL_GESPLAN_NATURAL_AREAS");?></h2>
                    <!-- <p>Es parte de 2 espacios naturales y esta relacionado con 1 más:</p> -->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="slick-enp">
                    <?php   
                                $espacios_naturales = explode(",",$this->item->jcfields[91]->rawvalue);
                                foreach($espacios_naturales as $key => $enp){
                                    if(!is_null($enp) && (!empty($enp))){
                                        $articleId = $enp;
                                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $articleId );
                                        $articleLang = Factory::getLanguage()->getTag();
                                        if(count($associations) > 0) {
                                            $articleId = intval(explode(":", $associations[$articleLang]->id)[0]);
                                        }

                                            $model->setState('filter.article_id', $articleId);
                            
                                            $helpArticle =   $model->getItems();
                                            $item = $helpArticle[0];
                                            $fields = FieldsHelper::getFields('com_content.article', $item, true);
                                            // Adding the fields to the object
                                            $item->jcfields = array();
                                            foreach ($fields as $key => $field)
                                            {
                                                $item->jcfields[$field->id] = $field;
                                            }
    
                                            ?>
    
                                                <div>
                                                    <figure>
                                                        <div class="caption">
                                                            <p class="transita"><span><?= Text::_("TPL_GESPLAN_TRANSITS")?></span></p>
                                                            <h3><?= $item->title?> </h3>
                                                            <!-- <p class="enp">Espacio natural protegido</p> -->
                                                            <!-- <p class="reserva"><?= $item->jcfields[186]->rawvalue?></p> -->
                                                            <p><?= preg_replace('/\s+?(\S+)?$/', '', substr(strip_tags($item->introtext), 0, 200));?> ...</p>
                                                            <a href="<?php echo Route::_(RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));?>"><?= Text::_("TPL_GESPLAN_DISCOVER_NATURAL_AREA");?></a>
                                                        </div>
                                                            <?php  $images = json_decode($item->images); ?>
                                                            <?php if(!empty($images->image_intro)) : ?>
                                                                <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy" >
                                                            <?php else: ?>
                                                                <img src="templates/gesplan/images/neutra-900.jpg" alt="Tenerife ON" loading="lazy" >
                                                            <?php endif; ?>
                                                    </figure>
                                                </div>
    
                                            <?php
    
                                        } // if(count($associations) > 0)
                                       
                                        
                                    } //if(!is_null($enp) && (!empty($enp)))
                                

                    ?>

                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <?php endif; ?>
    <?php 
        //If has municipio meteo
    
        if(!empty($this->item->jcfields[256]->rawvalue)) {

        $path = dirname(__FILE__) . "/partials/";
        require_once($path.'meteorologia.php');

         } 
    ?>

<script>
    var intrestingPoints = [];
   </script>
    <?php   if(!empty($this->item->jcfields[69]->rawvalue) && !is_null(($this->item->jcfields[69]->rawvalue))): ?>
        <?php $all_intresting_points = explode(",",$this->item->jcfields[69]->rawvalue); 
                $cleaned_intesting_points = [];
                //If field contains empty fields due to a updating isseu, clean them
                foreach($all_intresting_points as $point){
                    if($point != ""){
                        $cleaned_intesting_points[] = $point;
                    }
                }
            //Get all puntos de interes that are active
            $db = Factory::getDbo();
            $query = "select sc.id from sooeg_content sc left join sooeg_categories cc 
                on cc.id = sc.catid where cc.parent_id = '66' and sc.state = '1'
                and sc.id in (".implode(",", $cleaned_intesting_points).")";

            $db->setQuery($query);
            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
            $puntos_interes = $db->loadColumn();

        ?>
        <?php if(sizeOf($puntos_interes) > 0) : ?>
        <section class="lugares-interes">
        <?php
                $path = dirname(__FILE__) . "/partials/";
                require_once($path.'punto-de-interes.php');
        ?>
 

                    <?php foreach($points_for_modal as $key => $point) :?>
            
                        <script>
                                var lat = '<?= $point['lat'];?>';
                                var lon = '<?= $point['lon'];?>';
                                var title = '<?= $point['title']?>';

                                intrestingPoints.push({
                                    title: title,
                                    lat : lat,
                                    lon : lon
                                });

                        </script>
                    <?php endforeach; ?>

    <?php endif; ?>
<?php endif; ?>
    <section class="link-planificador">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <span class="head"><?= Text::_("TPL_GESPLAN_LIKE_TO_GO") ?></span>
                    <h2><?= Text::_("TPL_GESPLAN_PLAN_YOUR_NEXT_ADVENTURE") ?></h2>
                    <p><?= Text::_("TPL_GESPLAN_DESIGN_YOUR_OWN_ROUTE") ?>.</p>
                    <?php 

                        $planificadorItemId = 1156;
                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $planificadorItemId);
                        $articleLang = Factory::getLanguage()->getTag();
                        if(count($associations) > 0) {
                            $articleLang = Factory::getLanguage()->getTag();
                            $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                            $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                        }
                        else{
                            $articleLang = 'es-ES';
                            $idLang = $planificadorItemId;
                            $catidLang = 35; //Guía de actividades categoty id
                        }
                    ?>
                    <a title="Enlace a nuestro planificador de rutas" href="<?php  echo Route::_(RouteHelper::getArticleRoute($idLang, $catidLang, $articleLang)); ?>"><?= Text::_("TPL_GESPLAN_GOTO_PLANNER") ?></a>
                </div>
            </div>
        </div>
        <?= HTMLHelper::_('image','img-planificador-banner.svg', Text::_("Icono de Youtube"), null, true, 0) ?>
    </section>
        <?php
            $path = dirname(__FILE__) . "/partials/";
            require_once($path.'modal-favorito.php');
        ?>

    <?php if ($info == 0 && $params->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
		<?php $this->item->tagLayout = new FileLayout('joomla.content.tags'); ?>

		<?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
	<?php endif; ?>

<script>
    $(document).ready(function(){
        $('text[x=174]').html('<?= Text::_('TPL_GESPLAN_ALTITUDE');?>');
    });
    $(document).ready(function(){
        //debugger;
        var marker = [];

        intrestingPoints.forEach(fitInMap);

                function fitInMap(item,id){
                    //Object.values(typeNatural).includes(2)
                    let lat_lng = [item.lat, item.lon];
                    marker[id] = new L.marker(lat_lng);
                    // debugger;
             
                        marker[id] = new L.marker(lat_lng, {icon: pointsIcons.general}).addTo(map).bindPopup(item.title).openPopup();
                    
                }  
                    // Enable
                //L.geoJSON(incidence.features[0]).addTo(map);
              
               
    })
</script>


	<?php // Content is generated by content plugin event "onContentAfterDisplay" ?>
	<?php //echo $this->item->event->afterDisplayContent; ?>
