<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;

$app = Factory::getApplication();

$this->category->text = $this->category->description;
$app->triggerEvent('onContentPrepare', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$this->category->description = $this->category->text;

$results = $app->triggerEvent('onContentAfterTitle', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayTitle = trim(implode("\n", $results));

$results = $app->triggerEvent('onContentBeforeDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$beforeDisplayContent = trim(implode("\n", $results));

$results = $app->triggerEvent('onContentAfterDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayContent = trim(implode("\n", $results));

$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
//dd($this->category);
?>
	<section class="intro intro--basic faqs pb-0">
		<div class="container">
			<div class="row">
				<div class="col">
					<?php if ($this->params->get('show_category_title', 1)) : ?>
					<h1><?php echo $this->category->title; ?></h1>
					<?php endif; ?>
					<?php if ($this->params->get('show_description') && $this->category->description) : ?>
					<?= $this->category->description ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="faq pt-0 control-responsive">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="responsive-tabs">
						<ul class="nav nav-tabs" role="tablist">
							<?php echo $this->loadTemplate('tabschildren'); ?>
						</ul>
						<div id="content" class="tab-content" role="tablist">
							<?php echo $this->loadTemplate('tabscontentchildren'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Pintamos título y descripción de la categoría information -->


	<!-- Este código pinta las subcategorías pertenecientes a information -->

	<?php if ($this->maxLevel != 0 && !empty($this->children[$this->category->id])) : ?>
	

	<section id="news__list" class="news__list">
            
		<div class="container news__theme-list" data-aos="fade-up">
			<div class="row d-flex align-items-stretch" data-aos="fade-up">
				<?php echo $this->loadTemplate('children'); ?> </div>
			</div>
		</div>
	</section>	

	<?php endif; ?>




	<div id="pagination" class="container">
	<?php if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
		<div class="com-content-category-blog__navigation w-100">
			<?php if ($this->params->def('show_pagination_results', 1)) : ?>
				<!-- <p class="com-content-category-blog__counter counter float-end pt-3 pe-2">
					<?php echo $this->pagination->getPagesCounter(); ?>
				</p> -->
			<?php endif; ?>
			<div class="com-content-category-blog__pagination">
				<?php echo $this->pagination->getPagesLinks(); ?>
			</div>
		</div>
	<?php endif; ?>
	</div>
<!-- </div> -->
<?php
    $path = dirname(__DIR__, 1) . "/article/partials/";
    require_once($path.'ayuda.php');
?>
