<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;



// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);

?>


    <?php if ($canEdit) : ?>
		<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
	<?php endif; ?>
    <?php
    $user = Factory::getUser();
    $status = $user->guest;
    
   ?>
   <!-- loop start -->
 <?php //if($this->item->jcfields[215]->rawvalue == 1) ://If visible-alerta is true?>
                <div class="alert-item-card">
                    <div class="row">
                        <div class="col-1">
                        <?= HTMLHelper::_('image','icon-warning-list.png', Text::_('TPL_GESPLAN_ROUTE_ICON'), null, true, 0) ?>
                        </div>
                        <div class="col-11">
                            <p class="date">  <time datetime="<?php echo HTMLHelper::_('date', $this->item->jcfields[241]->rawvalue, 'c'); ?>" itemprop="datePublished">
                                            <?php echo HTMLHelper::_('date', $this->item->jcfields[241]->rawvalue, Text::_('DATE_FORMAT_LC2')); ?>
                                        </time></p>
                            <p class="title-notification"><?php echo $this->item->title;?></p>
                                <?php  
                                        $items_to_print = [];
                                        if($this->item->jcfields[233]->rawvalue != null || $this->item->jcfields[233]->rawvalue != ''){

                                            $affected_equipment = explode(',',$this->item->jcfields[233]->rawvalue);
                                            foreach($affected_equipment as $item){
                                                $items_to_print[] = $item;
                                            }
                                        }
                                        if($this->item->jcfields[242]->rawvalue != null || $this->item->jcfields[242]->rawvalue != ''){

                                            $affected_itineraries = explode(',',$this->item->jcfields[242]->rawvalue);
                                            if($affected_itineraries != null || $affected_itineraries != ''){
                                                foreach($affected_itineraries as $item){
                                                    $items_to_print[] = $item;
                                                }
                                            }
                                        }
                                        $affected_items = [];
                                        foreach($items_to_print as $item){
                                            $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                                            $appParams = JFactory::getApplication()->getParams();
                                            $model->setState('params', $appParams);
                                            $model->setState('filter.article_id', $item); //change that to your Category ID
                                            $model->setState('filter.published', '1');
                    
                                            ($affected_items =   $model->getItems());
                                        }
                                        if($affected_items != null || sizeof($affected_items) > 0): ?>
                                        <p class="body-notification"><strong>Afectaciones</strong></p>
                                        
                                        <ul class="conexiones">
                                            <?php foreach($affected_items as $item) : ?>
                                                
                                                <?php 
                                                    $categories = JCategories::getInstance('com_content', ['published' => false, 'access' => false]);
                                                    $category = $categories->get($item->parent_id);
                                                    $image = $category->getParams()->get('image');
                                                ?>
                                                    <div class="col-12">
                                                        <div class="link-routes">

                                                            <?php 
                                                                $link = (RouteHelper::getArticleRoute($item->id, $item->catid, $item->language ));
                                                                $link = Route::_($link);
                                                            ?>

                                                            <p class="conex"><a href="<?= $link?>" target="_blank"><img class="img-fluid" src="<?= $image ?>" style="margin-right: 10px" alt="<?= $category ?>"><?= $item->title ?></a></p>

                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <?php endif;?>

                        </div>

                    </div>
                </div>
<?php //endif; ?>
        <!-- end loop -->   

                   