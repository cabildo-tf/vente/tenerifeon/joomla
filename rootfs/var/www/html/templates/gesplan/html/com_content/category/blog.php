<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;

$app = Factory::getApplication();

$this->category->text = $this->category->description;
$app->triggerEvent('onContentPrepare', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$this->category->description = $this->category->text;

$results = $app->triggerEvent('onContentAfterTitle', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayTitle = trim(implode("\n", $results));

$results = $app->triggerEvent('onContentBeforeDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$beforeDisplayContent = trim(implode("\n", $results));

$results = $app->triggerEvent('onContentAfterDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayContent = trim(implode("\n", $results));

$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
?>
<div class="com-content-category-blog blog" itemscope itemtype="https://schema.org/Blog">
<?php if (empty($this->pageclass_sfx)) : ?>
	<section class="intro intro--basic">
		<div class="container">
			<div class="col-md-6">
                <div class="section-title">
					<h1><?php echo $this->category->title; ?></h1>
					<p><?php echo $this->category->description ?></p>							
				</div>
			</div>
		</div>
	</section>
<?php elseif ($this->pageclass_sfx == 'information') : ?>
	<section id="information__category__list" class="news__list info-page">          
		<div class="container">
			<div class="row">
				<div class="col">
				<h1><?php echo $this->category->title; ?></h1>
				<p><?php echo $this->category->description ?></p>	
				</div>
			</div>
		</div>
	</section>
	<?php elseif ($this->pageclass_sfx == 'espacio') :?>
	<section id="information__category__list" class="news__list info-page">          
		<div class="container">
			<div class="row">
				<div class="col">
				<h1><?php echo $this->category->title; ?></h1>
				<p><?php echo $this->category->description ?></p>	
				</div>
			</div>
		</div>
	</section>
<?php elseif ( ($this->pageclass_sfx == 'equipamiento') || ($this->pageclass_sfx == 'itinerario') ) : ?>
	<section id="information__category__list" class="news__list info-page">          
		<div class="container">
			<div class="row">
				<div class="col">
				<h1><?php echo $this->category->title; ?></h1>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

	<?php echo $afterDisplayTitle; ?>

	<?php if ($this->params->get('show_cat_tags', 1) && !empty($this->category->tags->itemTags)) : ?>
		<?php $this->category->tagLayout = new FileLayout('joomla.content.tags'); ?>
		<?php echo $this->category->tagLayout->render($this->category->tags->itemTags); ?>
	<?php endif; ?>

	<?php if ($beforeDisplayContent || $afterDisplayContent || $this->params->get('show_description', 1) || $this->params->def('show_description_image', 1)) : ?>
		<div class="category-desc clearfix">
			
			<?php echo $beforeDisplayContent; ?>
			
			<?php echo $afterDisplayContent; ?>
		</div>
	<?php endif; ?>


	<?php $leadingcount = 0; ?>
	<?php //Override imformation articles in category view ?>
	<?php if (!$this->pageclass_sfx == 'information') :?>
		<?php if (!empty($this->lead_items)) : ?>
			<section id="news__featured" class="news__featured" data-aos="fade-up">
				<div class="container">
					<div class="row">
						<div class="col">
						
						<?php foreach ($this->lead_items as &$item) : ?>
						
									<?php
									$this->item = & $item;
									echo $this->loadTemplate('item_main');
									?>
						
							<?php $leadingcount++; ?>
						<?php endforeach; ?>
						</div>
					</div>
				</div>
			</section>

		<?php endif; ?>
	<?php endif; ?>
	<?php
	$introcount = count($this->intro_items);
	$counter = 0;
	?>
	<?php if (empty($this->pageclass_sfx)) : ?>
			
	<section id="news__list" class="news__list">

		<?php if (!empty($this->intro_items)) : ?>
				
			<div class="container news__theme-list" data-aos="fade-up">
				<div class="row d-flex align-items-stretch" data-aos="fade-up">
							
					<?php foreach ($this->intro_items as $key => &$item) : ?>
						<div class="col-lg-6 col-sm-6 news__theme-card-container"
							itemprop="blogPost" itemscope itemtype="https://schema.org/BlogPosting">
								<?php
								$this->item = & $item;
								echo $this->loadTemplate('item');
								?>
						</div>
					<?php endforeach; ?>

				
				</div>
			</div>
		<?php endif; ?>
	</section>

	<?php elseif ($this->pageclass_sfx == 'information') : ?>
		<section id="information__category__list" class="news__list  info-page">
			
			<?php foreach ($this->items as $key => &$item) : ?>
				<div class="information__category__featured" data-aos="fade-up">
					<div class="container">
						<div class="row">
							<div class="col">
								<?php
								$this->item = & $item;
								echo $this->loadTemplate('item_information');
								?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

		</section>

		<?php elseif ($this->pageclass_sfx == 'equipamiento') : ?>
			<section class="sub-cat-itinerarios">
				<div class="container">
					<div class="row">
						<?php foreach ($this->items as $key => &$item) : ?>
							<?php
							$this->item = & $item;
							echo $this->loadTemplate('item_equipamiento');
						?>
						<?php endforeach; ?>
					</div>
				</div>
			</section>
		

		

		<?php elseif ($this->pageclass_sfx == 'itinerario') : ?>
		<section  class="sub-cat-itinerarios">
			<div class="container">
				<div class="row">	
					<?php foreach ($this->items as $key => &$item) : ?>
								<?php
								$this->item = & $item;
								echo $this->loadTemplate('item_itinerario');
								?>
					<?php endforeach; ?>
				</div>
			</div>
		</section>
		<?php elseif ($this->pageclass_sfx == 'espacio') : ?>
		<section  class="sub-cat-itinerarios">
			<div class="container">
				<div class="row">	
					<?php foreach ($this->items as $key => &$item) : ?>
								<?php
								$this->item = & $item;
								echo $this->loadTemplate('item_espacio');
								?>
					<?php endforeach; ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
	

	<!-- <?php if (!empty($this->link_items)) : ?>
		<div class="items-more">
			<?php echo $this->loadTemplate('links'); ?>
		</div>
	<?php endif; ?> -->

	<!-- <?php if ($this->maxLevel != 0 && !empty($this->children[$this->category->id])) : ?>
		<div class="com-content-category-blog__children cat-children">
			<?php if ($this->params->get('show_category_heading_title_text', 1) == 1) : ?>
				<h3> <?php echo Text::_('JGLOBAL_SUBCATEGORIES'); ?> </h3>
			<?php endif; ?>
			<?php echo $this->loadTemplate('children'); ?> </div>
	<?php endif; ?> -->
	<?php if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
		<div class="container">
			<div class="com-content-category-blog__navigation w-100">
				<?php if ($this->params->def('show_pagination_results', 1)) : ?>
					<p class="com-content-category-blog__counter counter float-end pt-3 pe-2">
						<?php echo $this->pagination->getPagesCounter(); ?>
					</p>
				<?php endif; ?>
				<div class="com-content-category-blog__pagination">
					<?php echo $this->pagination->getPagesLinks(); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
