<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;

$app = Factory::getApplication();

$this->category->text = $this->category->description;
$app->triggerEvent('onContentPrepare', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$this->category->description = $this->category->text;

$results = $app->triggerEvent('onContentAfterTitle', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayTitle = trim(implode("\n", $results));

$results = $app->triggerEvent('onContentBeforeDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$beforeDisplayContent = trim(implode("\n", $results));

$results = $app->triggerEvent('onContentAfterDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayContent = trim(implode("\n", $results));

$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';

?>
<!-- Pintamos título y descripción de la categoría information -->
<section id="news__list" class="news__list info-page">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php if ($this->params->get('show_page_heading')) : ?>
				<h1> <?php echo $this->escape($this->params->get('page_heading')); ?> </h1>
				<?php endif; ?>
				<?php if ($this->params->get('show_category_title', 1)) : ?>
				<h1><?php echo $this->category->title; ?></h1>
				<?php endif; ?>
				<?php //if ($this->params->get('show_description') && $this->category->description) : ?>
				<div class="col-lg-6"><?php echo $this->category->description ?></div>
				<?php //endif; ?>
			</div>
		</div>
	</div>
</section>


<!-- Este código pinta las subcategorías pertenecientes a information -->

<?php if ($this->maxLevel != 0 && !empty($this->children[$this->category->id])) : ?>


<section id="news__list" class="news__list info-page">

	<div class="container news__theme-list" data-aos="fade-up">
		<div class="row d-flex align-items-stretch" data-aos="fade-up">
			<?php echo $this->loadTemplate('children'); ?> </div>
	</div>
	</div>
</section>

<?php endif; ?>


<!-- Si hay articulos en la categoría/subcategoria pinta listado. 
	En la categoria padre(information) no hay articulos, entonces solamente pintará cuando entramos en una subcategoría -->
<?php if (!empty($this->items)) : ?>

<section id="information__category__list" class="news__list  info-page">

	<?php foreach ($this->items as $key => &$item) : ?>
	<div class="information__category__featured" data-aos="fade-up">
		<div class="container">
			<div class="row">
				<div class="col">
					<?php
								$this->item = & $item;
								echo $this->loadTemplate('item');
								?>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach; ?>

</section>

<?php endif; ?>





<?php if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
<div class="com-content-category-blog__navigation w-100">
	<?php if ($this->params->def('show_pagination_results', 1)) : ?>
	<p class="com-content-category-blog__counter counter float-end pt-3 pe-2">
		<?php echo $this->pagination->getPagesCounter(); ?>
	</p>
	<?php endif; ?>
	<div class="com-content-category-blog__pagination">
		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
</div>
<?php endif; ?>
<!-- </div> -->