<?php 

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
?><section class="link-planificador">
        <div class="container">
            <div class="row">
                <div class="<?= $this->pageclass_sfx == 'search' ? 'col-12' : 'col-lg-5'?>">
                    <span class="head"><?= Text::_("TPL_GESPLAN_GO_YOUR_WAY")?></span>
                    <h2>
                        <?= Text::_("TPL_GESPLAN_PLAN_YOUR_NEXT_ADVENTURE") ?>
                    </h2>
                    <p><?= Text::_("TPL_GESPLAN_DESIGN_YOUR_OWN_ROUTE") ?>.
                    </p>
                    <?php 

                        $planificadorItemId = 1156;
                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $planificadorItemId);
                        $articleLang = Factory::getLanguage()->getTag();
                        if(count($associations) > 0) {
                            $articleLang = Factory::getLanguage()->getTag();
                            $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
                            $catidLang = intval(explode(":", $associations[$articleLang]->catid)[0]);
                        }
                        else{
                            $articleLang = 'es-ES';
                            $idLang = $planificadorItemId;
                            $catidLang = 35; //Guía de actividades categoty id
                        }
                    ?>
                    <a title="Enlace a nuestro planificador de rutas" href="<?php  echo Route::_(RouteHelper::getArticleRoute($idLang, $catidLang, $articleLang)); ?>"><?= Text::_("TPL_GESPLAN_GOTO_PLANNER") ?></a>

                </div>
                <div class="<?= $this->pageclass_sfx == 'search' ? 'col-12 ' : 'col-lg-7'?>">
                    <?= HTMLHelper::_('image','img-planificador-banner.svg', Text::_("Icono de Youtube"), null, true, 0) ?>
                </div>
            </div>
        </div>

    </section>