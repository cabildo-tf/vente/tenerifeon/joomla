<?php

use Joomla\CMS\Language\Text;
?>
  <!-- Modal -->
  <div class="modal fade" id="mustlog" tabindex="-1" aria-labelledby="mustlog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <p><?= Text::_("TPL_GESPLAN_LOGIN_REQUIRED"); ?></p>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <p><?= Text::_("TPL_GESPLAN_NEED_TO_ACCES_TO_YOUR_ACCOUNT"); ?></p>
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?= Text::_("TPL_GESPLAN_CONTIUNUE_WITHOUT_LOGIN"); ?></a>
          <a type="button"  href="/login"  class="btn btn-primary"><?= Text::_("TPL_GESPLAN_LOGIN"); ?></a>
        </div>
      </div>
    </div>
  </div>