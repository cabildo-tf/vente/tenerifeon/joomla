<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
$category_title   = $this->escape($this->item->category_title);

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->

  <section class="head-enp">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <span class="enp-pre-title"><?= $this->item->category_title?></span>
                        <h1><?= $this->item->title?></h1>
                    </div>
                </div>
            </div>
            
                 <!-- Get images from galeria (cf 107) -->
                 <?php if(!empty($this->item->jcfields[107]->value)) :?>
                <?php $images = ($this->item->jcfields[107]->subform_rows) ?>

                <?php if(isset($images)) :?>
                    <div class="slick-itinerarios">
                        <?php foreach($images as $image) : ?>
                            <div>
                                <?php  $imgUrl = $image["imagen"]->rawvalue["imagefile"]; ?>
                                <?php if(!empty($imgUrl)) : ?>
                                    <figure><img src="<?php echo $imgUrl;?>" alt="<?php  echo($image["imagen"]->rawvalue["alt_text"]); ?>" loading="lazy"></figure>
                                <?php endif; ?>                               
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                <?php endif; ?>
                   
        </section>
        <section class="enp-descripcion">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?= Text::_("TPL_GESPLAN_DESCRIPTION")?></h2>
                    </div>
                    <div class="col-md-7">
                        <?= $this->item->text?>
                        <div class="tags-enp">
							<?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
                        </div>
                    </div>
                    <div class="col-md-3 offset-md-1">
                        <figure class="fecha-recomendada">
                            <i class="fa fa-calendar-check"></i>
                            <p class="head">  
                                <?= Text::_("TPL_GESPLAN_RECOMENDED_SEASON")?>:
                                <strong> <?= ucfirst($this->item->jcfields[187]->rawvalue)?></strong>
                            </p>
                        </figure>
                    </div>
                </div>
            </div>
        </section>
        <section class="geologia">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?= Text::_("TPL_GESPLAN_GEOLOGY")?> </h2>
                    </div>
                    <div class="col-md-7">
                        <?= $this->item->jcfields[188]->rawvalue?>
                    </div>
                    <div class="col-md-5">
                            <img class="w-100" src="<?= json_decode($this->item->jcfields[189]->rawvalue)->imagefile?>" alt="<?= json_decode($this->item->jcfields[189]->rawvalue)->alt_text?>">
                    </div>
                </div>
            </div>
        </section>
        <section class="floraYfauna">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?= Text::_("TPL_GESPLAN_FLORA_FAUNA")?></h2>
                    </div>
                    <div class="col-md-7">
                        <?= $this->item->jcfields[190]->rawvalue?>
                    </div>
                    <div class="col-md-5">
                        <img class="w-100 borderFlow" src="<?= json_decode($this->item->jcfields[191]->rawvalue)->imagefile?>" alt="<?= json_decode($this->item->jcfields[191]->rawvalue)->alt_text?>">
                        <figcaption><?= json_decode($this->item->jcfields[191]->rawvalue)->alt_text?></figcaption>
                    </div>
                </div>
            </div>
        </section>
        <?php if(!empty($this->item->jcfields[219]->rawvalue)) : ?>
        <?php $itinerarios = explode(",",$this->item->jcfields[219]->rawvalue)?>
        <?php   
                $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                $appParams = JFactory::getApplication()->getParams();  
                $model->setState('params', $appParams);
                
        ?>
        <section class="intinerarios-relacionados-enp">
            <div class="container">
                <div class="row content-horizontal-scrol">
                    <div class="col-md-12">
                        <h2 class="head-scroll"><?= Text::_("TPL_GESPLAN_INTRESTING_ROUTES")?></h2>
                    </div>
                    <div class="col-md-12">
                        <div class="multiple-items">                        

                        <?php foreach($itinerarios as $punto) : ?>
                                <?php 

                                    $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $punto );
                                    if(count($associations) > 0) { //If has associations, get id to show in current language
                                        $currentLang = Factory::getLanguage()->getTag();
                                        foreach($associations as $assoc){
                                            if($assoc->language == $currentLang){
                                                $articleLang = $currentLang;
                                                $articleId = intval(explode(":", $associations[$articleLang]->id)[0]);
                                            }
                                        }
                                    }
                                    else{
                                        $articleId = $punto; //If not association, spanish article id
                                    }
                                    $model->setState('filter.article_id', $articleId); //Banner home category ID
                                    $model->setState('filter.published', '1');                                                                  
                                    $helpArticle =   $model->getItems();
                                    if($helpArticle) :
                                        $item = $helpArticle[0];

                                        $fields = FieldsHelper::getFields('com_content.article', $item, true);
                                        // Adding the fields to the object
                                        $item->jcfields = array();

                                        foreach ($fields as $key => $field){
                                            $item->jcfields[$field->id] = $field;
                                        }
                                    ?>

                                    <div class="px-3">
                                        <div class="card-default">
                                            <a class="hyper-link" href="<?= Route::_(RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));?>"></a>

                                                <figure>
                                                    <div class="gradient"></div>
                                                    <?php
                                                        $images = json_decode($item->images); 
                                                    ?>
                                                    <?php if(empty($images->image_intro)) : ?>
                                                        <?= HTMLHelper::_('image','neutra-300.jpg', "Tenerife ON", null, true, 0) ?>
                                                    <?php else: ?>
                                                        <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" class="img-fluid" loading="lazy">
                                                    <?php endif; ?>                                    </figure>
                                                <div class="caption">
                                                    <div class="head-caption">
                                                        <span><?php $item->jcfields[57]->rawvalue //Situacion?></span>
                                                        <p><?= $item->title?></p>
                                                    </div>
                                                    <div class="actions-enp">
                                                    <div class="content_rating head-ficha">

                                                    <?php   $valoracion = null;
                                                            $db = Factory::getDbo();
                                                            $query = $db
                                                            ->getQuery(true)
                                                            ->select(array('SUM(rating) as rating, COUNT(id) as count'))
                                                            ->from($db->quoteName('#__sg_valoraciones'))
                                                            ->where($db->quoteName('item_id') . " = " . $db->quote($item->id));
                                                            // Reset the query using our newly populated query object.
                                                            $db->setQuery($query);
                                                            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                                            if($hasRating = $db->loadObjectList()){
                                                                if(!is_null($hasRating[0]->rating)){
                                                                    $rating = $hasRating[0]->rating;
                                                                    $count = $hasRating[0]->count;
                                                                    $item->valoracion = (float)($rating/$count);
                                                                    
                                                                }
                                                                else{
                                                                    $item->valoracion = 0;
                                                                } 
                                                            }
                                                    ?>
                                                    <?php 
                                                                
                                                                for ($i = 0; $i < $item->valoracion; $i++) {
                                                                    echo '<li class="vote-star"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                                                                                        </li>';
                                                                };
                                                                for ($i = 0; $i < (5 - $item->valoracion); $i++) {
                                                                    echo '<li class="vote-star-empty"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                                                                                        </li>';
                                                                }
                                                    ?>

                                                        </div>
                                                </div>
                                                <div class="link-card">
                                                    <p><?= Text::_("TPL_GESPLAN_MORE_INFO")?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif;?>
                            <?php endforeach; ?>
 
                          

                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <section class="ficha-tecnica-enp">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2><?= Text::_("TPL_GESPLAN_ECHNICAL_DATA")?></h2>
                        <?= $this->item->jcfields[192]->value?>
                    </div>
                    <div class="col-md-5 offset-md-1">
                        
                        <img class=" img-fluid" src="<?= json_decode($this->item->jcfields[224]->rawvalue)->imagefile?>" alt="<?= json_decode($this->item->jcfields[224]->rawvalue)->alt_text?>">

                    </div>
                </div>
                <?php if(!empty($this->item->jcfields[193]->rawvalue)) : ?>
                <?php $municipios = $this->item->jcfields[193]->rawvalue?>
                <div class="row">
                    <div class="col-12">
                        <h3><?= Text::_("TPL_GESPLAN_TOWNS")?></h3>
                        <ul>
                            <?php foreach($municipios as $municipio) : ?>
                                <li><?= $municipio ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </section>
        <?php   if(!empty($this->item->jcfields[217]->rawvalue) && !is_null(($this->item->jcfields[217]->rawvalue))): ?>
        <?php $all_intresting_points = explode(",",$this->item->jcfields[217]->rawvalue); 

            //Get all puntos de interes that are active
            $db = Factory::getDbo();
            $query = "select sc.id from sooeg_content sc left join sooeg_categories cc 
                on cc.id = sc.catid where cc.parent_id = '66' and sc.state = '1'
                and sc.id in (".$this->item->jcfields[217]->rawvalue.")";

            $db->setQuery($query);
            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
            $puntos_interes = $db->loadColumn();

        ?>
        <section class="lugares-interes">
            <?php
                $path = dirname(__FILE__) . "/partials/";
                require_once($path.'punto-de-interes.php');
            ?>
        </section>
        <?php endif; ?>


        <section class="floraYfauna">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2><?php echo Text::_( 'TPL_GESPLAN_LINKS' );?></h2>
                        <?= $this->item->jcfields[218]->value?>
                    </div>
                </div>
            </div>
        </section>
        <article class="news__view"><div class="container news__view-content-text">
            <div class="share-social">
                <p class="head"><?php echo Text::_( 'TPL_GESPLAN_SHARE' );?></p>
                </div>
                <div class="addthis_inline_share_toolbox"><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-621f488e60708602"></script></div>                        
            </div>
        </article>
        <?php
            $path = dirname(__FILE__) . "/partials/";
            require_once($path.'banner-planificador.php');
        ?>
