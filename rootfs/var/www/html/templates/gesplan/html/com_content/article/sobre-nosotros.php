<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Utilities\ArrayHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
<div class="com-content-article item-page<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="https://schema.org/Article">
	<section id="sobreNosotros">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<meta itemprop="inLanguage" content="<?php echo ($this->item->language === '*') ? Factory::getApplication()->get('language') : $this->item->language; ?>">
			
						<h1> <?php echo $this->item->title ?> </h1>
						<?php echo $this->item->text ?>
				</div>
			</div>

			<?php $blocks = ($this->item->jcfields[117]->subform_rows) ?>
			<!-- Bloque de conenido -->
			<?php foreach($blocks as $key => $block) : ?>
				<div class="row item-sobre-nosotros">

					<?php if($key % 2 == 0 ) : ?>

						<div class="col-md-6">

						
					<?php else : ?>

						<div class="col-md-5 order-2 order-md-1">
							<?php  $imgUrl = $block["imagen-parrafo"]->rawvalue["imagefile"]; ?>
							<?php if(!empty($imgUrl)) : ?>
								<img class=" w-100" src="<?php echo $imgUrl;?>" alt="<?php  echo($block["imagen-parrafo"]->rawvalue["alt_text"]); ?>" loading="lazy">
							<?php endif; ?>
						</div>
						<div class="col-md-6 offset-md-1 order-1 order-md-2">

					<?php endif; ?>

							<h2><?php  echo $block["titulo-bloque"]->rawvalue; ?></h2>
							<p><?php  echo $block["parrafo"]->rawvalue; ?></p>

					<?php if($key % 2 == 0 ) : ?>
		
						</div>
						<div class="col-md-5 offset-md-1">
							<?php  $imgUrl = $block["imagen-parrafo"]->rawvalue["imagefile"]; ?>
							<?php if(!empty($imgUrl)) : ?>
								<img class=" w-100" src="<?php echo $imgUrl;?>" alt="<?php  echo($block["imagen-parrafo"]->rawvalue["alt_text"]); ?>" loading="lazy">
							<?php endif; ?>
						</div>

					<?php else : ?>

						</div>
						
					<?php endif; ?>
				
				</div>
				
			<?php endforeach; ?>

			<!-- //Bloque de conenido -->
		</div>
	</section>
	<!-- Info de interés -->
	<section id="infoInteres">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2><?php echo Text::_("TPL_GESPLAN_INTRESTING_INFORMATION")?></h2>
                </div>
            </div>

			<!-- Set model -->
			<?php   
        
				$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
				$appParams = JFactory::getApplication()->getParams();  
			?>
			<!-- Get 2 random noticias -->

			<?php

                $catID = 18; //Informacion category ID
				$associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($catID)); //Category for Slider
				$catLang =   Factory::getLanguage()->getTag() ;
				$catId = $associations[ $catLang];   
				$model->setState('params', $appParams);
				$model->setState('filter.category_id', $catId); //change that to your Category ID
				$model->setState('filter.subcategories', true);
				$model->setState('list.limit', 2);
				$model->setState('filter.published', '1');
				$model->setState('list.ordering', Factory::getDbo()->getQuery(true)->rand());

				$articlesCategory =   $model->getItems();

				if($articlesCategory) : //If articles in noticias 

					foreach ($articlesCategory as $item) :

						$fields = FieldsHelper::getFields('com_content.article', $item, true);

						// Adding custom fields to the object
						$item->jcfields = array();

						foreach ($fields as $key => $field)
						{
							$item->jcfields[$field->id] = $field;
						}                       
			?>  


			<!-- loop -->
            <div class="row inf-inter-extra">
                <div class="col-md-7">
				<?php  $images = json_decode($item->images); ?>
                                        <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" class="w-100" loading="lazy">
                </div>
                <div class="col-md-5">
                    <p class="antetitulo"><?= isset($item->jcfields[9]->rawvalue) ? $item->jcfields[9]->rawvalue : $item->category_alias?></p>
                    <h3><?= $item->title?></h3>
					<?php   
						
						$textoLargo =  strip_tags($item->jcfields[14]->value);  //entradilla
						$textoCorto = substr($textoLargo, 0, 150);
						$resumen = substr($textoCorto, 0, strrpos($textoCorto, ' '));
						echo '<p>'.$resumen.' ...</p>';
					?>                    
					<a href="<?php echo Route::_(RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));?>"><?php echo Text::_( 'TPL_GESPLAN_VIEW_ARTICLE' );?></a>
                </div>
            </div>

			<!-- //loop -->
					<?php endforeach; ?>
				<?php endif; //If articles in noticias ?> 


        </div>
    </section>
	<!-- //Info de interés -->

</div>