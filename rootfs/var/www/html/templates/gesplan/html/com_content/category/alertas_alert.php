<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;



// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);

?>


    <?php if ($canEdit) : ?>
		<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
	<?php endif; ?>
    <?php
    $user = Factory::getUser();
    $status = $user->guest;
    //dump($this->item);
   ?>
   <!-- loop start -->
 <?php //if($this->item->jcfields[215]->rawvalue == 1) ://If visible-alerta is true?>
    <div class="alert-item-card">
        <div class="row">
            <div class="col-1">
            <?= HTMLHelper::_('image','notification.svg', Text::_('TPL_GESPLAN_ROUTE_ICON'), null, true, 0) ?>
            </div>
            <div class="col-11">
                <p class="date">  <time datetime="<?php echo HTMLHelper::_('date', $this->item->jcfields[181]->rawvalue, 'c'); ?>" itemprop="datePublished">
                                <?php echo HTMLHelper::_('date', $this->item->publish_up, Text::_('DATE_FORMAT_LC2')); ?>
                            </time></p>
                <p class="title-notification"><?php echo $this->item->jcfields[149]->rawvalue;?></p>
                <p class="body-notification">
                    <?php  
                            // $textoCorto = substr($this->item->jcfields[150]->rawvalue, 0, 150);
                            // $resumen = substr($textoCorto, 0, strrpos($textoCorto, ' '));
                            // echo '<p>'.$resumen.' ...</p>';
                            echo $this->item->jcfields[155]->rawvalue;
                    ?>
                </p>
                <?php if(isset($this->item->jcfields[150]->rawvalue)):?>
                    <?php if(!empty($this->item->jcfields[150]->rawvalue)):?>
                        <p class="alert-description"><?= $this->item->jcfields[150]->rawvalue;?></p>
                    <?php endif; ?>
                <?php endif; ?>
                <?php                    
                    //Get alert external link
                    $external_link = $this->item->jcfields[158]->rawvalue;
                ?>

                <?php  if($external_link != null || $external_link != ''): ?>
                
                    <a href="<?php echo $external_link;?>" target="_blank" title="<?php echo $this->item->title;?>"class="more-info"><?php echo Text::_( 'TPL_GESPLAN_MORE_INFO' );?></a>
           
                <?php endif; ?>

            </div>

        </div>
    </div>
<?php //endif; ?>
        <!-- end loop -->   

                   