<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;

$app = Factory::getApplication();

$this->category->text = $this->category->description;
$app->triggerEvent('onContentPrepare', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$this->category->description = $this->category->text;

$results = $app->triggerEvent('onContentAfterTitle', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayTitle = trim(implode("\n", $results));

$results = $app->triggerEvent('onContentBeforeDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$beforeDisplayContent = trim(implode("\n", $results));

$results = $app->triggerEvent('onContentAfterDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayContent = trim(implode("\n", $results));

//Pintamos título y descripción de la categoría FAQ
?>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->

<section class="intro intro--basic faqs pb-0">
    <div class="container">
        <div class="row">
            <div class="col">
				<?php if ($this->params->get('show_category_title', 1)) : ?>
                <h1><?php echo $this->category->title; ?></h1>
				<?php endif; ?>
                <?php if ($this->params->get('show_description') && $this->category->description) : ?>
				<?= $this->category->description ?>
				<?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php 
//Pintamos las subcategorías de faqs
if ($this->maxLevel != 0 && !empty($this->children[$this->category->id])) : ?>
<section class="faq pt-0 control-responsive">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="responsive-tabs">
                    <ul class="nav nav-tabs" role="tablist">
					    <?php echo $this->loadTemplate('tabschildren'); ?>
					</ul>
					<div id="content" class="tab-content" role="tablist">
					    <?php echo $this->loadTemplate('tabscontentchildren'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
<?php
    $path = dirname(__DIR__, 1) . "/article/partials/";
    require_once($path.'ayuda.php');
?>
