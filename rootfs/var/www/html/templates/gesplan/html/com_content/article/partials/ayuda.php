<?php   
/**
 * Template for content "¿Necesitas ayuda?"
 * Id article 40
 */

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
        use Joomla\CMS\Router\Route;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Utilities\ArrayHelper;

        $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
        $appParams = Factory::getApplication()->getParams(); 
        $model->setState('params', $appParams);
        $inicio_page_id = 32;  
        $associations = Associations::getAssociations('com_content', '#__content', 'com_content.item', $inicio_page_id );
        $articleLang = Factory::getLanguage()->getTag();
        $idLang = intval(explode(":", $associations[$articleLang]->id)[0]);
        $catID =  intval(explode(":", $associations[$articleLang]->catid)[0]);
        $model->setState('filter.article_id', $idLang); //Inicio page id
        $model->setState('filter.category_id', $catID); //change that to your Category ID  

        
        $helpArticle =   $model->getItems();
        $item = $helpArticle[0];
        $fields = FieldsHelper::getFields('com_content.article', $item, true);
        // Adding the fields to the object
        $item->jcfields = array();

        foreach ($fields as $key => $field)
        {
            $item->jcfields[$field->id] = $field;
        }

?>
<section class="ayuda">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-7">
                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="title-section">
                            <h2><?= $item->jcfields[34]->value; //Título ayuda?></h2>
                            <p><?= $item->jcfields[35]->value; //Descripción ayuda?></p>
                            <p><a href="mailto:<?= $item->jcfields[114]->value; //Correo ayuda?>" target="_blank"><?= $item->jcfields[114]->value; //Correo ayuda?></a></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mb-4">
                        <h3><?= $item->jcfields[36]->value; //Consulta faq?></h3>
                        <p><?= $item->jcfields[37]->value; //Desc consulta faq?></p>
                        <?php 
                                $catID = 10; //Inspirador category ID  
                                $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($catID));
                                $catLang =   Factory::getLanguage()->getTag() ;
                                $catId = $associations[ $catLang]; 
                        ?>
                        <a href="<?php  echo Route::_("index.php?option=com_content&view=category&layout=gesplan:faq&id=".$catId); //Button faq?>" title="<?= $item->jcfields[38]->value?>"><?= $item->jcfields[38]->value?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="deco-img">
        <figure>
            <?= $item->jcfields[112]->value; //imagen decorativa?>
        </figure>
    </div>
</section>