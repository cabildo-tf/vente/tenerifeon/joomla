<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 * Output articles list in noticias page
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);
0

?>

<div class="col-md-6 col-lg-4 mb-5">
	<div class="card-sub-itinerarios">
		<a class="hyper-link" href="<?=Route::_(RouteHelper::getArticleRoute($this->item->id, $this->item->catid, $this->item->language));?>"
										title="<?php echo Text::_( 'TPL_GESPLAN_VIEW_EQUIPMENT' );?> <?php echo $this->item->title;?>"></a>
		<figure>
			<?php
					$images = json_decode($this->item->images); 
					if(isset($images->image_intro) && (!is_null($images))) {
						$listImage['src'] = $images->image_intro ;
						$listImage['alt'] = $images->image_intro_alt;
					}else {
						$listImage['src'] = "templates/gesplan/images/neutra-300.jpg";
						$listImage['alt'] = "Tenerife ON";
					};
					?>
				<img src="<?php echo $listImage['src'] ; ?>"	alt="<?php echo $listImage['alt'] ; ?>"loading="lazy">
		</figure>
					
		<h2><?php echo $this->item->title;?></h2>
		
		<p class="btn-card-sub">
			<?php echo JText::_( 'TPL_GESPLAN_VIEW_EQUIPMENT' );?>	
		</p>	
	</div>
</div>


<?php echo $this->item->event->afterDisplayContent; ?>