<?php

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Language\Associations;

?>
<div class="container">
    <div class="row content-horizontal-scrol">
        <div class="col-md-12">
            <h2 class="head-scroll"><?= Text::_("TPL_GESPLAN_INTEREST_PLACES");?></h2>
            <p><?= Text::_("TPL_GESPLAN_SOME_INTRESTING_POINTS_ON_THE_ROUTE") ?>.</p>
        </div>
        <div class="col-md-12">
            <div class="multiple-items">

            <?php   
                //Get all Espacio natural by that contains cfield[69] valores-y-puntos-de-interes
                
                $points_for_modal = [];
                foreach($puntos_interes as $key => $punto){
                    if(!is_null($punto) && (!empty($punto))){
                        $associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $punto );
                        if(count($associations) > 0) { //If has associations, get id to show in current language
                            $currentLang = Factory::getLanguage()->getTag();
                            foreach($associations as $assoc){
                                if($assoc->language == $currentLang){
                                    $articleLang = $currentLang;
                                    $articleId = intval(explode(":", $associations[$articleLang]->id)[0]);
                                }
                            }
                        }
                        else{
                            $articleId = $punto; //If not association, spanish article id
                        }
                        $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
                        $appParams = Factory::getApplication()->getParams();  
                        $model->setState('params', $appParams);
                        $model->setState('filter.article_id', $articleId); //Ayuda article id
                        $helpArticle =   $model->getItems();
                        
                        $item = $helpArticle[0];
                        $fields = FieldsHelper::getFields('com_content.article', $item, true);
                        // Adding the fields to the object
                        $item->jcfields = array();
                        foreach ($fields as $field)
                        {
                            $item->jcfields[$field->id] = $field;
                        }
                        $points_for_modal[$key] = [
                            'title' => $item->title,
                            'introtext' => $item->introtext,
                            'lat' => $item->jcfields[202]->rawvalue,
                            'lon' => $item->jcfields[203]->rawvalue,
                            'tel' => '',
                            'email' => ''
                        ];
                        if($item->jcfields[167]->rawvalue != ''){
                            $points_for_modal[$key]['tel'] = $item->jcfields[167]->rawvalue;
                        }
                        if($item->jcfields[168]->rawvalue != ''){
                            $points_for_modal[$key]['email'] = $item->jcfields[168]->rawvalue;
                        }
                        ?>

                            <div class="px-3">
                                <div class="card-default">
                                    <figure>
                                        <div class="gradient"></div>
                                        <?php  $images = json_decode($item->images); ?>
                                        <?php if(!empty($images->image_intro)) : ?>
                                            <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy" >
                                            <?php $points_for_modal[$key]['image'] = $images->image_intro; ?>
                                        <?php else: ?>
                                            <img src="templates/gesplan/images/neutra-300.jpg" alt="Tenerife ON" loading="lazy" >
                                            <?php $points_for_modal[$key]['image'] ="templates/gesplan/images/neutra-300.jpg"; ?>
                                        <?php endif; ?>                                               
                                    </figure>
                                    <div class="caption">
                                        <div class="head-caption">
                                            <span><?= $item->category_title;?></span>
                                            <p><?= $item->title?></p>
                                            <div class="desc-intesting-point"><?php echo current(explode('::BR::', wordwrap($item->introtext, 100, '::BR::'))); if($item->introtext != ''){echo '...';}  ?></div>
                                            <?php if($item->introtext != ''): ?>
                                            <button type="button" class="btn interest-point" data-bs-toggle="modal" data-bs-target="#interestPoint-<?= $key;?>">
                                                <?= Text::_('TPL_GESPLAN_MORE_INFO'); ?>
                                            </button>
                                            <?php else : ?>
                                                <?php if ($item->jcfields[167]->rawvalue != ''): ?>
                                                    <a href="tel:<?= $item->jcfields[167]->rawvalue ?>"><?= $item->jcfields[167]->rawvalue ?></a>
                                                <?php endif; ?>
                                                <?php if ($item->jcfields[168]->rawvalue != ''): ?>
                                                    <a href="mailto:<?= $item->jcfields[168]->rawvalue ?>"><?= $item->jcfields[168]->rawvalue ?></a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                    <?php            
                    }
                }
                
                

            ?>
    
            </div>
        </div>
        <?php foreach($points_for_modal as $key => $point) :?>
            <!-- Modal -->
            <div class="modal fade" id="interestPoint-<?= $key;?>" tabindex="-1" aria-labelledby="interestPoint-<?= $key;?>Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="row">
                            <img src="<?php echo $point['image']; ; ?>" alt="<?= $point['title'];?>" loading="lazy" class="w-100" >
                        </div>

                        <div class="modal-header">
                            <h5 class="modal-title" id="interestPoint-<?= $key;?>Label"><?= $point['title']?></h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <?= $point['introtext']; ?><?php //dump($point); ?>
                            <div class="lugares-interes">
                                <?php if($point['tel'] != ''): ?>
                                <a href="tel:<?= $point['tel']?>"><?= $point['tel']?></a><br/>
                                <?php endif;; ?>
                                <?php if($point['email'] != ''): ?>
                                <a href="mailto:<?= $point['email']?>"><?= $point['email']?></a>
                                <?php endif;; ?>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>   
            
        <?php endforeach;?>
    </div>
</div>