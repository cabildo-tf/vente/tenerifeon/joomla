<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2010 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;

$lang   = Factory::getLanguage();
$user   = Factory::getUser();
$groups = $user->getAuthorisedViewLevels();

if ($this->maxLevel != 0 && count($this->children[$this->category->id]) > 0) : ?>

	<?php foreach ($this->children[$this->category->id] as $id => $child) : ?>



		<?php if (in_array($child->access, $groups)) : ?>
			<?php if ($this->params->get('show_empty_categories') || $child->numitems || count($child->getChildren())) : ?>
				<div class="col-lg-6 col-sm-6 news__theme-card-container position-relative">
					<a class="hyper-link" href="<?php echo Route::_(RouteHelper::getCategoryRoute($child->id, $child->language)); ?>" title="<?php echo JText::_( 'Ver todos los artículos de');?> <?php echo $this->escape($child->title); ?>">
					</a>
					<article class="news__theme-card d-flex align-items-start flex-column">
						<figure class="news__theme-card-image">
							<?php 
								$listImage = [];
								$image = json_decode($child->params);
								
								if(isset($image)):
									$listImage['src'] = $image->image;
									$listImage['alt'] = $image->image_alt;
								else :
									$listImage['src'] = "templates/gesplan/images/neutra-900.jpg";
									$listImage['alt'] = "Tenerife ON";
								endif;
							?>
							
							<img src="<?= $listImage['src'] ?>" alt="<?= $listImage['alt']?>" loading="lazy"/>
						</figure>

				
						<div class="news__theme-card-text">
							<h2><?php echo $this->escape($child->title); ?></h2>
							<?php echo $child->description ?>
						</div>

						<div class="news__theme-card-button mt-auto">
							<p class="link-primary">
								<?php echo Text::_( 'TPL_GESPLAN_SEE_ARTICLES');?>
							</p>
						</div>
						
		
					</article>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endforeach; ?>

<?php endif;
