<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Utilities\ArrayHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
$category_title   = $this->escape($this->item->category_title);

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
<?php $db = Factory::getDbo(); ?>
<?php   $user =   Factory::getUser();
        $status = $user->guest;
?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
   <section class="hero-banner">
        <div class="hero-slick">

        <?php   
        
            $model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
            $appParams = JFactory::getApplication()->getParams();  
        ?>
        <?php
            $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations(36)); //Category for Slider
            $catLang =   Factory::getLanguage()->getTag() ;
            $catId = $associations[ $catLang];               
            $model->setState('params', $appParams);
            $model->setState('filter.category_id', $catId); //change that to your Category ID
            $model->setState('filter.language', JFactory::getLanguage());
            $model->setState('list.ordering', 'created');
            $model->setState('filter.published', '1');
            $model->setState('list.direction', 'DESC');

            $articlesCategory =   $model->getItems();

                foreach ($articlesCategory as $index => $item) :

                    $fields = FieldsHelper::getFields('com_content.article', $item, true);
                    // Adding the fields to the object
                    $item->jcfields = array();

                    foreach ($fields as $key => $field)
                    {
                        $item->jcfields[$field->id] = $field;
                    }

        ?>
         <?php  $desktopImg = json_decode($item->jcfields[31]->rawvalue); ?>
         <?php  $mobileImg = json_decode($item->jcfields[32]->rawvalue); ?>
        <?php $imgUrl = explode('#', $desktopImg->imagefile);
                $imgUrl = $imgUrl[0];
        ?>
        <div style="background-image:url(<?php echo $imgUrl;?>)" class="banner-img">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-4">
                        <div class="caption">
                       
                            <?php if($index == 0) :?>
                                <h1><?= $item->title;?></h1>
                            <?php else :?>
                                <h2><?= $item->title;?></h2>
                            <?php endif; ?>
                                <?php echo $item->introtext ?>
                            <a href="<?php echo $item->jcfields[28]->rawvalue; //Enlace url?>" target=" <?php echo $item->jcfields[30]->rawvalue; ?>" title="<?php echo $item->jcfields[29]->rawvalue; //Texto enlace?>"><?php echo $item->jcfields[29]->rawvalue; //Texto enlace?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php endforeach; ?>
           
        </div>
    </section>
    <section id="news_home">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title">
                        <h2> <?php echo $this->item->jcfields[15]->value ; ?></h2>
                        <p> <?php echo $this->item->jcfields[16]->value ; ?></p>
                    </div>
                </div>
            </div>
            <div class="row col-separator">
                <?php  
                $catId = 8; //Noticias category
                $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($catId));
                $catLang =   Factory::getLanguage()->getTag() ;
                $catId = $associations[ $catLang];               
                $model->setState('params', $appParams);
                $model->setState('filter.category_id', $catId); //change that to your Category ID
                $model->setState('list.ordering', 'created');
                $model->setState('filter.subcategories', true);
				$model->setState('list.limit', 6);
                $model->setState('list.direction', 'DESC');
                $model->setState('filter.published', '1');

                $articlesCategory =   $model->getItems();

                if($articlesCategory) : //If articles in noticias 

                    foreach ($articlesCategory as $item) :

                        $fields = FieldsHelper::getFields('com_content.article', $item, true);
                        // Adding the fields to the object
                        $item->jcfields = array();

                        foreach ($fields as $key => $field)
                        {
                            $item->jcfields[$field->id] = $field;
                        }                       
                ?>              
                            <div class="col-md-6 col-lg-4 position-relative">
                                    <a class="news-linker" title="Link a la noticia '<?= $item->title?>'" href="<?php echo Route::_(RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));?>">
                                            <p><?php echo Text::_( 'TPL_GESPLAN_CONTINUE_READING' );?></p>
                                        </a>
                                          <!-- <a><i class="far fa-comment"></i><span>3</span></a> -->
                        
                                <?php   //Check if user has already masrked as favourite
                                        $query = $db
                                        ->getQuery(true)
                                        ->select(array('COUNT(id)'))
                                        ->from($db->quoteName('#__sg_favourites'))
                                        ->where($db->quoteName('user_id') . " = " . $db->quote($user->id))
                                        ->where($db->quoteName('item_id') . " = " . $db->quote($item->id));
                                        // Reset the query using our newly populated query object.
                                        $db->setQuery($query);
                                        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                                        $isFavourite = $db->loadObjectList();
                                ?>                               
                                    <button id="item-<?= $item->id?>" <?php if(!$status):?>
                                        onclick="addToFavourites(<?= $item->id?>)"
                                            <?php else: ?> 
                                        data-bs-toggle="modal" data-bs-target="#mustlog"
                                            <?php endif; ?>
                                            <?php if($isFavourite[0]->count):?>
                                                class="like-post checkd"  onclick="addToFav('remove',<?= $item->id?>)"
                                                <?php else:?>  
                                                    class="like-post"
                                                <?php endif; ?>>
                                            <i class="far fa-heart"></i></button>

                                    <!-- <button class="like-post"><i class="far fa-heart"></i></button> -->
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                                <script>

                                        function addToFavourites(item_id){

                                            var baseUrl = "/index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&item=" + item_id;

                                            Joomla.request({
                                                url: baseUrl +  "&plugin=Addtofavourites",
                                                method: 'GET',
                                                perform: true,
                                                onSuccess: function onSuccess(resp) {
                                                    var data = JSON.parse(resp);
                                                    if(data.data[0].message == "ADDED"){
                                                        $("#item-"+(data.data[0].item)+"").addClass('checkd');
                                                        Swal.fire({
                                                                html: '<p>¡Guardado en favoritos!</p>',
                                                                confirmButtonText: 'Aceptar'
                                                        })
                                                    }
                                                    else if(data.data[0].message == "REMOVED"){
                                                        $("#item-"+(data.data[0].item)+"").removeClass('checkd');
                                                        Swal.fire({
                                                                html: '<p>Eliminado de favoritos!</p>',
                                                                confirmButtonText: 'Aceptar'
                                                        })
                                                    }
                                                },
                                                onError: function onError() {
                                                    Joomla.renderMessages({
                                                    error: ['Something went wrong! Please close and reopen the browser and try again!']
                                                    });
                                                }
                                            });


                                        }
       


                                </script>
                                    <div class="card-default">
                                        
                                    <?php if($item->jcfields[40]->value != "") : //marcado novedad?>
                                        <span class="novedad"> <?= $item->jcfields[40]->value;?></span>
                                    <?php endif; ?>
                                        <figure>
                                            <div class="gradient"></div>
                                            <?php  $images = json_decode($item->images); ?>
                                            <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy" >

                                        </figure>

                                        <div class="caption">

                                            <div class="head-caption">
                                            <time datetime="<?php echo HTMLHelper::_('date', $item->publish_up, 'c'); ?>" itemprop="datePublished">
                                                <?php echo HTMLHelper::_('date', $item->publish_up, Text::_('DATE_FORMAT_LC3')); ?>
                                            </time>
                                                <span class="conector">_</span>
                                                <span><i><?php echo Text::_( 'en' );?></i> <?php echo $item->category_alias; ?></span>
                                            </div>
                                            <div class="body-caption">
                                                <h3><?= $item->title?></h3>
                                                <?php   $textoLargo =  strip_tags($item->jcfields[14]->value);  //entradilla
                                                        $textoCorto = substr($textoLargo, 0, 150);
                                                        $resumen = substr($textoCorto, 0, strrpos($textoCorto, ' '));
                                                        echo '<p>'.$resumen.' ...</p>';
                                                ?>
                                            </div>
                                            
                                        </div>
                                        <div class="link-card">
                                            <p><?php echo Text::_( 'TPL_GESPLAN_CONTINUE_READING' );?></p>
                                        </div>
                                        
                                    </div>
                             
                            </div>
                        

						<?php endforeach; ?>
                        <?php endif; //If articles in noticias ?> 
            </div>
        </div>
        <div class="fullPage-more-btn">
            <a href="<?php  echo Route::_("index.php?option=com_content&view=category&id=".$catId.""); ?>"><?php echo $this->item->jcfields[19]->value ;//enlace-ver-noticias ?></a>
        </div>
    </section>

    <!-- Section inspirador -->
    <?php   
        $catID = 95; //Inspirador category ID  
        $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($catID));
        $catLang =   Factory::getLanguage()->getTag() ;
        $catId = $associations[ $catLang];                      
        $model->setState('params', $appParams);
        $model->setState('filter.category_id', $catId); //change that to your Category ID
        $model->setState('list.ordering', 'created');
        $model->setState('list.limit', 4);
        $model->setState('list.direction', 'DESC');
        $model->setState('filter.published', '1');

        $articlesCategory =   $model->getItems();
    ?>
    <?php   if($articlesCategory) : //If articles in noticias ?>

     
    <section id="inspirador_home">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="section-title">
                        <h2>
                            <?= $this->item->jcfields[17]->rawvalue;?>
                            <strong><span style="text-transform: uppercase;"><?= Text::_("TPL_GESPLAN_INSPIRATOR");?></span></strong>
                        </h2>
                        <p><?= $this->item->jcfields[18]->rawvalue;?></p>
                    </div>
                </div>
            </div>
            <div class="row">

            <?php   foreach ($articlesCategory as $item) : //start the loop

                        $fields = FieldsHelper::getFields('com_content.article', $item, true);
                        // Adding the fields to the object
                        $item->jcfields = array();

                        foreach ($fields as $key => $field)
                        {
                            $item->jcfields[$field->id] = $field;
                        }                       
            ?>   
                <div class="col-md-6 col-lg-6 col-xl-3 my-4">
                    <div class="card-inspirador">
                        
                            <figure>
                            <?php  $images = json_decode($item->images); ?>
                                    <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy" >
                            </figure>
                        
                        <div class="body-inspirator">
                            <h3><?= $item->title ?></h3>
                            <p><?= $item->jcfields[274]->rawvalue //subtitulo-inspirador?></p>
                            <div class="link-card">
                                <a href="<?php echo Route::_(RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));?>"><?= Text::_("TPL_GESPLAN_MORE_INFO");?></a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>


            </div>
            <!-- <div class="row">
                <div class="col-12">
                    <a class="more-options" href="<?php  echo Route::_("index.php?option=com_content&view=article&id=1157"); ?>">Ver todas las opciones</a>
                </div>
            </div>  -->
        </div>
    </section>

    <?php endif; //Inspirador section?> 
    <?php
            $path = dirname(__FILE__) . "/partials/";
            require_once($path.'modal-favorito.php');
        ?>
    <?php
    $path = dirname(__FILE__) . "/partials/";
    require_once($path.'banner-planificador.php');
    require_once($path.'prefooter.php');
    //require_once($path.'instagram.php');
    require_once($path.'ayuda.php');
    //require_once($path.'colaboradores.php');
?>

