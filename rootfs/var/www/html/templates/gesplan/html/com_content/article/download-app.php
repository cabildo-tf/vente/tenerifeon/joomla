<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create shortcuts to some parameters.
$params  = $this->item->params;
$canEdit = $params->get('access-edit');
$user    = Factory::getUser();
$info    = $params->get('info_block_position', 0);
$htag    = $this->params->get('show_page_heading') ? 'h2' : 'h1';
$category_title   = $this->escape($this->item->category_title);

// Check if associations are implemented. If they are, define the parameter.
$assocParam        = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
?>
      <section id="download-app">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title">
                        <h1><?= $this->item->title?></h1>
                        <?php echo $this->item->introtext;?>                      
                    </div>
                    <div class="row mt-auto mb-0">
                        <div class="col-6">
                            <a href="<?= $this->item->jcfields[259]->rawvalue;?>">  <?= HTMLHelper::_('image','btn-Store-android.svg', Text::_("TPL_GESPLAN_PLANIFICADOR"), 'class="btn-store"', true, 0) ?></a>
                        </div>
                        <div class="col-6">
                            <a href="<?= $this->item->jcfields[260]->rawvalue;?>">  <?= HTMLHelper::_('image','btn-Store-ios.svg', Text::_("TPL_GESPLAN_PLANIFICADOR"), 'class="btn-store"', true, 0) ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                <?= HTMLHelper::_('image','/app-mockup.jpg', Text::_("TPL_GESPLAN_PLANIFICADOR"), 'class="w-100"', true, 0) ?>
                </div>

            </div>
        </div>
    </section>

  
    <?php
    $path = dirname(__FILE__) . "/partials/";
   
    require_once($path.'ayuda.php');
    //require_once($path.'colaboradores.php');
?>

