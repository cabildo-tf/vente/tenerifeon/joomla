<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 * Output articles list in noticias page
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);


?>

<div class="information__category__featured" data-aos="fade-up">
	<div class="row">
		<div class="col">
			<article class="information__category__featured-in">
				<div class="row">
					<div class="col-lg-7">
						<figure class="information__category__featured-image">
							<?php
								$images         = json_decode($this->item->images); 
							?>
							<img src="<?php echo $images->image_intro ; ?>"
								alt="<?php echo $images->image_intro_alt ; ?>" class="img-fluid" loading="lazy">
						</figure>
					</div>
					<div class="col-lg-5 d-flex align-items-start flex-column position-relative">
						<a class="hyper-link" href="<?=Route::_(RouteHelper::getArticleRoute($this->item->id, $this->item->catid, $this->item->language));?>"
										title="<?php echo JText::_( 'TPL_GESPLAN_VIEW_ARTICLE' );?> <?php echo $this->item->title;?>"></a>
						<div class="information__category__container__text">
							<div class="information__category__featured-text">
								<p class="information__category__featured-category">
									<?php echo($this->item->jcfields[9]->value); //cf antetitulo?></p>
								<h2><?php echo $this->item->title;?></h2>
								<p><?php echo($this->item->jcfields[14]->value); //cf entradila?></p>
							</div>
							<div class="information__category__featured-button mt-auto">
								<p class="m-0">
										<?php echo JText::_( 'TPL_GESPLAN_VIEW_ARTICLE' );?>	
								</p>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>


<?php echo $this->item->event->afterDisplayContent; ?>