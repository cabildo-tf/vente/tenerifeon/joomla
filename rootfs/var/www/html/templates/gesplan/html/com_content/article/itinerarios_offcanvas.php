  <!-- Modal itinerarios-->
  
  <div class="offCanvascontainer">
    {{-- incio del bucle --}}
    <div class="offCanvas-item">
      <button class="open-rout-btn">
        <i class="fa fa-times"></i>
      </button>
      <p class="title"><span class="badge">1</span>Las Mercedes - Cruz del Carmen</p>
      <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore.</p>
      <div class="content-actions">
          <div id="content-valoration">
            <img src="/img/icon-full-star.svg" alt="">
            <img src="/img/icon-full-star.svg" alt="">
            <img src="/img/icon-full-star.svg" alt="">
            <img src="/img/icon-full-star.svg" alt="">
            <img src="/img/icon-half-star.svg" alt="">
          </div>
          <a href="">
            <i class="far fa-comment"></i>
            3
          </a>
      </div>
      <div id="carouselOfCanvas" class="carousel slide my-2" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="/img/fake-img.png" class="d-block w-100" alt="..." loading="lazy">
          </div>
          <div class="carousel-item">
            <img src="/img/fake-img.png" class="d-block w-100" alt="..." loading="lazy">
          </div>
          <div class="carousel-item">
            <img src="/img/fake-img.png" class="d-block w-100" alt="..." loading="lazy">
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselOfCanvas" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselOfCanvas" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
      <div class="content-values">
        <div class="item-value">
            <i class="fa fa-long-arrow-alt-right"></i>
            <p class="value">2,7km</p>
        </div>
        <div class="item-value">
            <i class="far fa-clock"></i>
            <p class="value">1h 30m</p>
        </div>
        
        <div class="item-value">
            <i class="fas fa-level-up-alt"></i>
            <p class="value">300m</p>
        </div>
        <div class="item-value">
            <i class="fas fa-level-down-alt"></i>
            <p class="value">54m</p>
        </div>
        <div class="item-value alert">
            <i class="far fa-bell"></i>
            <p class="value">Alerta</p>
        </div>
      </div>
      <div class="more-actions">
        <a href="" title="añadir a mis rutas"><i class="far fa-heart"></i> <span>Añadir a mis rutas</span></a>
        <a href="" title="añadir a mis rutas"><i class="fa fa-share-alt"></i> <span>Compartir itinerario</span></a>
        <a href="" title="descargar itinerario"><i class="fa fa-cloud-download-alt"></i> <span>Descargar track</span></a>
      </div>
      <a href="" alt="ir a la ficha del itinerario" class="btn-ficha-completa">
        Ver la ficha completa
      </a>
    </div>
    {{-- fin del bucle --}}
  </div>