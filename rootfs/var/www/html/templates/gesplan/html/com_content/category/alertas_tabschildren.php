<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Categories\Categories;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;

$lang   = Factory::getLanguage();
$user   = Factory::getUser();
$groups = $user->getAuthorisedViewLevels();

?>
				<?php	$associations = Associations::getAssociations('com_content', '#__categories', 'com_categories.item', 55, 'id', 'alias', '');
						//dd($associations[$lang->getTag()]->id);
						$cat_id = (explode(":", $associations[$lang->getTag()]->id))[0];
						$categories = Categories::getInstance('com_content', ['published' => false, 'access' => false]);
						$category = $categories->get($cat_id);
				?>

				<li class="nav-item" role="presentation">
                    <button class="nav-link active" id="tabs-<?php echo $category->title;  ?>" data-bs-toggle="tab" data-bs-target="#<?php echo $category->title;  ?>" type="button" role="tab" aria-controls="home" aria-selected="true">
					<?php echo $category->title;  ?>
					</button>
                </li>
				<?php	$associations = Associations::getAssociations('com_content', '#__categories', 'com_categories.item', 90, 'id', 'alias', '');
						//dd($associations[$lang->getTag()]->id);
						$cat_id = (explode(":", $associations[$lang->getTag()]->id))[0];
						$categories = Categories::getInstance('com_content', ['published' => false, 'access' => false]);
						$category = $categories->get($cat_id);
				?>

				<li class="nav-item" role="presentation">
                    <button class="nav-link" id="tabs-<?php echo $category->title;  ?>" data-bs-toggle="tab" data-bs-target="#<?php echo $category->title;  ?>" type="button" role="tab" aria-controls="home" aria-selected="false">
					<?php echo $category->title;  ?>
					</button>
                </li>
