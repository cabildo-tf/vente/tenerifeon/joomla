<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_finder
 *
 * @copyright   (C) 2011 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Categories\Categories;
use Joomla\CMS\Factory;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Language\Associations;

/*
* This segment of code sets up the autocompleter.
*/
?>
<section>
	<div class="com-finder finder">
		<?php if ($this->params->get('show_page_heading')) : ?>
		<h1>
			<?php if ($this->escape($this->params->get('page_heading'))) : ?>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
			<?php else : ?>
			<?php echo $this->escape($this->params->get('page_title')); ?>
			<?php endif; ?>
		</h1>
		<?php endif; ?>
		<div id="search-form" class="com-finder__form">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<h1><?= $this->params->get('page_title'); ?></h1>
						<p><?= Text::_("TPL_GESPLAN_SEARCH_IN_WEB");?></p>
						<!-- buscador -->
						<form action="" method="get" class="js-finder-searchform">
                            <?php //echo $this->getFields(); ?>
                            <fieldset class="com-finder__search word mb-3">
                                <legend class="com-finder__search-legend visually-hidden">
                                    <?php echo Text::_('TPL_GESPLAN_FORM_LEGEND'); ?>
                                </legend>
                                <div class="form-inline">
                                    <label for="q" class="me-2">
                                        <?php echo Text::_('TPL_GESPLAN_TERMS'); ?>
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="q" id="q" class="js-finder-search-query form-control" value="<?php if(isset($_GET['q'])){
                                            echo($_GET["q"]); } ?>">
                                        <button type="submit" class="btn btn-primary">
                                            <span class="icon-search icon-white" aria-hidden="true"></span>
                                            <?php echo Text::_('JSEARCH_FILTER_SUBMIT'); ?>
                                        </button>
                                    </div>
                                </div>
                            </fieldset>

                                <fieldset id="advancedSearch" class="com-finder__advanced js-finder-advanced collapse show">
                                    <legend class="com-finder__search-advanced visually-hidden">
                                        <?php echo Text::_('TPL_GESPLAN_ADVANCED_LEGEND'); ?>
                                    </legend>

                                    <div id="finder-filter-window" class="com-finder__filter">

 
                                            <?php   
                                                $categories = [
                                                    'itineraries' => 38,
                                                    'equipments' => 39,
                                                    'natural' => 67,
                                                    'information' => 18,
                                                    'news' => 8
                                                ];
                                                $searchCategory = [];
                                                $catLang =  Factory::getLanguage()->getTag() ;
                                                foreach($categories as & $category){
                                                    $associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($category)); //Category for Itinerarios 
                                                    $catId = $associations[ $catLang]; 
                                                    array_push($searchCategory,$catId);
                                                    $model =  \Joomla\CMS\Categories\Categories::getInstance('com_content');
                                                    $category = $model->get($catId);
                                                }


                                            ?>
                                                <div class="filter-branch"><div class="control-group">
                                                    <div class="control-label">
                                                        <label for="tax-category"><?= Text::_('TPL_GESPLAN_SEARCH_IN_CATEGORY')?></label>
                                                    </div>
                                                    <div class="controls">
                                                        <select id="tax-category" name="cat" class="form-select advancedSelect">
                                                            <option value="" selected="selected"><?= Text::_('TPL_GESPLAN_SEARCH_ALL')?></option>
                                                            <?php foreach($categories as $cat) :?>

                                                                <option value="<?= $cat->id?>" <?php if(isset($_GET['cat'])){
                                                                    if($_GET['cat'] == $cat->id){
                                                                        echo "selected='selected'";
                                                                    }
                                                                }; ?> ><?= $cat->title; ?></option>

                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        

                                    </div>
                                </fieldset>
                        </form>

                        <section id="buscador">
             
                            <?php
                                if(isset($_GET['q'])){
                                    $searchQuery = $_GET["q"];
                                }
                                $searchCategory = implode(',', $searchCategory);
                                if(isset($_GET['cat'])){
                                    if(!empty($_GET['cat'])){
                                        $searchCategory = $_GET["cat"];
                                    }
                                }
                                if(isset($_GET['q']) && !empty($_GET['q'])){
                                    $model = JModelLegacy::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
                                    $appParams = JFactory::getApplication()->getParams();  
                                    $model->setState('params', $appParams);
                                    $model->setState('filter.category_id', $searchCategory); //change that to your Category ID
                                    $model->setState('filter.search', $searchQuery);
                                    $model->setState('list.ordering', 'publish_up');
                                    $model->setState('list.direction', 'DESC');
                                    $model->setState('filter.subcategories', true);
                                    //$model->setState('filter.language', Factory::getLanguage()->getTag());
                                // $model->setState('list.limit', $articlesPerPage);
                                // $model->setState('list.start', $offset);
                                    $model->setState('filter.published', '1');
        
                                    $content =   $model->getItems();
                                    if(sizeof($content) > 0 ){
                                        echo '<div class="listItemBusqueda" id="search-result-list">';
                                        foreach($content as $item){
                                            $categories = JCategories::getInstance('com_content', ['published' => false, 'access' => false]);
                                            if($item->parent_id == 1){
                                                $catid = $item->catid;
                                            }else{
                                                $catid = $item->parent_id;
                                            };
                                            $category = $categories->get($catid);
                                            $item->categoryIcon = $category->getParams()->get('image');
                                            $articleLink = (RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));
                                            $articleLink = Route::_($articleLink);
                                            $description = (HTMLHelper::_('string.truncate', filter_var($item->introtext, FILTER_SANITIZE_STRING), 150));
                                            echo '<div class="itemBusqueda">
                                                    <div class="icon"><img class="img-fluid" src="'.$item->categoryIcon.'" alt="'.$item->category_title.'"></div>
                                                    <div class="content-busqueda">
                                                        <p class="tipe">'.$item->category_title.'</p>
                                                        <a class="head-busqueda" href="'.$articleLink.'">'.$item->title.'</a>
                                                        <p class="des-busqueda">'.$description.'</p>
                                                    </div>	
                                                </div>';
                                        }
                                        echo '</div>';
                                    }
                                    else{
                                        echo '<div id="search-result-empty" class="com-finder__empty">
                                                <h2>'.Text::_('TPL_GESPLAN_NO_RESULTS_HEADING').'</h2><p>'.Text::sprintf('TPL_GESPLAN_NO_RESULTS_BODY', $this->escape($_GET['q'])).'
                                                </p>
                                            </div>';
                                    }

                                }


                            ?>
                        </section>
						<?php // Load the search results layout if we are performing a search. ?>
						<?php //if ($this->query->search === true) : ?>
						<?php //echo $this->loadTemplate('results'); ?>
						<?php //endif; ?>
					</div>
					<div class="col-lg-4">
                            <?php 
                                $path = dirname(__FILE__) . "/partials/";
                                require_once($path.'banner-planificador.php');
                            ?>
					</div>
				</div>
			</div>
	
		</div>
	</div>

</section>