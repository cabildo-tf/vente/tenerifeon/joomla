<?php

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
?>
  <!-- Modal itinerarios-->
  
  <div class="offCanvascontainer">
    <div class="offCanvas-item">
      <button class="open-rout-btn">
        <i class="fa fa-times"></i>
      </button>
      <p class="title"><span class="badge"></span><span class="title"></span></p>
      <p class="category"></p>
      <div class="content-actions">
          <div id="content-valoration">
            <img src="/img/icon-full-star.svg" alt="">
            <img src="/img/icon-full-star.svg" alt="">
            <img src="/img/icon-full-star.svg" alt="">
            <img src="/img/icon-full-star.svg" alt="">
            <img src="/img/icon-half-star.svg" alt="">
          </div>
          <!-- <a href="">
            <i class="far fa-comment"></i>
            3
          </a> -->
      </div>
      <div id="carouselOfCanvas" class="carousel slide my-2" data-bs-ride="carousel">
        <div class="carousel-inner">
          
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselOfCanvas" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselOfCanvas" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
      <div class="content-values">
        <div class="item-value">
            <!-- <i class="fa fa-long-arrow-alt-right"></i> -->
            <p class="description value"></p>
        </div>
        <div class="item-value alert" style="display:none">
            <?= HTMLHelper::_('image', 'icon-warning.svg', Text::_("TPL_GESPLAN_INCIDENCE"), 'class="icon-warning"', true, 0) ?>
            <p class="value"><?= Text::_('TPL_GESPLAN_INCIDENCE')?>
        </div>
      </div>
      <div class="more-actions">
        <div id="addToFavouritesModal"></div>
      </div>
      <a href="" alt="ir a la ficha del itinerario" class="btn-ficha-completa"><?= Text::_("TPL_GESPLAN_VIEW_FULL_DETAIL")?></a>
    </div>
 </div>