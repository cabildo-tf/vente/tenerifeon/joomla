<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);
?>
<div class="accordion-item">
	<?php if ($isUnpublished) : ?>
		<div class="system-unpublished">
	<?php endif; ?>

	<?php if ($canEdit) : ?>
		<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
	<?php endif; ?>

    <h2 class="accordion-header" id="heading-category<?= $this->item->id ?>">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-category<?= $this->item->id ?>" aria-expanded="false" aria-controls="collapse-category<?= $this->item->id ?>">
			<?= $this->item->title ?>
		</button>
    </h2>
    <div id="collapse-category<?= $this->item->id ?>" class="accordion-collapse collapse" aria-labelledby="heading-category<?= $this->item->id ?>" data-bs-parent="#accordion-faq-2">
        <div class="accordion-body">
			<?php echo $this->item->introtext; ?>
        </div>
    </div>
	<?php if ($isUnpublished) : ?>
		</div>
	<?php endif; ?>
</div>
