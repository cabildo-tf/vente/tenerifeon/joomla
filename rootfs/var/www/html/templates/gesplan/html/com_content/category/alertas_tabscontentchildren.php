<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 */

defined('_JEXEC') or die;

use Joomla\CMS\Categories\Categories;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;

$lang   = Factory::getLanguage();
$user   = Factory::getUser();
$groups = $user->getAuthorisedViewLevels();

 ?>
<?php //Print alertas content ?>
<?php	
		//Get alertas category id by language
		$associations = Associations::getAssociations('com_content', '#__categories', 'com_categories.item', 55, 'id', 'alias', '');
		$cat_id = (explode(":", $associations[$lang->getTag()]->id))[0];
		$categories = Categories::getInstance('com_content', ['published' => false, 'access' => false]);
		$category = $categories->get($cat_id);
?>
<div id="<?= $category->title?>" class="card tab-pane fade show active" role="tabpanel" aria-labelledby="<?= $category->title?>-tab">
	<section id="alertas">

			<div class="card-header" role="tab" id="heading-<?= $category->title?>">
				<div class="mb-0">
					<button data-bs-toggle="collapse" href="#collapse-<?= $category->title?>" aria-expanded="true" aria-controls="collapse-<?= $category->title?>"  type="button" role="tab"  aria-selected="true" class="btn accordion-button"><?php echo $this->escape($category->title); ?></button>
				</div>
			</div>
			<div id="collapse-<?= $category->title?>" class="collapse collapse__level  show " data-bs-parent="#content" role="tabpanel" aria-labelledby="heading-collapse-<?= $category->title?>">
				<div class="card-body">
					<div class="accordion" id="accordion-faq-<?= $category->title?>">

					<?php 
						//Use JModelLegacy to get all articles within $categoryID
						$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
						$appParams = JFactory::getApplication()->getParams();
						$model->setState('params', $appParams);
						$model->setState('filter.category_id', $category->id); //change that to your Category ID
						$model->setState('list.ordering', 'created');
						$model->setState('list.direction', 'DESC');
						$model->setState('filter.published', '1');


						($articlesCategory =   $model->getItems());
							foreach ($articlesCategory as $item) {
							$this->item = & $item;
							$fields = FieldsHelper::getFields('com_content.article', $item, true);
							// Adding the fields to the object
							$item->jcfields = array();
					
							foreach ($fields as $key => $field)
							{
								$item->jcfields[$field->id] = $field;
							}

							echo $this->loadTemplate('alert');
						}

					?>
				</div>
			</div>
		</div>
	</section>
</div>


<?php //Print incidencias content ?>
<?php	
		//Get Incidencias category id by language
		$associations = Associations::getAssociations('com_content', '#__categories', 'com_categories.item', 90, 'id', 'alias', '');
		$cat_id = (explode(":", $associations[$lang->getTag()]->id))[0];
		$categories = Categories::getInstance('com_content', ['published' => false, 'access' => false]);
		$category = $categories->get($cat_id);
		//Get incidencias subcategories
		($childCategories = $category->getChildren());
		$incidencesCatid = [];
		foreach($childCategories as $child){
			$incidencesCatid[] = $child->id;
		}
		//dd($incidencesCatid);
?>

<div id="<?= $category->title?>" class="card tab-pane fade" role="tabpanel" aria-labelledby="<?= $category->title?>-tab">
	<section id="incidencias">
			<div class="card-header" role="tab" id="heading-<?= $category->title?>">
				<div class="mb-0">
					<button data-bs-toggle="collapse" href="#collapse-<?= $category->title?>" aria-expanded="true" aria-controls="collapse-<?= $category->title?>"  type="button" role="tab"  aria-selected="true" class="btn accordion-button collapsed"><?php echo $this->escape($category->title); ?></button>
				</div>
			</div>
			<div id="collapse-<?= $category->title?>" class="collapse collapse__level" data-bs-parent="#content" role="tabpanel" aria-labelledby="heading-collapse-<?= $category->title?>">
				<div class="card-body">
					<div class="accordion" id="accordion-faq-<?= $category->title?>">

						<?php 
							//Use JModelLegacy to get all articles within $categoryID
							$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
							$appParams = JFactory::getApplication()->getParams();
							$model->setState('params', $appParams);
							$model->setState('filter.category_id', $incidencesCatid); //change that to your Category ID
							$model->setState('list.ordering', 'created');
							$model->setState('list.direction', 'DESC');
							$model->setState('filter.published', '1');


							($articlesCategory =   $model->getItems());
							foreach ($articlesCategory as $item) {
								$this->item = & $item;
								$fields = FieldsHelper::getFields('com_content.article', $item, true);
								// Adding the fields to the object
								$item->jcfields = array();
						
								foreach ($fields as $key => $field)
								{
									$item->jcfields[$field->id] = $field;
								}

								echo $this->loadTemplate('incidence');
							}

						?>
					</div>
				</div>
			</div>
	</section>
</div>
