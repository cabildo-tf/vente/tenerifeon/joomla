<?php
/**
 * @package     Gesplan.Site
 * @subpackage  com_content
 *
 * @copyright   (C) 2022 studiogenesis
 * Output articles list in noticias page
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;

// Create a shortcut for params.
$params = $this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));

$currentDate   = Factory::getDate()->format('Y-m-d H:i:s');
$isUnpublished = ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $this->item->publish_up > $currentDate)
	|| ($this->item->publish_down < $currentDate && $this->item->publish_down !== null);


?>

	<article class="news__theme-card d-flex align-items-start flex-column position-relative">
				<a class="hyper-link" href="<?php 	echo Route::_(RouteHelper::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language));?>" title="<?= Text::_('TPL_GESPLAN_NEW_LINK');?>: <?= $this->item->title;?>">
				</a>
				<figure class="news__theme-card-image">
				<?php
                            $images         = json_decode($this->item->images); 
                        ?>
                            <img src="<?php echo $images->image_intro ; ?>" alt="<?php echo $images->image_intro_alt ; ?>" loading="lazy">
				</figure>

				<div class="news__theme-card-text">
					<p class="rower">
						<time datetime="<?php echo HTMLHelper::_('date', $this->item->publish_up, 'c'); ?>" itemprop="datePublished">
                            <?php echo HTMLHelper::_('date', $this->item->publish_up, Text::_('DATE_FORMAT_LC3')); ?>
                        </time> <?= Text::_('TPL_GESPLAN_IN');?>
						<a href="<?php echo Route::_(ContentHelperRoute::getCategoryRoute($this->item->catid))?>"> <?= $this->item->category_title; ?></a>
					</p>
					<h3><?php echo $this->item->title;?></h3>
				</div>
				<div class="news__theme-card-button mt-auto">
					<p class="link-primary">
						<?= Text::_('TPL_GESPLAN_NEW_LINK');?>
					</p>
				</div>
				<div class="container"><?php echo $this->item->event->beforeDisplayContent; ?></div>

	</article>

	<?php echo $this->item->event->afterDisplayContent; ?>
