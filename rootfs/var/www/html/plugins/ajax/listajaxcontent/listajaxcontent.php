<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Ajax.listajaxcontent
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Router\ApiRouter;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\Component\Categories\Administrator\Helper\CategoriesHelper;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Language\Associations;

/**
 * Web Services adapter for com_content.
 *
 * @since  __BUMP_VERSION__
 */
class PlgAjaxListajaxcontent extends CMSPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  __BUMP_VERSION__
	 */
	protected $autoloadLanguage = true;

	/**
	 * @var    \Joomla\CMS\Application\CMSApplication
	 *
	 * @since  3.8.0
	 */
	protected $app;

	/**
	 * Registers com_conent's API's routes in the application
	 *
	 * @param   ApiRouter  &$router  The API Routing object
	 *
	 * @return  void
	 *
	 * @since   __BUMP_VERSION__
	 */
	public function onAjaxListajaxcontent()
	{

		//Get option from view input

        $modalidad = $this->app->input->get('modalidad',null,'STRING');
		$distance = $this->app->input->get('distance',null,'STRING');
		$difficulty = $this->app->input->get('difficulty',null,'STRING');
		$zones = $this->app->input->get('zones',null,'STRING');
		
		$categoryId = $this->app->input->get('category',null,'STRING');
		$item = $this->app->input->get('item',null,'STRING');
		$pageNum = $this->app->input->get('page');
		$northEast = $this->app->input->get('northEast');
		$southWest = $this->app->input->get('southWest');
		$given = $this->app->input->get('given');
		$download = $this->app->input->get('download',null,'STRING');
		$downloadkml = $this->app->input->get('downloadkml',null,'STRING');
		$myratings = $this->app->input->get('myratings',null,'STRING');
		$myfavourites = $this->app->input->get('myfavourites',null,'STRING');
		$myactivities = $this->app->input->get('myactivities',null,'STRING');
		$myroutes = $this->app->input->get('myroutes',null,'STRING');
		$contentType = $this->app->input->get('contentType', null,'STRING');
		$getData = $this->app->input->get('getdata', null,'INTEGER');
		$language = $this->app->input->get('language', null,'STRING');
		$getAssociation = $this->app->input->get('getAssociation', null,'STRING'); 


		function fillItemData($item){




			//Set fields field to be filled
			$fields = FieldsHelper::getFields('com_content.article', $item, true);
			//create  article link
			$articleLink = (RouteHelper::getArticleRoute($item->id, $item->catid, $item->language));
			$articleLink = Route::_($articleLink);
			// Adding the fields to the object
			$item->jcfields = array();

			foreach ($fields as $key => $field)
			{
				$item->jcfields[$field->id] = $field;
			}   

			$item->link = $articleLink;    

			$item->introtext = strip_tags ( $item->introtext );
				
			//If there are gallery images
			$galleryImages = [];
			if(isset($item->jcfields[107]->subform_rows)) :
			$images = $item->jcfields[107]->subform_rows;
				if(!empty($images)) :
					if(!empty($images[0]["imagen"]->rawvalue["imagefile"])){
						foreach($images as $key => $image) : 
							$galleryImages[$key]['src'] = $image["imagen"]->rawvalue["imagefile"]; 
							$galleryImages[$key]['alt'] = $image["imagen"]->rawvalue["alt_text"];
						endforeach;
					}else{
						$galleryImages['src'] = "templates/gesplan/images/neutra-300.jpg";
						$galleryImages['alt'] = "Tenerife ON";
					}

				endif;
			endif;
			//Get category equipments id for print icons
			if($item->parent_id == 39 || $item->parent_id == 99 || $item->parent_id == 102 || $item->parent_id == 103 || $item->parent_id == 117 || $item->parent_id == 118 ){
				$categoryCustomIdFeield = 257;
				$db = Factory::getDbo();
				$query = $db
					->getQuery(true)
					->select(array('value'))
					->from($db->quoteName('#__fields_values'))
					->where($db->quoteName('field_id') . " = " . $db->quote($categoryCustomIdFeield))
					->where($db->quoteName('item_id') . " = " . $db->quote($item->catid));
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
					// Load the results as a list of stdClass objects (see later for more options on retrieving data).
					$typeEquipment = $db->loadResult();
					$item->type_equipment = $typeEquipment;
			}
			//Get category equipments id for print icons
			if($item->parent_id == 66 || $item->parent_id == 139 || $item->parent_id == 140 ){
				$categoryCustomIdFeield = 205;
				$db = Factory::getDbo();
				$query = $db
					->getQuery(true)
					->select(array('value'))
					->from($db->quoteName('#__fields_values'))
					->where($db->quoteName('field_id') . " = " . $db->quote($categoryCustomIdFeield))
					->where($db->quoteName('item_id') . " = " . $db->quote($item->catid));
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
					// Load the results as a list of stdClass objects (see later for more options on retrieving data).
					$typeIntrestingPoint = $db->loadResult();
					$item->type_intresting_point = $typeIntrestingPoint;
			}
			$item->gallery = $galleryImages;
			
			$listImage = [];
			$images = json_decode($item->images); 
			if(isset($images->image_intro) && ($images->image_intro != "")) {
				$listImage['src'] = $images->image_intro ;
				$listImage['alt'] = $images->image_intro_alt;
			}else{
				$listImage['src'] = "templates/gesplan/images/neutra-300.jpg";
				$listImage['alt'] = "Tenerife ON";
			};
			if(isset($item->jcfields[207]->rawvalue)){
				$item->duration = date('G\H i\M', mktime(0, $item->jcfields[207]->rawvalue));
			}
			$item->incidence = "";
			//Check if itinerario has incidence, then check if is still active
			if(isset($item->jcfields[253]->rawvalue)){
				if($item->jcfields[253]->rawvalue != ""){
					$db = Factory::getDbo();
					$query = $db
					->getQuery(true)
					->select('state')
					->from($db->quoteName('#__content'))
					->where($db->quoteName('id') . " IN (" .$item->jcfields[253]->rawvalue.")");
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
					// Get state of all incidences
					$indicendeIsActive = $db->loadColumn();
					if (in_array(1, $indicendeIsActive)){
						$item->incidence = $item->jcfields[253]->rawvalue;
					}
	
				}
			}
			$item->originalId = getAssociation($item->id);
			//Check if equipamiento has incidence, then check if is still active
			if(isset($item->jcfields[254]->rawvalue)){
				
				if($item->jcfields[254]->rawvalue != ""){
					$db = Factory::getDbo();
					$query = $db
					->getQuery(true)
					->select('state')
					->from($db->quoteName('#__content'))
					->where($db->quoteName('id') . " = " . $db->quote($item->jcfields[254]->rawvalue));
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
					// Load the results as a list of stdClass objects (see later for more options on retrieving data).
					$indicendeIsActive = $db->loadResult();
	
					if($indicendeIsActive == 1){
						$item->incidence = $item->jcfields[254]->rawvalue;
					}
	
				}
			}

			$item->imageItem = $listImage;
			
			//Check if current item is user favourite
			$user       = Factory::getUser();
			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select('COUNT(*)')
			->from($db->quoteName('#__sg_favourites'))
			->where($db->quoteName('user_id') . " = " . $db->quote($user->id))
			->where($db->quoteName('item_id') . " = " . $db->quote($item->id));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$hasFavourite = $db->loadResult();
			$item->favourite = false;
			if(($hasFavourite > 0)){
					$item->favourite = true;
				}
			
			return $item;

		}
		function getAssociation($getAssociation){
			$model = JModelLegacy::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
			$appParams = JFactory::getApplication()->getParams();  
			$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $getAssociation );
			if(count($associations) > 0) {
				$getAssociation = intval(explode(":", $associations['es-ES']->id)[0]);

			}
			return $getAssociation;
		}
		//Get data for map
		if($getData == 1){
			$alreadyGivenElements = $given == null ? [] : explode('_', $given);
			$finalGiven = [];
			foreach ($alreadyGivenElements as $key => $element) {
				if (str_contains($element, '-')) {
					$separatedRange = explode('-', $element);

					$finalGiven = array_merge($finalGiven, range($separatedRange[0], $separatedRange[1]));
					
				}
				else{
					$finalGiven[] = intval($element);
				}
			}
			$alreadyGivenElements = $finalGiven;
			// Returns a geojson with the itineraries inside the bounding box also accepts to exclude the ids given

			$query = "
			SELECT  jsonb_build_object(
				'type',       'Feature',
				'id',         j_id,
				'geometry',   ST_AsGeoJSON(ST_Simplify(the_geom, 0.0001))::jsonb,
				'properties', jsonb_build_object('geom_id',itineraries.id, 'j_id',j_id,'type', type, 'color', sooeg_fields_values.value)
			) as item
						FROM itineraries left join sooeg_content on j_id = sooeg_content.id left join sooeg_fields_values on  j_id::varchar = item_id and '230' = field_id";

			if( (!empty($modalidad)) || (!empty($difficulty)) || (!empty($zones)) || (!empty($distance)) ){
				if( (!empty($modalidad)) ){
					$query = $query . " inner join sooeg_fields_values as fields1 on
						j_id::varchar = fields1.item_id ";
				}

				if( (!empty($difficulty)) ){
					$query = $query . " inner join sooeg_fields_values as fields2 on
						j_id::varchar = fields2.item_id ";
				}
				if( (!empty($zones)) ){
					$query = $query . " inner join sooeg_fields_values as fields3 on
						j_id::varchar = fields3.item_id ";
				}

				$query = $query . " where ";
				if(!empty($modalidad)){
					$modalidad = explode(',', $modalidad);
					if(sizeof($modalidad) == 1){
						$query = $query . " 
						(fields1.value = '".$modalidad[0]."'
						and fields1.field_id = '255')
						and";
					}
					else{
						$query = $query . " ( ";
						foreach($modalidad as $mod){
							$query = $query . " 
							(fields1.value = '".$mod."'
							and fields1.field_id = '255') ";
							if($mod == end($modalidad)){
								$query = $query . " ) ";
								$query = $query . "and";
							}
							else{
								$query = $query . "or";
							}
							
						}
						
					}
					

				}			
				if(!empty($difficulty)){
					$difficulty = explode(',', $difficulty);
					if(sizeof($difficulty) == 1){
						$query = $query . " 
						(fields2.value = '".$difficulty[0]."'
						and fields2.field_id = '206')
						and";
					}
					else{
						$query = $query . "(";
						foreach($difficulty as $dif){
							$query = $query . " 
							(fields2.value = '".$dif."'
							and fields2.field_id = '206') ";
							if($dif == end($difficulty)){
								$query = $query . ") ";
								$query = $query . "and";
							}
							else{
								$query = $query . "or";
							}
							
						}
						
					}

				}
				if(!empty($zones)){		
					$zones = explode(',', $zones);
					if(sizeof($zones) == 1){
						$query = $query . " 
						(fields3.value = '".$zones[0]."'
						and fields3.field_id = '277')
						and";
					}
					else{
						$query = $query . "(";
						foreach($zones as $zone){
							$query = $query . " 
							(fields3.value = '".$zone."'
							and fields3.field_id = '277') ";
							if($zone == end($zones)){
								$query = $query . ") ";
								$query = $query . "and";
							}
							else{
								$query = $query . "or";
							}
							
						}
						
					}
				}
				if(!empty($distance)){
					$distance = explode(',', $distance);
					if(sizeof($distance) == 1){
						switch ($distance[0]) {
							case 1:
								$low = 0;
								$high = 2000;
									break;
							case 2:
								$low = 2001;
								$high = 5000;
									break;
							case 3:
								$low = 5001;
								$high = 10000;
									break;
							case 4:
								$low = 10001;
								$high = 30000;
									break;
							case 5:
								$low = 30001;
								$high = 10000000;
									break;
							default:
								
							}
		
						$query = $query . "	length BETWEEN ".$low." and ".$high." and ";
					}
					else{
						$query = $query . "(";
						foreach($distance as $dist){
							switch ($dist) {
								case 1:
									$low = 0;
									$high = 2000;
										break;
								case 2:
									$low = 2001;
									$high = 5000;
										break;
								case 3:
									$low = 5001;
									$high = 10000;
										break;
								case 4:
									$low = 10001;
									$high = 30000;
										break;
								case 5:
									$low = 30001;
									$high = 10000000;
										break;
								default:
									
								}
			
							$query = $query . "(length BETWEEN ".$low." and ".$high." ) ";

							if($dist == end($distance)){
								$query = $query . ") ";
								$query = $query . " and";
							}
							else{
								$query = $query . " or";
							}
							
						}
						
					}

				}
			}
			else{
				$query = $query . " where ";
			}		


			$query = $query . " ST_Intersects(  ST_MakeEnvelope(" . $southWest['longitude'] . " ," . $southWest['latitude'] . ", " . $northEast['longitude'] . " ," . $northEast['latitude'] . ", 4326) , itineraries.the_geom) and state = 1";
			if(!empty($categoryId)){
				switch ($categoryId) {
					case 61 :
						$query = $query . " and itineraries.type = 1";
						break;
					case 123:
						$query = $query . " and itineraries.type = 1";
						break;
					case 124:
						$query = $query . " and itineraries.type = 1";
						break;
					case 62 :
						$query = $query . " and itineraries.type = 2";
						break;					
					case 131:
						$query = $query . " and itineraries.type = 2";
						break;
					case 132:
						$query = $query . " and itineraries.type = 2";
						break;
					case 63:
						$query = $query . " and itineraries.type = 3";
						break;
					case 133:
						$query = $query . " and itineraries.type = 3";
						break;
					case 134:
						$query = $query . " and itineraries.type = 3";
						break;
					
				  }
				//($query = $query . " and sooeg_content.language = '".Factory::getLanguage()->getTag()."'");
			}
			
			if (sizeof($alreadyGivenElements) >= 1) {
				$query = $query . " and itineraries.id NOT IN (" . implode(', ', $alreadyGivenElements) . ")";
			}
			//dd($query);
			$db = Factory::getDbo();
			($db->setQuery($query));
			($data = $db->loadObjectList());

			$query = "
			SELECT count('*') 
				FROM itineraries left join sooeg_content on j_id = sooeg_content.id where state = 1";

			$db->setQuery($query);
			$count = $db->loadResult();
			
			foreach ($data as &$item) {
				$item = json_decode($item->item);
			}
	
			return [
				'data' => $data,
				'total_count' => $count
			];
		}

		//Check if user has favourite content
		$user_id = $this->app->input->get('user');
		if(($user_id)){

			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select(array('item_id'))
			->from($db->quoteName('#__sg_favourites'))
			->where($db->quoteName('user_id') . " = " . $db->quote($user_id));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$hasFavourite = $db->loadObjectList();
		}
		
		$model = JModelLegacy::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
		$appParams = JFactory::getApplication()->getParams();  

		
		//If we pass only 1 itinerario
		if(!is_null($item)){
			$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $item );
			$articleLang = $language;
			if(count($associations) > 0) {
				$item = intval(explode(":", $associations[$articleLang]->id)[0]);
			}
			$model = JModelLegacy::getInstance('CustomArticles', 'ContentModel', array('ignore_request' => true));
			$appParams = Factory::getApplication()->getParams();
			$model->setState('params', $appParams);
			$model->setState('filter.article_id', $item); //change that to your Category ID
			//$model->setState('filter.published', '1');

			$item =   $model->getItems();
			// There is only one article in the array, the this is the article
			($item = $item[0]);
			return fillItemData($item);

		}
		//If has category, return articles by category
		elseif(!empty($categoryId) || (!empty($modalidad)) || (!empty($distance)) || (!empty($difficulty)) || (!empty($zones))){
			if($pageNum != 0){
				$articlesPerPage = 20;
				$offset = ($pageNum - 1) * $articlesPerPage;
			}
			else{
				$articlesPerPage = -1;
				$offset = 0;
			}
			$associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($categoryId)); //Category for Slider
			if(count($associations) == 0){ //If there is not multilingual content, get original category Id (spanish)
				$catId = $categoryId;
			}
			else{
				$catLang =   Factory::getLanguage()->getTag();
				$catId = $associations[ $catLang];               
			}
            $model->setState('params', $appParams);
            $model->setState('filter.category_id', $catId); //change that to your Category ID
			$model->setState('list.ordering', 'publish_up');
			$model->setState('list.direction', 'DESC');
			$model->setState('filter.subcategories', true);
			//$model->setState('filter.language', Factory::getLanguage()->getTag());
			$model->setState('list.limit', $articlesPerPage);
			$model->setState('list.start', $offset);
			$model->setState('filter.published', '1');
			if(!empty($modalidad)){
				$model->setState('filter.routeType', $modalidad);
			}
			if(!empty($distance)){
				$model->setState('filter.distance', $distance);
			}
			if(!empty($difficulty)){
				$model->setState('filter.difficulty', $difficulty);
			}
			if(!empty($zones)){
				$model->setState('filter.zones', $zones);
			}
			$content =   $model->getItems();
	//dd($content);
			if($content) : //If articles in noticias 
	
				foreach ($content as $item) :
					
					fillItemData($item);

				endforeach; //$content

			endif;
			//Get total number of items filtered
			$model = JModelLegacy::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
			$appParams = JFactory::getApplication()->getParams();  
			$associations = ArrayHelper::toInteger(CategoriesHelper::getAssociations($categoryId)); //Category for Slider
			if(count($associations) == 0){ //If there is not multilingual content, get original category Id (spanish)
				$catId = $categoryId;
			}
			else{
				$catLang =   Factory::getLanguage()->getTag();
				$catId = $associations[ $catLang];               
			}               
            $model->setState('params', $appParams);
            $model->setState('filter.category_id', $catId); //change that to your Category ID
			$model->setState('list.ordering', 'title');
			$model->setState('list.direction', 'ASC');
			$model->setState('filter.subcategories', true);
			$model->setState('filter.language', Factory::getLanguage()->getTag());
			$model->setState('filter.published', '1');
			if(!empty($modalidad)){
				$model->setState('filter.routeType', $modalidad);
			}
			if(!empty($distance)){
				$model->setState('filter.distance', $distance);
			}
			if(!empty($difficulty)){
				$model->setState('filter.difficulty', $difficulty);
			}
			if(!empty($zones)){
				$model->setState('filter.zones', $zones);
			}
			$total =   $model->getItems();

			$response["total"] = count($total);
			$response["data"] = $content;
			return $response;
		}
		elseif(!is_null($download)){ // download gpx
			$item_id = $download;
			$fiel_linestring = 145;
			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select(array('value'))
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('item_id') . " = " . $db->quote($item_id))
			->where($db->quoteName('field_id') . " = " . $db->quote($fiel_linestring));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$coordinates = json_decode($db->loadResult())->features[0]->geometry->coordinates;

			$query = $db
			->getQuery(true)
			->select(array('title'))
			->from($db->quoteName('#__content'))
			->where($db->quoteName('id') . " = " . $db->quote($item_id));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$title = $db->loadResult();

			if(!is_null($coordinates)){

					$gpxFile = '<?xml version="1.0" encoding="UTF-8"?>
						<gpx creator="Tenerife ON" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
						<metadata>
						<time>'.date("d/m/Y").'</time>
						</metadata>
						<trk>
						<name>'.$title.'</name>
						<type>9</type>
						<trkseg>
						';
					$gpxContent = "";
					foreach($coordinates as $trkpt){
						$gpxContent .= '<trkpt lat="'.$trkpt[1].'" lon="'.$trkpt[0].'">
											<ele>'.$trkpt[2].'</ele>
										</trkpt>';
					}
					$gpxFile .= $gpxContent.'</trkseg>
											</trk>
										</gpx>';
				
				}
				
				return $gpxFile;
		}
		elseif(!is_null($downloadkml)){ // download kml
			$item_id = $downloadkml;
			$fiel_linestring = 145;
			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select(array('value'))
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('item_id') . " = " . $db->quote($item_id))
			->where($db->quoteName('field_id') . " = " . $db->quote($fiel_linestring));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$coordinates = json_decode($db->loadResult())->features[0]->geometry->coordinates;

			$field_start = 58;
			$query = $db
			->getQuery(true)
			->select(array('value'))
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('item_id') . " = " . $db->quote($item_id))
			->where($db->quoteName('field_id') . " = " . $db->quote($field_start));
			$db->setQuery($query);
			$startPoint = $db->loadResult();

			$field_end = 59;
			$query = $db
			->getQuery(true)
			->select(array('value'))
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('item_id') . " = " . $db->quote($item_id))
			->where($db->quoteName('field_id') . " = " . $db->quote($field_end));
			$db->setQuery($query);
			$endPoint = $db->loadResult();


			$query = $db
			->getQuery(true)
			->select(array('title, introtext'))
			->from($db->quoteName('#__content'))
			->where($db->quoteName('id') . " = " . $db->quote($item_id));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$fileData = $db->loadObject();

			if(!is_null($coordinates)){

				    $kmlFile = '<?xml version="1.0" encoding="UTF-8"?>
								<kml xmlns="http://www.opengis.net/kml/2.2"> <Document>
								<name>'.$fileData->title.'</name>
								<description>'.strip_tags($fileData->introtext).'</description> <Style id="yellowLineGreenPoly">
								<LineStyle>
								<color>7f00ffff</color>
								<width>4</width>
								</LineStyle>
								<PolyStyle>
								<color>7f00ff00</color>
								</PolyStyle>
								</Style>
								<Placemark>
								<name>'.$startPoint.'</name>
								<description></description>
								<Point>
								<coordinates>'.$coordinates[0][0].','.$coordinates[0][1].',0</coordinates>
								</Point>
								</Placemark>
								<Placemark>
								<name>'.$endPoint.'</name>
								<description></description>
								<Point>
								<coordinates>'.end($coordinates)[0].','.end($coordinates)[1].',0</coordinates>
								</Point>
								</Placemark>  
								<Placemark>
								<name>'.$fileData->title.'</name>
								<description></description>
								<styleUrl>#yellowLineGreenPoly</styleUrl>
								<LookAt>
								<longitude>'.$coordinates[0][0].'</longitude>
								<latitude>'.$coordinates[0][1].'</latitude>
								<heading>-60</heading>
								<tilt>70</tilt>
								<range>6300</range>
								<gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>
							  </LookAt>
								<LineString>
								<extrude>1</extrude>
								<tessellate>1</tessellate>
								<coordinates>';
					 $kmlContent = "";
					 foreach($coordinates as $trkpt){
						 $kmlContent .= $trkpt[0].','.$trkpt[1].','.intval($trkpt[2])."\n";
					 }
					 $kmlFile .= $kmlContent.'</coordinates>
								</LineString> </Placemark>
								</Document> </kml>';
				
				}
				
				return $kmlFile;
		}
		elseif(!is_null($myratings)){

			$db = Factory::getDbo();
			//Select items rated and rating
			$query ="
			select
			ratings.id,ratings.item_id, ratings.rating, ratings.date_added
		   from
			   sooeg_content as article
			   
		   left join sooeg_sg_valoraciones as ratings on
			   ratings.item_id = article.id
		   where
			   state = 1
			   and ratings.user_id = ".$myratings."
			order by ratings.date_added DESC";
			$db->setQuery($query);
			$myratings = $db->loadObjectList();

			//Create a article object to get data
			foreach($myratings as $index => $item){
				$model = JModelLegacy::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
				$appParams = Factory::getApplication()->getParams();
				$model->setState('params', $appParams);
				$model->setState('filter.article_id', $item->item_id); //change that to your Category ID
	
				$item =   $model->getItems();
				// There is only one article in the array, the this is the article
				$item = $item[0];
	
				fillItemData($item);

				$myratings[$index]->title = $item->title;
				$myratings[$index]->catid = $item->catid;
				$myratings[$index]->images = $item->imageItem;
				$myratings[$index]->link = $item->link;

			}
			return json_encode($myratings);
		}
		elseif(!is_null($myfavourites)){

			$db = Factory::getDbo();
			//Select items rated and rating
			$query ="
			select
			favourite.item_id,  favourite.date_added, favourite.id
		   from
			   sooeg_content as article
			   
		   left join sooeg_sg_favourites as favourite on
		   favourite.item_id = article.id
		   where
			   state = 1
			   and favourite.user_id = ".$myfavourites."
			order by favourite.date_added DESC";
			$db->setQuery($query);
			$myfavourites = $db->loadObjectList();

			//Create a article object to get data
			foreach($myfavourites as $index => $item){
				$model = JModelLegacy::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
				$appParams = Factory::getApplication()->getParams();
				$model->setState('params', $appParams);
				$model->setState('filter.article_id', $item->item_id); //change that to your Category ID
	
				$item =   $model->getItems();
				// There is only one article in the array, the this is the article
				$item = $item[0];
				//Get all data of item
				fillItemData($item);
				//File only data to return
				$myfavourites[$index]->title = $item->title;
				$myfavourites[$index]->link = $item->link;
				$myfavourites[$index]->catid = $item->catid;
				$myfavourites[$index]->catName = $item->category_title;
				$myfavourites[$index]->images = $item->imageItem;
				
				//get item rating
				$valoracion = null;
				$db = Factory::getDbo();
				$query = $db
				->getQuery(true)
				->select(array('SUM(rating) as rating, COUNT(id) as count'))
				->from($db->quoteName('#__sg_valoraciones'))
				->where($db->quoteName('item_id') . " = " . $db->quote($item->id));
				// Reset the query using our newly populated query object.
				$db->setQuery($query);
				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				if($hasRating = $db->loadObjectList()){
					if(!is_null($hasRating[0]->rating)){
						$rating = $hasRating[0]->rating;
						$count = $hasRating[0]->count;
						$valoracion = (float)($rating/$count);
					}
				}
				$myfavourites[$index]->rating = $valoracion;
			}
			//dd($myfavourites);
			return json_encode($myfavourites);
		}
		elseif(!is_null($myactivities)){

			$db = Factory::getDbo();
			$user = Factory::getUser();
			if($user->id != $myactivities ){
				return;
			}
			//Select items rated and rating
			$query ="
			SELECT a.id, a.user_id, a.activity_name, a.activity_type, a.activity_date, a.activity_geom, a.activity_gain, a.activity_lost, a.activity_max_altitude, a.activity_min_alt, a.activity_duration, a.activity_distance, a.activity_place, m.name
			from
		   		sooeg_sg_my_activities a
			left join public.municipios m on m.id = a.activity_place
		   where
			   a.user_id = ".$myactivities."
			order by a.activity_date DESC";

			$db->setQuery($query);
			$myactivities = $db->loadObjectList();
			foreach ($myactivities as $activity){
				if($activity->activity_type == 2){
					$activity->type_name = Text::_("TPL_GESPLAN_BICA");
				}
				elseif($activity->activity_type == 3){
					$activity->type_name = Text::_("TPL_GESPLAN_MOTOR_VEHICLE");
				}
				else{
					$activity->type_name = Text::_("TPL_GESPLAN_HIKING");
				}
			}
			return json_encode($myactivities);
		}
		elseif(!is_null($myroutes)){

			$db = Factory::getDbo();
			$user = Factory::getUser();
			if($user->id != $myroutes ){
				return;
			}
			//Select items rated and rating
			$query ="
			select r.id, r.route_name, r.route_type, r.route_date, r.route_geom, r.route_gain, r.route_lost, r.route_max_altitude, r.route_min_alt, r.route_duration, r.route_itineraries, r.route_distance, r.route_place, m.name 
			from sooeg_sg_my_routes r
			left join municipios m on m.id = r.route_place
		  	where
			   r.user_id = ".$myroutes." 
			order by r.route_date DESC";

			$db->setQuery($query);
			$myroutes = $db->loadObjectList();

			foreach($myroutes as $route){
				
				if($route->route_type == 2){
					$route->type_name = Text::_("TPL_GESPLAN_BICA");
				}
				elseif($route->route_type == 3){
					$route->type_name = Text::_("TPL_GESPLAN_MOTOR_VEHICLE");
				}
				else{
					$route->type_name = Text::_("TPL_GESPLAN_HIKING");
				}
			}

			return json_encode($myroutes);
		}
		elseif(!is_null($contentType)){
			
			$id = $this->app->input->get('id',null,'STRING');
			//$user_id = $this->app->input->get('user',null,'STRING');
			$user = Factory::getUser();
			$db = Factory::getDbo();


			//Select items rated and rating
			$query ="
				DELETE FROM sooeg_sg_".$contentType."
				WHERE 
				id = ".$id." 
				and 
				user_id = ".$user->id."";

			$db->setQuery($query);
			$removeItem = $db->execute();

			if($removeItem){
				$response['success'] = "true";
				$response['message'] = "item removed";
			}
			else{
				$response['success'] = "false";
				$response['message'] = "item not removed";
			}
			// dd($myactivities);
			return json_encode($response);
		}
		elseif(!is_null($getAssociation)){
			$model = JModelLegacy::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
			$appParams = JFactory::getApplication()->getParams();  
			$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $getAssociation );
			$articleLang = $language;
			if(count($associations) > 0) {
				$item = intval(explode(":", $associations['es-ES']->id)[0]);
				$response['success'] = "true";
				$response['message'] = "item removed";
				$response['data'] = $item;
			}else{
				$response['success'] = "false";
				$response['message'] = "item not found";
			}
			// dd($myactivities);
			return json_encode($response);
		}
		//Code to return linestrings for maps (itinerarios and mapa-interactivo)
	

		
		
	}
}
