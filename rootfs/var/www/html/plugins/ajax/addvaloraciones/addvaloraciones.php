<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Ajax.addvaloraciones
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Router\ApiRouter;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Factory;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;



/**
 * Web Services adapter for com_content.
 *
 * @since  __BUMP_VERSION__
 */
class PlgAjaxAddvaloraciones extends CMSPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  __BUMP_VERSION__
	 */
	protected $autoloadLanguage = true;

	/**
	 * @var    \Joomla\CMS\Application\CMSApplication
	 *
	 * @since  3.8.0
	 */
	protected $app;

	/**
	 * Registers com_conent's API's routes in the application
	 *
	 * @param   ApiRouter  &$router  The API Routing object
	 *
	 * @return  void
	 *
	 * @since   __BUMP_VERSION__
	 */
	public function onAjaxAddvaloraciones()
	{
		$user_id =  $this->app->input->get('user');
		$item_id = $this->app->input->get('item');
		$rating = $this->app->input->get('rating');
		$response = "";
		$db = Factory::getDbo();

		//Check if already rated
		$query = $db
		->getQuery(true)
		->select('rating')
		->from($db->quoteName('#__sg_valoraciones'))
		->where($db->quoteName('user_id') . " = " . $db->quote($user_id))
		->where($db->quoteName('item_id') . " = " . $db->quote($item_id));
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		//If is not rated yet
		if(is_null($isRated = $db->loadResult())){

			//Rate the item
			$query = $db->getQuery(true);
			$columns = array('user_id', 'item_id', 'date_added', 'agent', 'rating');
			$values = $user_id.", ".$item_id.", '".date('Y-m-d H:i:s')."','web', ".$rating."";
			$query = $db
			->getQuery(true)
			->insert($db->quoteName('#__sg_valoraciones'))
			->columns($db->quoteName($columns))
			->values(''.$values.'');

			$db->setQuery($query);
			$itemRated = $db->execute();
		}
		else{

			($query = $db
			->getQuery(true)
			->update('#__sg_valoraciones')
			->set("rating = '".$rating."'")               // increment the row's value
			->where($db->quoteName('item_id') . " = " . $db->quote($item_id)) //id Espacio Natural
			->where($db->quoteName('user_id') . " = " . $db->quote($user_id))); //id cf field enp-puntos-de-interes

			$db->setQuery($query);
			$itemRated = $db->execute();
		}


		//If rated, get item rating to return to view
		if($itemRated){
			$query = $db
			->getQuery(true)
			->select(array('SUM(rating) as rating, COUNT(id) as count'))
			->from($db->quoteName('#__sg_valoraciones'))
			->where($db->quoteName('item_id') . " = " . $db->quote($item_id));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$itemRating = $db->loadObjectList();
		}
		$response = $itemRating;

		return $response;
	}
}
