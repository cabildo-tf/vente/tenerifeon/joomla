<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Ajax.addtofavourites
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Router\ApiRouter;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Factory;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;



/**
 * Web Services adapter for com_content.
 *
 * @since  __BUMP_VERSION__
 */
class PlgAjaxAddtofavourites extends CMSPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  __BUMP_VERSION__
	 */
	protected $autoloadLanguage = true;

	/**
	 * @var    \Joomla\CMS\Application\CMSApplication
	 *
	 * @since  3.8.0
	 */
	protected $app;

	/**
	 * Registers com_conent's API's routes in the application
	 *
	 * @param   ApiRouter  &$router  The API Routing object
	 *
	 * @return  void
	 *
	 * @since   __BUMP_VERSION__
	 */
	public function onAjaxAddtofavourites()
	{
		$user =   Factory::getUser();
		//Get parent category from input
		$user_id = $user->id;
		$item_id = $this->app->input->get('item');
		$response = [];
		$isFavourite = null;
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		if(($user_id)){

			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select(array('item_id'))
			->from($db->quoteName('#__sg_favourites'))
			->where($db->quoteName('user_id') . " = " . $db->quote($user_id))
			->where($db->quoteName('item_id') . " = " . $db->quote($item_id))
			->setLimit(1);
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$isFavourite = $db->loadObjectList();
		}
		//if it is favourite
		if(!$isFavourite){

			$query = $db->getQuery(true);
			$columns = array('user_id', 'item_id', 'date_added', 'agent');
			$values = $user_id.", ".$item_id.", '".date('Y-m-d H:i:s')."','web'";
			$query = $db
			->getQuery(true)
			->insert($db->quoteName('#__sg_favourites'))
			->columns($db->quoteName($columns))
			->values(''.$values.'');
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			$db->execute();
			$response['success'] = "true";
			$response['message'] = "ADDED";
			$response['item'] = $item_id;
		} 
		else{

			$query = $db->getQuery(true);
			$conditions = array(
				$db->quoteName('user_id') . ' = '.$user_id.'', 
				$db->quoteName('item_id') . ' = ' . $item_id
			);
			
			$query->delete($db->quoteName('#__sg_favourites'));
			$query->where($conditions);
			
			$db->setQuery($query);
			$db->execute();
			$response['success'] = "true";
			$response['message'] = "REMOVED";
			$response['item'] = $item_id;
		}

		return (($response));
	}
}
