<?php

/**
 * @copyright	Copyright (C) 2015 Cédric KEIFLIN alias ced1870
 * http://www.joomlack.fr
 * @license		GNU/GPL
 * */
 
 /**
 * Cookies management Javascript code from
 * @subpackage		Modules - mod_jbcookies
 * 
 * @author			JoomBall! Project
 * @link			http://www.joomball.com
 * @copyright		Copyright © 2011-2014 JoomBall! Project. All Rights Reserved.
 * @license			GNU/GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

JLoader::register('MenusHelper', JPATH_ADMINISTRATOR . '/components/com_menus/helpers/menus.php');


use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\Component\Content\Site\Helper\AssciationHelper;
use Joomla\String\StringHelper;

class plgSystemCookiesck extends JPlugin {

	private $paramsEnabled;

	private $readmoreLink = array();

	private $listCookies;

	function __construct(&$subject, $config) {
		$this->paramsEnabled = file_exists(JPATH_SITE . '/administrator/components/com_cookiesck/cookiesck.php');
		parent :: __construct($subject, $config);
	}

	function onAfterDispatch() {
		global $ckjqueryisloaded;
		$app = JFactory::getApplication();
		$document = JFactory::getDocument();
		$doctype = $document->getType();

		// si pas en frontend, on sort
		if ($app->isClient('administrator')) {
			return false;
		}

		// si pas HTML, on sort
		if ($doctype !== 'html') {
			return;
		}

		// si en mode imbriqué
		if ($app->input->get('tmpl','') != '') {
			return;
		}

		// load jquery
		$jquerycall = "";
		if (version_compare(JVERSION, '3') >= 1 ) {
			JHTML::_('jquery.framework', true);
		} else if (! $ckjqueryisloaded) {
			$document->addScript(JUri::base(true) . "/plugins/system/cookiesck/assets/jquery.min.js");
		}

		// initiate the scan
		if ($app->input->get('cookiesck','') == 'scan') {
			return $this->scan();
//			die;
		}

		// get the params from the plugin options
		$plugin = JPluginHelper::getPlugin('system', 'cookiesck');
		$pluginParams = new JRegistry($plugin->params);

		// load the language strings of the plugin
		$this->loadLanguage();

		$lifetime = (int) $pluginParams->get('lifetime', 365);
		$reload = (int) $pluginParams->get('reloadafteraccept', '0');
		$debug = (int) $pluginParams->get('debug', '0');
		// set the cookie from the ajax request on click
		if (isset($_POST['set_cookieck'])) {
				if($_POST['set_cookieck']==1)
					setcookie("cookiesck", "yes", time()+3600*24*$lifetime, "/");
				if($_POST['set_cookieck']==0)
					setcookie("cookiesck", "no", time()+3600*24*$lifetime, "/");
			exit;
		}

		// unset cookies if not yet accepted
		$cookiesckValue = isset($_COOKIE['cookiesck']) ? $_COOKIE['cookiesck'] : 'no';
		if (! isset($_COOKIE['cookiesck']) || $_COOKIE['cookiesck'] == 'no' && !isset($_POST['set_cookieck'])) {
//			@header_remove('Set-Cookie');
			// remove external ressources if option set in the plugin
			if ($pluginParams->get('blockingpolicy', '1') == '2')
			{
				@header("Content-Security-Policy: default-src 'self' 'unsafe-inline';");
				@header("X-Content-Security-Policy: default-src 'self' 'unsafe-inline';");
			}
		}

		$readmore_link = '';
		$link_rel = $pluginParams->get('link_rel') ? ' rel=\"' . $pluginParams->get('link_rel') . '\"' : '';
		if ($pluginParams->get('linktype', 'article') == 'article') {
			$id = $pluginParams->get('article_readmore');

			if ($id) {
				if (version_compare(JVERSION, '4') >= 0) {

					$langTag = $app->getLanguage()->getTag();
					$option = 'com_content';
					$component = $app->bootComponent($option);

					if ($component instanceof AssociationServiceInterface)
					{
						$assoc_articles = $component->getAssociationsExtension()->getAssociationsForItem();
					}
					else
					{
						// Load component associations
						$class = str_replace('com_', '', $option) . 'HelperAssociation';
						\JLoader::register($class, JPATH_SITE . '/components/' . $option . '/helpers/association.php');

						if (class_exists($class) && \is_callable(array($class, 'getAssociations')))
						{
							$assoc_articles = \call_user_func(array($class, 'getAssociations'));
						}
					}
					if (isset ($assoc_articles[$langTag])) {
						$readmore_link = JRoute::_($assoc_articles[$langTag]);
					} else {
						$db = JFactory::getDbo();
						$query = "SELECT * FROM #__content WHERE id = " . (int)$pluginParams->get('article_readmore');
						$db->setQuery($query);
						$item = $db->loadObject();
						$item->slug = $item->id.':'.$item->alias;
						// get the article link
						$readmore_link = Route::_(RouteHelper::getArticleRoute($item->slug, $item->catid, $item->language));
					}
				} else {
					require_once JPATH_SITE.'/components/com_content/helpers/route.php';
					require_once JPATH_SITE.'/components/com_content/helpers/association.php';
					JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_content/models', 'ContentModel');
					$langTag = $app->getLanguage()->getTag();

					$assoc_articles = ContentHelperAssociation ::getAssociations($id);
					if (isset ($assoc_articles[$langTag])) {
						$readmore_link = JRoute::_($assoc_articles[$langTag]);
					} else {
						// Get an instance of the generic article model
						$model = JModelLegacy::getInstance('Article', 'ContentModel', array('ignore_request' => true));
						// Set application parameters in model
						$appParams = JFactory::getApplication()->getParams();
						$model->setState('params', $appParams);
						//	Retrieve Content
						$item = $model->getItem($pluginParams->get('article_readmore'));
						$item->slug = $item->id.':'.$item->alias;
						// $item->catslug = $item->catid.':'.$item->category_alias;
						// get the article link
						$readmore_link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid));
					}
				}

				if ($link_anchor = $pluginParams->get('link_anchor')) {
					$readmore_link = $readmore_link . '#' . trim($link_anchor, '#');
				}
			}
		} else if ($pluginParams->get('linktype', 'article') == 'menuitem') {
			$readmore_link = $pluginParams->get('menuitem_readmore');
			$associations = MenusHelper::getAssociations($readmore_link);
			$langTag = $app->getLanguage()->getTag();
			$link_id = isset($associations[$langTag]) ? $associations[$langTag] : $readmore_link;

			// search for the link
			$db = JFactory::getDbo();
			$query = "SELECT link FROM #__menu WHERE id = " . (int)$link_id;
			$db->setQuery($query);
			$menuItem = $db->loadObject();

			$readmore_link = JRoute::_($menuItem->link);
		} else {
			$readmore_link = $pluginParams->get('link_readmore');
			if (substr($readmore_link, 0,4) != 'http') {
				$readmore_link = JUri::root(true) . '/' . trim($readmore_link, '/');
			}
		}

		// store for use in the interface
		$this->readmoreLink['href'] = $readmore_link;
		$this->readmoreLink['rel'] = $link_rel;
		$this->readmoreLink['target'] = ($pluginParams->get('link_target', 'same') == 'new' ? '_blank' : '');

		$where = 'top';
		switch ($pluginParams->get('position', 'absolute')) {
			case 'absolute':
			default:
				$position = 'absolute';
				break;
			case 'fixed':
				$position = 'fixed';
				break;
			case 'relative':
				$position = 'relative';
				break;
			case 'bottom':
				$position = 'fixed';
				$where = 'bottom';
				break;
		}
		// add styling
		$css = "
			#cookiesck {
				position:" . $position . ";
				left:0;
				right: 0;
				" . $where . ": 0;
				z-index: 1000000;
				min-height: 30px;
				color: " . $pluginParams->get('text_color', '#eee') . ";
				background: " . $this->hex2RGB($pluginParams->get('background_color', '#000000'), $pluginParams->get('background_opacity', '0.5')) . ";
				text-align: center;
				font-size: 14px;
				line-height: 14px;
			}
			#cookiesck_text {
				padding: 10px 0;
				display: inline-block;
			}
			#cookiesck_buttons {
				float: right;
			}
			.cookiesck_button,
			#cookiesck_accept,
			#cookiesck_decline,
			#cookiesck_settings,
			#cookiesck_readmore {
				float:left;
				padding:10px;
				margin: 5px;
				border-radius: 3px;
				text-decoration: none;
				cursor: pointer;
				transition: all 0.2s ease;
			}
			#cookiesck_readmore {
				float:right;
				color: #fff;
				border: 2px solid transparent;
				transition: all 0.2s ease;
			}
			#cookiesck_readmore:hover {
				border: 2px solid #fff;
			}
			#cookiesck_accept {
				background: #1176a6;
				border: 2px solid #1176a6;
				color: #f5f5f5;
			}
			#cookiesck_accept:hover {
				background: transparent;
				border: 2px solid darkturquoise;
				color: darkturquoise;
			}
			#cookiesck_decline {
				background: #000;
				border: 2px solid #000;
				color: #f5f5f5;
			}
			#cookiesck_decline:hover {
				background: transparent;
				border: 2px solid #fff;
				color: #fff;
			}
			#cookiesck_settings {
				background: #fff;
				border: 2px solid #fff;
				color: #000;
			}
			#cookiesck_settings:hover {
				background: transparent;
				border: 2px solid #fff;
				color: #fff;
			}
			#cookiesck_options {
				display: " . (isset($_COOKIE['cookiesck']) ? "block" : "none") . ";
				width: 30px;
				height: 30px;
				border-radius: 15px;
				box-sizing: border-box;
				position: fixed;
				bottom: 0;
				left: 0;
				margin: 10px;
				border: 1px solid #ccc;
				cursor: pointer;
				background: #fff url(" . JUri::root(true) . "/plugins/system/cookiesck/assets/cookies-icon.svg) center center no-repeat;
				background-size: 80% auto;
			}
			#cookiesck_options > .inner {
				display: none;
				width: max-content;
				margin-top: -40px;
				background: rgba(0,0,0,0.7);
				position: absolute;
				font-size: 14px;
				color: #fff;
				padding: 4px 7px;
				border-radius: 3px;
			}
			#cookiesck_options:hover > .inner {
				display: block;
			}
			#cookiesck > div {
				display: flex;
				justify-content: space-around;
				align-items: center;
				flex-direction: column;
			}
			" . ($this->params->get('blockiframes_image', '') ? "
			iframe[data-cookiesck-src] {
				background: #ddd url(" . JUri::root(true) . '/' . $this->params->get('blockiframes_image') . ") center center no-repeat;
			}" : "") . "

		";

		$layout = 'layout1';
		if (! $this->paramsEnabled) {
			$document->addStyleDeclaration($css);
		} else {
			$styles = $this->getStylesCss();
			// if no style saved in the interface, then still use the default styles
			if (!isset($styles->layoutcss) || ! $styles->layoutcss) {
				$document->addStyleDeclaration($css);
			} else {
				$this->loadAssets($styles, $where, $position);
				$stylesParams = json_decode($styles->params);
				$layout = isset($stylesParams->barlayout) ? $stylesParams->barlayout : 'layout1';
			}
		}

		$ckcookieswizard = $pluginParams->get('ckcookieswizard', '{}');
		$this->listCookies = $this->listCookies($ckcookieswizard);
		$allowedCookies = $this->getAllowedCookies($ckcookieswizard);

		// remove the existing non allowed cookies
		if (! empty($_COOKIE) && $cookiesckValue != 'yes') {
			$explode = explode('.', $_SERVER['HTTP_HOST'], substr_count($_SERVER['HTTP_HOST'], '.'));
			$domain = '.' . array_pop($explode);
			foreach ($_COOKIE as $name => $value) {

				// check if the cookie exists in the list
				foreach ($allowedCookies as $allowedCookie) {
					if (substr($name, 0, strlen($allowedCookie)) == $allowedCookie) {
						goto cookiesckskip;
					}
				}

				// simple additional check
				if (! in_array($name, $allowedCookies)) {
					setcookie($name, '', time()-3600);
					setcookie($name, '', 1, '', $domain);
				}
				cookiesckskip :
			}
		}

		$defaultValue = $this->getDefaultValue($ckcookieswizard);

		// setup variables
		$js = '
var COOKIESCK = {
	ALLOWED : ' . json_encode($allowedCookies) . '
	, VALUE : \'' . $defaultValue . '\'
	, LIST : \'' .  addslashes($ckcookieswizard) . '\'
	, LIFETIME : \'' .  $lifetime . '\'
	, DEBUG : \'' .  $debug . '\'
	, TEXT : {
		INFO : \'' . JText::_('COOKIESCK_INFO', true) . '\'
		, ACCEPT_ALL : \'' . JText::_('COOKIESCK_ACCEPT_ALL', true) . '\'
		, ACCEPT_ALL : \'' . JText::_('COOKIESCK_ACCEPT_ALL', true) . '\'
		, DECLINE_ALL : \'' . JText::_('COOKIESCK_DECLINE_ALL', true) . '\'
		, SETTINGS : \'' . JText::_('COOKIESCK_SETTINGS', true) . '\'
		, OPTIONS : \'' . JText::_('COOKIESCK_OPTIONS', true) . '\'
		, CONFIRM_IFRAMES : \'' . JText::_('COOKIESCK_CONFIRM_IFRAMES', true) . '\'
	}
};
';

		// load script if needed
		if ($this->listCookies === false) {
			$js .= 'console.log("COOKIES CK MESSAGE : The list of cookies is empty. Please check the documentation");'
					. 'jQuery(document).ready(function(){ckInitCookiesckIframes();});'
					;
		} else {
			$code = 'new Cookiesck({'
						. 'lifetime: "' . $lifetime . '"'
						. ', layout: "' . $layout . '"'
						. ', reload: "' . $reload . '"'
					. '}); ';
			$js .= '
if( document.readyState !== "loading" ) {
' . $code . '
} else {
	document.addEventListener("DOMContentLoaded", function () {
		' . $code . '
	});
}';

		}

		$document->addScriptDeclaration($js);
		$document->addScript(JUri::base(true) . "/plugins/system/cookiesck/assets/front.js?ver=3.2.0");
		$document->addStylesheet(JUri::base(true) . "/plugins/system/cookiesck/assets/front.css?ver=3.2.0");
	}

	/**
	 * Convert a hexa decimal color code to its RGB equivalent
	 *
	 * @param string $hexStr (hexadecimal color value)
	 * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
	 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
	 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
	 */
	function hex2RGB($hexStr, $opacity) {
		if ($opacity > 1) $opacity = $opacity/100;
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
		$rgbArray = array();
		if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
			$colorVal = hexdec($hexStr);
			$rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
			$rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
			$rgbArray['blue'] = 0xFF & $colorVal;
		} elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
			$rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
			$rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
			$rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
		} else {
			return false; //Invalid hex color code
		}
		$rgbacolor = "rgba(" . $rgbArray['red'] . "," . $rgbArray['green'] . "," . $rgbArray['blue'] . "," . $opacity . ")";

		return $rgbacolor;
	}

	/**
	 * Load the scripts and styles
	 */
	protected function loadAssets($styles, $where, $position) {
		if (! $this->paramsEnabled) return;

		// loads the helper in any case
		require_once JPATH_SITE . '/administrator/components/com_cookiesck/helpers/helper.php';
		$doc = JFactory::getDocument();

		$stylescss = $styles->layoutcss;
		$cssreplacements = CookiesckHelper::getCssReplacement();
		global $ckcustomgooglefontslist;
		foreach ($cssreplacements as $tag => $rep) {
			$stylescss = str_replace($tag, $rep, $stylescss);

			$stylesParams = json_decode($styles->params);
//			$layout = isset($stylesParams->barlayout) ? $stylesParams->barlayout : 'layout1';
			$search = array('[', ']');
			$replace = array('', '');
			$var = str_replace($search, $replace, $tag);

			if (isset($stylesParams->{$var . 'textisgfont'}) && $stylesParams->{$var . 'textisgfont'} == '1' 
				&& isset($stylesParams->{$var . 'textgfont'}) && $stylesParams->{$var . 'textgfont'} != '') {
				$ckcustomgooglefontslist[] = $stylesParams->{$var . 'textgfont'};
				
			}
		}
		

		// $styles = str_replace('|ID|', '.customfieldsck.' . $fieldClass, $styles);
		$stylescss = str_replace('#cookiesck_overlay {', '#cookiesck_overlay { 
position: fixed;
display: block;
content: \"\";
top: 0;
bottom: 0;
left: 0;
right: 0;
z-index: 1000;
background-size: cover !important;', $stylescss);
		$stylescss = str_replace('|ID|', '', $stylescss);

		$stylescss .= "
#cookiesck {
	position:" . $position . ";
	left:0;
	right: 0;
	" . $where . ": 0;
	z-index: 1001;
	min-height: 30px;
	box-sizing: border-box;
}
#cookiesck_text {
	display: inline-block;
}
.cookiesck_button {
	display: inline-block;
	cursor: pointer;
	padding:10px;
	margin: 5px;
	border-radius: 3px;
	text-decoration: none;
	cursor: pointer;
	transition: all 0.2s ease;
}
#cookiesck > .inner {
	display: block;
	flex: 1 1 auto;
	text-align: center;
}
#cookiesck[data-layout=\"layout1\"] #cookiesck_buttons {
	float: right;
}
#cookiesck[data-layout=\"layout2\"] #cookiesck_text,
#cookiesck[data-layout=\"layout2\"] #cookiesck_buttons, 
#cookiesck[data-layout=\"layout3\"] #cookiesck_text,
#cookiesck[data-layout=\"layout3\"] #cookiesck_buttons {
	display: block;
}
#cookiesck[data-layout=\"layout3\"] {
	bottom: 0;
	top: 0;
	display: flex;
	align-items: center;
	margin: auto;
	position: fixed;
}

#cookiesck_options {
	display: " . (isset($_COOKIE['cookiesck']) ? "block" : "none") . ";
	width: 30px;
	height: 30px;
	border-radius: 15px;
	box-sizing: border-box;
	position: fixed;
	bottom: 0;
	left: 0;
	margin: 10px;
	border: 1px solid #ccc;
	cursor: pointer;
	background: #fff url(" . JUri::root(true) . "/plugins/system/cookiesck/assets/cookies-icon.svg) center center no-repeat;
	background-size: 80% auto;
}
#cookiesck_options > .inner {
	display: none;
	width: max-content;
	margin-top: -40px;
	background: rgba(0,0,0,0.7);
	position: absolute;
	font-size: 14px;
	color: #fff;
	padding: 4px 7px;
	border-radius: 3px;
}
#cookiesck_options:hover > .inner {
	display: block;
}
" . ($this->params->get('blockiframes_image', '') ? "
iframe[data-cookiesck-src] {
	background: #ddd url(" . JUri::root(true) . '/' . $this->params->get('blockiframes_image') . ") center center no-repeat;
}" : "") . "


";

		$doc->addStyleDeclaration($stylescss);
	}

	/**
	 * Check if we need to load the styles in the page
	 */
	public function onBeforeRender() {
		$this->loadCustomGoogleFontsList();
	}

	/**
	 * Load the fonts only if not already registered by another extension
	 */
	public function loadCustomGoogleFontsList() {
		global $ckcustomgooglefontslist;

		if (! empty($ckcustomgooglefontslist)) {
			$doc = JFactory::getDocument();
			foreach ($ckcustomgooglefontslist as $ckcustomgooglefont) {
				$ckcustomgooglefont = str_replace(' ', '+', $ckcustomgooglefont);
				$doc->addStylesheet('//fonts.googleapis.com/css?family=' . $ckcustomgooglefont);
			}
		}
	}

	/**
	 * Get the css rules from the styles
	 *
	 * @param int $id
	 * @return string
	 */
	protected function getStylesCss() {
		$this->searchTable('#__cookiesck_styles');

		$db = JFactory::getDbo();
		$q = "SELECT params,layoutcss,state from #__cookiesck_styles ORDER BY id DESC LIMIT 1";
		$db->setQuery($q);
		$styles = $db->loadObject();

		return $styles;
	}

	/**
	 * Look if the table exists, if not then create it
	 * 
	 * @param type $tableName
	 */
	private static function searchTable($tableName) {
		$db = JFactory::getDbo();
		$tablesList = $db->getTableList();
		$tableExists = in_array($db->getPrefix() . $tableName, $tablesList);
		// test if the table not exists

		if (! $tableExists) {
			self::createTable($tableName);
		}
	}

	private static function createTable($tableName) {
				$query = "CREATE TABLE IF NOT EXISTS `#__cookiesck_styles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `state` int(10) NOT NULL DEFAULT '1',
  `params` longtext NOT NULL,
  `checked_out` varchar(10) NOT NULL,
  `layoutcss` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";

		$db = JFactory::getDbo();
		$db->setQuery($query);
		if (! $db->execute($query)) {
			// echo '<p class="alert alert-danger">Error during table ' . $tableName . ' creation process !</p>';
		} else {
			// echo '<p class="alert alert-success">Table ' . $tableName . ' created with success !</p>';
		}
	}

	public function onAfterRender() {
		$app = JFactory::getApplication();
		$document = JFactory::getDocument();
		$doctype = $document->getType();

		// stop if we are in admin
		if ($app->isClient('administrator') || $doctype !== 'html') {
			return;
		}

		// get the page code
		if (version_compare(JVERSION, '4') >= 0) {
			$body = JFactory::getApplication()->getBody(); 
		} else {
			$body = JResponse::getBody();
		}

		// look for the tags and replace
		if ($this->lookForIframes($body)) {
			$regex = "#<iframe(.*?)>(.*?)</iframe>#s"; // masque de recherche pour le tag
			$body = preg_replace_callback($regex, array($this, 'replaceIframe'), $body);
		}

		// insert the interface html code
		$body = str_replace('</body>', '<div id="cookiesck_interface">' . $this->listCookies . '</div></body>', $body);

		if (version_compare(JVERSION, '4') >= 0) {
			JFactory::getApplication()->setBody($body); 
		} else {
			JResponse::setBody($body);
		}
	}

	private function lookForIframes($body) {
		// don't block iframes if already accepted
		if (isset($_COOKIE['cookiesckiframes']) && $_COOKIE['cookiesckiframes'] === '1') {
			return false;
		}

		// don't block iframes if all cookies accepted
		if (isset($_COOKIE['cookiesck']) && $_COOKIE['cookiesck'] === 'yes') {
			return false;
		}

		// get the params from the plugin options
		if ($this->params->get('blockiframes', '0') == '0') {
			return false;
		}

		$app = JFactory::getApplication();
		$input = $app->input;

		if ($input->get('layout') == 'edit' || $input->get('task') == 'edit' || $input->get('func') == 'edit') {
			return false;
		}

		// test if there is no iframe, then exit immediately
		if (!stristr($body, "<iframe"))
			return;

		return true;
	}

	private function replaceIframe($matches) {
		$iframe = $matches[0];
		$iframe = '<div class="cookiesck-iframe-wrap">' . str_replace('src=', 'data-cookiesck-src=', $iframe) . '</div>';

		return $iframe;
	}

	/**
	* Provide a list of known cookies
	*/
	private function getList() {
		$key = 'database';
		$cache = JFactory::getCache('cookiesck', '');
		if ($cache->contains($key))
		{
			return $list = $cache->get($key);
		}
		
		$listFile = JPATH_SITE . '/plugins/system/cookiesck/assets/open-cookie-database.csv';

		$list = file_get_contents($listFile);
		$Data = str_getcsv($list, "\n");

		$Rows = array();
		foreach($Data as &$Row) {
			$Row = str_getcsv($Row, ",");
			if (isset($Row[3])) $Rows[$Row[3]] = $Row;
		}
		$cache->store($Rows, $key);

		return $Rows;

	}

	// scan the website to find the cookies, only for admin
	private function scan() {
		// get the list of existing cookies
		$cookiesList = $this->getList();

		$session = JFactory::getSession();

		$list = array();

		foreach ($_COOKIE as $key => $value) {
			$string = '';
			// do not list the session, nor the cookiesck
			if (
				$key != 'cookiesck'
				&& $value != $session->getId()
				&& $key != $session->getName()
				)
			{
				if (array_key_exists($key, $cookiesList)) {
					$string = addslashes(implode('|CK|', $cookiesList[$key]));
					$list[] = $string;
				} else if (
					$key != 'cookiesck'
					&& $value != $session->getId()
					&& $key != $session->getName()
					)
				{ 
					foreach ($cookiesList as $name => $cookie) {
						if (substr($key, 0, strlen($name)) == $name) {
							$list[] = addslashes(implode('|CK|', $cookiesList[$name]));
						}
					}

					// if the cookie does not exist in the list, set as unknown
					$string = addslashes(implode('|CK|', array('', 'Unknown', 'Unknown', $key, '', '')));
					if (strlen($key) !== 32 && ! in_array($string, $list)) {
						$list[] = $string;
					}
				}
			}
		}

		// close the list
		$js = "var COOKIESCK_LIST = ['" . implode("','", $list) . "'];";
		$js .= "
window.addEventListener('load', function(event) {
	window.parent.ckGetScannedCookies(COOKIESCK_LIST);
});";
		$doc = JFactory::getDocument();
		$doc->addScript(JUri::root(true) . '/plugins/system/cookiesck/assets/admin.js');
		$doc->addScriptDeclaration($js);
	}

	private function listCookies($value) {
		$defaultCats = ['functional', 'analytics', 'marketing', 'essential'];
		$hasCookies = false;
		$tree = json_decode(str_replace('|QQ|', '"', $value));
		$html = '<div class="cookiesck-main">'
//				. '<div class="cookiesck-main-close">×</div>'
				. '<div class="cookiesck-main-close">' . (JText::_('COOKIESCK_SAVE')) . '</div>'
				. '<div class="cookiesck-main-title">' . (JText::_('COOKIESCK_USER_INTERFACE')) . '</div>'
				. '<div class="cookiesck-main-desc">' . (JText::_('COOKIESCK_INFO2')) . '</div>'
				. '<div class="cookiesck-main-buttons">'
				. '<div class="cookiesck-accept cookiesck_button">' . JText::_('COOKIESCK_ACCEPT_ALL') . '</div>'
				. '<div class="cookiesck-decline cookiesck_button">' . JText::_('COOKIESCK_DECLINE_ALL') . '</div>'
				. ($this->params->get('enable_readmore','1') === '1' && $this->readmoreLink['href'] ? '<a class="cookiesck_button" href="' . $this->readmoreLink['href'] . '" ' . $this->readmoreLink['rel'] . ' target="' . $this->readmoreLink['target'] . '" id="cookiesck_readmore">' . addslashes(JText::_('COOKIESCK_MORE')) . '</a>' : '')
				. '</div>'
				;

		foreach ($tree as $category) {
			$hasCookies = true;
			if (in_array(strtolower($category->name), $defaultCats) && ! trim($category->desc)) {
				$desc = trim($category->desc) ? JText::_($category->desc) : JText::_('COOKIESCK_CATEGORY_' . strtoupper($category->name) . '_DESC');
			} else {
				$desc = trim($category->desc) ? JText::_($category->desc) : '';
			}
			$html .= '<div class="cookiesck-category" data-category="' . htmlspecialchars(strtolower(JText::_($category->name))) . '">'
			. '<div class="cookiesck-category-name">' . JText::_($category->name) . '</div>'
			. '<div class="cookiesck-category-desc">' . $desc . '</div>';

			foreach ($category->platforms as $platform) {
				$html .= '<div class="cookiesck-platform" data-platform="' . htmlspecialchars(JText::_($platform->name)) . '">'
				. '<div class="cookiesck-platform-name">' . JText::_($platform->name) . '</div>'
				. '<div class="cookiesck-platform-desc">' . JText::_($platform->desc) . '</div>'
				. '<div class="cookiesck-accept cookiesck_button">' . JText::_('COOKIESCK_ACCEPT') . '</div>'
				. (strtolower($category->name) !== 'essential' ? '<div class="cookiesck-decline cookiesck_button">' . JText::_('COOKIESCK_DECLINE') . '</div>' : '')
				. '</div>'
				;

			}
			$html .= '</div>';
		}

		if ($this->params->get('blockiframes', '0') == '1') {
			// add buttons to manage the iframes choice @TODO
		}

		// close tjhe main section
		$html .= '</div>';

		// echo'<pre>';var_dump($html);echo'</pre>';
		return $hasCookies ? $html : false;
	}

	private function getAllowedCookies($value) {
		$tree = json_decode(str_replace('|QQ|', '"', $value));
		$session = JFactory::getSession();

		// setup default allowed cookies
		$allowed = array('cookiesck', 'cookiesckiframes', $session->getName());
		$platforms = array();

		foreach ($tree as $category) {
			// special needs for essential cookies
			if (strtolower($category->name) == 'essential') {
				foreach($category->platforms as $platform) {
					foreach ($platform->cookies as $cookie) {
						$allowed[] = $cookie->key;
					}
				}
			}

			// construct the platforms
			foreach ($category->platforms as $platform) {
				$platforms[$platform->name] = $platform;
			}
		}

		if (isset($_COOKIE['cookiesck'])) {
			$cookiesckvalue = $_COOKIE['cookiesck'];
			$cookiesckvalue = urldecode($cookiesckvalue);
			// check if the new value from V3 has been set, or if value = yes or no
			if (strpos($cookiesckvalue, '|ck|')) {
				$cookiesckrows = explode('|ck|', $cookiesckvalue);

				foreach ($cookiesckrows as $cookiesckrow) {
					$values = explode('|val|', $cookiesckrow);
					if ($values[1] === '1') {
						if (isset($platforms[$values[0]])) {
							foreach($platforms[$values[0]]->cookies as $cookie) {
								$allowed[] = $cookie->key;
							}
						}
					}
				}
			}
		}

		// hack for admin session, else it will be lost
		if (! empty($_COOKIE)) {
			foreach ($_COOKIE as $name => $key) {
				if (strlen($name) === 32) $allowed[] = $name;
			}
		}

		return $allowed;
	}

	private function getDefaultValue($value) {
		$tree = json_decode(str_replace('|QQ|', '"', $value));

		$value = '';
		$count = 0;
		foreach ($tree as $category) {
			foreach($category->platforms as $platform) {
				$value .= '|ck|' . $platform->name . '|val|';
				if (strtolower($category->name) == 'essential') {
					$value .= '1';
				} elseif (isset($platform->legal)) {
					$value .= $platform->legal;
					$count += $platform->legal;
				} else {
					$value .= '0';
				}
			}
		}
		$value = substr($value, 4);

		if (substr_count($value, '|val|0') == 0) {
			$value = 'yes';
		} elseif ($count == 0) {
			$value = 'no';
		}

		return $value;
	}
}