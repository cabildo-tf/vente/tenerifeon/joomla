<?php return array(
    'root' => array(
        'pretty_version' => '3.x-dev',
        'version' => '3.9999999.9999999.9999999-dev',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../../../../',
        'aliases' => array(),
        'reference' => 'f844ed99a0644dab4613226b7b691a029e9e2a8a',
        'name' => 'akeeba/sociallogin',
        'dev' => false,
    ),
    'versions' => array(
        'akeeba/sociallogin' => array(
            'pretty_version' => '3.x-dev',
            'version' => '3.9999999.9999999.9999999-dev',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../../../../',
            'aliases' => array(),
            'reference' => 'f844ed99a0644dab4613226b7b691a029e9e2a8a',
            'dev_requirement' => false,
        ),
        'codercat/jwk-to-pem' => array(
            'pretty_version' => '1.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codercat/jwk-to-pem',
            'aliases' => array(),
            'reference' => 'e428b7abba5b37676e30e968930f718cf26724ac',
            'dev_requirement' => false,
        ),
        'lcobucci/clock' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lcobucci/clock',
            'aliases' => array(),
            'reference' => '353d83fe2e6ae95745b16b3d911813df6a05bfb3',
            'dev_requirement' => false,
        ),
        'lcobucci/jwt' => array(
            'pretty_version' => '4.1.5',
            'version' => '4.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lcobucci/jwt',
            'aliases' => array(),
            'reference' => 'fe2d89f2eaa7087af4aa166c6f480ef04e000582',
            'dev_requirement' => false,
        ),
        'phpseclib/bcmath_compat' => array(
            'pretty_version' => '1.0.6',
            'version' => '1.0.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpseclib/bcmath_compat',
            'aliases' => array(),
            'reference' => 'f6f03d3af2ef95fc35b30abdd29f20ee11276a65',
            'dev_requirement' => false,
        ),
        'phpseclib/phpseclib' => array(
            'pretty_version' => '2.0.31',
            'version' => '2.0.31.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpseclib/phpseclib',
            'aliases' => array(),
            'reference' => '233a920cb38636a43b18d428f9a8db1f0a1a08f4',
            'dev_requirement' => false,
        ),
    ),
);
