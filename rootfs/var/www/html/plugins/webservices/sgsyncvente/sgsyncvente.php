<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Webservices.sgsyncvente
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Router\ApiRouter;
use Joomla\Router\Route;


/**
 * Web Services adapter for com_content.
 *
 * @since  __BUMP_VERSION__
 */
class PlgWebservicesSgsyncvente extends CMSPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  __BUMP_VERSION__
	 */
	protected $autoloadLanguage = true;

	/**
	 * Registers com_conent's API's routes in the application
	 *
	 * @param   ApiRouter  &$router  The API Routing object
	 *
	 * @return  void
	 *
	 * @since   __BUMP_VERSION__
	 */
	public function onBeforeApiRoute(&$router)
	{
		$defaults    = ['component' => 'com_sgsyncvente'];
		$getDefaults = array_merge(['public' => false], $defaults);
		$routes = [
			new Route(['GET'], 'v1/sgsyncvente/syncequipamientos/syncequipamientos', 'syncequipamientos.syncEquipamientos', [], $getDefaults),
			new Route(['GET'], 'v1/sgsyncvente/syncrutas/syncrutas', 'syncrutas.syncRutas', [], $getDefaults),
			new Route(['POST'], 'v1/sgsyncvente/syncrutas/checksyncprocess', 'syncrutas.checksyncprocess', [], $getDefaults),
			new Route(['GET'], 'v1/sgsyncvente/syncalertas/syncalertas', 'syncalertas.syncAlertas', [], $getDefaults),
			new Route(['GET'], 'v1/sgsyncvente/syncincidencias/syncincidencias', 'syncincidencias.syncincIdencias', [], $getDefaults),
			new Route(['GET'], 'v1/sgsyncvente/syncpuntosinteres/syncpuntosinteres', 'syncpuntosinteres.syncPuntosinteres', [], $getDefaults),
			new Route(['GET'], 'v1/sgsyncvente/syncsearchablepoints/syncsearchablepoints', 'syncsearchablepoints.syncSearchablepoints', [], $getDefaults),
		];
		$router->addRoutes($routes);
	}
}
