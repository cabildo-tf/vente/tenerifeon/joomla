<?php

/**
 * @package     Joomla.Plugin
 * @subpackage  Webservices.sgoauth
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Router\ApiRouter;
use Joomla\Input\Input;
use Joomla\Router\Route;


/**
 * Web Services adapter for com_content.
 *
 * @since  __BUMP_VERSION__
 */
class PlgWebservicesSgoauth extends CMSPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  __BUMP_VERSION__
	 */
	protected $autoloadLanguage = true;

	/**
	 * Registers com_conent's API's routes in the application
	 *
	 * @param   ApiRouter  &$router  The API Routing object
	 *
	 * @return  void
	 *
	 * @since   __BUMP_VERSION__
	 */
	public function onBeforeApiRoute(&$router)
	{
		require_once JPATH_ROOT  .DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';
		
		$defaults    = ['component' => 'com_sgoauth'];
		$getDefaults = array_merge(['public' => false], $defaults);
		$routes = [
			new Route(['POST'], 'v1/sgoauth/registeroauth/registeroauth', 'registeroauth.registerOauth', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/loginoauth/loginoauth', 'loginoauth.loginOauth', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/googleoauth/googleoauth', 'googleoauth.googleOauth', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/facebookoauth/facebookoauth', 'facebookoauth.facebookOauth', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/appleoauth/appleoauth', 'appleoauth.appleOauth', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/useroauth/useroauth', 'useroauth.userOauth', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/useroauth/modifypass', 'useroauth.changeUserPassword', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/useroauth/deleteuser', 'useroauth.deleteUser', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/useroauth/saveprofile', 'useroauth.saveProfile', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/useroauth/resetpassword', 'useroauth.resetPassword', [], array_merge(['public' => true], $defaults)),
			new Route(['POST'], 'v1/sgoauth/useroauth/usercolaborator', 'useroauth.userColaborator', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgitinerarios/sgitinerarios', 'sgitinerarios.sgItinerarios', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgactivities/sgactivities', 'sgactivities.sgActivities', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgactivities/deletesgactivities', 'sgactivities.deleteSgActivities', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgactivities/savesgactivities', 'sgactivities.saveSgActivities', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgequipamientos/sgequipamientos', 'sgequipamientos.sgEquipamientos', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgservicios/sgservicios', 'sgservicios.sgServicios', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/editfavourites/editfavourites', 'editfavourites.editFavourites', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/editvaloraciones/editvaloraciones', 'editvaloraciones.editValoraciones', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/planificadoritinerarios/planificadoritinerarios', 'planificadoritinerarios.planificadorItinerarios', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/planificadoritinerarios/planificadoraltimetria', 'planificadoritinerarios.planificadorAltimetria', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/planificadoritinerarios/searchablepoints', 'planificadoritinerarios.searchablePoints', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/planificadoritinerarios/connecteditineraries', 'planificadoritinerarios.connecteditIneraries', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgpuntosinteres/sgpuntosinteres', 'sgpuntosinteres.sgPuntosinteres', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgincidences/sgincidences', 'sgincidences.sgIncidences', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sguserincidences/sguserincidences', 'sguserincidences.sgUserincidences', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sguserincidences/sggetuserincidences', 'sguserincidences.SgGetUserIncidences', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgroutes/sgroutes', 'sgroutes.sgRoutes', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgroutes/savesgroutes', 'sgroutes.saveSgRoutes', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/sgroutes/deletesgroutes', 'sgroutes.deleteSgRoutes', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/useroauth/social/google', 'socialoauth.google', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/useroauth/social/facebook', 'socialoauth.facebook', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/useroauth/social/apple', 'socialoauth.apple', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/geopoints/geopoints', 'geopoints.geoPoints', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/syncvente/rutas', 'syncvente.rutas', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/syncvente/equipamientos', 'syncvente.equipamientos', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/syncvente/puntosinteres', 'syncvente.puntosinteres', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/syncvente/alertas', 'syncvente.alertas', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/syncvente/incidencias', 'syncvente.incidencias', [], $getDefaults),
			new Route(['POST'], 'v1/sgoauth/syncvente/syncsearchablepoints', 'syncvente.syncsearchablepoints', [], $getDefaults),
		];
		$router->addRoutes($routes);
	}
}
