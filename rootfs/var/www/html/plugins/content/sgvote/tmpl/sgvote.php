<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.vote
 *
 * @copyright   (C) 2016 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;

/**
 * Layout variables
 * -----------------
 * @var   string   $context  The context of the content being passed to the plugin
 * @var   object   &$row     The article object
 * @var   object   &$params  The article params
 * @var   integer  $page     The 'page' number
 * @var   array    $parts    The context segments
 * @var   string   $path     Path to this file
 */

// $uri = clone Uri::getInstance();
// $uri->setVar('hitcount', '0');

// Create option list for voting select box
$options = array();

for ($i = 1; $i < 6; $i++)
{
	$options[] = HTMLHelper::_('select.option', $i, Text::sprintf('PLG_SGVOTE_VOTE', $i));
}

	$app = Factory::getApplication();
	$menu = $app->getMenu();
	$user = Factory::getUser();
	//Check if user is logged in And is not in front page as we dont't want voing here
	if(!$user->guest && ($menu->getActive() != $menu->getDefault( 'es-ES' ))) :
	
	//Check if user has already rated
	$db = Factory::getDbo();
	$query = $db
	->getQuery(true)
	->select(array('COUNT(id)'))
	->from($db->quoteName('#__sg_valoraciones'))
	->where($db->quoteName('user_id') . " = " . $db->quote($user->id))
	->where($db->quoteName('item_id') . " = " . $db->quote($row->id));
	// Reset the query using our newly populated query object.
	$db->setQuery($query);
	// Load the results as a list of stdClass objects (see later for more options on retrieving data).
	$isRated = $db->loadObjectList();
?>
	<?php //if($isRated[0]->count == 0) :?> 
	<form class="form-inline mb-2">
		<span class="content_vote">
			<label class="visually-hidden" for="content_vote_<?php echo (int) $row->id; ?>"><?php echo Text::_('PLG_VOTE_LABEL'); ?></label>
			<?php echo HTMLHelper::_('select.genericlist', $options, 'user_rating', 'class="form-select form-select-sm w-auto"', 'value', 'text', '5', 'content_vote_' . (int) $row->id); ?>
			<button class="btn btn-sm btn-primary" id="submit_vote"  data-itemId="<?= $row->id;?>"><?php echo Text::_('PLG_SGVOTE_RATE'); ?></button>
			<input type="hidden" name="task" value="article.vote">
			<input type="hidden" name="hitcount" value="0">
			<?php echo HTMLHelper::_('form.token'); ?>
		</span>
	</form>
	<script>

		$(document).ready(function () {
			$("#submit_vote").on('click',function(event){
				event.preventDefault();
				item_id = $(this).data("itemid");
				rating = ($("#content_vote_<?= $row->id;?>").val());
				rateItem(<?= $user->id;?>,item_id, rating);
			});
		});
		//Get content by Ajax
		function rateItem(user_id,item_id, rating){

			var baseUrl = "/index.php?option=com_ajax&format=json&group=ajax&" + Joomla.getOptions('csrf.token') + "=1&user=" + user_id + "&item=" + item_id + "&rating=" + rating;

			Joomla.request({
				url: baseUrl +  "&plugin=Addvaloraciones",
				method: 'GET',
				perform: true,
				onSuccess: function onSuccess(resp) {
					const obj = JSON.parse(resp);
					console.log(resp);
					if(obj.success){

						location.reload();
						
					}
					else{
						alert("No se ha podido realizar la valoración");
					}
				},
				onError: function onError() {
					Joomla.renderMessages({
					error: ['Something went wrong! Please close and reopen the browser and try again!']
					});
				}
			});


		}


	</script>
	<?php //endif; ?>
<?php endif; ?>