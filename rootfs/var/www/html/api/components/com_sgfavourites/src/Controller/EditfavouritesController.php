<?php
/**
 * @package     Joomla.API
 * @subpackage  com_articles
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgfavourites\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;

/**
 * The articles controller
 *
 * @since  4.0.0
 */
class EditfavouritesController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function editFavourites (){

		$user =   Factory::getUser();
		//Get parent category from input
		$user_id = $user->id;
		$item_id = $this->app->input->get('item');
		$response = [];
		$isFavourite = null;
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		if(($user_id)){

			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select(array('item_id'))
			->from($db->quoteName('#__sg_favourites'))
			->where($db->quoteName('user_id') . " = " . $db->quote($user_id))
			->where($db->quoteName('item_id') . " = " . $db->quote($item_id))
			->setLimit(1);
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$isFavourite = $db->loadObjectList();
		}
		//if it is favourite
		if(!$isFavourite){

			$query = $db->getQuery(true);
			$columns = array('user_id', 'item_id', 'date_added', 'agent');
			$values = $user_id.", ".$item_id.", '".date('Y-m-d H:i:s')."','web'";
			$query = $db
			->getQuery(true)
			->insert($db->quoteName('#__sg_favourites'))
			->columns($db->quoteName($columns))
			->values(''.$values.'');
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			$db->execute();
			$response['success'] = "true";
			$response['message'] = "ADDED";
		} 
		else{

			$query = $db->getQuery(true);
			$conditions = array(
				$db->quoteName('user_id') . ' = '.$user_id.'', 
				$db->quoteName('item_id') . ' = ' . $item_id
			);
			
			$query->delete($db->quoteName('#__sg_favourites'));
			$query->where($conditions);
			
			$db->setQuery($query);
			$db->execute();
			$response['success'] = "true";
			$response['message'] = "REMOVED";
		}

		die (json_encode($response));
	
	}
}