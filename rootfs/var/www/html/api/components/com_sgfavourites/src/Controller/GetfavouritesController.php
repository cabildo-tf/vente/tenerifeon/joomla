<?php
/**
 * @package     Joomla.API
 * @subpackage  com_articles
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgfavourites\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;


/**
 * The articles controller
 *
 * @since  4.0.0
 */
class GetfavouritesController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function getFavourites (){

		$user =   Factory::getUser();
		$user_id = $user->id;
		if(($user_id)){

			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select(array('item_id'))
			->from($db->quoteName('#__sg_favourites'))
			->where($db->quoteName('user_id') . " = " . $db->quote($user_id));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$hasFavourite = $db->loadObjectList();
		}
		if(isset($hasFavourite)){

			$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
			$appParams = Factory::getApplication()->getParams();  
			$model->setState('params', $appParams);

			dd($favoritos = implode($hasFavourite));
			foreach($favoritos as $key => $punto){
				if(!is_null($punto) && (!empty($punto))){

					$model->setState('filter.article_id', $punto); //Ayuda article id
		
					$helpArticle =   $model->getItems();
					$item = $helpArticle[0];
					$fields = FieldsHelper::getFields('com_content.article', $item, true);
					// Adding the fields to the object
					$item->jcfields = array();
					foreach ($fields as $key => $field)
					{
						$item->jcfields[$field->id] = $field;
					}
					
				}
			}
		}
	
	}
}