<?php

/**
 * @package     Joomla.API
 * @subpackage  com_articles
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Factory;
use Facebook\Facebook;
use Google\Client as Google_Client;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\String\PunycodeHelper;
use Joomla\CMS\User\User;
use Joomla\Component\Users\Site\Model\RegistrationModel;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Signature\Algorithm\ES256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Firebase\JWT\JWT;

/**
 * The articles controller
 *
 * @since  4.0.0
 */
class SocialoauthController  extends ApiController
{
    /**
     * The content type of the item.
     *
     * @var    string
     * @since  4.0.0
     */
    protected $contentType = 'articles';

    /**
     * The default view for the display method.
     *
     * @var    string
     * @since  4.0.0
     */
    protected $default_view = 'articles';


    /**
     * Google ® Login API
     *
     * @return void
     */
    public function google()
    {
        // Get inputs
        $tokenId = $this->input->get('token_id', null, 'STRING');
        $registerProcess = $this->input->get('register_process', null, 'int') == 1 ? true : false;
        $os = $this->input->get('os', null, 'STRING');
        if ($tokenId == null) {
            return die(json_encode(['success' => 'false', 'message' => 'token_id is required']));
        }

        // Get joomla configuration
        $config = Factory::getConfig();

        // Get google configuration
        $googleIosKey = $config->get('googleIosKey');
        $googleAndroidKey = $config->get('googleAndroidKey');
        $googleWebKey = $config->get('googleWebKey');



        if ($os == 'ios') {
            // If was ios
            $client = new Google_Client(['client_id' => $googleIosKey]);
        } else {
            // If is android
            $client = new Google_Client(['client_id' => $googleWebKey]);
        }
        $result = $client->verifyIdToken($tokenId);

        if ($result != false) {
            if ($os == 'ios' || $os == 'android') {
                // Check user exists
                $user = $this->getExistentUser($result['email']);

                // If exists then login, if not then register
                if ($user) {
                    if(!empty($user->activation)){
                        return die(json_encode(['message' => 'User not active', 'success' => 'false', 'data' => ['token' => $this->getUserToken($user)]]));
                    }
                    // Get the user token
                    return die(json_encode(['message' => 'Iniciado sesión correctamente.', 'success' => 'true', 'data' => ['token' => $this->getUserToken($user)]]));
                } else {
                    if ($registerProcess) {
                        // Register the user
                        $user = $this->createUser($result['given_name'], $result['family_name'] ?? '', $result['email']);

                        return die(json_encode(['message' => 'Registrado correctamente.', 'success' => 'true', 'data' => ['token' => $this->getUserToken($user)]]));
                    } else {
                        return die(json_encode(['message' => 'El usuario no existe.', 'success' => 'false']));
                    }
                }
            }
        } else {
            // Api token not good return error
            throw new Exception("Error with the provided token comparation.");
        }
    }

    /**
     * Facebook ® Login API
     *
     * @return void
     */
    public function facebook()
    {
        // Get inputs
        $tokenId = $this->input->get('token_id', null, 'STRING');
        $registerProcess = $this->input->get('register_process', null, 'int') == 1 ? true : false;

        if ($tokenId == null) {
            return die(json_encode(['success' => 'false', 'message' => 'token_id is required']));
        }

        // Get joomla configuration
        $config = Factory::getConfig();

        // Set app facebook configuration
        $appId = $config->get('facebookAppId');
        $secret = $config->get('facebookSecret');
        $completeToken = $appId . '|' . $secret;
        $facebook = new Facebook(['app_id' => $appId, 'app_secret' => $secret]);
        $oAuthClient = $facebook->getOAuth2Client();

        $meta = $oAuthClient->debugToken($completeToken);

        if ($meta != NULL) {
            // This means the application is found
            try {
                $meta->validateAppId($appId);
            } catch (Exception $e) {
                throw new Exception('Invalid Facebook credentials on server side.');
            }


            $resp = $facebook->get('/me?fields=id,first_name,last_name,email', $tokenId);
            if ($resp) {
                $userData = $resp->getGraphUser();

                // Check user exists
                $user = $this->getExistentUser($userData['email']);

                // If exists then login, if not then register
                if ($user) {
                    if(!empty($user->activation)){
                        return die(json_encode(['message' => 'User not active', 'success' => 'false', 'data' => ['token' => $this->getUserToken($user)]]));
                    }
                    // Get the user token
                    return die(json_encode(['message' => 'Iniciado sesión correctamente.', 'success' => 'true', 'data' => ['token' => $this->getUserToken($user)]]));
                } else {
                    // If is in register process
                    if ($registerProcess) {
                        // Register the user
                        $user = $this->createUser($userData['first_name'], $userData['last_name'] ?? '', $userData['email']);

                        return die(json_encode(['message' => 'Registrado correctamente.', 'success' => 'true', 'data' => ['token' => $this->getUserToken($user)]]));
                    } else {
                        // Return error
                        return die(json_encode(['success' => 'false', 'message' => 'El usuario no existe.']));
                    }
                }
            } else {
                return die(json_encode(['message' => 'Datos de inicio de sesión con Facebook incorrectos.', 'success' => 'false']));
            }
        }
    }

    /**
     * Apple ® Login API
     *
     * @return void
     */
    public function apple()
    {
        /** Code from request: https://appleid.apple.com/auth/authorize?response_type=code&client_id={$clientId}&scope=email%20name&response_mode=form_post&redirect_uri={$redirectUri} */
        $authorizationCode = $this->input->get('authorization_code', null, 'STRING');
        $givenName = $this->input->get('given_name', null, 'STRING');
        $familyName = $this->input->get('family_name', null, 'STRING');
        $registerProcess = $this->input->get('register_process', null, 'int') == 1 ? true : false;

        if ($authorizationCode == null) {
            return die(json_encode(['success' => 'false', 'message' => 'authorization_code is required']));
        }
        if ($givenName == null) {
            return die(json_encode(['success' => 'false', 'message' => 'given_name and family_name are required']));
        }

        // Get joomla configuration
        $config = Factory::getConfig();

        /** Your team identifier: https://developer.apple.com/account/#/membership/ (Team ID) */
        $teamId = $config->get('appleTeamId');
        /** The client id of your service: https://developer.apple.com/account/resources/identifiers/list/serviceId */
        $appleBundleId = $config->get('appleBundleId');
        /** The ID of the key file: https://developer.apple.com/account/resources/authkeys/list (Key ID) */
        $appleKeyId = $config->get('appleKeyId');

        $appleSecretKey = $config->get('appleSecretKey');

        //This code was written considering uploading the secret key to server. Now set the applesecreykey in configuration and change the addSignature method
        // $keyFilePath = JPATH_ROOT . '/storage/apple_signin/AuthKey_' . $appleKeyId . '.p8';

        // // Check the file exists
        // if (!file_exists($keyFilePath)) {
        //     throw new Exception('Apple Key File not found. Ensure the file exists and try again.');
        // }
        /** The path of the file which you downloaded from https://developer.apple.com/account/resources/authkeys/list */
       // $keyFileName = $keyFilePath;

        $clientId = $appleBundleId;
        $code = $authorizationCode;

        /** The redirect uri of your service which you used in the $code request */
        //$redirectUri = 'https://example.org';

        $algorithmManager = new AlgorithmManager([new ES256()]);

        $jwsBuilder = new JWSBuilder($algorithmManager);
        $jws = $jwsBuilder
            ->create()
            ->withPayload(json_encode([
                'iat' => time(),
                'exp' => time() + 3600,
                'iss' => $teamId,
                'aud' => 'https://appleid.apple.com',
                'sub' => $clientId
            ]))

            ->addSignature(JWKFactory::createFromKey($appleSecretKey), [
            //->addSignature(JWKFactory::createFromKeyFile($keyFileName), [
                'alg' => 'ES256',
                'kid' => $appleKeyId
            ])
            ->build();

        $serializer = new CompactSerializer();
        $token = $serializer->serialize($jws, 0);

        $data = [
            'client_id' => $clientId,
            'client_secret' => $token,
            'code' => $code,
            'grant_type' => 'authorization_code',
        ];

        
        // asForm method will set Content-Type as encoded-url (required by Apple API)
        // Make a curl

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => 'https://appleid.apple.com/auth/token',
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_RETURNTRANSFER => true
        ]);
        $response = curl_exec($ch);
        curl_close($ch);
        // Decode the response
        $response = json_decode($response);
        // Check if the response is valid
        if ($response != null) {
            if ($response->error != null) {
                return die(json_encode(['success' => 'false', 'message' => 'Error found on Apple API credentials.']));
            }

            // If apple returned a json right
            $tks = explode('.', $response->id_token);
            list($headb64, $bodyb64, $cryptob64) = $tks;
            $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64));

            // Check user exists
            $user = $this->getExistentUser($payload->email);

            // If exists then login, if not then register
            if ($user) {
                if(!empty($user->activation)){
                    return die(json_encode(['message' => 'User not active', 'success' => 'false', 'data' => ['token' => $this->getUserToken($user)]]));
                }
                // Get the user token
                return die(json_encode(['message' => 'Iniciado sesión correctamente.', 'success' => 'true', 'data' => ['token' => $this->getUserToken($user)]]));
            } else {
                // Register the user but only if cames from the register process
                if ($registerProcess) {
                    $user = $this->createUser($givenName, $familyName ?? '', $payload->email);

                    return die(json_encode(['message' => 'Registrado correctamente.', 'success' => 'true', 'data' => ['token' => $this->getUserToken($user)]]));
                } else {
                    return die(json_encode(['message' => 'El usuario no existe.', 'success' => 'false']));
                }
            }
        } else {
            return die(json_encode(['message' => 'No se ha podidoi contactar con el servidor de apple.', 'success' => 'false']));
        }
    }




    protected function getExistentUser($email): ?User
    {
        $db = Factory::getDbo();

        $query = $db
            ->getQuery(true)
            ->select('id')
            ->from($db->quoteName('#__users'))
            ->where($db->quoteName('email') . " = " . $db->quote($email));
        $db->setQuery($query);
        $userId = $db->loadResult();

        if ($userId != null) {
            $user = User::getInstance($userId);

            return $user;
        } else {
            return null;
        }
    }

    protected function createUser($name, $surname, $email): ?User
    {
        //Get input data
        $temp['name'] = $name;
        $temp['surname'] = $surname;
        $temp['username'] = strtolower($name) . '_' . rand(100, (9999999 * 100));
        // TODO: check username no t already exists
        $temp['email1'] = $email;
        $temp['privacyconsent'] = array(
            "privacy" => "1",
        );

        $params = ComponentHelper::getParams('com_users');

        // Initialise the table with Joomla\CMS\User\User.
        $user = new User;

        $data['groups'] = array(
            0 => "2"
        );
        //TODO: get from request
        $data['language'] = "es-ES";

        // Merge in the registration data.
        foreach ($temp as $k => $v) {
            $data[$k] = $v;
        }

        // Prepare the data for the user object.
        $data['email'] = PunycodeHelper::emailToPunycode($data['email1']);
        $data['password'] = password_hash('api_register_social_network', PASSWORD_DEFAULT);
        $data['useractivation'] = 1;

        $registrationModel = new RegistrationModel();
        // Bind the data.
        if (!$user->bind($data)) {
            $registrationModel->setError($user->getError());

            return false;
        }

        // Load the users plugin group.
        PluginHelper::importPlugin('user');
        //Set response variable to return json encoded
        $response = [
            'success' => 'true',
            'message' => ''
        ];
        // Store the data.
        if (!$user->save()) {
            $response['success'] = "false";
            $response['message'] = $user->getError();
            die(json_encode($response));

            return false;
        }

        $app = Factory::getApplication();
        $db = $registrationModel->getDbo();
        $query = $db->getQuery(true);

        // Get the user
        $user = $this->getExistentUser($data['email']);
        return $user;
    }



    /**
     * Get the user token and also generates it if it does not exist.
     *
     * @return String
     */
    protected function getUserToken($user): String
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true)
            ->select('profile_value')
            ->from($db->quoteName('#__user_profiles'))
            ->where($db->quoteName('profile_key') . " = " . $db->quote('joomlatoken.token'))
            ->where($db->quoteName('user_id') . " = " . $db->quote($user->id));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);
        //get the user.profile token saved in db
        $tokenSeed = $db->loadResult();

        $siteSecret = Factory::getApplication()->get('secret');
        $algorithm = "sha256";
        $rawToken  = base64_decode($tokenSeed);
        $tokenHash = hash_hmac($algorithm, $rawToken, $siteSecret);
        $userId    = $user->id;
        $userToken   = base64_encode("$algorithm:$userId:$tokenHash");

        return $userToken;
    }
}
