<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\CMS\User\UserHelper;
use Joomla\Plugin\User\Token\Field\JoomlatokenField;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class LoginoauthController extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function loginOauth()
	{
		
		$userName = $this->input->get('username',null,'STRING');
		$inputPassword =$this->input->get('password',null, null);
		
		$response = [
			'success' => 'true',
			'message' => '',
			'data' => ''
		];
		
		// Check inputs are not null and are valids
		if (empty($userName) ) {
			die($userNameErr = "Nombre usuario vacío");
		}

		//Check in database if exists user email
		$db = Factory::getDbo();

		$query = $db
		->getQuery(true)
		->select('id')
		->from($db->quoteName('#__users'))
		->where($db->quoteName('username') . " = " . $db->quote($userName));

		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		$user = Factory::getUser($userExists = $db->loadResult());
		if(!empty($user->activation)){
			$response['success'] = "false";
			$response['message'] = "User not active";
			die (json_encode($response));
		}
		// Get user password from the user
		if($userExists){

			$query = $db
			->getQuery(true)
			->select('password')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('id') . " = " . $db->quote($userExists));

			// Reset the query using our newly populated query object.
			$db->setQuery($query);

			$userPasswdFromDb = $db->loadResult();

			//Check if input password is user password
			$passwordIsOk = UserHelper::verifyPassword($inputPassword,$userPasswdFromDb);

			// Get user token
			if($passwordIsOk){

				$query = $db
				->getQuery(true)
				->select('profile_value')
				->from($db->quoteName('#__user_profiles'))
				->where($db->quoteName('profile_key') . " = " . $db->quote('joomlatoken.token'))
				->where($db->quoteName('user_id') . " = " . $db->quote($userExists));

				// Reset the query using our newly populated query object.
				$db->setQuery($query);
				//get the user.profile token saved in db
				$tokenSeed = $db->loadResult();
				
				$siteSecret = Factory::getApplication()->get('secret');
				$algorithm = "sha256";
				$rawToken  = base64_decode($tokenSeed);
				$tokenHash = hash_hmac($algorithm, $rawToken, $siteSecret);
				$userId    = $userExists;
				$userToken   = base64_encode("$algorithm:$userId:$tokenHash");
				$response['data'] =['token' => $userToken];			
			}
			else{
				$response['success'] = "false";
				$response['message'] = "Passwd is not ok";
				//die (json_encode($response));
			}
		}
		else{
			$response['success'] = "false";
			$response['message'] = "User does not exists";
			die (json_encode($response));
		}

		
		die (json_encode($response));
		
	}
}
