<?php

/**
 * @package     Joomla.API
 * @subpackage  com_articles
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Factory;
use Facebook\Facebook;
use Google\Client as Google_Client;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\String\PunycodeHelper;
use Joomla\CMS\User\User;
use Joomla\Component\Users\Site\Model\RegistrationModel;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Signature\Algorithm\ES256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Firebase\JWT\JWT;

/**
 * The articles controller
 *
 * @since  4.0.0
 */
class SyncventeController  extends ApiController
{
    /**
     * The content type of the item.
     *
     * @var    string
     * @since  4.0.0
     */
    protected $contentType = 'articles';

    /**
     * The default view for the display method.
     *
     * @var    string
     * @since  4.0.0
     */
    protected $default_view = 'articles';


    /**
     * Rutas ® Syncro vente
     *
     * @return void
     */
    public function rutas()
    {
        $user = Factory::getUser();
        $userToken = $this->getUserToken($user);
        $sync_id = $this->input->get('sync_id',null,'INTEGER');
        $siteUrl = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://'.$_SERVER['SERVER_NAME']; 

        exec("curl --location --request GET '".$siteUrl."/api/index.php/v1/sgsyncvente/syncrutas/syncrutas?sync_id=".$sync_id."' --header 'Authorization: Bearer ".$userToken."'");

    }

    /**
     * Equipamientos ® Syncro vente
     *
     * @return void
     */
    public function equipamientos()
    {       
        $user = Factory::getUser();
        $userToken = $this->getUserToken($user);
        $sync_id = $this->input->get('sync_id',null,'INTEGER');
        $siteUrl = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://'.$_SERVER['SERVER_NAME']; 

        exec("curl --location --request GET '$siteUrl/api/index.php/v1/sgsyncvente/syncequipamientos/syncequipamientos?sync_id=".$sync_id."' --header 'Authorization: Bearer ".$userToken."'");
 
    }

    /**
     * Puntos interés ® Syncro vente
     *
     * @return void
     */
    public function puntosinteres()
    {       
        $user = Factory::getUser();
        $userToken = $this->getUserToken($user);
        $sync_id = $this->input->get('sync_id',null,'INTEGER');
        $siteUrl = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://'.$_SERVER['SERVER_NAME']; 

        exec("curl --location --request GET '$siteUrl/api/index.php/v1/sgsyncvente/syncpuntosinteres/syncpuntosinteres?sync_id=".$sync_id."' --header 'Authorization: Bearer ".$userToken."'");
 
    }

    /**
     * Alertas ® Syncro vente
     *
     * @return void
     */
    public function alertas()
    {       
        $user = Factory::getUser();
        $userToken = $this->getUserToken($user);
        $sync_id = $this->input->get('sync_id',null,'INTEGER');
        $siteUrl = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://'.$_SERVER['SERVER_NAME']; 

        exec("curl --location --request GET '$siteUrl/api/index.php/v1/sgsyncvente/syncalertas/syncalertas?sync_id=".$sync_id."' --header 'Authorization: Bearer ".$userToken."'");
 
        
    }

    /**
     * Incidencias ® Syncro vente
     *
     * @return void
     */
    public function incidencias()
    {       
        $user = Factory::getUser();
        $userToken = $this->getUserToken($user);
        $sync_id = $this->input->get('sync_id',null,'INTEGER');
        $siteUrl = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://'.$_SERVER['SERVER_NAME']; 

        exec("curl --location --request GET '$siteUrl/api/index.php/v1/sgsyncvente/syncincidencias/syncincidencias?sync_id=".$sync_id."' --header 'Authorization: Bearer ".$userToken."'");
 
    }
    
    /**
     * Syncsearchablepoints ® Syncro vente
     *
     * @return void
     */
    public function syncsearchablepoints()
    {       
        $user = Factory::getUser();
        $userToken = $this->getUserToken($user);
        $sync_id = $this->input->get('sync_id',null,'INTEGER');
        $siteUrl = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://'.$_SERVER['SERVER_NAME']; 

        exec("curl --location --request GET '$siteUrl/api/index.php/v1/sgsyncvente/syncsearchablepoints/syncsearchablepoints?sync_id=".$sync_id."' --header 'Authorization: Bearer ".$userToken."'");
 
    }

    /**
     * Get the user token and also generates it if it does not exist.
     *
     * @return String
     */
    protected function getUserToken($user)
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true)
            ->select('profile_value')
            ->from($db->quoteName('#__user_profiles'))
            ->where($db->quoteName('profile_key') . " = " . $db->quote('joomlatoken.token'))
            ->where($db->quoteName('user_id') . " = " . $db->quote($user->id));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);
        //get the user.profile token saved in db
        $tokenSeed = $db->loadResult();

        $siteSecret = Factory::getApplication()->get('secret');
        $algorithm = "sha256";
        $rawToken  = base64_decode($tokenSeed);
        $tokenHash = hash_hmac($algorithm, $rawToken, $siteSecret);
        $userId    = $user->id;
        $userToken   = base64_encode("$algorithm:$userId:$tokenHash");

        return $userToken;
    }
}
