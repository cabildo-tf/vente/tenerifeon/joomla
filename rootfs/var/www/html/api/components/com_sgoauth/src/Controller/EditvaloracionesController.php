<?php
/**
 * @package     Joomla.API
 * @subpackage  com_articles
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;

/**
 * The articles controller
 *
 * @since  4.0.0
 */
class EditvaloracionesController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function editValoraciones (){

		$user =   Factory::getUser();
		//Get parent category from input
		$user_id = $user->id;
		$item_id = $this->app->input->get('item');
		$rating = $this->app->input->get('rating');
		$agent = $this->app->input->get('agent');
		if(is_null($agent)){
			$agent = 'app';
		}

		//Check if user has already rated
		$db = Factory::getDbo();
		$query = $db
		->getQuery(true)
		->select(array('COUNT(id)'))
		->from($db->quoteName('#__sg_valoraciones'))
		->where($db->quoteName('user_id') . " = " . $db->quote($user->id))
		->where($db->quoteName('item_id') . " = " . $db->quote($item_id));
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$isRated = $db->loadObjectList();

		if($isRated[0]->count == 0){
			$response = "";

			//Rate the item
			$db = Factory::getDbo();
			$query = $db->getQuery(true);
			$columns = array('user_id', 'item_id', 'date_added', 'agent', 'rating');
			$values = $user_id.", ".$item_id.", '".date('Y-m-d H:i:s')."','".$agent."', ".$rating."";
			$query = $db
			->getQuery(true)
			->insert($db->quoteName('#__sg_valoraciones'))
			->columns($db->quoteName($columns))
			->values(''.$values.'');
	
			$db->setQuery($query);
	
			$response = [];
	
			if($itemRated = $db->execute()){
				$response['success'] = "true";
				$response['message'] = "RATED";
			} 
		}
		else{
			$response['success'] = "false";
			$response['message'] = "KO";
		}

		die (json_encode($response));
	
	}
}