<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\CMS\User\UserHelper;
use Joomla\Component\Users\Site\Model\RegistrationModel;
use Joomla\Plugin\User\Token\Field\JoomlatokenField;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SgactivitiesController extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SgActivities()
	{
		$user = Factory::getUser();
		$db = Factory::getDbo();
		$offset = $this->input->get('offset',null,'INTEGER');
		$limit = $this->input->get('limit',null,'INTEGER');
		$type = $this->input->get('type',null,'INTEGER');
		$date = $this->input->get('date',null,'STRING'); //month - year
		$date = explode("-",$date);

		$query ="
		select  *
	   from
			   sooeg_sg_my_activities
	   where
		   user_id = ".$user->id."";
		if((!is_null($type))){
			$query .= " and activity_type = " .$type. "";
		}
		// if( $date[0] != "" && $date[1] != "" ){
		// 	$fromYear = $date[1];
		// 	$fromMonth = $date[0];
		// 	$toYear = $date[1];
		// 	$toMonth = ($date[0] + 1);
		// 	if($fromMonth == 12){
		// 		$toYear = $date[1] + 1;
		// 		$toMonth = '01';
		// 	}
		// 	$query .= " and activity_date BETWEEN '".$fromYear."-".$fromMonth."-01 00:00:00' AND '".$toYear."-".$toMonth."-01 00:00:00'";
		// }
		$query .= " order by activity_date DESC";
		$query .= " offset ".$offset."
					limit ".$limit."";
		$db->setQuery($query);
		$myactivities['data'] = $db->loadObjectList();
		$myactivities['total-count'] = count($myactivities['data']);

	
		die (json_encode($myactivities));
		
		
	}

	public function SaveSgActivities()
	{
		try
		{	
			$user = Factory::getUser();
			$db = Factory::getDbo();

			$temp['user_id'] = $user->id;
			$temp['activity_name'] = $this->input->get('activity_name',null,'STRING');
			$temp['activity_type'] = $this->input->get('activity_type',null,'INTEGER');
			$temp['activity_date'] = $this->input->get('activity_date',null,'RAW');
			$temp['activity_geom'] = $this->input->get('activity_geom',null,'STRING');
			$temp['activity_gain'] = $this->input->get('activity_gain',null,'RAW');
			$temp['activity_lost'] = $this->input->get('activity_lost',null,'RAW');
			$temp['activity_max_altitude'] = $this->input->get('activity_max_altitude',null,'RAW');
			$temp['activity_min_alt'] = $this->input->get('activity_min_alt',null,'RAW');
			$temp['activity_duration'] = $this->input->get('activity_duration',null,'INTEGER');
			$temp['activity_distance'] = $this->input->get('activity_distance',null,'INTEGER');
			
			$statrtLatLon = json_decode($temp['activity_geom'])->features[0]->geometry->coordinates[0][0];
			$query = 	"SELECT m.id
							FROM municipios m
							WHERE ST_Contains(m.geom, 'POINT(".$statrtLatLon[0]." ".$statrtLatLon[1].")');";
			
			$db->setQuery($query);
			$temp['activity_place'] = $db->loadResult();
		

			//Select items rated and rating
			$query = "INSERT INTO sooeg_sg_my_activities (user_id, activity_name, activity_type, activity_date, activity_geom, activity_gain, activity_lost, activity_max_altitude, activity_min_alt, activity_duration, activity_distance";
			if($temp['activity_place']){
				$query .= ", activity_place";
			}
			$query .= ") ";
			$query .= "values (".$user->id.", '".$temp['activity_name']."', '".$temp['activity_type']."', '".$temp['activity_date']."', '".$temp['activity_geom']."','".$temp['activity_gain']."','".$temp['activity_lost']."','".$temp['activity_max_altitude']."','".$temp['activity_min_alt']."','".$temp['activity_duration']."','".$temp['activity_distance']."'";
			if($temp['activity_place']){
				$query .= ",'".$temp['activity_place']."'";
			}
			$query .= ");";
			$db->setQuery($query)->execute();

			$response['success'] = "true";
			$response['message'] = "Actividad creada correctamente";
		}
		catch (\Exception $exception)
		{
			$response['success'] = "false";
			$response['message'] = "No se ha podido crear la  actividad";
		}
		
		die (json_encode($response));
	}
	public function DeleteSgActivities(){

		$user = Factory::getUser();
		$db = Factory::getDbo();
		$id = $this->input->get('id',null,'INTEGER');

		$query ="
		DELETE FROM sooeg_sg_my_activities
		WHERE
 	    user_id = ".$user->id."
		AND id = ".$id."";

		$db->setQuery($query);
		$db->execute();

		$response['success'] = "true";
		$response['message'] = "Se ha borrado la actividad, si existía";
			
		die (json_encode($response));
		
	}
}
