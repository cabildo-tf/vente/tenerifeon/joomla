<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Router\Route;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\User\User;
use Joomla\Database\ParameterType;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Response\JsonResponse;
use Joomla\CMS\User\UserHelper;
use Joomla\Component\Users\Site\Model\ResetModel;
use Joomla\Plugin\User\Token\Field\JoomlatokenField;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class UseroauthController extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'user';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'user';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function userOauth()
	{
		
		$user =   Factory::getUser();

		foreach (FieldsHelper::getFields('com_users.user', $user, true) as $field)
		{
			if($field->name == "intereses"){
				
				$user->interesesList = $field->fieldparams['options'];
			}
			$user->{$field->name} = isset($field->apivalue) ? $field->apivalue : $field->rawvalue;
			if($field->name == 'avatar'){
				if(isset((json_decode($field->rawvalue)->storedname))  && json_decode($field->rawvalue)->storedname != null){
					$user->avatar = 'media/plg_fields_bffile/com_users.user/' . (json_decode($field->rawvalue)->storedname ?? '/');
				} else{
					$user->avatar = null;
				}
				
			}
		}
		$db = Factory::getDbo();

		$query = $db
		->getQuery(true)
		->select('profile_value')
		->from($db->quoteName('#__user_profiles'))
		->where($db->quoteName('profile_key') . " = " . $db->quote('profile.phone'))
		->where($db->quoteName('user_id') . " = " . $db->quote($user->id));

		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		//get the user.profile token saved in db
		$phone = $db->loadResult();

		if(!is_null($phone)){
			$user->phone = str_replace('"','',$phone);
		}
		else{
			$user->phone = null;
		}

		//die (json_encode($user));
		die(json_encode($user));
		
	}

	public function changeUserPassword() {
		$user =   Factory::getUser();
		$db = Factory::getDbo();
		$actual_password = $this->input->get('actual_password',null,'STRING');
		$new_password = $this->input->get('new_password',null,'STRING');

		$query = $db
			->getQuery(true)
			->select('password')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('id') . " = " . $user->id);

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		$userPasswdFromDb = $db->loadResult();

		$passwordIsOk = UserHelper::verifyPassword($actual_password, $userPasswdFromDb);

		if($passwordIsOk) {
			$data['password'] = $new_password;
			$data['password1'] = $new_password;
			$data['password2'] = $new_password;

			if($user->bind($data)) {
				if (!$user->save()) {
					$response['success'] = "false";
					$response['message'] = $user->getError();
				} else {
					$response['success'] = "true";
					$response['message'] = "Changed successfully";
				}
			} else {
				$response['success'] = "false";
				$response['message'] = $user->getError();
			}
		} else {
			$response['success'] = "false";
			$response['message'] = "Actual passwd is not ok";
		}

		die(json_encode($response));
	}

	public function deleteUser() {
		$user =   Factory::getUser();
		$db = Factory::getDbo();

		$query = $db
			->getQuery(true)
			->delete($db->quoteName('#__users'))
			->where($db->quoteName('id') . " = " . $user->id);

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		if($db->execute()) {
			$response['success'] = "true";
			$response['message'] = "Deleted";
		} else {
			$response['success'] = "false";
			$response['message'] = "Error";
		}

		die(json_encode($response));
	}

	public function saveProfile() {
		try {	
			$user = Factory::getUser();
			$db = Factory::getDbo();

			//Update CustomFiles -> Update phone from user_profiles -> Update email from users
			$temp['intereses'] = $this->input->get('intereses',null,'STRING');
			$temp['name'] = $this->input->get('name',null,'STRING');
			$temp['apellidos'] = $this->input->get('apellidos',null,'STRING');
			$temp['montanismo'] = $this->input->get('montanismo',null,'INTEGER');
			$temp['licencia-montana'] = $this->input->get('licencia-montana',null,'RAW');
			$temp['federado-en-algun-deporte'] = $this->input->get('federado-en-algun-deporte',null,'STRING');
			$temp['ciclismo'] = $this->input->get('ciclismo',null,'RAW');
			$temp['licencia-ciclismo'] = $this->input->get('licencia-ciclismo',null,'RAW');
			$temp['avatar'] = $this->input->files->get('avatar',null, 'ARRAY');
			$temp['colaborador'] = $this->input->get('colaborador',null,'RAW');

			$temp['profile.phone'] = $this->input->get('phone',null,'RAW');
			$temp['email'] = $this->input->get('email',null,'RAW');

			// If the user provides an avatar
			if(isset($temp['avatar']) && $temp['avatar'] != null && $temp['avatar']['size'] > 0){
				// Get uploaded file
				$file = file_get_contents($temp['avatar']['tmp_name']);
				$extension = explode('.',$temp['avatar']['name']) [sizeof(explode('.',$temp['avatar']['name'])) - 1];
				$path = '../media/plg_fields_bffile/com_users.user/avatar.' .$user->id. '.'.$extension ;	
				
				file_put_contents($path, $file);
				//dd($temp['avatar']['name']);
				
				($field_value = '{"filename":"'.$temp['avatar']['name'].'","context":"com_users.user","storedname":"avatar.' .$user->id. '.'.$extension.'"}');
				$object = (object)[
					'item_id'  => $user->id,
					'field_id' => '133',
					'value'    => $field_value,
				];

				$query = $db
					->getQuery(true)
					->select('field_id')
					->from($db->quoteName('#__fields_values'))
					->where($db->quoteName('field_id') . " = 133")
					->where($db->quoteName('item_id') . " = " . $db->quote($user->id));
			
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
			
					// Load the results as a list of stdClass objects (see later for more options on retrieving data).
					$exists = $db->loadResult();
					if($exists) {
						if($db->updateObject('#__fields_values', $object, ['field_id', 'item_id'])){
							$response['success'] = "true";
							$response['message'] = "Actualizado correctamente";
						} else {
							$response['success'] = "false";
							$response['message'] = "No se ha podido guardar el perfil en update";
						}
					} else{
						if($db->insertObject('#__fields_values', $object)){
							$response['success'] = "true";
							$response['message'] = "Creado Correctamente";
						} else {
							$response['success'] = "false";
							$response['message'] = "No se ha podido guardar el perfil en create";
						}
					}

			}

			//Delete interesrs form user profile
			$query = $db->getQuery(true);

			$conditions = array(
				$db->quoteName('item_id') . ' = '. $db->quote($user->id), 
				$db->quoteName('field_id') . ' = ' . $db->quote('88')
			);
			
			$query->delete($db->quoteName('#__fields_values'));
			($query->where($conditions));

			($db->setQuery($query));
			($result = $db->execute());
			foreach (FieldsHelper::getFields('com_users.user', $user, true) as $field) {
				if($field->id == 88){

					($interesList = explode(',', $temp['intereses']));
					foreach($interesList as $interes){
						$object = (object)[
							'item_id'  => $user->id,
							'field_id' => $field->id,
							'value'    => $interes,
						];
						$db->insertObject('#__fields_values', $object);
					}

				}
				elseif($field->id == 263){

					$object = (object)[
						'item_id'  => $user->id,
						'field_id' => $field->id,
						'value'    => $field->rawvalue,
					];
					$db->updateObject('#__fields_values', $object, ['field_id', 'item_id']);

				}
				else{
					$object = (object)[
						'item_id'  => $user->id,
						'field_id' => $field->id,
						'value'    => $temp[$field->name],
					];
	
					// Create a new query object.
					$query = $db
					->getQuery(true)
					->select('field_id')
					->from($db->quoteName('#__fields_values'))
					->where($db->quoteName('field_id') . " = " . $field->id)
					->where($db->quoteName('item_id') . " = " . $db->quote($user->id));
			
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
			
					// Load the results as a list of stdClass objects (see later for more options on retrieving data).
					$exists = $db->loadResult();
					if($exists) {
						if($db->updateObject('#__fields_values', $object, ['field_id', 'item_id'])){
							$response['success'] = "true";
							$response['message'] = "Actualizado correctamente";
						} else {
							$response['success'] = "false";
							$response['message'] = "No se ha podido guardar el perfil en update";
						}
					} else{
						if($db->insertObject('#__fields_values', $object)){
							$response['success'] = "true";
							$response['message'] = "Creado Correctamente";
						} else {
							$response['success'] = "false";
							$response['message'] = "No se ha podido guardar el perfil en create";
						}
					}

				}



			}

			// Create a new query object.
			$query = $db
			->getQuery(true)
			->select('user_id')
			->from($db->quoteName('#__user_profiles'))
			->where($db->quoteName('profile_key') . " = 'profile.phone'")
			->where($db->quoteName('user_id') . " = " . $db->quote($user->id));

			// Reset the query using our newly populated query object.
			$db->setQuery($query);

			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$exists = $db->loadResult();

			$object = (object)[
				'user_id'  => $user->id,
				'profile_key' =>'profile.phone',
				'profile_value'    => $temp['profile.phone'],
			];

			if($exists) {
				if($db->updateObject('#__user_profiles', $object, ['profile_key', 'user_id'])){
					$response['success'] = "true";
					$response['message'] = "Actualizado correctamente";
				}
			} else{
				if($db->insertObject('#__user_profiles', $object)){
					$response['success'] = "true";
					$response['message'] = "Creado Correctamente";
				}
			}

			$user->email = $temp['email'];
			$user->name = $temp['name'];


			if($db->updateObject('#__users', $user, ['id'])){
				$response['success'] = "true";
				$response['message'] = "Actualizado correctamente";
			}

			$response['success'] = "true";
			$response['message'] = "Perfil actualizado correctamente";
		}
		catch (\Exception $exception) {
			$response['success'] = "false";
			$response['message'] = "No se ha podido guardar el perfil";
			return die($exception->getMessage());
		}

		die(json_encode($response));
	}

	public function resetPassword() {
		$app = Factory::getApplication();
		$db = Factory::getDbo();
		$user = Factory::getUser();
		$email = $this->input->get('email',null,'RAW');

		// Create a new query object.
		$query = $db
		->getQuery(true)
		->select('id')
		->from($db->quoteName('#__users'))
		->where($db->quoteName('email') . " = '" . $email . "'");
		
		$db->setQuery($query);
		$exists = $db->loadResult();
		
		if($exists) {
			$user = User::getInstance($exists);
			// Set the confirmation token.
			$token = ApplicationHelper::getHash(UserHelper::genRandomPassword());
			$hashedToken = UserHelper::hashPassword($token);

			$user->activation = $hashedToken;
			$user->save();

			// Assemble the password reset confirmation link.
			$mode = $app->get('force_ssl', 0) == 2 ? 1 : (-1);
			$link = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/index.php?option=com_users&view=reset&layout=confirm&token=' . $token;
			
			$app = Factory::getApplication();
			$query = $db->getQuery(true);
			$query->select('*')
				->from($db->quoteName('#__mail_templates'))
				->where($db->quoteName('template_id') . " = 'com_users.password_reset'")
				//->whereIn($db->quoteName('language'), ['', $app->getLanguage()->getTag()], ParameterType::STRING)
				->order($db->quoteName('language') . ' DESC');
			$db->setQuery($query);

			$object = $db->loadObject();
			$body = $object->htmlbody;
			$subject = $object->subject;
			
			$to = $this->input->get('email',null,'RAW');
			
			$config = Factory::getConfig();
			$mailfrom = $config->get('mailfrom');
			$fromname =  $config->get('fromname');
			$from = array($mailfrom, $fromname);
	
			//$from = array("pre-vente-noreply@studiogenesis.es", "Pre-Vente");
	
			# Invoke JMail Class
			$mailer = Factory::getMailer();
			
			# Set sender array so that my name will show up neatly in your inbox
			$mailer->setSender($from);
	
			# Add a recipient -- this can be a single address (string) or an array of addresses
			$mailer->addRecipient($to);

			$config = Factory::getConfig();
			$subject = str_replace("{SITENAME}", $config->get('sitename'), $subject);
			$mailer->setSubject($subject);
			
			if($body != '') {
				$body = str_replace("{SITENAME}", $config->get('sitename'), $body);
				$body = str_replace("{LINK_TEXT}",Route::_($link, false, $mode),$body);
				$body = str_replace("{TOKEN}",$hashedToken,$body);
				$mailer->setBody($body);
			}

			# If you would like to send as HTML, include this line; otherwise, leave it out
			$mailer->isHTML();
	
			# Send once you have set all of your options
			if($mailer->send()){
				$response['success'] = "true";
				$response['message'] = "Email enviado correctamente";
			} else {
				$response['success'] = "false";
				$response['message'] = "No se ha podido enviar el mail";
			}
		} else {
			$response['success'] = "false";
			$response['message'] = "No se ha encontrado el usuario";
		}

		die(json_encode($response));
	}
}
