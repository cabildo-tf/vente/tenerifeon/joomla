<?php

/**
 * @package     Joomla.API
 * @subpackage  com_articles
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Factory;
use Facebook\Facebook;
use Google\Client as Google_Client;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\String\PunycodeHelper;
use Joomla\CMS\User\User;
use Joomla\Component\Users\Site\Model\RegistrationModel;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Signature\Algorithm\ES256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Firebase\JWT\JWT;
use Joomla\CMS\Language\Text;

/**
 * The articles controller
 *
 * @since  4.0.0
 */
class GeopointsController  extends ApiController
{
    /**
     * The content type of the item.
     *
     * @var    string
     * @since  4.0.0
     */
    protected $contentType = 'articles';

    /**
     * The default view for the display method.
     *
     * @var    string
     * @since  4.0.0
     */
    protected $default_view = 'articles';


    /**
     * Google ® Login API
     *
     * @return void
     */
    public function geoPoints()
    {

        $db = Factory::getDbo();

        //Create searchable points table
        $query =    "CREATE TABLE IF NOT EXISTS public.searchablepoints (
                        gid serial, 
                        control_id varchar(50) unique,
                        article_id int not null,
                        point_name varchar(255),
                        geo_point geography(POINT,4326),
                        PRIMARY KEY (gid),
                        CONSTRAINT fk_article_id
                            FOREIGN KEY (article_id )
                            REFERENCES public.sooeg_content(id)
                            ON DELETE CASCADE
                    )";
        $db->setQuery($query);	
        $db->execute();
        //Create searchable ponits itineraries related
        $query =    "DROP TABLE IF EXISTS public.relateditinerariesandpoints;";
        $db->setQuery($query);	
        $db->execute();

        $query =    "CREATE TABLE IF NOT EXISTS public.relateditinerariesandpoints (
                    gid serial, 
                    point_gid int not null,
                    itinerary_id int not null,
                    PRIMARY KEY (gid),
                    CONSTRAINT fk_point_gid
                        FOREIGN KEY (point_gid )
                        REFERENCES public.searchablepoints(gid)
                        ON DELETE CASCADE,
                    CONSTRAINT fk_itinerary_id
                        FOREIGN KEY (itinerary_id )
                        REFERENCES public.itineraries(id)
                        ON DELETE CASCADE
                        )";
        $db->setQuery($query);	
        $db->execute();

        //EQUIPAMIENTOS
        $db = Factory::getDbo();
        $query = $db
        ->getQuery(true)
        ->select(array('a.id', 'a.title'))
        ->from($db->quoteName('#__content', 'a'))
        ->join('LEFT', $db->quoteName('#__categories', 'ca'), $db->quoteName('a.catid') . ' = ' . $db->quoteName('ca.id'))
        ->where($db->quoteName('ca.parent_id') . " = " . $db->quote(39));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);	

        $equipamientos = $db->loadObjectList();
        $equipamientosPostgis = [];
        foreach($equipamientos as $index => $equip){
            //Get latitude 
            $field_equip_latitude = 177;
            $query = $db
			->getQuery(true)
			->select('value')
			->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($equip->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_equip_latitude)); //
			$db->setQuery($query);
			$equipamientosPostgis['latitude'] = $db->loadResult();

            //Get longitude
            $field_equip_longitude = 178;
            $query = $db
			->getQuery(true)
			->select('value')
			->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($equip->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_equip_longitude)); //
			$db->setQuery($query);
			$equipamientosPostgis['longitude'] = $db->loadResult();

            //Fill name and id and inster into searchablepoints table
            $equipamientosPostgis['name'] = $equip->title;
            $equipamientosPostgis['id'] = $equip->id;
            $query =    "INSERT INTO searchablepoints(control_id, article_id, point_name, geo_point)
                        VALUES('".$equipamientosPostgis['id']."',".$equipamientosPostgis['id'].", '".$equipamientosPostgis['name']."', ST_GeomFromText('POINT(".$equipamientosPostgis['longitude']." ".$equipamientosPostgis['latitude'].")', 4326))
                        ON CONFLICT (control_id) DO UPDATE SET point_name = '".$equipamientosPostgis['name']."',
                                                                geo_point = ST_GeomFromText('POINT(".$equipamientosPostgis['longitude']." ".$equipamientosPostgis['latitude'].")', 4326)";
            $db->setQuery($query);	
            $db->execute();

            //Get itineraries related by conexiones-con-otras-rutas field
            $field_equip_related_itineraries = 68;
            $query = $db
			->getQuery(true)
			->select('value')
			->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($equip->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_equip_related_itineraries)); //
			$db->setQuery($query);
			$idRutas = $db->loadResult();
            //Check if has related itineraries
            if(!is_null($idRutas)){
                $idRutas = explode(",", $idRutas);
                if(count($idRutas) > 0){

                    foreach($idRutas as $ruta){
                        //Check if exists in itineraries table
                        //Get gid field from searchablepoints table
                        $query = $db
                        ->getQuery(true)
                        ->select('gid')
                        ->from($db->quoteName('public.searchablepoints'))
                        ->where($db->quoteName('article_id') . " = " . $db->quote($equipamientosPostgis['id']))
                        ->where($db->quoteName('point_name') . " = " . $db->quote($equipamientosPostgis['name'])); //
                        $db->setQuery($query);
                        $point_gid = $db->loadResult();

                        $query = $db
                        ->getQuery(true)
                        ->select('id')
                        ->from($db->quoteName('public.itineraries'))
                        ->where($db->quoteName('j_id') . " = " . $db->quote($ruta)); //
                        $db->setQuery($query);
                        $itinerary_id = $db->loadResult();
                        if(!is_null($itinerary_id)){
                            $query =    "INSERT INTO relateditinerariesandpoints(point_gid, itinerary_id)
                                VALUES(".$point_gid.",".$itinerary_id.")";
                            $db->setQuery($query);	
                            $db->execute();
                        }

                    }
                }
            }

        }

        //PUNTOS DE INTERES
        $db = Factory::getDbo();
        $query = $db
        ->getQuery(true)
        ->select(array('a.id', 'a.title'))
        ->from($db->quoteName('#__content', 'a'))
        ->join('LEFT', $db->quoteName('#__categories', 'ca'), $db->quoteName('a.catid') . ' = ' . $db->quoteName('ca.id'))
        //->where($db->quoteName('a.state') . " = " . $db->quote(1))
        ->where($db->quoteName('ca.parent_id') . " = " . $db->quote(66));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);	

        $puntosInteres = $db->loadObjectList();
        $puntosInteresPostgis = [];
        foreach($puntosInteres as $index => $punto){
            $field_punto_latitude = 202;
            $query = $db
			->getQuery(true)
			->select('value')
			->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($punto->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_punto_latitude)); //
			$db->setQuery($query);
	
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$puntosInteresPostgis['latitude'] = $db->loadResult();
            $field_punto_longitude = 203;
            $query = $db
			->getQuery(true)
			->select('value')
			->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($punto->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_punto_longitude)); //
			$db->setQuery($query);
	
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$puntosInteresPostgis['longitude'] = $db->loadResult();
            $puntosInteresPostgis['name'] = $punto->title;
            $puntosInteresPostgis['id'] = $punto->id;

            $query =    "INSERT INTO searchablepoints(control_id, article_id, point_name, geo_point)
                        VALUES('".$puntosInteresPostgis['id']."',".$puntosInteresPostgis['id'].", '".$puntosInteresPostgis['name']."', ST_GeomFromText('POINT(".$puntosInteresPostgis['longitude']." ".$puntosInteresPostgis['latitude'].")', 4326))
                        ON CONFLICT (control_id) DO  UPDATE SET point_name = '".$puntosInteresPostgis['name']."',
                                                                geo_point = ST_GeomFromText('POINT(".$puntosInteresPostgis['longitude']." ".$puntosInteresPostgis['latitude'].")', 4326)";
            $db->setQuery($query);	
            $db->execute();
        }


        //SERVICIOS
        $db = Factory::getDbo();
        $query = $db
        ->getQuery(true)
        ->select(array('a.id', 'a.title'))
        ->from($db->quoteName('#__content', 'a'))
        ->join('LEFT', $db->quoteName('#__categories', 'ca'), $db->quoteName('a.catid') . ' = ' . $db->quoteName('ca.id'))
        //->where($db->quoteName('a.state') . " = " . $db->quote(1))
        ->where($db->quoteName('ca.parent_id') . " = " . $db->quote(99));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);	

        $servicios = $db->loadObjectList();
        $serviciosPostgis = [];
        foreach($servicios as $index => $servicio){
            $field_servicios_latitude = 177;
            $query = $db
            ->getQuery(true)
            ->select('value')
            ->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($servicio->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_servicios_latitude)); //
            $db->setQuery($query);
    
            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
            $serviciosPostgis['latitude'] = $db->loadResult();
            $field_servicio_longitude = 178;
            $query = $db
            ->getQuery(true)
            ->select('value')
            ->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($servicio->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_servicio_longitude)); //
            $db->setQuery($query);
    
            // Load the results as a list of stdClass objects (see later for more options on retrieving data).
            $serviciosPostgis['longitude'] = $db->loadResult();
            $serviciosPostgis['name'] = $servicio->title;
            $serviciosPostgis['id'] = $servicio->id;
            $query =    "INSERT INTO searchablepoints(control_id, article_id, point_name, geo_point)
                        VALUES('".$serviciosPostgis['id']."',".$serviciosPostgis['id'].", '".$serviciosPostgis['name']."', ST_GeomFromText('POINT(".$serviciosPostgis['longitude']." ".$serviciosPostgis['latitude'].")', 4326))
                        ON CONFLICT (control_id) DO  UPDATE SET point_name = '".$serviciosPostgis['name']."',
                                                                geo_point = ST_GeomFromText('POINT(".$serviciosPostgis['longitude']." ".$serviciosPostgis['latitude'].")', 4326)";
            $db->setQuery($query);	
            $db->execute();

            //Get itineraries related by conexiones-con-otras-rutas field
            $field_equip_related_itineraries = 68;
            $query = $db
            ->getQuery(true)
            ->select('value')
            ->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($servicio->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_equip_related_itineraries)); //
            $db->setQuery($query);
            $idRutas = $db->loadResult();
            //Check if has related itineraries
            if(!is_null($idRutas)){
                ($idRutas = explode(",", $idRutas));
                if(count($idRutas) > 0){

                    foreach($idRutas as $ruta){
                        //Check if exists in itineraries table
                        //Get gid field from searchablepoints table
                        $query = $db
                        ->getQuery(true)
                        ->select('gid')
                        ->from($db->quoteName('public.searchablepoints'))
                        ->where($db->quoteName('article_id') . " = " . $db->quote($serviciosPostgis['id']))
                        ->where($db->quoteName('point_name') . " = " . $db->quote($serviciosPostgis['name'])); //
                        $db->setQuery($query);
                        $point_gid = $db->loadResult();

                        $query = $db
                        ->getQuery(true)
                        ->select('id')
                        ->from($db->quoteName('public.itineraries'))
                        ->where($db->quoteName('j_id') . " = " . $db->quote($ruta)); //
                        $db->setQuery($query);
                        $itinerary_id = $db->loadResult();
                        if(!is_null($itinerary_id)){
                            $query =    "INSERT INTO relateditinerariesandpoints(point_gid, itinerary_id)
                                VALUES(".$point_gid.",".$itinerary_id.")";
                            $db->setQuery($query);	
                            $db->execute();
                        }

                    }
                }
            }
        }



        //ITINERARIOS
        $db = Factory::getDbo();
        $query = $db
        ->getQuery(true)
        ->select(array('a.id', 'a.title'))
        ->from($db->quoteName('#__content', 'a'))
        ->join('LEFT', $db->quoteName('#__categories', 'ca'), $db->quoteName('a.catid') . ' = ' . $db->quoteName('ca.id'))
        //->where($db->quoteName('a.state') . " = " . $db->quote(1))
        ->where($db->quoteName('ca.parent_id') . " = " . $db->quote(38));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);	

        $itinerarios = $db->loadObjectList();

        $itinerarioPostgisInicio = [];
        $itinerarioPostgisFin = [];

        foreach($itinerarios as $index => $ruta){
            $field_linestring = 145;
            //Get linestrig
            $query = $db
            ->getQuery(true)
            ->select('value')
            ->from($db->quoteName('#__fields_values'))
            ->where($db->quoteName('item_id') . " = " . $db->quote($ruta->id)) //
            ->where($db->quoteName('field_id') . " = " . $db->quote($field_linestring)); //
            $db->setQuery($query);
            $linestring = $db->loadResult();
            $linestring = json_decode($linestring);
            //Get first lat lon  as start
            if( isset($linestring->features[0]->geometry)){
                dump($index,$ruta->id);
                if($linestring->features[0]->geometry->type == "MultiLineString"){
                    $startPoint = $linestring->features[0]->geometry->coordinates[0][0];
                }
                else if($linestring->features[0]->geometry->type == "LineString"){
                    $startPoint = $linestring->features[0]->geometry->coordinates[0];
                }
                $itinerarioPostgisInicio['latitude'] = $startPoint[1];
                $itinerarioPostgisInicio['longitude'] = $startPoint[0];
                $itinerarioPostgisInicio['name'] = $ruta->title.', inicio';
                $itinerarioPostgisInicio['id'] = $ruta->id;
    
                if($itinerarioPostgisInicio['latitude'] != "" && (!is_null($itinerarioPostgisInicio['latitude']))){
                    //dd($linestring->features[0]->geometry->type);
    
                    $query =    "INSERT INTO searchablepoints(control_id, article_id, point_name, geo_point)
                                VALUES('".$itinerarioPostgisInicio['id']."', ".$itinerarioPostgisInicio['id'].", '".$itinerarioPostgisInicio['name']."', ST_GeomFromText('POINT(".$itinerarioPostgisInicio['longitude']." ".$itinerarioPostgisInicio['latitude'].")', 4326))
                                ON CONFLICT (control_id) DO  UPDATE SET point_name = '".$itinerarioPostgisInicio['name']."',
                                                                        geo_point = ST_GeomFromText('POINT(".$itinerarioPostgisInicio['longitude']." ".$itinerarioPostgisInicio['latitude'].")', 4326)";
                    $db->setQuery($query);	
                    $db->execute();
                    //Insert to relatedpoints if itinerary is in itineraries table
                    $query = $db
                    ->getQuery(true)
                    ->select('gid')
                    ->from($db->quoteName('public.searchablepoints'))
                    ->where($db->quoteName('article_id') . " = " . $db->quote($itinerarioPostgisInicio['id']))
                    ->where($db->quoteName('point_name') . " = " . $db->quote($itinerarioPostgisInicio['name'])); //
                    $db->setQuery($query);
                    $point_gid = $db->loadResult();

                    //Check if exists in itineraries table
                    $query = $db
                    ->getQuery(true)
                    ->select('id')
                    ->from($db->quoteName('public.itineraries'))
                    ->where($db->quoteName('j_id') . " = " . $db->quote($ruta->id)); //
                    $db->setQuery($query);
                    ($itinerary_id = $db->loadResult());
                    if(!is_null($itinerary_id)){
                        $query =    "INSERT INTO relateditinerariesandpoints(point_gid, itinerary_id)
                            VALUES(".$point_gid.",".$itinerary_id.")";
                        $db->setQuery($query);	
                        $db->execute();
                    }
                }
    
                
               //Get last lat lon as end
                if($linestring->features[0]->geometry->type == "MultiLineString"){

                    $endPoint = end(end($linestring->features[0]->geometry->coordinates));                
                }
                else if($linestring->features[0]->geometry->type == "LineString"){
                    $endPoint = end($linestring->features[0]->geometry->coordinates);                
                }
              
                $itinerarioPostgisFin['latitude'] = $endPoint[1];
                $itinerarioPostgisFin['longitude'] = $endPoint[0];
                $itinerarioPostgisFin['name'] = $ruta->title.', final';
                $itinerarioPostgisFin['id'] = $ruta->id;

                if($itinerarioPostgisFin['latitude'] != "" && (!is_null($itinerarioPostgisFin['latitude']))){
                    $query =    "INSERT INTO searchablepoints(control_id, article_id, point_name, geo_point)
                                VALUES('".$itinerarioPostgisFin['id']."_1', ".$itinerarioPostgisFin['id'].", '".$itinerarioPostgisFin['name']."', ST_GeomFromText('POINT(".$itinerarioPostgisFin['longitude']." ".$itinerarioPostgisFin['latitude'].")', 4326))
                                ON CONFLICT (control_id) DO  UPDATE SET point_name = '".$itinerarioPostgisFin['name']."',
                                                                        geo_point = ST_GeomFromText('POINT(".$itinerarioPostgisFin['longitude']." ".$itinerarioPostgisFin['latitude'].")', 4326)";
                    $db->setQuery($query);	
                    $db->execute();

                    //Insert to relatedpoints if itinerary is in itineraries table
                    $query = $db
                    ->getQuery(true)
                    ->select('gid')
                    ->from($db->quoteName('public.searchablepoints'))
                    ->where($db->quoteName('article_id') . " = " . $db->quote($itinerarioPostgisFin['id']))
                    ->where($db->quoteName('point_name') . " = " . $db->quote($itinerarioPostgisFin['name'])); //
                    $db->setQuery($query);
                    $point_gid = $db->loadResult();

                    //Check if exists in itineraries table
                    $query = $db
                    ->getQuery(true)
                    ->select('id')
                    ->from($db->quoteName('public.itineraries'))
                    ->where($db->quoteName('j_id') . " = " . $db->quote($ruta->id)); //
                    $db->setQuery($query);
                    ($itinerary_id = $db->loadResult());
                    if(!is_null($itinerary_id)){
                        $query =    "INSERT INTO relateditinerariesandpoints(point_gid, itinerary_id)
                            VALUES(".$point_gid.",".$itinerary_id.")";
                        $db->setQuery($query);	
                        $db->execute();
                    }
                }

                //Check puntos de interes for each itinerary and insert to table
                $field_puntos_interes_itinerary = 69;
                $query = $db
                ->getQuery(true)
                ->select('value')
                ->from($db->quoteName('#__fields_values'))
                ->where($db->quoteName('item_id') . " = " . $db->quote($ruta->id)) //
                ->where($db->quoteName('field_id') . " = " . $db->quote($field_puntos_interes_itinerary)); //
                $db->setQuery($query);
                ($idPuntosInteres = $db->loadResult());
                //Check if has related itineraries
                if(!is_null($idPuntosInteres)){
                    ($idPuntosInteres = explode(",", $idPuntosInteres));
                    if(count($idPuntosInteres) > 0){

                        foreach($idPuntosInteres as $punto){
                            //Get gid field from searchablepoints table
                            $query = $db
                            ->getQuery(true)
                            ->select('gid')
                            ->from($db->quoteName('public.searchablepoints'))
                            ->where($db->quoteName('article_id') . " = " . $db->quote($punto)); //
                            $db->setQuery($query);
                            ($point_gid = $db->loadResult());

                            //Check if exists in itineraries table
                            $query = $db
                            ->getQuery(true)
                            ->select('id')
                            ->from($db->quoteName('public.itineraries'))
                            ->where($db->quoteName('j_id') . " = " . $db->quote($ruta->id)); //
                            $db->setQuery($query);
                            ($itinerary_id = $db->loadResult());
                            if(!is_null($itinerary_id)){
                                $query =    "INSERT INTO relateditinerariesandpoints(point_gid, itinerary_id)
                                    VALUES(".$point_gid.",".$itinerary_id.")";
                                $db->setQuery($query);	
                                $db->execute();
                            }
                        }
                    }
                }
            }

        }
    }

}