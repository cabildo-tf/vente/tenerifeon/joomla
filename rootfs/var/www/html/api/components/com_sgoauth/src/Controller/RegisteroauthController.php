<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\User\UserHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\User\User;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\String\PunycodeHelper;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Mail\MailTemplate;
use Joomla\CMS\Log\Log;
use Joomla\Component\Users\Site\Model\RegistrationModel;
use Joomla\CMS\Language\Language;
use Joomla\Database\ParameterType;

/**
 * The users controller
 *
 * @since  4.0.0
 */
class RegisteroauthController extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'users';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'users';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function registerOauth()	{
		//Get input data
		$temp['name'] = $this->input->get('name',null,'STRING');
		$temp['username'] = $this->input->get('username');
		$temp['email1'] = $this->input->get('email',null,'STRING');
		$temp['password1'] = $this->input->get('password',null,'RAW');
		$temp['password2'] = $this->input->get('password',null,'RAW');
		$temp['privacyconsent'] = array(
			"privacy" => "1",
		);
		
		$params = ComponentHelper::getParams('com_users');

		// Initialise the table with Joomla\CMS\User\User.
		$user = new User;
		
		$data['groups'] = array(
			0 => "2"
		);
		$data['language'] = "es-ES";

		// Merge in the registration data.
		foreach ($temp as $k => $v)
		{
			$data[$k] = $v;
		}
		
		// Prepare the data for the user object.
		$data['email'] = PunycodeHelper::emailToPunycode($data['email1']);
		$data['password'] = $data['password1'];
		$useractivation = $params->get('useractivation');
		$sendpassword = $params->get('sendpassword', 1);

		// Check if the user needs to activate their account.
		if (($useractivation == 1) || ($useractivation == 2))
		{
			$data['activation'] = ApplicationHelper::getHash(UserHelper::genRandomPassword());
			$data['block'] = 1;
		}
		$registrationModel = new RegistrationModel();
		// Bind the data.
		if (!$user->bind($data))
		{
			$registrationModel->setError($user->getError());

			return false;
		}

		// Load the users plugin group.
		PluginHelper::importPlugin('user');
		//Set response variable to return json encoded
		$response = [
			'success' => 'true',
			'message' => ''
		];
		// Store the data.
		if (!$user->save())
		{
			$response['success'] = "false";
			$response['message'] = $user->getError();
			die(json_encode($response));

			return false;
		}

		$app = Factory::getApplication();
		$db = $registrationModel->getDbo();
		$query = $db->getQuery(true);

		// Compile the notification mail values.
		$data = $user->getProperties();
		$data['fromname'] = $app->get('fromname');
		$data['mailfrom'] = $app->get('mailfrom');
		$data['sitename'] = $app->get('sitename');
		$data['siteurl'] = Uri::root();
		// Handle account activation/confirmation emails.
		if ($useractivation == 2)
		{
			// Set the link to confirm the user email.
			$linkMode = $app->get('force_ssl', 0) == 2 ? Route::TLS_FORCE : Route::TLS_IGNORE;

			$data['activate'] = Route::link(
				'site',
				'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
				false,
				$linkMode,
				true
			);

			$mailtemplate = 'com_users.registration.user.admin_activation';
		}
		elseif ($useractivation == 1)
		{
			// Set the link to activate the user account.
			$linkMode = $app->get('force_ssl', 0) == 2 ? Route::TLS_FORCE : Route::TLS_IGNORE;

			$data['activate'] = Route::link(
				'site',
				'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
				false,
				$linkMode,
				true
			);
			$mailtemplate = 'com_users.registration.user.self_activation';

		}
		else
		{
			$mailtemplate = 'com_users.registration.user.registration_mail';
		}

		if ($sendpassword)
		{
			$mailtemplate .= '_w_pw';
		}

		// Try to send the registration email.
		try
		{	
			Factory::$language = Language::getInstance('es-ES');
			$app = Factory::getApplication();
			$query = $db->getQuery(true);
			$query->select('*')
				->from($db->quoteName('#__mail_templates'))
				->where($db->quoteName('template_id') . " = '$mailtemplate'")
				//->whereIn($db->quoteName('language'), ['', $app->getLanguage()->getTag()], ParameterType::STRING)
				->order($db->quoteName('language') . ' DESC');
			$db->setQuery($query);

			$object = $db->loadObject();
			$body = $object->htmlbody;
			$subject = $object->subject;

			$config = Factory::getConfig();
			$mailfrom = $config->get('mailfrom');
			$fromname =  $config->get('fromname');
			$from = array($mailfrom, $fromname);
	
			//$from = array("pre-vente-noreply@studiogenesis.es", "Pre-Vente");
	
			# Invoke JMail Class
			$mailer = Factory::getMailer();
			
			# Set sender array so that my name will show up neatly in your inbox
			$mailer->setSender($from);
	
			# Add a recipient -- this can be a single address (string) or an array of addresses
			$mailer->addRecipient($temp['email1']);

			$config = Factory::getConfig();
			$subject = str_replace("{SITENAME}", $config->get('sitename'), $subject);
			$subject = str_replace("{NAME}", $data['name'], $subject);
			$mailer->setSubject($subject);
			if($body != '') {
				$body = str_replace("{NAME}", $temp['name'], $body);
				$body = str_replace("{SITENAME}", $config->get('sitename'), $body);
				$body = str_replace("{ACTIVATE}", $data['activate'], $body);
				$body = str_replace("{USERNAME}", $temp['username'],$body);
				$body = str_replace("{SITEURL}", $data['siteurl'],$body);
				$mailer->setBody($body);
			}
			# If you would like to send as HTML, include this line; otherwise, leave it out
			$mailer->isHTML();

			$mailSended = $mailer->send();

		}
		catch (\Exception $exception)
		{
			$response['success'] = "false";
			$response['message'] = "No se ha podido enviar el email de confirmacion";
			die(json_encode($response));
		}

		if($mailSended){
			$response['success'] = "true";
			$response['message'] = "Usuario registrado correctamente";
			die(json_encode($response));
		}
	}

}
