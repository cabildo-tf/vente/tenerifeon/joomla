<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Response\JsonResponse;
use Joomla\CMS\User\UserHelper;
use Joomla\Plugin\User\Token\Field\JoomlatokenField;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SguserincidencesController extends ApiController
{
	/**
	 * The content tipo of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contenttipo = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SgUserincidences()
	{
		$user = Factory::getUser();
		$typeIncidence = $this->input->get('typeIncidence',null,'STRING');
		$id = $this->input->get('id',null, null);
		$incidenceDescription = $this->input->get('incidenceDescription',null, 'STRING');
		$images = $this->input->files->get('images',[], 'array');
		// Returns a list of all equipamientos

		//dd($user->username, $typeIncidence, $id, $incidenceDescription, $images);

		//Create article
		$cat_id = 93; //Incidencias colaborador
		//superadmin user id
		$user_id = 848;

		$images_text = '{';

		foreach($images as $index => $image){
			$path = '../images/incidencias-usuario/' . $image['name'];
			if($index != 0) {
				$images_text = $images_text . ',';
			} 
			$images_text = $images_text . '"row'.$index.'":{"field106":{"imagefile":"' . $path . '","alt_text":""},"field108":""}';
			$file = file_get_contents($image['tmp_name']);
			file_put_contents($path, $file);
		}

		$images_text = $images_text . '}';

		$article = [
			"catid" => $cat_id,
			"title"		=> $typeIncidence.' en '.$id,
			"id"	=> 0,
			'created_by' => $user->id,
			'created_by_alias'=> '',
			'alias' => ApplicationHelper::stringURLSafe($typeIncidence.' en '.$id),
			'language' => 'es-ES',
			'metakey' => '',
			'metadesc' => '',
			'state' => 0 ,
			'access' => 1,
			'introtext' => "",
			"com_fields" => array(
					"tipo-de-incidencia-colaborador" => $typeIncidence,
					"matricula-itinerario-incidencia" => $id,
					"descripcion-incidencia-colaborador" => $incidenceDescription,
					"usuario-colaborador" => $user->id,
					"imagenes" => $images_text
				)
			];

		//dd($article);
		$app = Factory::getApplication();			

		$articleModel = $app->bootComponent('com_content')
		->getMVCFactory()->createModel('Article', 'Administrator');
		Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');

		// Needs to be set because com_fields needs the data in jform to determine the assigned catid
		$this->input->set('jform', $article);
		$form = $articleModel->getForm($article, false);

		$validData = $articleModel->validate($form, $article);
		
		$articleModel->save($validData);

		if(sizeof($articleModel->getErrors()) > 0) {
			$response['success'] = "false";
			$response['message'] = "Error al crear la incidencia: " . $articleModel->getErrors()[0];
		} else {
			$response['success'] = "true";
			$response['message'] = "Incidencia creada correctamente";
		}
		//var_dump($articleModel->getErrors());
		//dd($articleModel->getItem()->id);
		die (json_encode($response));
		
	}

	public function SgGetUserIncidences() {
		try {
			$user = Factory::getUser();
			$db = Factory::getDbo();
	
			// Create a new query object.
			$query = $db
			->getQuery(true)
			->select('*')
			->from($db->quoteName('#__content'))
			->where($db->quoteName('catid') . " = 93")
			->where($db->quoteName('created_by') . " = " . $db->quote($user->id));
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
	
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$data = $db->loadResult();
	
			$response['success'] = "true";
			$response['data'] = $data;
			$response['message'] = "Incidencias del usuario devueltas correctamente";
		} catch(Exception $e) {
			$response['success'] = "false";
			$response['data'] = null;
			$response['message'] = "Error al recuperar las incidencias del usuario " . $e->getMessage();
		}

		die(json_encode($response));
	}
}
