<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Factory;
use Joomla\Component\Content\Api\Helper\ContentHelper;
use Joomla\Component\Content\Site\Model\ArticleModel;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Registry\Registry;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SgincidencesController extends ApiController
{
	/**
	 * The content tipo of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contenttipo = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SgIncidences()
	{
		//Get language from request
		$filters = $this->app->input->get('filter');
		$language = $filters['language'];

		$equipmentsIncidences = $this->getEquipmentIncidences($language);

		$itinerariesIncidences = $this->getItinerariesIncidences($language);

		$response = [
			'data' => [
				['equipments' => $equipmentsIncidences],
				['itineraries' => $itinerariesIncidences]
			]
		];
	
		die (json_encode($response));		
	}

	protected function getItinerariesIncidences($language){

		//Puntos de interes parent category id
		$languagesCategory = [
			'es-ES' => '92',
			'de-DE' => '193',
			'en-GB' =>'194'
		];

		$query = "select c.id, c.title, c.introtext, c.images, cast (v.value AS json) as geometry
		from sooeg_content c		
		left join sooeg_fields_values v on
			c.id::varchar = v.item_id
		where
			c.state = '1'
			and c.catid = '".$languagesCategory[$language]."'
			and v.field_id = '251'";

	


		$db = Factory::getDbo();
		$db->setQuery($query);
		$data = $db->loadObjectList();
		foreach($data as &$val){
			$val->geometry = (json_decode($val->geometry));
		}

		return $data;
	}
	
	protected function getEquipmentIncidences($language){

				
		//Puntos de interes parent category id
		$languagesCategory = [
			'es-ES' => '91',
			'de-DE' => '191',
			'en-GB' =>'192'
		];

		$query = "select id, title, introtext, images, replace(sum(latitude)::varchar,',','.') as latitude, replace(sum(longitude)::varchar,',','.') as longitude
		from (
		/* latitude */
		select
		 article.id as id, article.title as title, introtext as introtext,images as images, cast(fields.value as float)  as latitude, 0 as longitude
		from
			sooeg_content as article
			
		left join sooeg_fields_values as fields on
			article.id::varchar = fields.item_id
		where
			state = 1
			and article.catid = '".$languagesCategory[$language]."'
			and fields.field_id = 249
		
		union
		/* longitude */	
		select
		article.id as id, article.title as title, introtext as introtext,images as images, 0 as latitude, cast(fields.value as float) as longitude
		from
			sooeg_content as article
					
		left join sooeg_fields_values as fields on
			article.id::varchar = fields.item_id
		where
			state = 1
			and article.catid = '".$languagesCategory[$language]."'
			and fields.field_id = 250
		) q
		group by id, title, introtext,images	
			";


		$db = Factory::getDbo();
		$db->setQuery($query);
		$data = $db->loadObjectList();

		return $data;
	
	}
}
