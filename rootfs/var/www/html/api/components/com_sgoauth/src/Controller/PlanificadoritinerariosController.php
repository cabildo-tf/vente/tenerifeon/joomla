<?php

/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Factory;
use Joomla\CMS\Filter\InputFilter;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Model\ArticleModel;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use stdClass;
use Tobscure\JsonApi\Exception\InvalidParameterException;

/**
 * The users controller
 *
 * @since  4.0.0
 */
class PlanificadoritinerariosController extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'users';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'users';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function planificadorItinerarios()
	{

		$points = $this->app->input->get('points');
		$type = $this->app->input->get('type', null, 'int');
		$response = [];

		if (sizeof($points) > 10) {
			return "too many points";
		}
		$listOfRoutes = [];

		// For each point of the desired route
		for ($i = 0; $i < (sizeof($points) - 1); $i++) {
			// If complete linestring is empty
			if (empty((array) $listOfRoutes)) {
				// Set the start as the very beggining
				$start = $points[$i];
			} else {
				// If is not then use the last point of the last route as the start

				// Check if the last element is a multilinestring with ->type or not, and get the last coordinate of the last linestring
				if ($listOfRoutes[sizeof($listOfRoutes) - 1]->type == "MultiLineString") {
					// Get from coordinates the last linestring, from them get the last coordinates
					$start = $listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates[sizeof($listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates) - 1][sizeof($listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates[sizeof($listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates) - 1]) - 1];
					$start = [
						'latitude' => $start[1],
						'longitude' => $start[0]
					];
				} else {
					// Is linestring
					$start = $listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates[sizeof($listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates) - 1];
					$start = [
						'latitude' => $start[1],
						'longitude' => $start[0]
					];
				}
			}
			$end = $points[$i + 1];

			/// MAGIC QUERY!! GET NEAREST ROUTES WITHIN THE MARKED POINT AND USES ITS NEAREST NODES TO CALCULATE THE ROUTE
			$query = "with calculated_route as (
				select
						
				*
				from
					pgr_withPoints(
					'SELECT itineraries_routing_noded.id, itineraries_routing_noded.source, itineraries_routing_noded.target, itineraries_routing_noded.length as cost FROM itineraries_routing_noded join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id left join itineraries on itineraries_routing_noded_itineraries.itinerary_id = itineraries.id left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type',
					'with poi as (
				with start_point as (
				with nearest_point as (
					select case when fraction = 0.0 then 0.000001 when fraction = 1.0 then 0.999999 else fraction END from
					(
				select
					ST_LineLocatePoint(
				
				itinerary,
					cp_line_pt) as fraction
				from
					(
					select
						itineraries_routing_noded.the_geom as itinerary
					from
						itineraries_routing_noded
					join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
					join
						itineraries
					on
						itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
						left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
					order by
						ST_Distance(St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
						4326) ,
						itineraries_routing_noded.the_geom) asc
					limit 1) as nearest_itinerary
				,
					(
					select
						ST_ClosestPoint(nearest_itinerary.the_geom,
						St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
						4326)) as cp_line_pt
					from
						(
						select
							itineraries_routing_noded.the_geom,
							ST_Distance(St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
							4326) ,
							itineraries_routing_noded.the_geom) as dist
						from
							itineraries_routing_noded
						join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
							join
							itineraries
						on
							itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
							left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
						order by
							dist asc
						limit 1) as nearest_itinerary) as nearest_point) as fraction),
				
				nearest_itinerary as (
				select
					itineraries_routing_noded.id as edge_id
				from
					itineraries_routing_noded
					join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
					join
					itineraries
				on
					itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
					left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
				order by
					ST_Distance(St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
					4326) ,
					itineraries_routing_noded.the_geom) asc
				limit 1
				)
				 select
				*
				from
					nearest_point as fraction,
						(values(1)) as e(pid),
					nearest_itinerary as edge_id
					
					
				),end_point as (
				with nearest_point as (
					select case when fraction = 0.0 then 0.000001 when fraction = 1.0 then 0.999999 else fraction END from (
				select
					ST_LineLocatePoint(
				
				itinerary,
					cp_line_pt) as fraction
				from
					(
					select
						itineraries_routing_noded.the_geom as itinerary
					from
						itineraries_routing_noded
					join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
						join
						itineraries
					on
						itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
						left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
					order by
						ST_Distance(St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
						4326) ,
						itineraries_routing_noded.the_geom) asc
					limit 1) as nearest_itinerary
				,
					(
					select
						ST_ClosestPoint(nearest_itinerary.the_geom,
						St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
						4326)) as cp_line_pt
					from
						(
						select
							itineraries_routing_noded.the_geom,
							ST_Distance(St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
							4326) ,
							itineraries_routing_noded.the_geom) as dist
						from
							itineraries_routing_noded
						join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
							join
							itineraries
						on
							itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
							left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
						order by
							dist asc
						limit 1) as nearest_itinerary) as nearest_point) as fraction),
				
				nearest_itinerary as (
				select
					itineraries_routing_noded.id as edge_id
				from
					itineraries_routing_noded
				join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
					join
					itineraries
				on
					itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
					left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
				order by
					ST_Distance(St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
					4326) ,
					itineraries_routing_noded.the_geom) asc
				limit 1
				)
				 select
				*
				from
					nearest_point as fraction,
						(values(2)) as e(pid),
					nearest_itinerary as edge_id
				)  select * from (SELECT * FROM start_point
				UNION
				SELECT * FROM end_point) as v
				
				) SELECT pid, edge_id, fraction from poi',
					-1,
					-2,
					directed := false,
					details := true) as route
				
				
				
				),
				geom as (
				select
					case
						when node != -1
						and node != -2
						and node = itineraries_routing_noded.source then the_geom
						-- if the next marked node is not the same as target then must revert
						when node = -1
						and (lead(node) over (
						order by seq)) != target then st_reverse(the_geom)
						when node = -1
						and (lead(node) over (
						order by seq)) = target then the_geom
						-- if the next marked node is not the same as target then must revert
						when node = -2
						and (lag(node) over (
						order by seq)) != source then st_reverse(the_geom)
						when node = -2
						and (lag(node) over (
						order by seq)) = source then the_geom
						-- otherwise reverse
						else ST_Reverse(the_geom)
					end as geom
				from
					calculated_route
				left join itineraries_routing_noded on
							edge = itineraries_routing_noded.id)
				
						select
						st_asGeoJson(ST_LineSubstring(st_makeline(el.geom),
					ST_LineLocatePoint(st_makeline(el.geom),
					(
					select
						St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
						4326))),
					ST_LineLocatePoint(st_makeline(el.geom),
					(
					select
						St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
						4326)))))::jsonb as route
				from
					(
					select
						(st_dumppoints(geom)).geom
					from
						geom) as el";		
//dd($query);
			$db = Factory::getDbo();
			$db->setQuery($query);
			$data = $db->loadObject();

			// Push routing result as a new route
			if ($data->route == null) {
				$response['success'] = "false";
				$response['message'] = "No se ha encontrado una ruta";
				die(json_encode($response));
			}
			// $route = json_decode($data->route);
			// if ($route->type == 'MultiLineString') {
			// 	return "MULTI";
			// }
			$route = json_decode($data->route);
			// Mix all array elements from route cordinates itself as a single array
			$listOfRoutes = array_merge($listOfRoutes, [$route]);
		}
		/// MIX OF THE DIFFERENT ROUTES

		$linestringList = [];
		// Converts list of coordinates to linestring for postgis

		foreach ($listOfRoutes as $route) {
			/// Check if is going to be linestring or multilinestring
			if ($route->type == "MultiLineString") {
				// If is multilinestring, then insert it as linestring
				foreach ($route->coordinates as $singleLineString) {
					// Generate linestring on database from array of coordinates
					$lineStringText = "ST_GeomFromText('LINESTRING(";
					foreach ($singleLineString as $coordinate) {
						$lineStringText .= $coordinate[0] . " " . $coordinate[1] . ",";
					}
					$lineStringText = substr($lineStringText, 0, -1);
					$lineStringText .= ")', 4326)";
					$linestringList[] = $lineStringText;
				}
			} else {
				// Generate linestring on database from array of coordinates
				$lineStringText = "ST_GeomFromText('LINESTRING(";
				foreach ($route->coordinates as $coordinate) {
					$lineStringText .= $coordinate[0] . " " . $coordinate[1] . ",";
				}
				$lineStringText = substr($lineStringText, 0, -1);
				$lineStringText .= ")', 4326)";
				$linestringList[] = $lineStringText;
			}
		}

		// create a string like this:
		// $linestring = "ST_MakeLine( 'LINESTRING(0 0, 1 1)', 'LINESTRING(2 2, 3 3)' ");
		$query = "ST_MakeLine(array[";
		for ($i = 0; $i < sizeof($linestringList); $i++) {
			$query = $query . $linestringList[$i] . ",";
		}

		$query = substr($query, 0, -1) . "])";
		$query = "SELECT  jsonb_build_object(
			'type',       'Feature',
			'geometry',   ST_AsGeoJSON($query)::jsonb,
			'properties', json_build_object(
			'itineraries',	(SELECT STRING_AGG(j_id::varchar,',') from itineraries where ST_Intersects(st_linemerge($query), the_geom) and itineraries.type = $type),
			'distance', ST_LENGTH(ST_AsText(ST_Transform(st_linemerge($query),26986)))
			)
		
		) as route;";

		$db = Factory::getDbo();
		$db->setQuery($query);
		$data = $db->loadObject();

		//TODO: get elevation and fix issues with routes separated
		$response['success'] = "true";
		$response['message'] = "OK";
		$response['data'] = json_decode($data->route);
		die(json_encode($response));
	}
	public function planificadorAltimetria()
	{

		$points = $this->app->input->get('points');
		$type = $this->app->input->get('type', null, 'int');
		$response = [];

		if (sizeof($points) > 10) {
			return "too many points";
		}
		$listOfRoutes = [];

		// For each point of the desired route
		for ($i = 0; $i < (sizeof($points) - 1); $i++) {
			// If complete linestring is empty
			if (empty((array) $listOfRoutes)) {
				// Set the start as the very beggining
				$start = $points[$i];
			} else {
				// If is not then use the last point of the last route as the start

				// Check if the last element is a multilinestring with ->type or not, and get the last coordinate of the last linestring
				if ($listOfRoutes[sizeof($listOfRoutes) - 1]->type == "MultiLineString") {
					// Get from coordinates the last linestring, from them get the last coordinates
					$start = $listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates[sizeof($listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates) - 1][sizeof($listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates[sizeof($listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates) - 1]) - 1];
					$start = [
						'latitude' => $start[1],
						'longitude' => $start[0]
					];
				} else {
					// Is linestring
					$start = $listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates[sizeof($listOfRoutes[sizeof($listOfRoutes) - 1]->coordinates) - 1];
					$start = [
						'latitude' => $start[1],
						'longitude' => $start[0]
					];
				}
			}
			$end = $points[$i + 1];

			$query = "with calculated_route as (
				select
						
				*
				from
					pgr_withPoints(
					'SELECT itineraries_routing_noded.id, itineraries_routing_noded.source, itineraries_routing_noded.target, itineraries_routing_noded.length as cost FROM itineraries_routing_noded join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id left join itineraries on itineraries_routing_noded_itineraries.itinerary_id = itineraries.id left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type',
					'with poi as (
				with start_point as (
				with nearest_point as (
					select case when fraction = 0.0 then 0.000001 when fraction = 1.0 then 0.999999 else fraction END from
					(
				select
					ST_LineLocatePoint(
				
				itinerary,
					cp_line_pt) as fraction
				from
					(
					select
						itineraries_routing_noded.the_geom as itinerary
					from
						itineraries_routing_noded
					join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
					join
						itineraries
					on
						itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
						left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
					order by
						ST_Distance(St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
						4326) ,
						itineraries_routing_noded.the_geom) asc
					limit 1) as nearest_itinerary
				,
					(
					select
						ST_ClosestPoint(nearest_itinerary.the_geom,
						St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
						4326)) as cp_line_pt
					from
						(
						select
							itineraries_routing_noded.the_geom,
							ST_Distance(St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
							4326) ,
							itineraries_routing_noded.the_geom) as dist
						from
							itineraries_routing_noded
						join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
							join
							itineraries
						on
							itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
							left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
						order by
							dist asc
						limit 1) as nearest_itinerary) as nearest_point) as fraction),
				
				nearest_itinerary as (
				select
					itineraries_routing_noded.id as edge_id
				from
					itineraries_routing_noded
					join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
					join
					itineraries
				on
					itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
					left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
				order by
					ST_Distance(St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
					4326) ,
					itineraries_routing_noded.the_geom) asc
				limit 1
				)
				 select
				*
				from
					nearest_point as fraction,
						(values(1)) as e(pid),
					nearest_itinerary as edge_id
					
					
				),end_point as (
				with nearest_point as (
					select case when fraction = 0.0 then 0.000001 when fraction = 1.0 then 0.999999 else fraction END from (
				select
					ST_LineLocatePoint(
				
				itinerary,
					cp_line_pt) as fraction
				from
					(
					select
						itineraries_routing_noded.the_geom as itinerary
					from
						itineraries_routing_noded
					join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
						join
						itineraries
					on
						itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
						left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
					order by
						ST_Distance(St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
						4326) ,
						itineraries_routing_noded.the_geom) asc
					limit 1) as nearest_itinerary
				,
					(
					select
						ST_ClosestPoint(nearest_itinerary.the_geom,
						St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
						4326)) as cp_line_pt
					from
						(
						select
							itineraries_routing_noded.the_geom,
							ST_Distance(St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
							4326) ,
							itineraries_routing_noded.the_geom) as dist
						from
							itineraries_routing_noded
						join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
							join
							itineraries
						on
							itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
							left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
						order by
							dist asc
						limit 1) as nearest_itinerary) as nearest_point) as fraction),
				
				nearest_itinerary as (
				select
					itineraries_routing_noded.id as edge_id
				from
					itineraries_routing_noded
				join itineraries_routing_noded_itineraries on itineraries_routing_noded.id = itineraries_routing_noded_itineraries.itineraries_routing_noded_id
					join
					itineraries
				on
					itineraries_routing_noded_itineraries.itinerary_id = itineraries.id
					left join sooeg_content on j_id = sooeg_content.id where sooeg_content.state = 1 and itineraries.type = $type
				order by
					ST_Distance(St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
					4326) ,
					itineraries_routing_noded.the_geom) asc
				limit 1
				)
				 select
				*
				from
					nearest_point as fraction,
						(values(2)) as e(pid),
					nearest_itinerary as edge_id
				)  select * from (SELECT * FROM start_point
				UNION
				SELECT * FROM end_point) as v
				
				) SELECT pid, edge_id, fraction from poi',
					-1,
					-2,
					directed := false,
					details := true) as route
				
				
				
				),
				geom as (
				select
					case
						when node != -1
						and node != -2
						and node = itineraries_routing_noded.source then the_geom
						-- if the next marked node is not the same as target then must revert
						when node = -1
						and (lead(node) over (
						order by seq)) != target then st_reverse(the_geom)
						when node = -1
						and (lead(node) over (
						order by seq)) = target then the_geom
						-- if the next marked node is not the same as target then must revert
						when node = -2
						and (lag(node) over (
						order by seq)) != source then st_reverse(the_geom)
						when node = -2
						and (lag(node) over (
						order by seq)) = source then the_geom
						-- otherwise reverse
						else ST_Reverse(the_geom)
					end as geom
				from
					calculated_route
				left join itineraries_routing_noded on
							edge = itineraries_routing_noded.id)
				
						select
						st_asGeoJson(ST_LineSubstring(st_makeline(el.geom),
					ST_LineLocatePoint(st_makeline(el.geom),
					(
					select
						St_setsrid(St_makepoint(".$start['longitude'].",".$start['latitude']."),
						4326))),
					ST_LineLocatePoint(st_makeline(el.geom),
					(
					select
						St_setsrid(St_makepoint(".$end['longitude'].",".$end['latitude']."),
						4326)))))::jsonb as route
				from
					(
					select
						(st_dumppoints(geom)).geom
					from
						geom) as el";				//	dd($query);
						
			$db = Factory::getDbo();
			$db->setQuery($query);
			$data = $db->loadObject();

			// Push routing result as a new route
			if ($data->route == null) {
				return "KO";
			}
			// $route = json_decode($data->route);
			// if ($route->type == 'MultiLineString') {
			// 	return "MULTI";
			// }
			$route = json_decode($data->route);
			// Mix all array elements from route cordinates itself as a single array
			($listOfRoutes = array_merge($listOfRoutes, [$route]));
		}
			/// MIX OF THE DIFFERENT ROUTES
			$linestringList = [];
			// Converts list of coordinates to linestring for postgis

			foreach ($listOfRoutes as $route) {
				/// Check if is going to be linestring or multilinestring
				if ($route->type == "MultiLineString") {
					// If is multilinestring, then insert it as linestring
					foreach ($route->coordinates as $singleLineString) {
						// Generate linestring on database from array of coordinates
						$lineStringText = "ST_GeomFromText('LINESTRING(";
						foreach ($singleLineString as $coordinate) {
							$lineStringText .= $coordinate[0] . " " . $coordinate[1] . ",";
						}
						$lineStringText = substr($lineStringText, 0, -1);
						$lineStringText .= ")', 4326)";
						$linestringList[] = $lineStringText;
					}
				} else {
					// Generate linestring on database from array of coordinates
					$lineStringText = "ST_GeomFromText('LINESTRING(";
					foreach ($route->coordinates as $coordinate) {
						$lineStringText .= $coordinate[0] . " " . $coordinate[1] . ",";
					}
					$lineStringText = substr($lineStringText, 0, -1);
					$lineStringText .= ")', 4326)";
					$linestringList[] = $lineStringText;
				}
			}

			// create a string like this:
			// $linestring = "ST_MakeLine( 'LINESTRING(0 0, 1 1)', 'LINESTRING(2 2, 3 3)' ");
			$query = "ST_MakeLine(array[";
			for ($i = 0; $i < sizeof($linestringList); $i++) {
				$query = $query . $linestringList[$i] . ",";
			}

			$query = substr($query, 0, -1) . "])";

			$query = "											            
							with
							seqs as (
							select
								pts.path[1] as elem_seq,
								pts.path[2] as pnt_seq,
								pts.geom as geom
							from
								(
								select
									(ST_DumpPoints(route)).*
								from
									(
									SELECT * FROM " . $query . " AS route
						
						) as el
								) as pts
							)

						select
							jsonb_build_object(
							
							'type', 'FeatureCollection',
						'features', array[jsonb_build_object('type','Feature','geometry', ST_AsGeoJSON(st_simplify(ST_MakeLine(ST_MakePoint(ST_X(s.geom),
						ST_Y(s.geom),
						q.st_z)), 0.00008))::jsonb)]
						) as route
						from
							seqs as s
						join lateral (
							select
								ST_Z(ST_3DClosestPoint(
								s.geom, the_geomz)) as st_z
							from
								itineraries
							where
								
										itineraries.type = " . $type . "
							order by
								the_geom <-> s.geom
							limit 1
						) as q
						on
							true 
						";
			//dd($query);
			$db = Factory::getDbo();
			$db->setQuery($query);
			$data = $db->loadObject();


			// Push routing result as a new route
			if ($data->route == null) {
				return "KO";
			}
			// $route = json_decode($data->route);
			// if ($route->type == 'MultiLineString') {
			// 	return "MULTI";
			// }
			$route = json_decode($data->route);
			$response['success'] = "true";
			$response['message'] = "OK";
			$response['data'] = $route;
			die(json_encode($response));
		
	}
	public function searchablePoints()
	{
		$type = $this->app->input->get('type', null, 'int');
		$searchLocation =  $this->app->input->get('search', null, 'STRING');
		$firstLat =  $this->app->input->get('firstLat', null, 'STRING');
		$firstLng =  $this->app->input->get('firstLng', null, 'STRING');
		$apiFilterInfo = $this->input->get('filter', [], 'array');
		$language =  $apiFilterInfo['language'];
		$db = Factory::getDbo();
		if ($language != 'es-ES') {
			if ($firstLng == "") {

				$query = "	SELECT asso2.id as article_id
							FROM sooeg_associations AS asso1
							INNER JOIN sooeg_associations AS asso2 ON asso1.key = asso2.key
							inner join sooeg_content sc3 on sc3.id = asso2.id 
							WHERE asso1.id in (SELECT distinct s.article_id
							FROM
							searchablepoints s 
							WHERE
							s.article_id in (select a.id from sooeg_content a where a.id in (SELECT asso2.id 
							FROM sooeg_associations AS asso1
							INNER JOIN sooeg_associations AS asso2 ON asso1.key = asso2.key
							WHERE asso1.id in (select a.id from sooeg_content a where ";
				
				$query .= "a.title ilike '%" . $searchLocation . "%' and ";
				
				$query .= "a.language = '" . $language . "') AND asso1.context = 'com_content.item') and a.language = 'es-ES')
							and s.gid  in (select distinct r.point_gid from public.relateditinerariesandpoints r
							left join public.itineraries i2 on i2.id = r.itinerary_id
							where i2.type =  " . $type . " )) 
							and sc3.language = '" . $language . "'";
				$query .= "limit 10";

			} elseif ($firstLng != "") {

				$query = "	SELECT asso2.id as article_id
							FROM sooeg_associations AS asso1
							INNER JOIN sooeg_associations AS asso2 ON asso1.key = asso2.key
							inner join sooeg_content sc3 on sc3.id = asso2.id 
							WHERE asso1.id in (with reachable_geometry as(
									select
									st_simplify(multilinestring.geom, 0.00008) as cluster
									from (select ST_CollectionExtract(ST_SetSRID(unnest(ST_ClusterIntersecting(the_geom)), 4326), 2) as geom from itineraries 
									left join sooeg_content on itineraries.j_id = sooeg_content.id 
									where itineraries.type = " . $type . " and sooeg_content.state = 1) as multilinestring CROSS JOIN LATERAL ST_Dump(multilinestring.geom)
									AS dump where  ST_Intersects(dump.geom, ST_SetSRID(st_geomfromtext('POINT (" . $firstLng . " " . $firstLat . ")'), 4326)) group by multilinestring.geom
									),
									reachable_itineraries_ids as (
									SELECT id from itineraries, reachable_geometry where ST_Intersects(reachable_geometry.cluster, the_geom) and itineraries.type = " . $type . "
									)
									SELECT distinct s.article_id
									FROM
									searchablepoints s 
									left join relateditinerariesandpoints on s.gid = relateditinerariesandpoints.point_gid
									left join reachable_itineraries_ids on relateditinerariesandpoints.itinerary_id = reachable_itineraries_ids.id
									WHERE
									s.article_id in (select a.id from sooeg_content a where a.id in (SELECT asso2.id 
									FROM sooeg_associations AS asso1
									INNER JOIN sooeg_associations AS asso2 ON asso1.key = asso2.key
									WHERE asso1.id in (select a.id from sooeg_content a where a.title ilike '%" . $searchLocation . "%' and a.language = '" . $language . "') AND asso1.context = 'com_content.item') and a.language = 'es-ES') and reachable_itineraries_ids.id notnull) 
							and sc3.language = '" . $language . "'";
							$query .= "limit 10";

			}
		} else {
			if ($firstLng == "") { //first time we get searchable points
				$query = "SELECT distinct s.article_id
							FROM
							searchablepoints s 
							WHERE ";
				$query .= "s.point_name ilike '%" . $searchLocation . "%' and ";
				
				$query .= "s.gid  in (select distinct r.point_gid from public.relateditinerariesandpoints r
							left join public.itineraries i2 on i2.id = r.itinerary_id
							where i2.type = " . $type . ")";
				$query .= "limit 10";

			} elseif ($firstLng != "") {
				$query = "with reachable_geometry as(
						select
						st_simplify(multilinestring.geom, 0.00008) as cluster
						from (select ST_CollectionExtract(ST_SetSRID(unnest(ST_ClusterIntersecting(the_geom)), 4326), 2) as geom from itineraries 
						left join sooeg_content on itineraries.j_id = sooeg_content.id 
						where itineraries.type = " . $type . " and sooeg_content.state = 1) as multilinestring CROSS JOIN LATERAL ST_Dump(multilinestring.geom)
						AS dump where  ST_Intersects(dump.geom, ST_SetSRID(st_geomfromtext('POINT (" . $firstLng . " " . $firstLat . ")'), 4326)) group by multilinestring.geom
						),
						reachable_itineraries_ids as (
						SELECT id from itineraries, reachable_geometry where ST_Intersects(reachable_geometry.cluster, the_geom) and itineraries.type = " . $type . "
						)
						
						SELECT distinct article_id
						FROM
						searchablepoints s
						left join relateditinerariesandpoints on s.gid = relateditinerariesandpoints.point_gid
						left join reachable_itineraries_ids on relateditinerariesandpoints.itinerary_id = reachable_itineraries_ids.id
						WHERE ";
				$query .= "s.point_name ilike '%" . $searchLocation . 
				$query .= "limit 10";

			}
		}





		$db->setQuery($query);
		$result = $db->loadObjectList();

		$content = [];
		foreach ($result as $item) {
			$app = Factory::getApplication();
			$model = ArticleModel::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
			$model->setState('params', $app);
			$model->setState('filter.article_id', $item->article_id); //Banner home category ID
			$helpArticle =   $model->getItems();
			$item = $helpArticle[0];
			$this->fillItemData($item, $language);
			$content[] = $item;
		}
		if (sizeof($content) == 0) {
			$response['success'] = "false";
			$response['message'] = "KO";
		} else {
			$response['success'] = "true";
			$response['message'] = "OK";
			$response['data'] = $content;
		}

		die(json_encode($response));
	}

	public function connectedItineraries()
	{
		$type = $this->app->input->get('type', null, 'int');
		$firstLat =  $this->app->input->get('firstLat', null, 'STRING');
		$firstLng =  $this->app->input->get('firstLng', null, 'STRING');
		$db = Factory::getDbo();
		$query = "
		  select
		  jsonb_build_object(
				  'type', 'Feature',
				  'id', 'itinerary_custom',
				  'properties', json_build_object(),
				  'geometry', ST_AsGeoJSON(geom)::jsonb
			  ) as item
	  from
		  (
		  select
			  st_simplify(multilinestring.geom,
			  0.000008) as geom
		  from
			  (
			  select
				  ST_CollectionExtract(ST_SetSRID(unnest(ST_ClusterIntersecting(the_geom)),
				  4326),
				  2) as geom
			  from
				  itineraries
			  left join sooeg_content on
				  itineraries.j_id = sooeg_content.id
			  where
				  itineraries.type = " . $type . "
				  and sooeg_content.state = 1) as multilinestring,
			  (
			  select
				  ST_ClosestPoint(the_geom,
				  ST_SetSRID(st_geomfromtext('POINT (" . $firstLng . " " . $firstLat . ")'),
				  4326)) as point
			  from
				  itineraries
			  left join sooeg_content on
				  itineraries.j_id = sooeg_content.id
			  where
				  itineraries.type = " . $type . "
				  and sooeg_content.state = 1
				order by itineraries.the_geom <-> 'SRID=4326;POINT(" . $firstLng . " " . $firstLat . ")'::geometry
			  limit 1) as point
		  cross join lateral ST_Dump(multilinestring.geom) as dump
		  where
			  ST_DISTANCE(dump.geom,
			  point.point) = 0
		  group by
			  multilinestring.geom 
			  limit 1) as cluster
		
		 
		  ";

		$db->setQuery($query);
		$data = $db->loadObjectList();
		if (is_null($data)) {
			$response['success'] = "false";
			$response['message'] = "KO";
		} else {
			$response['success'] = "true";
			$response['message'] = "OK";

			foreach ($data as &$item) {
				$item = json_decode($item->item);
			}
			$response['data'] = $data;
		}

		die(json_encode($response));
	}

	protected function fillItemData($item, $language)
	{
		//Set fields field to be filled
		$fields = FieldsHelper::getFields('com_content.article', $item, true);
		//create  article link
		$articleLink = RouteHelper::getArticleRoute($item->id, $item->catid, $item->language);
		$articleLink = Route::_($articleLink);
		// Adding the fields to the object
		$item->jcfields = array();

		foreach ($fields as $key => $field) {
			$item->jcfields[$field->id] = $field;
		}
		//dd($item);
		//Get lat lon of equipamientos and servicios
		if ($item->parent_category_id == 39 || $item->parent_category_id == 99 || $item->parent_category_id == 102 || $item->parent_category_id == 117 || $item->parent_category_id == 103 || $item->parent_category_id == 118) {
			$item->latitude = $item->jcfields[177]->rawvalue;
			$item->longitude = $item->jcfields[178]->rawvalue;
			$item->titleResult = $item->title . ', ' . $item->category_title;
		}
		//Get lat lon of puntos de interés
		elseif ($item->parent_category_id == 66 || $item->parent_category_id == 139 || $item->parent_category_id == 140) {
			$item->latitude = $item->jcfields[202]->rawvalue;
			$item->longitude = $item->jcfields[203]->rawvalue;
			$item->titleResult = $item->title . ', ' . $item->category_title;
		} elseif ($item->parent_category_id == 38 || $item->parent_category_id == 104 || $item->parent_category_id == 105) {
			if ($language == 'es-ES') {
				$start = 'inicio';
				$end = 'final';
			} elseif ($language == 'de-DE') {
				$start = 'start';
				$end = 'ende';
			} elseif ($language == 'en-GB') {
				$start = 'start';
				$end = 'end';
			}
			$item->titleResult =  $item->title . ', ' . $start;
			$linestring = json_decode($item->jcfields[145]->rawvalue);
			if( $linestring != "") {
				$routeStartPoint = $linestring->features[0]->geometry->coordinates[0];
				$routeEndPoint = end($linestring->features[0]->geometry->coordinates);
				$item->latitude = $routeStartPoint[1];
				$item->longitude = $routeStartPoint[0];
				$item->titleResultFin =  $item->title . ', ' . $end;
				$item->latitudeFin = $routeEndPoint[1];
				$item->longitudeFin = $routeEndPoint[0];
			}

		}
		if ($item->parent_category_id == 66 || $item->parent_category_id == 139 || $item->parent_category_id == 140) {
			$categoryCustomIdFeield = 205;
			$db = Factory::getDbo();
			$query = $db
				->getQuery(true)
				->select(array('value'))
				->from($db->quoteName('#__fields_values'))
				->where($db->quoteName('field_id') . " = " . $db->quote($categoryCustomIdFeield))
				->where($db->quoteName('item_id') . " = " . $db->quote($item->catid));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$typeIntrestingPoint = $db->loadResult();
			$item->typeIntrestingPoint = $typeIntrestingPoint;
		}
		if ($item->parent_category_id == 39 || $item->parent_category_id == 99 || $item->parent_category_id == 102 || $item->parent_category_id == 102 || $item->parent_category_id == 117 || $item->parent_category_id == 118) {
			$categoryCustomIdFeield = 257;
			$db = Factory::getDbo();
			$query = $db
				->getQuery(true)
				->select(array('value'))
				->from($db->quoteName('#__fields_values'))
				->where($db->quoteName('field_id') . " = " . $db->quote($categoryCustomIdFeield))
				->where($db->quoteName('item_id') . " = " . $db->quote($item->catid));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$typeEquipment = $db->loadResult();
			$item->typeEquipment = $typeEquipment;
		}
		//if this item is in favourites list for the current user
		if (isset($hasFavourite)) {
			$favouriteIds = array_column($hasFavourite, 'item_id');
			if (in_array($item->id, $favouriteIds)) {
				$item->favourite = true;
			}
		}
		$item->incidence = "";

		//Check if itinerario has incidence, then check if is still active
		if (isset($item->jcfields[253]->rawvalue)) {
			if ($item->jcfields[253]->rawvalue != "") {
				$db = Factory::getDbo();
				$query = $db
					->getQuery(true)
					->select('state')
					->from($db->quoteName('#__content'))
					->where($db->quoteName('id') . " = " . $db->quote($item->jcfields[253]->rawvalue));
				// Reset the query using our newly populated query object.
				$db->setQuery($query);
				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				$indicendeIsActive = $db->loadResult();

				if ($indicendeIsActive == 1) {
					$item->incidence = $item->jcfields[253]->rawvalue;
				}
			}
		}
		$item->url = $articleLink;
		return $item;
	}
}
