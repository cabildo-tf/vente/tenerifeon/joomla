<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\CMS\User\UserHelper;
use Joomla\Plugin\User\Token\Field\JoomlatokenField;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SgitinerariosController extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SgItinerarios()
	{
		$northEast = $this->app->input->get('northEast');
		$southWest = $this->app->input->get('southWest');
		$given = $this->app->input->get('given');
		
		$alreadyGivenElements = $given == null ? [] : explode('_', $given);
		$finalGiven = [];
		foreach ($alreadyGivenElements as $key => $element) {
			if (str_contains($element, '-')) {
				$separatedRange = explode('-', $element);

				$finalGiven = array_merge($finalGiven, range($separatedRange[0], $separatedRange[1]));
				
			}
			else{
				$finalGiven[] = intval($element);
			}
		}
		$alreadyGivenElements = $finalGiven;

		// Returns a geojson with the itineraries inside the bounding box also accepts to exclude the ids given

		$query = "
		SELECT  jsonb_build_object(
			'type',       'Feature',
			'id',         j_id,
			'geometry',   ST_AsGeoJSON(ST_Simplify(the_geom, 0.0001))::jsonb,
			'properties', jsonb_build_object('geom_id',itineraries.id, 'j_id',j_id,'type', type, 'color', sooeg_fields_values.value)
		) as item
					FROM itineraries left join sooeg_content on j_id = sooeg_content.id left join sooeg_fields_values on  j_id::varchar = item_id and '230' = field_id and state = 1";

					

		$query = $query . " where  ST_Intersects(  ST_MakeEnvelope(" . $southWest['longitude'] . " ," . $southWest['latitude'] . ", " . $northEast['longitude'] . " ," . $northEast['latitude'] . ", 4326) , itineraries.the_geom) and state = 1";

		if (sizeof($alreadyGivenElements) >= 1) {
			$query = $query . " and itineraries.id NOT IN (" . implode(', ', $alreadyGivenElements) . ")";
		}

		$db = Factory::getDbo();
		$db->setQuery($query);
		$data = $db->loadObjectList();

		$query = "
		SELECT count('*') 
			FROM itineraries left join sooeg_content on j_id = sooeg_content.id where state = 1";

		$db->setQuery($query);
		$count = $db->loadResult();
		
		foreach ($data as &$item) {
			$item = json_decode($item->item);
		}

		$response = [
			'data' => $data,
			'total_count' => $count
		];

		
		die (json_encode($response));
		
	}
}
