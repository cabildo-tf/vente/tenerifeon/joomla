<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Factory;
use Joomla\Component\Content\Api\Helper\ContentHelper;
use Joomla\Component\Content\Site\Model\ArticleModel;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Registry\Registry;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SgpuntosinteresController extends ApiController
{
	/**
	 * The content tipo of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contenttipo = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SgPuntosinteres()
	{
		//Get language from request
		$filters = $this->app->input->get('filter');
		$language = $filters['language'];
		//Puntos de interes parent category id
		$languagesCategory = [
			'es-ES' => '66',
			'de-DE' => '139',
			'en-GB' =>'140'
		];

		$db = Factory::getDbo();
		//get id of puntos de interés for current language
		$query = "SELECT article.id as id FROM sooeg_content article 
		left join sooeg_categories category on article.catid = category.id 
		WHERE article.state = '1' and category.parent_id = '".$languagesCategory[$language]."'";

		$db->setQuery($query);
		$data = $db->loadRowList();
		$content = [];
		foreach ($data as $item) {
			$app = Factory::getApplication();
			$model = ArticleModel::getInstance('Customarticles', 'ContentModel', array('ignore_request' => true));
			$model->setState('params', $app);
			$model->setState('filter.article_id', $item[0]); 
			$helpArticle =   $model->getItems();
			$item = $helpArticle[0];
			// if (isset($item->images))
			// {
			// 	$registry = new Registry($item->images);
			// 	$item->images = $registry->toArray();
	
			// 	if (!empty($item->images['image_intro']))
			// 	{
			// 		$item->images['image_intro'] = ContentHelper::resolve($item->images['image_intro']);
			// 	}
	
			// 	if (!empty($item->images['image_fulltext']))
			// 	{
			// 		$item->images['image_fulltext'] = ContentHelper::resolve($item->images['image_fulltext']);
			// 	}
			// }

			$fields = FieldsHelper::getFields('com_content.article', $item, true);

			// Adding the fields to the object
			$item->jcfields = array();

			$item->tipo = null;
			
			foreach ($fields as $key => $field) {
				if($field->id == 164){

					$typeNatural = [1,2,6,12,14];
					$typeCultural = [0,3,4,5,7,8,9,10,15,11,13];

						switch ($field->rawvalue) {
							case in_array($field->rawvalue, $typeNatural):
								$item->tipo = 1;
								break;
							case in_array($field->rawvalue, $typeCultural):
								$item->tipo = 2;
								break;
							default:
								$item->tipo = 3;
							break;

						}

				}elseif($field->id == 167){ //telefono-puntointeres
					
					$item->{$field->name} = isset($field->apivalue) ? $field->apivalue : $field->rawvalue;
					if($item->{$field->name} == ""){
						$item->{$field->name} = null;
					}

				}elseif($field->id == 168){ //email-puntointeres

					$item->{$field->name} = isset($field->apivalue) ? $field->apivalue : $field->rawvalue;
					if($item->{$field->name} == ""){
						$item->{$field->name} = null;
					}
				}else{
					$item->{$field->name} = isset($field->apivalue) ? $field->apivalue : $field->rawvalue;
				}




			}
			$content[] = $item;
		}

		$response = [
				'data' => $content
		];
		die (json_encode($response));

		// Returns a list of all puntos de interés

		// $query = "select id, title, introtext, images, replace(sum(latitude)::varchar,',','.') as latitude, replace(sum(longitude)::varchar,',','.') as longitude, sum(tipo) as tipo 
		// from (
		// /* latitude */
		// select
		//  article.id as id, article.title as title, introtext as introtext,images as images, cast(fields.value as float)  as latitude, 0 as longitude, 0 as tipo 
		// from
		// 	sooeg_content as article
			
		// left join sooeg_categories as category on
		// 	article.catid = category.id
			
		// left join sooeg_fields_values as fields on
		// 	article.id::varchar = fields.item_id or article.catid::varchar = fields.item_id
		// where
		// 	state = 1
		// 	and category.parent_id = '".$languagesCategory[$language]."'
		// 	and fields.field_id = 202
		
		// union
		// /* longitude */	
		// select
		// article.id as id, article.title as title, introtext as introtext,images as images, 0 as latitude, cast(fields.value as float) as longitude, 0 as tipo 
		// from
		// 	sooeg_content as article
			
		// left join sooeg_categories as category on
		// 	article.catid = category.id
			
		// left join sooeg_fields_values as fields on
		// 	article.id::varchar = fields.item_id or article.catid::varchar = fields.item_id
		// where
		// 	state = 1
		// 	and category.parent_id = '".$languagesCategory[$language]."'
		// 	and fields.field_id = 203
		
		// union
		// /* tipo */	
		// select
		// article.id as id, article.title as title, introtext as introtext,images as images, 0 as latitude, 0 as longitude, cast(fields.value as int) as tipo  
		// from
		// 	sooeg_content as article
			
		// left join sooeg_categories as category on
		// 	article.catid = category.id
			
		// left join sooeg_fields_values as fields on
		// 	article.id::varchar = fields.item_id or article.catid::varchar = fields.item_id
		// where
		// 	state = 1
		// 	and category.parent_id = '".$languagesCategory[$language]."'
		// 	and fields.field_id = 205
		// ) q
		// group by id, title, introtext,images	
		// 	";


		// $db = Factory::getDbo();
		// $db->setQuery($query);
		// $data = $db->loadObjectList();

		// $response = [
		// 	'data' => $data
		// ];
	
		// die (json_encode($response));
		
	}
}
