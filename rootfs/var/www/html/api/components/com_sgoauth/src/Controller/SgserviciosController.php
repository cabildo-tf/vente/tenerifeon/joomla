<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\CMS\User\UserHelper;
use Joomla\Plugin\User\Token\Field\JoomlatokenField;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SgserviciosController extends ApiController
{
	/**
	 * The content tipo of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contenttipo = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SgServicios()
	{

		// Returns a list of all services

		$query = "select id, title, replace(sum(latitude)::varchar,',','.') as latitude, replace(sum(longitude)::varchar,',','.') as longitude, sum(tipo) as tipo 
		from (
		/* latitude */
		select
		 article.id as id, article.title as title, cast(fields.value as float)  as latitude, 0 as longitude, 0 as tipo 
		from
			sooeg_content as article
			
		left join sooeg_categories as category on
			article.catid = category.id
			
		left join sooeg_fields_values as fields on
			article.id::varchar = fields.item_id or article.catid::varchar = fields.item_id
		where
			state = 1
			and category.parent_id = 99
			and fields.field_id = 177
		
		union
		/* longitude */	
		select
		 article.id as id, article.title as title, 0 as latitude, cast(fields.value as float) as longitude, 0 as tipo 
		from
			sooeg_content as article
			
		left join sooeg_categories as category on
			article.catid = category.id
			
		left join sooeg_fields_values as fields on
			article.id::varchar = fields.item_id or article.catid::varchar = fields.item_id
		where
			state = 1
			and category.parent_id = 99
			and fields.field_id = 178
		
		union
		/* tipo */	
		select
		 article.id as id, article.title as title, 0 as latitude, 0 as longitude, cast(fields.value as int) as tipo  
		from
			sooeg_content as article
			
		left join sooeg_categories as category on
			article.catid = category.id
			
		left join sooeg_fields_values as fields on
			article.id::varchar = fields.item_id or article.catid::varchar = fields.item_id
		where
			state = 1
			and category.parent_id = 99
			and fields.field_id = 257
		) q
		group by id, title		
			";


		$db = Factory::getDbo();
		$db->setQuery($query);
		$data = $db->loadObjectList();

		$response = [
			'data' => $data
		];
	
		die (json_encode($response));
		
	}
}
