<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\CMS\User\UserHelper;
use Joomla\Component\Users\Site\Model\RegistrationModel;
use Joomla\Plugin\User\Token\Field\JoomlatokenField;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SgroutesController extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SgRoutes()
	{
		$user = Factory::getUser();
		$db = Factory::getDbo();
		$offset = $this->input->get('offset',null,'INTEGER');
		$limit = $this->input->get('limit',null,'INTEGER');


		$query ="
		select  COUNT(*) as total
	   from
			   sooeg_sg_my_routes
	   where
		   user_id = ".$user->id."";

		$db->setQuery($query);
		$myroutes['total-count'] = $db->loadResult();

		$query ="
		select  *
	   from
			   sooeg_sg_my_routes
	   where
		   user_id = ".$user->id."
		order by route_date DESC 
		offset ".$offset."
		limit ".$limit."";

		$db->setQuery($query);
		$myroutes['data'] = $db->loadObjectList();
		die (json_encode($myroutes));
		
		
	}

	public function SaveSgRoutes()
	{
		try
		{	
			$user = Factory::getUser();
			$db = Factory::getDbo();

			$temp['user_id'] = $user->id;
			$temp['route_name'] = $this->input->get('route_name',null,'STRING');
			$temp['route_type'] = $this->input->get('route_type',null,'INTEGER');
			$temp['route_date'] = $this->input->get('route_date',null,'RAW');
			$temp['route_geom'] = $this->input->get('route_geom',null,'STRING');
			$temp['route_gain'] = $this->input->get('route_gain',null,'RAW');
			$temp['route_lost'] = $this->input->get('route_lost',null,'RAW');
			$temp['route_max_altitude'] = $this->input->get('route_max_altitude',null,'RAW');
			$temp['route_min_alt'] = $this->input->get('route_min_alt',null,'RAW');
			$temp['route_duration'] = $this->input->get('route_duration',null,'INTEGER');
			$temp['route_distance'] = $this->input->get('route_distance',null,'FLOAT');
			$temp['route_itineraries'] = $this->input->get('route_itineraries',null,'INTEGER');
			$statrtLatLon = json_decode($temp['route_geom'])->features[0]->geometry->coordinates[0];

			$query = 	"SELECT m.id
							FROM municipios m
							WHERE ST_Contains(m.geom, 'POINT(".$statrtLatLon[0]." ".$statrtLatLon[1].")');";
			
			$db->setQuery($query);
			$temp['route_place'] = $db->loadResult();

			//Select items rated and rating
			$query = "INSERT INTO sooeg_sg_my_routes (user_id, route_name, route_type, route_date, route_geom, route_gain, route_lost, route_max_altitude, route_min_alt, route_duration, route_distance, route_itineraries, route_place) 
				values (".$user->id.", '".$temp['route_name']."', '".$temp['route_type']."', '".$temp['route_date']."', '".$temp['route_geom']."','".$temp['route_gain']."','".$temp['route_lost']."','".$temp['route_max_altitude']."','".$temp['route_min_alt']."','".$temp['route_duration']."','".$temp['route_distance']."','".$temp['route_itineraries']."','".$temp['route_place']."');";
			$db->setQuery($query)->execute();

			$response['success'] = "true";
			$response['message'] = "Actividad creada correctamente";
		}
		catch (\Exception $exception)
		{
			$response['success'] = "false";
			$response['message'] = "No se ha podido crear la  actividad";
		}
		
		die (json_encode($response));
	}
	public function DeleteSgRoutes(){

		$user = Factory::getUser();
		$db = Factory::getDbo();
		$id = $this->input->get('id',null,'INTEGER');

		$query ="
		DELETE FROM sooeg_sg_my_routes
		WHERE
 	    user_id = ".$user->id."
		AND id = ".$id."";

		$db->setQuery($query);
		$db->execute();

		$response['success'] = "true";
		$response['message'] = "Se ha borrado la ruta, si existía";
			
		die (json_encode($response));
		
	}
}
