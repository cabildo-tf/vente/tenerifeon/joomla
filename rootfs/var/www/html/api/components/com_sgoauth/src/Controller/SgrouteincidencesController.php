<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgoauth\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Factory;
use Joomla\Component\Content\Api\Helper\ContentHelper;
use Joomla\Component\Content\Site\Model\ArticleModel;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Registry\Registry;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SgrouteincidencesController extends ApiController
{
	/**
	 * The content tipo of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contenttipo = 'token';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'token';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SgRouteincidences()
	{
		//Get language from request
		$filters = $this->app->input->get('filter');
		$language = $filters['language'];
		//Puntos de interes parent category id
		$languagesCategory = [
			'es-ES' => '92',
			'de-DE' => '193',
			'en-GB' =>'194'
		];

		$query = "select c.id, c.title, c.introtext, c.images, cast (v.value AS json) as geometry
		from sooeg_content c		
		left join sooeg_fields_values v on
			c.id::varchar = v.item_id
		where
			c.state = '1'
			and c.catid = '".$languagesCategory[$language]."'
			and v.field_id = '251'";

	


		$db = Factory::getDbo();
		$db->setQuery($query);
		$data = $db->loadObjectList();
		foreach($data as &$val){
			$val->geometry = (json_decode($val->geometry));
		}

		$response = [
			'data' => $data
		];
	
		die (json_encode($response));
		
	}
}
