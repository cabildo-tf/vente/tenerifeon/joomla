<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgsyncvente\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Arcgis\ArcgisClient;
use Firebase\JWT\Key;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SyncalertasController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function syncAlertas (){

		//Check if we have itinerarios search by codigo-alertas cfield
			$db = Factory::getDbo();

			$codigoAlerta_field = 148;
			$query = $db
			->getQuery(true)
			->select('COUNT(*)')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('field_id') . " = " . $db->quote($codigoAlerta_field));	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
	
			$import_content = $db->loadResult();

			//if we have some itinerarios, the we want to get ONLY newer content
			if($import_content > 0){
				//The last_date_edited ID in JOOMLA
				$last_edited_date_alerta = 153;
				// Create a new query object.
				$query = $db
				->getQuery(true)
				->select('MAX(value)')
				->from($db->quoteName('#__fields_values'))
				->where($db->quoteName('field_id') . " = " . $db->quote($last_edited_date_alerta));
				// Reset the query using our newly populated query object.
				$db->setQuery($query);

				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				$have_last_edited = $db->loadResult();
				//set where statement for query
				$where = "last_edited_date  >'".date("Ymd h:i:s",strtotime($have_last_edited))."'";
			}
			//if no equipamientos, get full equipamientos
			else{
				$where = "1=1";
			}
							
			//If want to update all equipamientos, then set true
			$importar_de_nuevo = true;
			if($importar_de_nuevo) $where = "1=1";
			
			//LLamada a arcgisclient
			$body = array('where' => $where,'text' => '','objectIds' => '','time' => '','geometry' => '','geometryType' => 'esriGeometryEnvelope','inSR' => '','spatialRel' => 'esriSpatialRelIntersects','relationParam' => '','outFields' => '*','returnGeometry' => 'true','returnTrueCurves' => 'false','maxAllowableOffset' => '','geometryPrecision' => '','outSR' => '','having' => '','returnIdsOnly' => 'false','returnCountOnly' => 'false','orderByFields' => '','groupByFieldsForStatistics' => '','outStatistics' => '','returnZ' => 'false','returnM' => 'false','gdbVersion' => '','historicMoment' => '','returnDistinctValues' => 'false','resultOffset' => '','resultRecordCount' => '','queryByDistance' => '','returnExtentOnly' => 'false','datumTransformation' => '','parameterValues' => '','rangeValues' => '','quantizationParameters' => '','featureEncoding' => 'esriDefault','f' => 'geojson');
			$client = new ArcgisClient("https://vente-pre.tenerife.es"); 
			$path = '/server/rest/services/ciudadanos/v_ext_alertas/FeatureServer/0/query';
			$response = $client->post($path, $body);

			//Fin llamada a arcgisclient
			$alertas = $response->features;
			//dd($alertas);
			$app = Factory::getApplication();			
	
			//superadmin user id
			$user_id = 848;
		
			$articleModel = $app->bootComponent('com_content')
			->getMVCFactory()->createModel('Article', 'Administrator');
			
			foreach($alertas as $alerta){

				//Check if already exists
				$alerta_exists = $this->checkIfExists($alerta);
				//Alertas category id
				$cat_id = 55;
				//Clean observaciones
				$obs = str_replace("\n","",$alerta->properties->observaciones);
				$article = [
					"catid" => $cat_id,
					"title"		=> $alerta->properties->codigo,
					"id"	=> is_null($alerta_exists['id']) ? 0 : $alerta_exists['id'],
					'created_user_id' => $user_id,
					'created_by_alias'=> '',
					'alias' => ApplicationHelper::stringURLSafe($alerta->properties->codigo),
					'introtext' => " ",
					'language' => 'es-ES',
					'metakey' => '',
					'metadesc' => '',
					'state' => isset($alerta_exists['state']) ? $alerta_exists['state'] : 1 ,
					'access' => 1,
					"com_fields" => array(
						"vid-alerta" => $alerta->properties->vid,
						"codigo-alerta" => $alerta->properties->codigo,
						"motivo-alerta" => $alerta->properties->causa,
						"observaciones-alerta" => $obs,
						"estado-alerta" => $alerta->properties->estado,
						"last-edited-date-alerta" => $alerta->properties->last_edited_date != null ? date('Y-m-d H:i:s', substr($alerta->properties->last_edited_date,0,10)) : "",
						"situacion-alerta" => $alerta->properties->situacion,
						"enlace-alerta" => $alerta->properties->enlace,
						"origen-alerta" => $alerta->properties->origen,
						"fecha-emision-alerta" => $alerta->properties->fecha_emision != null ? date('Y-m-d H:i:s', substr($alerta->properties->fecha_emision,0,10)) : "",
						"fecha-inicio-alerta" => $alerta->properties->fecha_inicio != null ? date('Y-m-d H:i:s', substr($alerta->properties->fecha_inicio,0,10)) : "",
						"fecha-fin-alerta" => $alerta->properties->fecha_fin != null ? date('Y-m-d H:i:s', substr($alerta->properties->fecha_fin,0,10)) : "",
					)
					];
	
				//dd($article);
				Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');

				// Needs to be set because com_fields needs the data in jform to determine the assigned catid
				$this->input->set('jform', $article);
				$form = $articleModel->getForm($article, false);

				$validData = $articleModel->validate($form, $article);
				
				$articleModel->save($validData);
				var_dump($articleModel->getErrors());								
			};	
		}
	
		protected function checkIfExists($alerta){
	
			$codigoAlerta_field = 148;
			//Check if the alerta already exists in JOOMLA
			// Get a db connection.
			$db = Factory::getDbo();
	
			// Create a new query object.
			$query = $db
			->getQuery(true)
			->select('item_id')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('field_id') . " = " . $db->quote($codigoAlerta_field)) //id cf codigo alerta
			->where($db->quoteName('value') . " = " . $db->quote($alerta->properties->codigo)); //value codigo alerta
	
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
	
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$alerta_exists['id'] = $db->loadResult();
	
			//If exists check state to pass to article update
			if(!is_null($alerta_exists['id'])){
				$query = $db
				->getQuery(true)
				->select('state')
				->from($db->quoteName('#__content'))
				->where($db->quoteName('id') . " = " . $db->quote($alerta_exists['id']));
		
				// Reset the query using our newly populated query object.
				$db->setQuery($query);	
	
				$alerta_exists['state'] = $db->loadResult();
			}
	
			return $alerta_exists;
	
		}
}
