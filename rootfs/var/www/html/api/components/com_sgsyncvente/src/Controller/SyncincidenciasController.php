<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgsyncvente\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Arcgis\ArcgisClient;
use Firebase\JWT\Key;
use Joomla\CMS\Language\Associations;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SyncincidenciasController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function SyncIncidencias (){

		ini_set('memory_limit','512M');
		ini_set('max_execution_time','3600');
		//get the syncro process id in order to set end datetime when finished
		$sync_id = $this->input->get('sync_id',null,'STRING');	

		// Get a db connection.
		$db = Factory::getDbo();

			$incidenciasId = [
				'equipments' => '0',
				'routes' => '5',
			];
			$catIncidenciasItinerarios = 91;
			$catIncidenciasEquipamientos = 92;
			$total = 0;				
			//If want to update all equipamientos, then set true
			$importar_de_nuevo = true;
			if($importar_de_nuevo) $where = "1=1";
			//LLamada a arcgisclient
			$body = array('where' => $where,'text' => '','objectIds' => '','time' => '','geometry' => '','geometryType' => 'esriGeometryEnvelope','inSR' => '','spatialRel' => 'esriSpatialRelIntersects','relationParam' => '','outFields' => '*','returnGeometry' => 'true','returnTrueCurves' => 'false','maxAllowableOffset' => '','geometryPrecision' => '','outSR' => '','having' => '','returnIdsOnly' => 'false','returnCountOnly' => 'false','orderByFields' => '','groupByFieldsForStatistics' => '','outStatistics' => '','returnZ' => 'false','returnM' => 'false','gdbVersion' => '','historicMoment' => '','returnDistinctValues' => 'false','resultOffset' => '','resultRecordCount' => '','queryByDistance' => '','returnExtentOnly' => 'false','datumTransformation' => '','parameterValues' => '','rangeValues' => '','quantizationParameters' => '','featureEncoding' => 'esriDefault','f' => 'geojson');
			$config = Factory::getConfig();
			$client = new ArcgisClient($config->get('venteServicesUrl')); 

			$this->cleanIncidencesInArticles();

			foreach($incidenciasId as $key => $incidenciaId){

				$path = '/server/rest/services/ciudadanos/v_ext_restricciones/FeatureServer/'.$incidenciaId.'/query';
				//$path = '/server/rest/services/ciudadanos/v_ext_restricciones/FeatureServer/5/query';

				$response = $client->post($path, $body);

				if(isset($response->error) && isset($response->error->code)){
					$error = $response->error->code;
					if($error == 400){
						$this->sendEmailSync($error);
					}
					elseif($error == 500){
						$this->sendEmailSync($error);
					}
				};
				//Fin llamada a arcgisclient
				$incidencias = $response->features;

				$this->checkOldIncidences($incidencias, $key);
				//dd($incidencias);

				$app = Factory::getApplication();			
		
				//superadmin user id
				$user_id = 848;
				$articleModel = $app->bootComponent('com_content')
				->getMVCFactory()->createModel('Article', 'Administrator');
				//foreach of incidencias (equip, routes)
				foreach($incidencias as $incidencia){


					//Check if already exists
					$cat_id = 0;
					//Fill incidencia lat lon and get equipment id if it affects one of these 
					$codigo_equipamiento = "";
					if($key == "equipments"){
						$cat_id = $catIncidenciasItinerarios;

						$linestring = "";
						$latitud = $incidencia->geometry->coordinates[1];
						$longitud = $incidencia->geometry->coordinates[0];

					}elseif($key == "routes"){
						$cat_id = $catIncidenciasEquipamientos;
						//get associated categories
						$linestring = '{"name":"'.$incidencia->properties->nombre.'","type":"FeatureCollection","features":['.json_encode($incidencia).']}';
						$latitud = "";
						$longitud = "";
						$codigo_equipamiento = "";
					}
					//get associated categories
					$associations = Associations::getAssociations('com_content', '#__categories', 'com_categories.item', (int) $cat_id, 'id', 'alias', '');
					foreach($associations as $lang){

						$incidencia_exists = $this->checkIfExists($incidencia,$lang->language);

						//Clean observaciones
						$descripcion = str_replace("\n"," ",$incidencia->properties->descripcion);

						//Get equipamiento ID
						$codigo_equipamiento = "";
						if(isset($incidencia->properties->codigo_equipamiento)){
							$incidencia->properties->codigo_equipamiento;
							$codigo_equipamiento = $this->getEquipamientoId($incidencia->properties->codigo_equipamiento, $lang);
						}
						//Get afected itinearies
						$itinerarios_afectados = "";
						if(isset($incidencia->properties->itinerarios_afectados) && $incidencia->properties->itinerarios_afectados != '' && $incidencia->properties->itinerarios_afectados != '{0}'){
							$itinerarios_afectados = $this->getItinerarios($incidencia->properties->itinerarios_afectados, $lang);
						}
						$title = $incidencia->properties->{'motivo_'.explode("-",$lang->language)[0]}." - ".$incidencia->properties->{'afecta_itinerario_'.explode("-",$lang->language)[0]};

						$article = [
							"catid" =>  intval(explode(":", $lang->id)[0]),
							"title"		=> $title,
							"id"	=> is_null($incidencia_exists['id']) ? 0 : $incidencia_exists['id'],
							'created_by_alias'=> '',
							'alias' => ApplicationHelper::stringURLSafe($title." ".$incidencia->properties->codigo_incidencia),
							'publish_down' => $incidencia->properties->fecha_fin_restriccion != null ? date('Y-m-d H:i:s', substr($incidencia->properties->fecha_fin_restriccion,0,10)) : "",
							'introtext' => " ",
							'language' => $lang->language,
							'metakey' => '',
							'metadesc' => '',
							'state' => $codigo_equipamiento != "" || $itinerarios_afectados != "" ? 1 : 0,
							'access' => 1,
							"com_fields" => array(
								"vid-incidencia" => $incidencia->properties->vid,
								"codigo-equipamiento-incidencia" => $codigo_equipamiento,
								"codigo-incidencia" => $incidencia->properties->codigo_incidencia,
								"motivo-incidencia" => $incidencia->properties->{'motivo_'.explode("-",$lang->language)[0]},
								"municipio-incidencia" =>  $incidencia->properties->municipio_id,
								"descripcion-incidencia" => $descripcion,
								"estado-incidencia" => $incidencia->properties->estado_incidencia_id,
								"last-edited-date-incidencia" => $incidencia->properties->last_edited_date_incidencia != null ? date('Y-m-d H:i:s', substr($incidencia->properties->last_edited_date_incidencia,0,10)) : "",
								"tipo-limitacion-incidencia" => $incidencia->properties->tipo_limitacion,
								"enlace-incidencia" => $incidencia->properties->enlace,
								"fecha-inicio-incidencia" => $incidencia->properties->fecha_inicio_restriccion != null ? date('Y-m-d H:i:s', substr($incidencia->properties->fecha_inicio_restriccion,0,10)) : "",
								"fecha-fin-incidencia" => $incidencia->properties->fecha_fin_restriccion != null ? date('Y-m-d H:i:s', substr($incidencia->properties->fecha_fin_restriccion,0,10)) : "",
								"afecta-itinerario-incidencia" => $incidencia->properties->{'afecta_itinerario_'.explode("-",$lang->language)[0]},
								"itinerarios-afectados-incidencia" =>  $itinerarios_afectados,
								"push-incidencia" => $incidencia->properties->push,
								"visible-incidencia" => $incidencia->properties->visible,
								"estado-incidencia" => $incidencia->properties->estado_incidencia_id,
								"geometria-incidencia" =>$linestring,
								"latitud-incidencia" => $latitud,
								"longitud-incidencia" => $longitud, 
							)
						];
			
						//dd($article,$key);
						Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');

						// Needs to be set because com_fields needs the data in jform to determine the assigned catid
						$this->input->set('jform', $article);
						$form = $articleModel->getForm($article, false);

						($validData = $articleModel->validate($form, $article));
						
						$articleModel->save($validData);

						//$activo = $incidencia->properties->visible;
							//If incidencia affects itinerarios
							if(!empty($itinerarios_afectados)){

								$this->updateItinerarios($itinerarios_afectados, $articleModel->getItem()->id);

							}

							//If incidencia affects equipamientos
							if(!empty($codigo_equipamiento)){
			
								$this->updateEquipamientos($codigo_equipamiento, $articleModel->getItem()->id);
								
							}

						//dump($articleModel->getErrors());
						$total++;	
						$allIncidences[] = $incidencia;
						
					};
				};
			}

			
		}
	
		protected function checkIfExists($incidencia,$lang){
		
			$codigo_incidencia_field = 234;
			//Check if the incidencia already exists in JOOMLA
			// Get a db connection.
			$db = Factory::getDbo();
			// Create a new query object.
			($query = "select fv.item_id from sooeg_fields_values fv left join sooeg_content sc
					on sc.id::varchar = fv.item_id where fv.field_id = '".$codigo_incidencia_field."' and fv.value = '".$incidencia->properties->codigo_incidencia."'
					and sc.language = '".$lang."'");
	
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
	
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$incidencia_exists['id'] = $db->loadResult();

			return $incidencia_exists;
	
		}

		function getItinerarios($itinerarios,$lang){

			$itinerariosObjectid = trim($itinerarios,'{}');
			$itinerariosObjectid = str_replace('"','',$itinerariosObjectid);				
			$itinerariosObjectid = explode(',', $itinerariosObjectid);
			$itinerariosIds = [];
			//Get itineraries joomla id if they are published
			foreach ($itinerariosObjectid as $itinerarioId){
				$db = Factory::getDbo();
				$itinerary_internal_id = 279;
				$query = "SELECT fv.item_id FROM sooeg_fields_values fv 
						LEFT JOIN sooeg_content sc on sc.id = fv.item_id::int
						WHERE fv.value = '".$itinerarioId."' and fv.field_id = '".$itinerary_internal_id."'
						and sc.language = 'es-ES' and sc.catid in (61,62,63) and sc.state = '1'";
						$db->setQuery($query);
				$itinerariosIds = $db->loadRow();
			}
			//If any itinerary found, then get the language associated
			if(!is_null($itinerariosIds)){

				foreach($itinerariosIds as $itinerario){
					$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $itinerario);
					if(sizeof($associations) > 0){
						$itinerariosIds = [];
						$itinerariosIds [] = explode(":", $associations[$lang->language]->id)[0];
					}
				}
				$itinerarios_afectados = implode(",",$itinerariosIds);
			}
			//If no id found, then return empty in order to update the incidence to unpublished
			else{
				$itinerarios_afectados = "";
			}
			return $itinerarios_afectados;
		}

		function getEquipamientoId($equipamiento, $lang){

				$db = Factory::getDbo();
				$codigoEquipamiento_field = 146;
					
					$query = "SELECT fv.item_id FROM sooeg_fields_values fv 
					LEFT JOIN sooeg_content sc on sc.id = fv.item_id::int
					WHERE fv.value = '".$equipamiento."' and fv.field_id = '".$codigoEquipamiento_field."'
					and sc.state = '1'";
					$db->setQuery($query);
					
					//Get the lowest id, meaning its the spanish, the original
					$equipamientos = $db->loadObjectList();
				
				if($equipamientos == null){
					$equipamientoId = "";
					return $equipamientoId;
				}else{
					$equipamientoId = min($equipamientos);
					$equipamientoId = $equipamientoId->item_id;
				}
				//Get associeted equipamiento ID
				($associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $equipamientoId));
				if(sizeof($associations) > 0){
					$equipamientoId = explode(":", $associations[$lang->language]->id)[0];
				}
			return $equipamientoId;
		}

		function updateItinerarios($itinerarios_afectados, $article_id){

			$itinerarios_afectados = explode(",",$itinerarios_afectados);
			
			$field_incidencias = 253;//Field to save thre incidencias in itinerario
		
			// Get a db connection.
			$db = Factory::getDbo();
			foreach($itinerarios_afectados as $itinerario){

				if(!empty($itinerario)){
					//Check if this itinerario has already incidencias 
					$query = $db
					->getQuery(true)
					->select('value')
					->from($db->quoteName('#__fields_values'))
					->where($db->quoteName('item_id') . " = " . $db->quote($itinerario)) //id Itinerario
					->where($db->quoteName('field_id') . " = " . $db->quote($field_incidencias)); //id cf field incidencias en itinerario

					// Reset the query using our newly populated query object.
					$db->setQuery($query);
					$incidencias_in_article = $db->loadResult();

					//if still itinerario has not incidencias
					if(is_null($incidencias_in_article)){
						
						//if($activo){
							$columns = array('field_id', 'item_id', 'value');
							$values = $field_incidencias.', '.$itinerario.', '.$article_id;
							$query = $db
							->getQuery(true)
							->insert($db->quoteName('#__fields_values'))
							->columns($db->quoteName($columns))
							->values(''.$values.'');

							// Reset the query using our newly populated query object.
							$db->setQuery($query);
							$incidenciaInArticle = $db->execute();
						//}
					}						

					else{
						//Get the ID of incidencias already in the itinerario
						$check_if_has_this_incidencia = explode(",", $incidencias_in_article);
						if (substr($incidencias_in_article, -1, 1) == ','){
							$incidencias_in_article = substr($incidencias_in_article, 0, -1);
						}

						//Add new incidencia in list of other incidencias if already is not setted on the article
						if (!in_array($article_id, $check_if_has_this_incidencia))				{
							$incidencias_in_article = strval($incidencias_in_article).",".$article_id;
							$query = $db
							->getQuery(true)
							->update('#__fields_values')
							->set("value = '".$incidencias_in_article."'")               // increment the row's value
							->where($db->quoteName('item_id') . " = " . $db->quote($itinerario)) //id Espacio Natural
							->where($db->quoteName('field_id') . " = " . $db->quote($field_incidencias)); //id cf field enp-puntos-de-interes

							// Reset the query using our newly populated query object.
							$db->setQuery($query);
							$incidenciaInArticle = $db->execute();
						}
					
					}
				}
			}
				

		}
			
		
		function updateEquipamientos($equipamiento, $incidencia_id){
			
			$field_incidencias = 254;//Field to save the incidencias in equipamiento
		
			// Get a db connection.
			$db = Factory::getDbo();
			//Check if this equipamiento has already incidencias 
			$query = $db
			->getQuery(true)
			->select('value')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('item_id') . " = " . $db->quote($equipamiento)) //id equipamiento
			->where($db->quoteName('field_id') . " = " . $db->quote($field_incidencias)); //id cf field incidencias en equipamiento

			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			$incidencias_in_article = $db->loadResult();

			//if already equipamiento has not incidencias
			if(is_null($incidencias_in_article)){
				
					$columns = array('field_id', 'item_id', 'value');
					$values = $field_incidencias.', '.$equipamiento.', '.$incidencia_id;
					$query = $db
					->getQuery(true)
					->insert($db->quoteName('#__fields_values'))
					->columns($db->quoteName($columns))
					->values(''.$values.'');

					// Reset the query using our newly populated query object.
					$db->setQuery($query);
					$incidenciaInArticle = $db->execute();

			}
			else{
				//Get the ID of incidencias already in the equipamiento
				$check_if_has_this_incidencia = explode(",", $incidencias_in_article);
				
				//Add new incidencia in list of other incidencias
				if (!in_array($incidencia_id, $check_if_has_this_incidencia))				{
					$incidencias_in_article = strval($incidencias_in_article).",".$incidencia_id;
					$query = $db
					->getQuery(true)
					->update('#__fields_values')
					->set("value = '".$incidencias_in_article."'")               // increment the row's value
					->where($db->quoteName('item_id') . " = " . $db->quote($equipamiento)) //id Espacio Natural
					->where($db->quoteName('field_id') . " = " . $db->quote($field_incidencias)); //id cf field enp-puntos-de-interes

					// Reset the query using our newly populated query object.
					$db->setQuery($query);
					$incidenciaInArticle = $db->execute();
				}
				
			}
			
		}
		protected function cleanIncidencesInArticles(){

			//Search all item _id and the value (codigo incidence) already exists
			$db = Factory::getDbo();
			$incidence_in_equipment_field = 254;
			$incidence_in_itinerary = 253;
			// Create a new query object.
			$query = "delete from sooeg_fields_values fv where fv.field_id = '".$incidence_in_equipment_field."' or fv.field_id = '".$incidence_in_itinerary."'";
			$db->setQuery($query);
			//Get all incidencias already in database
			$incidencia_exists = $db->execute();

		}

		protected function checkOldIncidences($incidences, $key){
			if($key == 'equipments'){
				$categories = "(91,191,192)";
			}elseif($key == 'routes'){
				$categories = "(92,193,194)";
			}
			//Search all item _id and the value (codigo incidence) already exists
			$db = Factory::getDbo();
			$codigoAIncidence_field = 234;
			// Create a new query object.
			// $query = $db
			// ->getQuery(true)
			// ->select(array('item_id','value'))
			// ->from($db->quoteName('#__fields_values'))
			// ->where($db->quoteName('field_id') . " = " . $db->quote($codigoAIncidence_field)); //id cf codigo incidence
			// $db->setQuery($query);
			$query =	"select fv.item_id,fv.value from sooeg_fields_values fv left join sooeg_content sc on sc.id = fv.item_id::integer
						where fv.field_id = '".$codigoAIncidence_field."' and sc.catid IN ".$categories."";
						//dd($query);
			$db->setQuery($query);
			$incidencesCode = [];
			foreach($incidences as $incidence){
				$incidencesCode[] = $incidence->properties->codigo_incidencia;
			}
			($incidence_exists = $db->loadObjectList());

			foreach($incidence_exists as $exists){
				if (!in_array($exists->value, $incidencesCode))
				{
					$query = $db
					->getQuery(true)
					->update('#__content')
					->set("state = -2")               // increment the row's value
					->where($db->quoteName('id') . " = " . $db->quote($exists->item_id)) ;//id Espacio Natural
					$db->setQuery($query);
					$db->execute();
				}
			}
		}

		protected function sendEmailSync($error){

			date_default_timezone_set('Europe/Madrid');
			$dateTime = strftime("%Y-%m-%d %X");
			$body ="Ha habido un error en la sincronización de incidencias, parece que vuestro servicio no está funcionando correctamente a las ".$dateTime.".";
			$subject = "Error ".$error." en la actualización de incidencias";
			$to = Factory::getUser()->email;
			$config = Factory::getConfig();
			$mailfrom = $config->get('mailfrom');
			$fromname =  $config->get('fromname');
			$from = array($mailfrom, $fromname);
	
			# Invoke JMail Class
			$mailer = Factory::getMailer();
			
			# Set sender array so that my name will show up neatly in your inbox
			$mailer->setSender($from);
	
			# Add a recipient -- this can be a single address (string) or an array of addresses
			$mailer->addRecipient($to);

			$config = Factory::getConfig();
			$mailer->setSubject($subject);
			$mailer->setBody($body);
			# Send once you have set all of your options
			if($mailer->send()){
				$response['success'] = "true";
				$response['message'] = "Email enviado correctamente";
				
			} else {
				$response['success'] = "false";
				$response['message'] = "No se ha podido enviar el mail";
			}
			// $db = Factory::getDbo();

			// 	$query = $db
			// 	->getQuery(true)
			// 	->update('#__sg_syncro_vente')
			// 	->set("end_sync_date = '".$dateTime."'")//current date
			// 	->where('id = ' . $sync_id.'');

			// 	// Reset the query using our newly populated query object.
			// 	($db->setQuery($query));
			// 	$db->execute();
			die(json_encode($response));
		}
}
