<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgsyncvente\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Arcgis\ArcgisClient;
use Joomla\CMS\Language\Associations;

/**
 * The users controller
 *
 * @since  4.0.0
 */
class SyncequipamientosController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function syncEquipamientos (){
			ini_set('memory_limit','2048M');
			ini_set('max_execution_time','3600');

			$db = Factory::getDbo();

			$sync_id = $this->input->get('sync_id',null,'STRING');

			
			//Check if we have equipamientos search by codigo-equipamiento cfield
			$codigoEquipamiento_field = 146;
			$query = $db
			->getQuery(true)
			->select('COUNT(*)')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('field_id') . " = " . $db->quote($codigoEquipamiento_field));	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			$import_content = $db->loadResult();

			//if we have some equipamientos, the we want to get ONLY newer content
			if($import_content > 0){
				//The last_date_edited ID in JOOMLA
				$last_date_edited = 136;
				// Create a new query object.
				$query = "select MAX(value) from sooeg_fields_values fv 
				left join sooeg_content sc on sc.id = fv.item_id::int
				where fv.field_id = '".$last_date_edited."' and sc.catid in (48,49,50,51,60,65)";
				// Reset the query using our newly populated query object.
				$db->setQuery($query);
				$have_last_edited = $db->loadResult();
				$where = "last_edited_date  >'".date("Ymd h:i:s",strtotime($have_last_edited))."'";
			}
			//if no equipamientos, get full equipamientos
			else{
				$where = "1=1";
			}
			
			//If want to update all equipamientos, then set true
			$importar_de_nuevo = false;
			if($importar_de_nuevo) $where = "1=1";

			//LLamada a arcgisclient
			$body = array('where' => $where,'objectIds' => '','time' => '','geometry' => '','geometryType' => 'esriGeometryEnvelope','inSR' => '','spatialRel' => 'esriSpatialRelIntersects','distance' => '','meters' => 'esriSRUnit_Meter','relationParam' => '','outFields' => '*','returnGeometry' => 'true','maxAllowableOffset' => '','geometryPrecision' => '','outSR' => '','having' => '','gdbVersion' => '','historicMoment' => '','returnDistinctValues' => 'false','returnIdsOnly' => 'false','returnCountOnly' => 'false','returnExtentOnly' => 'false','orderByFields' => '','groupByFieldsForStatistics' => '','outStatistics' => '','returnZ' => 'false','returnM' => 'false','multipatchOption' => 'xyFootprint','resultOffset' => '','resultRecordCount' => '','returnTrueCurves' => 'false','returnExceededLimitFeatures' => 'false','quantizationParameters' => '','returnCentroid' => 'false','sqlFormat' => 'none','resultType' => '','featureEncoding' => 'esriDefault','f' => 'geojson');
			$config = Factory::getConfig();
			$client = new ArcgisClient($config->get('venteServicesUrl'));
			$path = '/server/rest/services/ciudadanos/v_ext_equipamientos/FeatureServer/0/query';
			$response = $client->post($path, $body);
			//Check for errors
			if(isset($response->error) && isset($response->error->code)){
				$error = $response->error->code;
				if($error == 400){
					$this->sendEmailSync($error);
				}
				elseif($error == 500){
					$this->sendEmailSync($error);
				}
			};

			//Fin llamada a arcgisclient
			$equipamientos = $response->features;
			//dd($equipamientos);
			if(empty($equipamientos)){
				die("Content already up to date");
			};
			$app = Factory::getApplication();			
	
			//superadmin user id
			$user_id = 848;
		
			$articleModel = $app->bootComponent('com_content')
			->getMVCFactory()->createModel('Article', 'Administrator');
			
			$languagesCategory = [
				'es-ES' => [
					'48' => 48,
					'49' => 49,
					'50' => 50,
					'51' => 51,
					'60' => 60,
					'65' => 65
				],
				'de-DE' => [
					'48' => 109,
					'49' => 111,
					'50' => 114,
					'51' => 115,
					'60' => 119,
					'65' => 122
				],
				'en-GB' => [
					'48' => 110,
					'49' => 112,
					'50' => 113,
					'51' => 116,
					'60' => 120,
					'65' => 121
				],
			];

			foreach($equipamientos as $equipo){
			
				$cat_id = $this->findOrCreateCategory($equipo,$user_id);
				//Check if already exists
				$equipamiento_exists = $this->checkIfExists($equipo);
				//Conexiones con itinerarios
				$conexionesIds = $this->getConexiones($equipo->properties->conexiones);
				//Municipio meteo
				$municipio_meteo = $this->getMunicipioId($equipo->properties->municipio);
				//Get puntos de interés
				$puntos_interes = $this->getPuntosInteres($equipo->properties->puntos_interes);
				//Get espacios naturales
				$enp =  $this->getEspaciosNaturales($equipo->properties->enp);

				if( sizeof($equipamiento_exists) > 0) {
					foreach($equipamiento_exists as $equip_id){
						$equipamientoData = $this->getEquipamientoData($equip_id->item_id);

						$article = [
							"catid" => $languagesCategory[$equipamientoData['article_data'][0]->language][$cat_id],
							//"title"		=> $equipamientoData['article_data'][0]->title,
							"title"		=> $equipamientoData['article_data'][0]->title,
							"id"	=> (int)$equip_id->item_id,
							'created_user_id' => $user_id,
							'created_by_alias'=> '',
							'alias' => $equipamientoData['article_data'][0]->alias,
							'language' => $equipamientoData['article_data'][0]->language,
							'metakey' => '',
							//'metadesc' => $equipamientoData['article_data'][0]->metadesc,
							'state' => $equipamientoData['article_data'][0]->state,
							'access' => 1,
							"com_fields" => array(
								"codigo-equipamiento" => $equipo->properties->codigo_equipamiento,
								"equipamiento-latitud" => $equipo->geometry->coordinates[1],
								"equipamiento-longitud" => $equipo->geometry->coordinates[0],
								"municipio" => $equipo->properties->municipio,
								"gestor-id" => $equipo->properties->gestor_equipamiento,
								"vid-equipamiento" => $equipo->properties->vid,
								"last-edited-date" => $equipo->properties->last_edited_date != null ? date('Y-m-d H:i:s', substr($equipo->properties->last_edited_date,0,10)) : "",
								"conexiones-con-otras-rutas" => implode(',',$conexionesIds),
								"aforo-capacidad" => $equipo->properties->aforo_maximo,
								"puntos-y-valores-de-interes" => $puntos_interes,
								"espacio-natural-protegido" => implode(',',$enp),
								"municipio-meteo" => $municipio_meteo,
							)
							];

						//dump($article);
						Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');
		
						// Needs to be set because com_fields needs the data in jform to determine the assigned catid
						$this->input->set('jform', $article);
						$form = $articleModel->getForm($article, false);
		
						$validData = $articleModel->validate($form, $article);
						
						$articleModel->save($validData);

					}
				}
				else{

					if( (!empty($equipo->properties->nombre_equipamiento)) && ($equipo->properties->nombre_equipamiento != " ") ){
						$article = [
							"catid" => $cat_id,
							"title"		=> $equipo->properties->nombre_equipamiento,
							"id"	=> 0,
							'created_user_id' => $user_id,
							'created_by_alias'=> '',
							'alias' => ApplicationHelper::stringURLSafe($equipo->properties->nombre_equipamiento),
							'language' => 'es-ES',
							'metakey' => '',
							'metadesc' => '',
							'introtext' => '',
							'state' => 0,
							'access' => 1,
							"com_fields" => array(
								"codigo-equipamiento" => $equipo->properties->codigo_equipamiento,
								"equipamiento-latitud" => $equipo->geometry->coordinates[1],
								"equipamiento-longitud" => $equipo->geometry->coordinates[0],
								"municipio" => $equipo->properties->municipio,
								"gestor-id" => $equipo->properties->gestor_equipamiento,
								"vid-equipamiento" => $equipo->properties->vid,
								"last-edited-date" => $equipo->properties->last_edited_date != null ? date('Y-m-d H:i:s', substr($equipo->properties->last_edited_date,0,10)) : "",
								"conexiones-con-otras-rutas" => implode(',',$conexionesIds),
								"aforo-capacidad" => $equipo->properties->aforo_maximo,
								"puntos-y-valores-de-interes" => $puntos_interes,
								"espacio-natural-protegido" => implode(',',$enp),
								"municipio-meteo" => $municipio_meteo,
							)
							];

						//dump($article);
						Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');
		
						// Needs to be set because com_fields needs the data in jform to determine the assigned catid
						$this->input->set('jform', $article);
						$form = $articleModel->getForm($article, false);
		
						($validData = $articleModel->validate($form, $article));
						
						$articleModel->save($validData);
					}
						


				}

			};
			if($sync_id){
				$this->sendEmailSync($sync_id);	
			}
	
	
	}
	
	protected function findOrCreateCategory($equipo,$user_id){
		//dd($equipo);
		$cat_equipamientos = 39;
		$field_tabla_vente = 137;
		$field_id_equipamiento_recreativo = 138;
		// Get a db connection.
		$db = Factory::getDbo();

		// Check if the tabla value exists in any category


		$query = "SELECT fv.item_id FROM sooeg_fields_values fv 
		LEFT JOIN sooeg_categories sc on sc.id = fv.item_id::int
		WHERE fv.value  = '".$equipo->properties->tabla."' and fv.field_id = '".$field_tabla_vente."'
		and sc.language = 'es-ES'";

		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		if($tabla_exists_id = $db->loadResult()){
			if( !is_null($tabla_exists_id) && $equipo->properties->tabla == "instalacion_recreativa"){

				$query = "SELECT fv.item_id FROM sooeg_fields_values fv 
							LEFT JOIN sooeg_categories sc on sc.id = fv.item_id::int
							WHERE fv.value = '".$equipo->properties->tipo_instalacion_recreativa_id."' and fv.field_id = '".$field_id_equipamiento_recreativo."'
							and sc.language = 'es-ES'";
					$db->setQuery($query);
					$tabla_exists_id = $db->loadResult();	
			}
		}

		if( !is_null($tabla_exists_id) ){
			$cat_id = $tabla_exists_id;
			return $cat_id;
		}
		
	}

	protected function checkIfExists($equipo){

		$codigoEquipamiento_field = 146;
		//Check if the equipamiento already exists in JOOMLA
		// Get a db connection.
		$db = Factory::getDbo();

		// Create a new query object.
		$query = $db
		->getQuery(true)
		->select('item_id')
		->from($db->quoteName('#__fields_values'))
		->where($db->quoteName('field_id') . " = " . $db->quote($codigoEquipamiento_field))
		->where($db->quoteName('value') . " = " . $db->quote($equipo->properties->codigo_equipamiento));


		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$equipamiento_exists = $db->loadObjectList();
		if( (sizeof($equipamiento_exists) > 1) ){
			$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $equipamiento_exists[0]->item_id);
			$associated_items = [];
			foreach($associations as $association){
				//dump();
				$associated_items[]['item_id'] = (explode(":", $association->id)[0]);
			}
			if(!empty($associated_items)){
				$equipamiento_exists = json_decode(json_encode($associated_items));
			};
			
		}
		return $equipamiento_exists;

	}

	protected function getEquipamientoData($equip_id){

		// Get a db connection.
		$db = Factory::getDbo();
		$query = $db
			->getQuery(true)
			->select(array(('title,introtext,alias,state,metadesc,language')))
			->from($db->quoteName('#__content'))
			->where($db->quoteName('id') . " = " . $db->quote($equip_id));
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);	
			$equip_data['article_data'] = $db->loadObjectList();

			return $equip_data;

	}

	function getConexiones($conexiones){
			$conexionesMatriculas = str_replace('"','',$conexiones);
			$conexionesMatriculas = str_replace('{','',$conexionesMatriculas);				
			$conexionesMatriculas = str_replace('}','',$conexionesMatriculas);				
			$conexionesMatriculas = explode(',', $conexionesMatriculas);
			$conexionesIds = [];

			foreach ($conexionesMatriculas as $matricula){
				$db = Factory::getDbo();
				$matriculaFieldId = 140;
				$query = "SELECT item_id FROM sooeg_fields_values fv
							left join sooeg_content sc on sc.id = fv.item_id::int
							WHERE fv.field_id = '".$matriculaFieldId."' and fv.value = '".$matricula."' and sc.language = 'es-ES'";
					$db->setQuery($query);
					if(!is_null($id = $db->loadResult())){
						$conexionesIds[] = $id;
					}
					
			}
			return $conexionesIds;
	}
	protected function getPuntosInteres($punto){

		
		$puntos_de_interes ="";
		//Check if field is not empty 
		if(!empty($punto)){
						
			$puntosinteres_field = 204;
			// Get a db connection.
			$db = Factory::getDbo();
			$puntos_de_interes_raw = explode(',',str_replace('{', '', str_replace('"', '', str_replace('}', '', $punto))));
			foreach($puntos_de_interes_raw as $punto){

				$query = "SELECT fv.item_id FROM sooeg_fields_values fv
				LEFT JOIN sooeg_content sc on sc.id = fv.item_id::int
				WHERE fv.field_id = '".$puntosinteres_field."' and fv.value = '".$punto."'
				and sc.language = 'es-ES'"; //value codigo ruta
		
				// Reset the query using our newly populated query object.
				$db->setQuery($query);
		
				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				$item_id = $db->loadResult();
				$puntos_de_interes = $puntos_de_interes.$item_id.",";
		
			}
		
			return substr_replace($puntos_de_interes, "", -1);

		}
		else{
			return $puntos_de_interes;
		}
		

	}

	protected function getEspaciosNaturales($espacio){
		
		$espacios_naturales = [];
		//Check if field is not empty 
		if(!empty($espacio)){
			
			// Get a db connection.
			$db = Factory::getDbo();
			$espacios_naturales_raw = explode(',',str_replace('{', '', str_replace('"', '', str_replace('}', '', $espacio))));
			foreach($espacios_naturales_raw as $espacio){

				$query = "SELECT id FROM sooeg_content where title @@ plainto_tsquery('";
					$query .= " ".$espacio." |";
				
				$query .= "') and language = 'es-ES' and catid = 67";
				// Reset the query using our newly populated query object.
				$db->setQuery($query);
		
				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				$espacio_id = $db->loadResult();
				if(!is_null($espacio_id)) {
					$espacios_naturales [] = $espacio_id;
				}

			}

			return $espacios_naturales;

		}
		else{
			return $espacios_naturales;
		}
		

	}
	protected function getMunicipioId($municipio){
		
		//Check if field is not empty 
		if(!empty($municipio)){
			
			$municipioCat = 88; 
			// Get a db connection.
			$db = Factory::getDbo();
			
			$query = $db
			->getQuery(true)
			->select('id')
			->from($db->quoteName('#__content'))
			->where($db->quoteName('title') . " = " . $db->quote($municipio))
			->where($db->quoteName('catid') . " = " . $db->quote($municipioCat)); 
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
	
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$municipioId = $db->loadResult();

			return $municipioId;

		}

	}
	protected function sendEmailSync($error){

		date_default_timezone_set('Europe/Madrid');
		$dateTime = strftime("%Y-%m-%d %X");
		$body ="Ha habido un error en la actualización de equipamientos realizada a ".$dateTime.".";
		$subject = "Error ".$error." en la actualización equipamientos";
		//$to = "ivolado@gmail.com";
		$to = $user = Factory::getUser()->email;
		$config = Factory::getConfig();
		$mailfrom = $config->get('mailfrom');
		$fromname =  $config->get('fromname');
		$from = array($mailfrom, $fromname);

		# Invoke JMail Class
		$mailer = Factory::getMailer();
		
		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$config = Factory::getConfig();
		$mailer->setSubject($subject);
		$mailer->setBody($body);
		# Send once you have set all of your options
		if($mailer->send()){
			$response['success'] = "true";
			$response['message'] = "Email enviado correctamente";
			
		} else {
			$response['success'] = "false";
			$response['message'] = "No se ha podido enviar el mail";
		}
		// $db = Factory::getDbo();

		// 	$query = $db
		// 	->getQuery(true)
		// 	->update('#__sg_syncro_vente')
		// 	->set("end_sync_date = '".strftime("%Y-%m-%d %X")."'")//current date
		// 	->where('id = ' . $sync_id.'');

		// 	// Reset the query using our newly populated query object.
		// 	($db->setQuery($query));
		// 	$db->execute();
		die(json_encode($response));
	}
}
