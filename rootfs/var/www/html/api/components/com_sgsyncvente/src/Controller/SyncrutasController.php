<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgsyncvente\Api\Controller;

\defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Arcgis\ArcgisClient;
use Joomla\CMS\Language\Associations;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SyncrutasController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function syncRutas (){
		ini_set('memory_limit','2048M');
		ini_set('max_execution_time','3600');

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

		//Check if we have itinerarios search by codigo-rutas cfield
			$db = Factory::getDbo();
			$sync_id = $this->input->get('sync_id',null,'STRING');

			$codigoRuta_field = 56;
			$query = $db
			->getQuery(true)
			->select('COUNT(*)')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('field_id') . " = " . $db->quote($codigoRuta_field));	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
	
			$import_content = $db->loadResult();

			//if we have some itinerarios, the we want to get ONLY newer content
			if($import_content > 0){
				//The last_date_edited ID in JOOMLA
				$last_edited_date_ruta = 136;
				// We query last modified date from all articles with category sendero, bica o vm in spanish, just because in other languages this field is copied from spanish.
				$query = "select MAX(value) from sooeg_fields_values fv 
							left join sooeg_content sc on sc.id = fv.item_id::int
							where fv.field_id = '".$last_edited_date_ruta."' and sc.catid in (61,62,63)";
				// Reset the query using our newly populated query object.
				$db->setQuery($query);

				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				$have_last_edited = $db->loadResult();
				//set where statement for query
				$where = "last_edited_date  >'".date("Ymd h:i:s",strtotime($have_last_edited))."'";
				//$where = "last_edited_date  >'2022-08-11 13:26:00'";
			}
			//if no equipamientos, get full equipamientos
			else{
				$where = "1=1";
			}
			//If want to update all rutas, then set true
			$importar_de_nuevo = false;
			if($importar_de_nuevo) $where = "1=1";

			//LLamada a arcgisclient
			$body = array('where' => $where,'text' => '','objectIds' => '','time' => '','geometry' => '','geometryType' => 'esriGeometryEnvelope','inSR' => '','spatialRel' => 'esriSpatialRelIntersects','relationParam' => '','outFields' => '*','returnGeometry' => 'true','returnTrueCurves' => 'false','maxAllowableOffset' => '','geometryPrecision' => '','outSR' => '','having' => '','returnIdsOnly' => 'false','returnCountOnly' => 'false','orderByFields' => '','groupByFieldsForStatistics' => '','outStatistics' => '','returnZ' => 'false','returnM' => 'false','gdbVersion' => '','historicMoment' => '','returnDistinctValues' => 'false','resultOffset' => '','resultRecordCount' => '','queryByDistance' => '','returnExtentOnly' => 'false','datumTransformation' => '','parameterValues' => '','rangeValues' => '','quantizationParameters' => '','featureEncoding' => 'esriDefault','f' => 'geojson');
			$config = Factory::getConfig();
			$client = new ArcgisClient($config->get('venteServicesUrl')); 
			$path = '/server/rest/services/ciudadanos/v_ext_rutas/FeatureServer/0/query';
			$response = $client->post($path, $body);

			//Check for errors
			if(isset($response->error) && isset($response->error->code)){
				$error = $response->error->code;
				if($error == 400){
					$this->sendEmailSync($error);
				}
				elseif($error == 500){
					$this->sendEmailSync($error);
				}
			};

			//Fin llamada a arcgisclient
			$rutas = $response->features;
			
			//dd($rutas);
			$app = Factory::getApplication();			
	
			//superadmin user id
			$user_id = 848;
		
			$languagesCategory = [
				'es-ES' => [
					'61' => 61,
					'62' => 62,
					'63' => 63
				],
				'de-DE' => [
					'61' => 124,
					'62' => 131,
					'63' => 133
				],
				'en-GB' => [
					'61' => 123,
					'62' => 132,
					'63' => 134
				],
			];

			foreach($rutas as $key => $ruta){			
				
				try{
					try{
						$cat_id = $this->findOrCreateCategory($ruta);
					}
					catch(Exception $e){
						error_log($e->getMessage());
					}
					try{
						//Check if already exists
						$ruta_exists = $this->checkIfExists($ruta);
					}
					catch(Exception $e){
						error_log($e->getMessage());
					}
					try{					
						//Get puntos de interés
						$puntos_interes = $this->getPuntosInteres($ruta->properties->puntos_interes);
					}
					catch(Exception $e){
						error_log($e->getMessage());
					}
					try{
						//Get espacios naturales
						$enps =  $this->getEspaciosNaturales($ruta->properties->enp);
					}
					catch(Exception $e){
						error_log($e->getMessage());
					}
					//
					try{
						//Get zonas de tenerife
						$zonas =  $this->getZonasTenerife($ruta->properties->zona_up);
					}
					catch(Exception $e){
						error_log($e->getMessage());
					}
					try{					
						//Get conexiones
						$conexionesIds = $this->getConexiones($ruta->properties->conexiones);
					}
					catch(Exception $e){
						error_log($e->getMessage());
					}
					try{
						//Municipio meteo
						$municipio_meteo = $this->getMunicipioId($ruta->properties->municipio_meteo);
					}
					catch(Exception $e){
						error_log($e->getMessage());
					}

					//Save geometry in json format
					if (isset($linestring)) {
						unset($linestring);
					}
					$linestring = '{"name":"'.$ruta->properties->nombre.'","type":"FeatureCollection","features":['.json_encode($ruta).']}';
					$original_id = 0;
					//If article already exists, get ids for all languages
	
					$articleModel = $app->bootComponent('com_content')
					->getMVCFactory()->createModel('Article', 'Administrator');
						
					if( sizeof($ruta_exists) > 0) {

						foreach($ruta_exists as $ruta_id){
	
							($itinerarioData = $this->getItinerarioData($ruta_id->item_id));
							//dd($languagesCategory[$itinerarioData['article_data'][0]->language][$cat_id]);
							$article = [
								"catid" => $languagesCategory[$itinerarioData['article_data'][0]->language][$cat_id],
								"title"		=> $itinerarioData['article_data'][0]->title,
								"id"	=> (int)$ruta_id->item_id,
								'created_user_id' => $user_id,
								'created_by_alias'=> '',
								'alias' => $itinerarioData['article_data'][0]->alias,
								'language' => $itinerarioData['article_data'][0]->language,
								'metakey' => '',
								'state' => $itinerarioData['article_data'][0]->state,
								'access' => 1,
								"com_fields" => array(
									"object-id" => $ruta->properties->objectid,
									"codigo" => $ruta->properties->codigo,
									"matricula" => $ruta->properties->matricula,
									// "inicio" => $ruta->properties->inicio,
									// "fin" => $ruta->properties->fin,
									"modalidad" => $ruta->properties->modalidad,
									"desnivel-acumulado-en-ascenso" => $ruta->properties->desnivel_acumulado1,
									"desnivel-acumulado-en-descenso" => $ruta->properties->desnivel_acumulado2,
									"longitud" => $ruta->properties->distancia,
									"altitud-minima" => $ruta->properties->altura_minima,
									"altitud-maxima" => $ruta->properties->altura_maxima,
									"municipios-que-transita" => $ruta->properties->municipios,
									"municipio-meteo" => $municipio_meteo,
									"conexiones-con-otras-rutas" => implode(',',$conexionesIds),
									"linestring" => $linestring,
									"last-edited-date" => $ruta->properties->last_edited_date != null ? date('Y-m-d H:i:s', substr($ruta->properties->last_edited_date,0,10)) : "",
									"valores-y-puntos-de-interes" => $puntos_interes,
									"espacio-natural-protegido" =>implode(',',$enps),
									"zonas-tenerife" => $zonas,
									"internal-id-sendero" => $ruta->properties->id
									)
								];
	
							if($itinerarioData['article_data'][0]->language == 'es-ES'){
								$original_id = (int)$ruta_id->item_id;
							}
							//dd($article);
							Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');
			
							// Needs to be set because com_fields needs the data in jform to determine the assigned catid
							$this->input->set('jform', $article);
							$form = $articleModel->getForm($article, false);
			
							($validData = $articleModel->validate($form, $article));
							
							$articleModel->save($validData);
							error_log($articleModel->getItem()->id." - ".$ruta->properties->codigo." mem: ". round(memory_get_usage() / 1024)." key: ".$key,0);	
	
						}
					}
					else{
	
						//foreach($languagesCategory as $key => $cat){
	
							$article = [
								"catid" => $languagesCategory['es-ES'][$cat_id],
								"title"		=> $ruta->properties->nombre,
								"id"	=> 0,
								'created_user_id' => $user_id,
								'created_by_alias'=> '',
								'alias' => ApplicationHelper::stringURLSafe($ruta->properties->nombre),
								'language' => 'es-ES',
								'metakey' => '',
								'metadesc' => '',
								'introtext' => '',
								'state' => 0,
								'access' => 1,
								"com_fields" => array(
									"object-id" => $ruta->properties->objectid,
									"codigo" => $ruta->properties->codigo,
									"matricula" => $ruta->properties->matricula,
									"inicio" => $ruta->properties->inicio,
									"fin" => $ruta->properties->fin,
									"modalidad" => $ruta->properties->modalidad,
									"desnivel-acumulado-en-ascenso" => $ruta->properties->desnivel_acumulado1,
									"desnivel-acumulado-en-descenso" => $ruta->properties->desnivel_acumulado2,
									"longitud" => $ruta->properties->distancia,
									"altitud-minima" => $ruta->properties->altura_minima,
									"altitud-maxima" => $ruta->properties->altura_maxima,
									"municipios-que-transita" => $ruta->properties->municipios,
									"municipio-meteo" => $municipio_meteo,
									"conexiones-con-otras-rutas" => implode(',',$conexionesIds),
									"linestring" => $linestring,
									"last-edited-date" => $ruta->properties->last_edited_date != null ? date('Y-m-d H:i:s', substr($ruta->properties->last_edited_date,0,10)) : "",
									"valores-y-puntos-de-interes" => $puntos_interes,
									"espacio-natural-protegido" => implode(',',$enps),
									"zonas-tenerife" => $zonas,
									"internal-id-sendero" => $ruta->properties->codigo
									)
								];
	
							//dd($article);
							Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');
			
							// Needs to be set because com_fields needs the data in jform to determine the assigned catid
							$this->input->set('jform', $article);
							$form = $articleModel->getForm($article, false);
			
							$validData = $articleModel->validate($form, $article);
							
							$articleModel->save($validData);
							$original_id = $articleModel->getItem()->id;
	
						//}
	
					}
					//If punto de interes is in a espacio natural
					if( !is_null($enps) || empty($enps) ){
	
						$field_enp_itinerarios = 219;
	
						foreach($enps as $enp){
	
							if(!empty($enp)){
								
								$db = Factory::getDbo();
								//Check if this espacio natural has already itinerarios 
								$query = $db
								->getQuery(true)
								->select('value')
								->from($db->quoteName('#__fields_values'))
								->where($db->quoteName('item_id') . " = " . $db->quote($enp)) //id Espacio Natural
								->where($db->quoteName('field_id') . " = " . $db->quote($field_enp_itinerarios)); //id cf field enp-puntos-de-interes
	
								// Reset the query using our newly populated query object.
								$db->setQuery($query);
								$itinerarios_in_enp = $db->loadResult();
	
								//if this itinerarios
								if(is_null($itinerarios_in_enp)){
									
										$columns = array('field_id', 'item_id', 'value');
										$values = $field_enp_itinerarios.', '.$enp.', '.$original_id;
										$query = $db
										->getQuery(true)
										->insert($db->quoteName('#__fields_values'))
										->columns($db->quoteName($columns))
										->values(''.$values.'');
	
										// Reset the query using our newly populated query object.
										$db->setQuery($query);
	
								}
								else{
									//Get the ID of itinerarios already in the espacio natural
									$existingItinerarios = explode(",", $itinerarios_in_enp);
									
									//If this punto de interes does not already exixts in the espacio natural
									if (!in_array($original_id, $existingItinerarios))				{
										$puntosdeinteres_in_espacio = strval($itinerarios_in_enp).",".$original_id;
										$query = $db
										->getQuery(true)
										->update('#__fields_values')
										->set("value = '".$puntosdeinteres_in_espacio."'")               // increment the row's value
										->where($db->quoteName('item_id') . " = " . $db->quote($enp)) //id Espacio Natural
										->where($db->quoteName('field_id') . " = " . $db->quote($field_enp_itinerarios)); //id cf field enp-puntos-de-interes
	
										// Reset the query using our newly populated query object.
										$db->setQuery($query);
									}
									
								}
							}
	
	
						}
						
						
			
						$espacio_with_puntosdeinteres = $db->execute();
					}
	
					//IMPORT TO GIS
					if(!is_null( $ruta->geometry )){
						if ($ruta->geometry->type == "LineString") {
							//Check if this linestring is already on table
							$alreadyExists = 0;
							try{
								$query = $db
								->getQuery(true)
								->select('COUNT(*)')
								->from($db->quoteName('public.itineraries'))
								->where($db->quoteName('j_id') . " = " . $db->quote($original_id)); //value codigo ruta
								// Reset the query using our newly populated query object.
								$db->setQuery($query);
								$alreadyExists = $db->loadResult();
							}
							catch(Exception $e){
								error_log($e->getMessage()." aqui 1 ".$original_id, $alreadyExists);
							}

							if($alreadyExists == 0){
								$lineString = '';
								$lineStringZ = '';
								foreach ($ruta->geometry->coordinates as $row) {
									$lineString = $lineString . $row[0] . ' ' . $row[1]. ',';
									$lineStringZ = $lineStringZ . $row[0] . ' ' . $row[1]. ' ' .  $row[2] . ',';
								}
								$lineString = substr($lineString, 0, -1);
								$lineStringZ = substr($lineStringZ, 0, -1);
			
								$columns = array('j_id', 'name', 'the_geomz', 'type', 'the_geom');
								($values = $original_id.", '".$ruta->properties->nombre."', 'SRID=4326;LINESTRINGZ(" . $lineStringZ . ")', ".$ruta->properties->clase.", 'SRID=4326;LINESTRING(" . $lineString . ")'");
								try{
									$query = $db
									->getQuery(true)
									->insert($db->quoteName('public.itineraries'))
									->columns($db->quoteName($columns))
									->values(''.$values.'');
				
									// Reset the query using our newly populated query object.
									($db->setQuery($query));
									$importToGis = $db->execute();
								}

								catch(Exception $e){
									error_log($e->getMessage()." aqui 2");
								}
								try{
									$query = $db
									->getQuery(true)
									->update('public.itineraries')
									->set("length = ST_Length(the_geom::geography)")               // increment the row's value
									->where($db->quoteName('j_id') . " = " . $db->quote($original_id)); //id cf field enp-puntos-de-interes
				
									// Reset the query using our newly populated query object.
									$db->setQuery($query);
									$updateLength = $db->execute();
								}
								catch(Exception $e){
									error_log($e->getMessage()." aqui 3");
								}

							}
							else{
								$lineString = '';
								$lineStringZ = '';
								foreach ($ruta->geometry->coordinates as $row) {
									$lineString = $lineString . $row[0] . ' ' . $row[1]. ',';
									$lineStringZ = $lineStringZ . $row[0] . ' ' . $row[1]. ' ' .  $row[2] . ',';
								}
								$lineString = substr($lineString, 0, -1);

								$lineStringZ = substr($lineStringZ, 0, -1);
			
								try{

									($query = "UPDATE public.itineraries
									SET the_geom='SRID=4326;LINESTRING(".$lineString.")'::public.geometry,the_geomz='SRID=4326;LINESTRINGZ(".$lineStringZ.")'::public.geometry
									WHERE j_id=".$original_id);

									$db->setQuery($query);
									($updateGeom = $db->execute());
								}

								catch(Exception $e){
									error_log($e->getMessage()." aqui 4");
								}
								try{
									//Update length with new geom value
									$query = $db
									->getQuery(true)
									->update('public.itineraries')
									->set("length = ST_Length(the_geom::geography)")               // increment the row's value
									->where($db->quoteName('j_id') . " = " . $db->quote($original_id)); //id cf field enp-puntos-de-interes
				
									// Reset the query using our newly populated query object.
									$db->setQuery($query);
									$updateLength = $db->execute();
								}
								catch(Exception $e){
									error_log($e->getMessage()." aqui 5");
								}

							}
							
		
						}	
					}
					//../IMPORT TO GIS
				}
				catch(Exception $e) {
					error_log('Message: ' .$e->getMessage());
					error_log($original_id." - ".$ruta->properties->codigo." mem: ". round(memory_get_usage() / 1024)." key: ".$key,0);	
				}

				
			};
			if(sizeof($rutas) > 0 ) {
				$this->updateRoutingRables();
			}
			die("Done");

		}
	
		protected function findOrCreateCategory($ruta){

			$cat_otras_rutas = 64;
			$field_id_ruta_clase = 139;
			// Get a db connection.
			$db = Factory::getDbo();
	
			// Check if the clase id match a itinerarios subcategory
			$query = "SELECT fv.item_id FROM sooeg_fields_values fv 
						LEFT JOIN sooeg_categories sc on sc.id = fv.item_id::int
						WHERE fv.value  = '".$ruta->properties->clase."' and fv.field_id = '".$field_id_ruta_clase."'
						and sc.language = 'es-ES'";

			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$ruta_clase_exists_id = $db->loadResult();

			unset($db);
			if( !is_null($ruta_clase_exists_id) ){
				
				$cat_id = $ruta_clase_exists_id;
				return $cat_id;
			}
			else{
				return $cat_otras_rutas;
			}
		}
	
		protected function checkIfExists($ruta){
	
			$codigoruta_field = 279;
			//Check if the ruta already exists in JOOMLA
			// Get a db connection.
			$db = Factory::getDbo();
	
			// Create a new query object.
			$query = $db
			->getQuery(true)
			->select('item_id')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('field_id') . " = " . $db->quote($codigoruta_field)) //id cf codigo ruta
			->where($db->quoteName('value') . " = " . $db->quote($ruta->properties->id)); //value codigo ruta
	
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			$ruta_exists = $db->loadObjectList();

			if( (sizeof($ruta_exists) > 0) ){
				$associations = associations::getAssociations('com_content', '#__content', 'com_content.item', $ruta_exists[0]->item_id);
				$ruta_exists = [];
				foreach($associations as $association){
					$ruta_exists[]['item_id'] = explode(":", $association->id)[0];
				}
				$ruta_exists = json_decode(json_encode($ruta_exists));
			}
			unset($db);
			return $ruta_exists;
	
		}

		protected function getItinerarioData($ruta_id){

			// Get a db connection.
			$db = Factory::getDbo();
			$query = $db
				->getQuery(true)
				->select(array(('title,introtext,alias,state,metadesc,language')))
				->from($db->quoteName('#__content'))
				->where($db->quoteName('id') . " = " . $db->quote($ruta_id));
		
				// Reset the query using our newly populated query object.
				$db->setQuery($query);	
				$ruta_data['article_data'] = $db->loadObjectList();
			unset($db);
				return $ruta_data;

		}

		protected function getPuntosInteres($punto){

			
			$puntos_de_interes ="";
			//Check if field is not empty 
			if(!empty($punto)){
							
				$puntosinteres_field = 204;
				// Get a db connection.
				$db = Factory::getDbo();
				$puntos_de_interes_raw = explode(',',str_replace('{', '', str_replace('"', '', str_replace('}', '', $punto))));
				foreach($puntos_de_interes_raw as $punto){
 
					$query = "SELECT fv.item_id FROM sooeg_fields_values fv
								LEFT JOIN sooeg_content sc on sc.id = fv.item_id::int
								WHERE fv.field_id = '".$puntosinteres_field."' and fv.value = '".$punto."'
								and sc.language = 'es-ES'"; //value codigo ruta
			
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
			
					// Load the results as a list of stdClass objects (see later for more options on retrieving data).
					$item_id = $db->loadResult();
					$puntos_de_interes = $puntos_de_interes.$item_id.",";
			
				}
				unset($db);
				return substr_replace($puntos_de_interes, "", -1);

			}
			else{
				return $puntos_de_interes;
			}
			
	
		}

		function getConexiones($conexiones){

			$conexionesMatriculas = str_replace('"','',$conexiones);
			$conexionesMatriculas = str_replace('{','',$conexionesMatriculas);				
			$conexionesMatriculas = str_replace('}','',$conexionesMatriculas);				
			$conexionesMatriculas = explode(',', $conexionesMatriculas);
			$conexionesIds = [];

			$db = Factory::getDbo();
			foreach ($conexionesMatriculas as $matricula){

				$matriculaFieldId = 140;
				$query = "SELECT item_id FROM sooeg_fields_values fv
							left join sooeg_content sc on sc.id = fv.item_id::int
							WHERE fv.field_id = '".$matriculaFieldId."' and fv.value = '".$matricula."' and sc.language = 'es-ES'";
					$db->setQuery($query);
					if(!is_null($id = $db->loadResult())){
						$conexionesIds[] = $id;
					}
					
			}
			unset($db);
			return $conexionesIds;
		}
		protected function getEspaciosNaturales($espacio){
			
			$espacios_naturales = [];
			//Check if field is not empty 
			if(!empty($espacio)){
				
				// Get a db connection.
				$db = Factory::getDbo();
				$espacios_naturales_raw = explode(',',str_replace('{', '', str_replace('"', '', str_replace('}', '', $espacio))));
				foreach($espacios_naturales_raw as $espacio){
	
					$query = "SELECT id FROM sooeg_content where title @@ plainto_tsquery('";
						$query .= " ".$espacio." |";
					
					$query .= "') and language = 'es-ES' and catid = 67";
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
			
					// Load the results as a list of stdClass objects (see later for more options on retrieving data).
					$espacio_id = $db->loadResult();
					if(!is_null($espacio_id)) {
						$espacios_naturales [] = $espacio_id;
					}
	
				}
				unset($db);
				return $espacios_naturales;
	
			}
			else{	
				unset($db);
				return $espacios_naturales;
			}
			
	
		}
		protected function getMunicipioId($municipio){
			
			//Check if field is not empty 
			if(!empty($municipio)){
				
				$municipioCat = 88; 
				// Get a db connection.
				$db = Factory::getDbo();
				
				$query = $db
				->getQuery(true)
				->select('id')
				->from($db->quoteName('#__content'))
				->where($db->quoteName('title') . " ILIKE " . $db->quote("%".$municipio."%"))
				->where($db->quoteName('catid') . " = " . $db->quote($municipioCat)); 
		//dd($query);
				// Reset the query using our newly populated query object.
				$db->setQuery($query);
		
				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				$municipioId = $db->loadResult();

				unset($db);
				return $municipioId;

			}
	
		}
		protected function getZonasTenerife($zonas){
			
			$zonasTenerife = [];
			//Check if field is not empty 
			if(!empty($zonas)){
				
				$zonasCat = 98; 
				$zonasTitle = explode(',',str_replace('{', '', str_replace('"', '', str_replace('}', '', $zonas))));

				// Get a db connection.
				$db = Factory::getDbo();
				
				foreach($zonasTitle as $zona){
					$query = $db
					->getQuery(true)
					->select('id')
					->from($db->quoteName('#__content'))
					->where($db->quoteName('title') . " ILIKE " . $db->quote("%".$zona."%"))
					->where($db->quoteName('catid') . " = " . $db->quote($zonasCat)); 
			
					// Reset the query using our newly populated query object.
					$db->setQuery($query);
			
					// Load the results as a list of stdClass objects (see later for more options on retrieving data).
					$zonaId = $db->loadResult();
					if(!is_null($zonaId)) {
						$zonasTenerife[] = $zonaId;
					}
			
				}
				//$zonasTenerife = implode(', ', $zonasTenerife);
				return $zonasTenerife;

			}
	
		}
		
		protected function sendEmailSync($error){

			date_default_timezone_set('Europe/Madrid');
			$dateTime = strftime("%Y-%m-%d %X");
			$body ="Ha habido un error en la actualización de itinerarios realizada a  ".$dateTime.".";
			$subject = "Error ".$error." en la ctualización de itinerarios";
	
			$to = Factory::getUser()->email;
			$config = Factory::getConfig();
			$mailfrom = $config->get('mailfrom');
			$fromname =  $config->get('fromname');
			$from = array($mailfrom, $fromname);
	
			# Invoke JMail Class
			$mailer = Factory::getMailer();
			
			# Set sender array so that my name will show up neatly in your inbox
			$mailer->setSender($from);
	
			# Add a recipient -- this can be a single address (string) or an array of addresses
			$mailer->addRecipient($to);

			$config = Factory::getConfig();
			$mailer->setSubject($subject);
			$mailer->setBody($body);
			# Send once you have set all of your options
			if($mailer->send()){
				$response['success'] = "true";
				$response['message'] = "Email enviado correctamente";
				
			} else {
				$response['success'] = "false";
				$response['message'] = "No se ha podido enviar el mail";
			}
			// $db = Factory::getDbo();

			// 	$query = $db
			// 	->getQuery(true)
			// 	->update('#__sg_syncro_vente')
			// 	->set("end_sync_date = '".$dateTime."'")//current date
			// 	->where('id = ' . $sync_id.'');

			// 	// Reset the query using our newly populated query object.
			// 	($db->setQuery($query));
			// 	$db->execute();
			die(json_encode($response));
		}

		public function checkSyncProcess() {
			$contentType = $this->input->get('contentType',null,'STRING');
			$db = Factory::getDbo();
			
			$query = $db
			->getQuery(true)
			->select(array('user_id, start_sync_date'))
			->from($db->quoteName('#__sg_syncro_vente'))
			->where($db->quoteName('content_sync') . " = " . $db->quote($contentType))
			->where($db->quoteName('end_sync_date') . " is null"); 
		
			$db->setQuery($query);
			$syncInProcess = $db->loadObjectList();
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).

			if(sizeOf($syncInProcess) == 0){ //if there is not any proces running
				
				$user = Factory::getUser();
				$columns = array('user_id', 'content_sync', 'start_sync_date');
				$values = $user->id.", '".$contentType."', '".strftime("%Y-%m-%d %X")."'";
				$query = $db
				->getQuery(true)
				->insert($db->quoteName('#__sg_syncro_vente'))
				->columns($db->quoteName($columns))
				->values(''.$values.'');
				$db->setQuery($query);
				//dd($query);
				$newSyncProcess = $db->execute();
				$new_row_id = $db->insertid();


				$response = [
					'success' => 'true',
					'message' => 'Un nuevo proceso "'.$contentType.'" iniciado',
					'sync_id' => $new_row_id
				];
			}
			else{
				$user = Factory::getUser($syncInProcess[0]->user_id);
				$response = [
					'success' => "false",
					'message' => 'Hay un proceso de "'.$contentType.'" iniciado por '.$user->name.' a las '.$syncInProcess[0]->start_sync_date.''
				];
			}
			die (json_encode($response));
		}

		protected function updateRoutingRables(){
			$db = Factory::getDbo();
			//-- PROCESO COMPLETO DE GENERACIÓN DE RED Y TOPOLOGÍA

			//-- Previo: Limpiado de las tablas temporales
			$query = "drop table if exists itineraries_routing_phase_1;";
			$db->setQuery($query);
			$fasePrevio = $db->execute();

			$query = "drop table if exists itineraries_routing_phase_2;";
			$db->setQuery($query);
			$fasePrevio = $db->execute();

			$query ="drop table if exists itineraries_routing;";
			$db->setQuery($query);
			$fasePrevio = $db->execute();

			//-- Fase 1: Aplanar y hacer merge de todos los itinerarios (los linestring extraños acaban siendo multilinestrings así que serán linestrings separados)
			$query = "CREATE TABLE itineraries_routing_phase_1
			   as SELECT 1 as id, (ST_DUMP(ST_LINEMERGE(ST_UNION(the_geom)))).geom as the_geom from itineraries as the_geom;";
			$db->setQuery($query);
			$fasePrimer = $db->execute();

			//-- Fase 2: Dividir los linestrings en tramos allá donde empiece o acabe un itinerario real para asegurarnos de que se puede empezar y acabar una ruta desde esos puntos.
			$query = "CREATE TABLE itineraries_routing_phase_2
			  as  
			select
				   (ST_Dump(
				   ST_Split(
					 a.the_geom,
					 (select st_collect(geom) from (
				select
					st_startpoint(the_geom) as geom
				from
					itineraries
			union
				select
					st_endpoint(the_geom) as geom
				from
					itineraries) AS b WHERE ST_Intersects(a.the_geom, b.geom))
					))).geom as the_geom
			FROM   itineraries_routing_phase_1 AS a
			UNION ALL
			SELECT
				   the_geom
			FROM   itineraries_routing_phase_1  AS a
			WHERE NOT EXISTS (
			  select geom from (
				select
					st_startpoint(the_geom) as geom
				from
					itineraries
			union
				select
					st_endpoint(the_geom) as geom
				from
					itineraries)as b
			  WHERE  ST_Intersects(a.the_geom, b.geom)
			);";
			$db->setQuery($query);
			$faseSegunda = $db->execute();

			//-- Fase 3: Dividir los linestrings procesados anteriormente también por el principio y el final de una superposición de un itinerario en el mergeado partido.
			$query = "CREATE TABLE itineraries_routing (id serial not null, the_geom geometry(LineString, 4326));";
			$db->setQuery($query);
			$faseTercera = $db->execute();

			$query ="insert INTO itineraries_routing (the_geom)
			  (
			select
				   (ST_Dump(
				   ST_Split(
					 a.the_geom,
					 (select st_collect(geom) from (
				with e as (select (st_dump(st_linemerge(st_union(st_intersection)))).geom from (select st_intersection(a.the_geom, b.the_geom)  from itineraries_routing_phase_2 as a, itineraries as b where st_overlaps(a.the_geom, b.the_geom) group by st_intersection
			) as overlapping_sections) select st_startpoint(e.geom) as geom from e union select st_endpoint(e.geom) as geom from e) AS b WHERE ST_Intersects(a.the_geom, b.geom))
					))).geom as the_geom
			FROM   itineraries_routing_phase_2 AS a
			UNION ALL
			SELECT
				   the_geom
			FROM   itineraries_routing_phase_2  AS a
			WHERE NOT EXISTS (
			  select geom from (with e as (select (st_dump(st_linemerge(st_union(st_intersection)))).geom from (select st_intersection(a.the_geom, b.the_geom)  from itineraries_routing_phase_2 as a, itineraries as b where st_overlaps(a.the_geom, b.the_geom) group by st_intersection
			) as overlapping_sections) select st_startpoint(e.geom) as geom from e union select st_endpoint(e.geom) as geom from e)as b
			  WHERE  ST_Intersects(a.the_geom, b.geom)
			));";
			$db->setQuery($query);
			$faseTercera = $db->execute();
			
			//-- Create the node network
			$query = "drop table if exists itineraries_routing_noded;";
			$db->setQuery($query);
			$createNetwork = $db->execute();

			$query = "select pgr_nodenetwork('itineraries_routing', 0.000001);";
			$db->setQuery($query);
			$createNetwork = $db->execute();
			
			$query = "ALTER TABLE itineraries_routing_noded ADD length float8 NULL;";
			$db->setQuery($query);
			$createNetwork = $db->execute();

			$query = "update itineraries_routing_noded set length = st_length(the_geom::geography);";
			$db->setQuery($query);
			$createNetwork = $db->execute();
			
			//-- Create the topology (creates the nodes)
			$query = "drop table if exists itineraries_routing_noded_vertices_pgr;";
			$db->setQuery($query);
			$createTopology = $db->execute();

			$query = "select pgr_createTopology('itineraries_routing_noded', 0.000001);";
			$db->setQuery($query);
			$createTopology = $db->execute();
			
			//-- PASO FINAL: obtener los itinerarios que pasan por cada tramo y guardar su referencia
			
			//-- Creates relational table for itineraries and the segmentations in itineraries_routing_noded table
			$query = "drop table if exists itineraries_routing_noded_itineraries;";
			$db->setQuery($query);
			$pasoFinal = $db->execute();

			$query = "CREATE TABLE itineraries_routing_noded_itineraries (
				itinerary_id int8 NOT NULL,
				itineraries_routing_noded_id int8 NOT NULL
			);";
			$db->setQuery($query);
			$pasoFinal = $db->execute();

			$query = "CREATE INDEX itinerary_id_idx ON itineraries_routing_noded_itineraries(itinerary_id);";
			$db->setQuery($query);
			$pasoFinal = $db->execute();

			$query = "CREATE INDEX itineraries_routing_noded_id_idx ON itineraries_routing_noded_itineraries(itineraries_routing_noded_id);";
			$db->setQuery($query);
			$pasoFinal = $db->execute();
			
			//-- Inserts the relational data
			
			$query = "insert INTO itineraries_routing_noded_itineraries (itinerary_id, itineraries_routing_noded_id)
			  (
			  select 
			b.id as id_itinerario,a.id as id_tramo
			from itineraries_routing_noded as a, itineraries as b where
			st_within(a.the_geom,b.the_geom)
			);";
			$db->setQuery($query);
			$insertRelational = $db->execute();
			

			//--Remove temporary tables
			$query = "drop table if exists itineraries_routing_phase_1;";
			$db->setQuery($query);
			$pasoFinal = $db->execute();

			$query = "drop table if exists itineraries_routing_phase_2;";
			$db->setQuery($query);
			$pasoFinal = $db->execute();

			$query = "drop table if exists itineraries_routing;";
			$db->setQuery($query);
			$pasoFinal = $db->execute();
		}
}
