<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgsyncvente\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Arcgis\ArcgisClient;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SyncpuntosinteresController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function syncPuntosinteres (){
		ini_set('memory_limit','2048M');
		ini_set('max_execution_time','3600');

		//get the syncro process id in order to set end datetime when finished
		$sync_id = $this->input->get('sync_id',null,'STRING');	

		//Check if we have puntos de interés search by nombre-puntosinteres cfield
		$db = Factory::getDbo();

		$codigoPuntointeres_field = 204;
		$query = $db
		->getQuery(true)
		->select('COUNT(*)')
		->from($db->quoteName('#__fields_values'))
		->where($db->quoteName('field_id') . " = " . $db->quote($codigoPuntointeres_field));	
		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		$import_content = $db->loadResult();

		//if we have some itinerarios, the we want to get ONLY newer content
		if($import_content > 0){
			//The last_date_edited ID in JOOMLA
			$last_edited_date_puntointeres = 171;
			// Create a new query object.
			$query = $db
			->getQuery(true)
			->select('MAX(value)')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('field_id') . " = " . $db->quote($last_edited_date_puntointeres));
			// Reset the query using our newly populated query object.
			$db->setQuery($query);

			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			($have_last_edited = $db->loadResult());
			//set where statement for query
			if(!is_null($have_last_edited)){
				$where = "last_edited_date  >'".date("Ymd h:i:s",strtotime($have_last_edited))."'";
			}
			else{
				$where = "1=1";
			}
		}
		//if no equipamientos, get full equipamientos
		else{
			$where = "1=1";
		}
						
		//If want to update all equipamientos, then set true
		$importar_de_nuevo = true;
		if($importar_de_nuevo) $where = "1=1"; //Caseria de la cumbrilla 1002
	
		//LLamada a arcgisclient
		$body = array('where' => $where,'text' => '','objectIds' => '','time' => '','geometry' => '','geometryType' => 'esriGeometryEnvelope','inSR' => '','spatialRel' => 'esriSpatialRelIntersects','relationParam' => '','outFields' => '*','returnGeometry' => 'true','returnTrueCurves' => 'false','maxAllowableOffset' => '','geometryPrecision' => '','outSR' => '','having' => '','returnIdsOnly' => 'false','returnCountOnly' => 'false','orderByFields' => '','groupByFieldsForStatistics' => '','outStatistics' => '','returnZ' => 'false','returnM' => 'false','gdbVersion' => '','historicMoment' => '','returnDistinctValues' => 'false','resultOffset' => '','resultRecordCount' => '','queryByDistance' => '','returnExtentOnly' => 'false','datumTransformation' => '','parameterValues' => '','rangeValues' => '','quantizationParameters' => '','featureEncoding' => 'esriDefault','f' => 'geojson');
		$config = Factory::getConfig();
		$client = new ArcgisClient($config->get('venteServicesUrl')); 
		$path = '/server/rest/services/ciudadanos/v_ext_puntosinteres/FeatureServer/0/query';
		$response = $client->post($path, $body);

		//Check for errors
		if(isset($response->error) && isset($response->error->code)){
			$error = $response->error->code;
			if($error == 400){
				$this->sendEmailSync($error);
			}
			elseif($error == 500){
				$this->sendEmailSync($error);
			}
		};

		//Fin llamada a arcgisclient
		$puntosInteres = $response->features;

		///dd($puntosInteres);
		//$this->sendEmailSync($sync_id);
		//dd("fin");
		$app = Factory::getApplication();			

		//superadmin user id
		$user_id = 848;
	
		$articleModel = $app->bootComponent('com_content')
		->getMVCFactory()->createModel('Article', 'Administrator');
		
		foreach($puntosInteres as $punto){

			$cat_id = $this->findOrCreateCategory( $punto );
			//Check if already exists
			$punto_exists = $this->checkIfExists( $punto->properties->nombre, $punto->geometry->coordinates[0] );
			
			//Get espacios naturales
			$enp =  $this->getEspaciosNaturales( $punto->properties->enp );
			//geometry
			$latitude = $punto->geometry->coordinates[1];
			$longitude = $punto->geometry->coordinates[0];
			//Clean observaciones
			$obs = str_replace( "\n","",$punto->properties->descripcion );
			$article = [
				"catid" => $cat_id,
				"title"		=> is_null( $punto_exists['content'][0]->title ) ? $punto->properties->nombre : $punto_exists['content'][0]->title,
				"id"	=> is_null( $punto_exists['id'])  ? 0 : $punto_exists['id'],
				'created_user_id' => $user_id,
				'created_by_alias'=> '',
				'alias' => is_null ( $punto_exists['content'][0]->alias ) ? ApplicationHelper::stringURLSafe( $punto->properties->nombre ) : $punto_exists['content'][0]->alias,
				'language' => 'es-ES',
				'metakey' => '',
				'state' => isset( $punto_exists['state'] ) ? $punto_exists['state'] : 1 ,
				'access' => 1,
				"com_fields" => array(
					"vid-puntointeres" => $punto->properties->vid,
					"globalid-puntointeres" =>  str_replace("{","",str_replace("}","",$punto->properties->globalid)),
					"tipo-puntointeres" => $punto->properties->tipo,
					"subtipo-puntointeres" => $punto->properties->subtipo,
					"descripcion-puntointeres" => $obs,
					"telefono-puntointeres" => $punto->properties->telefono,
					"email-puntointeres" => $punto->properties->email,
					"web-puntointeres" => $punto->properties->web,
					"last-edited-date-puntointeres" => $punto->properties->last_edited_date != null ? date('Y-m-d H:i:s', substr($punto->properties->last_edited_date,0,10)) : "",
					"near_dist-puntointeres" => $punto->properties->near_dist,
					"near_validate-puntointeres" => $punto->properties->near_validate,
					"latitud-puntointeres" => $latitude,
					"longitud-puntointeres" => $longitude,
					"enp-puntointeres" => $enp,
					)
				];
				//New article
			if(is_null($punto_exists['id'])){
				$article['introtext'] = $obs;
				$article['metadesc'] = $obs;
			}
			//New article
			if( is_null($punto_exists['content'][0]->introtext )  || empty( $punto_exists['content'][0]->introtext ) ){
				$article['introtext'] = $obs;
			}
			//Existing article without metadesc
			if( is_null( $punto_exists['content'][0]->metadesc ) || empty( $punto_exists['content'][0]->metadesc ) ){
				$article['metadesc'] = $obs;
			}


			Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');

			// Needs to be set because com_fields needs the data in jform to determine the assigned catid
			$this->input->set('jform', $article);
			$form = $articleModel->getForm($article, false);

			$validData = $articleModel->validate($form, $article);
			//Save punto de inyteés
			$articleModel->save($validData);
			
			//If punto de interes is in a espacio natural
			if(!empty($enp)){
				
				//Check if this espacio natural has already puntos de interes 

				$field_enp_puntos_de_interes = 217;
				$query = $db
				->getQuery(true)
				->select('value')
				->from($db->quoteName('#__fields_values'))
				->where($db->quoteName('item_id') . " = " . $db->quote($enp)) //id Espacio Natural
				->where($db->quoteName('field_id') . " = " . $db->quote($field_enp_puntos_de_interes)); //id cf field enp-puntos-de-interes
		
				// Reset the query using our newly populated query object.
				$db->setQuery($query);
				$puntosdeinteres_in_espacio = $db->loadResult();

				//if already espqacio natural has not eny puntos interes
				if(is_null($puntosdeinteres_in_espacio)){
					
						$columns = array('field_id', 'item_id', 'value');
						$values = $field_enp_puntos_de_interes.', '.$enp.', '.$articleModel->getItem()->id;
						$query = $db
						->getQuery(true)
						->insert($db->quoteName('#__fields_values'))
						->columns($db->quoteName($columns))
						->values(''.$values.'');
				
						// Reset the query using our newly populated query object.
						$db->setQuery($query);
			
				}
				else{
					//Get the ID of puntos de interes already in the espacio natural
					$check_if_has_this_puntosinteres = explode(",", $puntosdeinteres_in_espacio);
					
					//If this punto de interes does not already exixts in the espacio natural
					if (!in_array($articleModel->getItem()->id, $check_if_has_this_puntosinteres))				{
						$puntosdeinteres_in_espacio = strval($puntosdeinteres_in_espacio).",".$articleModel->getItem()->id;
						$query = $db
						->getQuery(true)
						->update('#__fields_values')
						->set("value = '".$puntosdeinteres_in_espacio."'")               // increment the row's value
						->where($db->quoteName('item_id') . " = " . $db->quote($enp)) //id Espacio Natural
						->where($db->quoteName('field_id') . " = " . $db->quote($field_enp_puntos_de_interes)); //id cf field enp-puntos-de-interes
				
						// Reset the query using our newly populated query object.
						$db->setQuery($query);
					}
					
				}
	
				$espacio_with_puntosdeinteres = $db->execute();
			}
		
			//var_dump($articleModel->getErrors());								
		};	
		if($sync_id){
			$this->sendEmailSync($sync_id);	
		}
	}

	protected function checkIfExists($nombre, $longitude){

		// Get a db connection.
		$db = Factory::getDbo();
		// Create a new query object.
		
		//$query = "SELECT id FROM sooeg_content WHERE title ilike '%".$nombre."%'";
		$query = "SELECT id FROM sooeg_content where title @@ plainto_tsquery(' ".$nombre." ') and language = 'es-ES' OR alias like '".ApplicationHelper::stringURLSafe($nombre)."' ";

		$db->setQuery($query);
		($puntos_con_mismo_nombre = $db->loadObjectList());

		foreach($puntos_con_mismo_nombre as $punto){
			//If there are more than 1 punto with same name, distinct by its long
			($query = "SELECT item_id FROM sooeg_fields_values WHERE value = '".$longitude."' and field_id = '203'");
			$db->setQuery($query);
			($puntos_con_mismo_longitude = $db->loadObjectList());

			//It should be only 1 point with title and long
			if(sizeof($puntos_con_mismo_longitude) == 1){
				if($punto->id == $puntos_con_mismo_longitude[0]->item_id){
					$punto_exists['id'] = $punto->id;
					$query = "SELECT alias, title, metadesc, introtext  FROM sooeg_content WHERE id = ".$punto->id."" ;
					$db->setQuery($query);
					($content = $db->loadObjectList());
					$punto_exists['content'] = $content;
				}
			}else if(sizeof($puntos_con_mismo_longitude) > 1){
				foreach($puntos_con_mismo_longitude as $punto){
					$query = "SELECT id FROM sooeg_content where title @@ plainto_tsquery(' ".$nombre." ') and language = 'es-ES' OR alias like '".ApplicationHelper::stringURLSafe($nombre)."' ";
					$db->setQuery($query);
					$puntoIdThatHasSameLongButSearcheTitle = $db->loadObjectList();
					
					if(in_array($puntoIdThatHasSameLongButSearcheTitle[0]->id, (array)$punto) ){
						$punto_exists['id'] = $puntoIdThatHasSameLongButSearcheTitle[0]->id;
						$query = "SELECT alias, title, metadesc, introtext FROM sooeg_content WHERE id = ".$puntoIdThatHasSameLongButSearcheTitle[0]->id."" ;
						$db->setQuery($query);
						($content = $db->loadObjectList());
						$punto_exists['content'] = $content;
					}
				}
			}

		}
		//Esto habrá que descomentarlo
					// //Check if the punto already exists in JOOMLA
					// $codigoPuntointeres_field = 204;
					// $punto_exists = [];

					// $query = $db
					// ->getQuery(true)
					// ->select('item_id')
					// ->from($db->quoteName('#__fields_values'))
					// ->where($db->quoteName('field_id') . " = " . $db->quote($codigoPuntointeres_field)) //id cf codigo punto
					// ->where($db->quoteName('value') . " = " . $db->quote($punto)); //value codigo punto


					// // Reset the query using our newly populated query object.
					// $db->setQuery($query);

					// // Load the results as a list of stdClass objects (see later for more options on retrieving data).
					// $punto_exists['id'] = $db->loadResult();

		//If exists check state to pass to article update
		if(!is_null($punto_exists['id'])){
			$query = $db
			->getQuery(true)
			->select('state')
			->from($db->quoteName('#__content'))
			->where($db->quoteName('id') . " = " . $db->quote($punto_exists['id']));
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);	

			$punto_exists['state'] = $db->loadResult();
		}

		return $punto_exists;

	}

	protected function findOrCreateCategory($punto){

		$cat_puntointeres = 66;
		$field_id_punto_tipo = 205;
		// Get a db connection.
		$db = Factory::getDbo();
		// Check if the clase id match a itinerarios subcategory
		$query = $db
		->getQuery(true)
		->select('item_id')
		->from($db->quoteName('#__fields_values'))
		->where($db->quoteName('value') . " = " . $db->quote($punto->properties->tipo)) //id tipo in vente db
		->where($db->quoteName('field_id') . " = " . $db->quote($field_id_punto_tipo)); //id cf used for clase value
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$punto_clase_exists_id = $db->loadResult();

		if( !is_null($punto_clase_exists_id) ){
			
			$cat_id = $punto_clase_exists_id;
			return $cat_id;
		}
		else{
			return $cat_puntointeres;
		}
	}

	protected function getEspaciosNaturales($espacio){
			
		$espacios_naturales = "";
		//Check if field is not empty 
		if(!empty($espacio)){
						
			// Get a db connection.
			$db = Factory::getDbo();
			$espacios_naturales_raw = explode(',',str_replace('{', '', str_replace('"', '', str_replace('}', '', $espacio))));
			($array_de_espacios = explode(' ',$espacios_naturales_raw[0]));
			//foreach($espacios_naturales_raw as $espacio){
				$query = "SELECT id FROM sooeg_content where title @@ plainto_tsquery('";
				foreach($array_de_espacios as $espacio){
					$query .= " ".$espacio." |";
				}
				$query .= "') and language = 'es-ES' and catid = 67";
				// Reset the query using our newly populated query object.
				$db->setQuery($query);
		
				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				($espacio_id = $db->loadResult());
				if(!is_null($espacio_id)) {
					$espacios_naturales = $espacios_naturales.$espacio_id;
					
				}
		
			//}

			return $espacios_naturales;

		}
		else{
			return $espacios_naturales;
		}
		

	}

	protected function sendEmailSync($error){
		date_default_timezone_set('Europe/Madrid');
		$dateTime = strftime("%Y-%m-%d %X");
		$body ="Ha abido un error en la actualización de Puntos de interés realizada a las ".$dateTime.".";
		$subject = "Error ".$error." en la actualización puntos de interés";

		$to = Factory::getUser()->email;
		$config = Factory::getConfig();
		$mailfrom = $config->get('mailfrom');
		$fromname =  $config->get('fromname');
		$from = array($mailfrom, $fromname);

		# Invoke JMail Class
		$mailer = Factory::getMailer();
		
		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$config = Factory::getConfig();
		$mailer->setSubject($subject);
		$mailer->setBody($body);
		# Send once you have set all of your options
		if($mailer->send()){
			$response['success'] = "true";
			$response['message'] = "Email enviado correctamente";
			
		} else {
			$response['success'] = "false";
			$response['message'] = "No se ha podido enviar el mail";
		}
		// $db = Factory::getDbo();

		// 	$query = $db
		// 	->getQuery(true)
		// 	->update('#__sg_syncro_vente')
		// 	->set("end_sync_date = '".$dateTime."'")//current date
		// 	->where('id = ' . $sync_id.'');

		// 	// Reset the query using our newly populated query object.
		// 	($db->setQuery($query));
		// 	$db->execute();
		die(json_encode($response));
	}

	public function checkSyncProcess() {
		$contentType = $this->input->get('contentType',null,'STRING');
		$db = Factory::getDbo();
		
		$query = $db
		->getQuery(true)
		->select(array('user_id, start_sync_date'))
		->from($db->quoteName('#__sg_syncro_vente'))
		->where($db->quoteName('content_sync') . " = " . $db->quote($contentType))
		->where($db->quoteName('end_sync_date') . " is null"); 
	
		$db->setQuery($query);
		$syncInProcess = $db->loadObjectList();
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).

		if(sizeOf($syncInProcess) == 0){ //if there is not any proces running
			
			$user = Factory::getUser();
			$columns = array('user_id', 'content_sync', 'start_sync_date');
			$values = $user->id.", '".$contentType."', '".strftime("%Y-%m-%d %X")."'";
			$query = $db
			->getQuery(true)
			->insert($db->quoteName('#__sg_syncro_vente'))
			->columns($db->quoteName($columns))
			->values(''.$values.'');
			$db->setQuery($query);
			//dd($query);
			$newSyncProcess = $db->execute();
			$new_row_id = $db->insertid();


			$response = [
				'success' => 'true',
				'message' => 'Un nuevo proceso "'.$contentType.'" iniciado',
				'sync_id' => $new_row_id
			];
		}
		else{
			$user = Factory::getUser($syncInProcess[0]->user_id);
			$response = [
				'success' => "false",
				'message' => 'Hay un proceso de "'.$contentType.'" iniciado por '.$user->name.' a las '.$syncInProcess[0]->start_sync_date.''
			];
		}
		die (json_encode($response));
	}

}
