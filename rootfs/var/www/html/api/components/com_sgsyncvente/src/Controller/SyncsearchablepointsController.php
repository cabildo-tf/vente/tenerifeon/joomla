<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgsyncvente\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Arcgis\ArcgisClient;
use Firebase\JWT\Key;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SyncsearchablepointsController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function Syncsearchablepoints (){

		$db = Factory::getDbo();

		//Get syncro process id
		$sync_id = $this->input->get('sync_id',null,'STRING');

		//Update equipments
		//Get all equipments with connected itineraries from vente system
		$query = "select sc.id, sfv.value, sc.title from sooeg_content sc
			join sooeg_fields_values sfv on sfv.item_id::int = sc.id
			where sfv.field_id = '68' and sc.catid in (48,49,50,51,60,65)";

		$db->setQuery($query);
		$connectedEquipments = $db->loadObjectList();
		$this->updateSearchableEquipments($connectedEquipments);

		//Update itineraries
		//Get all itineraries already in itineraries / linestrig geometries table
		$query = "select i.j_id, i.name from public.itineraries i
						order by i.j_id ";

		$db->setQuery($query);
		$itineraries = $db->loadObjectList();
		$this->updateSearchableItineraries($itineraries);

		//Update interest points
		//Get all itineraries already in itineraries / linestrig having related puntos de interes
		$query = "select i.j_id, fv.value from public.itineraries i
					left join sooeg_fields_values fv on fv.item_id::int = i.j_id
					where fv.field_id  = '69'";
		$db->setQuery($query);
		$itinerariesWithPuntos = $db->loadObjectList();
		$this->updateSearchableInterestPoints($itinerariesWithPuntos);

		//Update table relateditinerariesandpoints
		$this->updateRelatedItinerariesAndPoints($itinerariesWithPuntos, $connectedEquipments);

		//Send email after
		if($sync_id){
			$this->sendEmailSync($sync_id);	
		}

			
		}
	
		protected function updateSearchableEquipments($connectedEquipments){
	
			$db = Factory::getDbo();

			//Loop the searchable equipments list
			foreach($connectedEquipments as $equipment){
				$query = "select s.gid id from searchablepoints s 
						where s.article_id = '".$equipment->id."'";

				$db->setQuery($query);
				$isAlreadySearchable = $db->loadResult();

				//if is not already in table
				if(!$isAlreadySearchable){
					//Get latitude
					$query = "select * from sooeg_fields_values fv
					where fv.field_id = '177' and fv.item_id = '".$equipment->id."' ";
					$db->setQuery($query);
					$latitude = $db->loadObject()->value;
					//Get longitude
					$query = "select * from sooeg_fields_values fv
					where fv.field_id = '178' and fv.item_id = '".$equipment->id."' ";
					$db->setQuery($query);
					$longitude = $db->loadObject()->value;

					$query = "INSERT INTO public.searchablepoints
								(control_id, article_id, point_name, geo_point)
								VALUES('".$equipment->id."', ".$equipment->id.", '".$equipment->title."', 'SRID=4326;POINT (".$longitude." ".$latitude.")'::public.geography);
								";

					$db->setQuery($query);
					$newSearchablePoint = $db->execute();
				
				}
			}
	
		}

		protected function updateSearchableItineraries($connectedItineraries){
	
			$db = Factory::getDbo();

			//Loop the searchable equipments list
			foreach($connectedItineraries as $itinerary){
				//dd($itinerary);
				$query = "select s.gid id from searchablepoints s 
						where s.article_id = '".$itinerary->j_id."'";

				$db->setQuery($query);
				$isAlreadySearchable = $db->loadResult();

				//if is not already in table
				if(!$isAlreadySearchable){

					$query = "select ST_AsText(ST_StartPoint(i.the_geom)) as start_point, ST_AsText(ST_EndPoint(i.the_geom)) as end from public.itineraries i
					where i.j_id = '".$itinerary->j_id."' ";
					$db->setQuery($query);
					$linestring = $db->loadObject();

					//Set start point
					($query = "INSERT INTO public.searchablepoints
								(control_id, article_id, point_name, geo_point)
								VALUES('".$itinerary->j_id."', ".$itinerary->j_id.", '".$itinerary->name.", inicio', 'SRID=4326;".$linestring->start_point."'::public.geography);
								");

					$db->setQuery($query);
					$newSearchablePoint = $db->execute();

					//Set end point
					($query = "INSERT INTO public.searchablepoints
								(control_id, article_id, point_name, geo_point)
								VALUES('".$itinerary->j_id."_1', ".$itinerary->j_id.", '".$itinerary->name.", final', 'SRID=4326;".$linestring->end."'::public.geography);
								");

					$db->setQuery($query);
					$newSearchablePoint = $db->execute();
				}
			}
		
		}

		protected function updateSearchableInterestPoints($itinerariesWithPuntos){

			$db = Factory::getDbo();

			//Loop the searchable itineraries list
			foreach($itinerariesWithPuntos as $itinerary){
		
				//Get itineraries
				$query = "select value from sooeg_fields_values fv
				where fv.field_id = '69' and fv.item_id = '".$itinerary->j_id."' ";

				$db->setQuery($query);
				$puntosByItinerary = $db->loadRow();

				$puntosByItinerary =  explode (",", $puntosByItinerary[0]);
				foreach($puntosByItinerary as $punto){
					$query = "select gid from searchablepoints s
							where s.article_id  = ".$punto."";

					$db->setQuery($query);
					$isAlreadySearchable = $db->loadResult();

					if(!$isAlreadySearchable){
						//Get title
						$query = "select c.title from sooeg_content c
						where c.id = ".$punto."";
						$db->setQuery($query);
						$title = $db->loadObject()->title;
						//Get latitude
						$query = "select * from sooeg_fields_values fv
						where fv.field_id = '202' and fv.item_id = '".$punto."' ";
						$db->setQuery($query);
						$latitude = $db->loadObject()->value;
						//Get longitude
						$query = "select * from sooeg_fields_values fv
						where fv.field_id = '203' and fv.item_id = '".$punto."' ";
						$db->setQuery($query);
						$longitude = $db->loadObject()->value;
	
						$query = "INSERT INTO public.searchablepoints
									(control_id, article_id, point_name, geo_point)
									VALUES('".$punto."', ".$punto.", '".$title."', 'SRID=4326;POINT (".$longitude." ".$latitude.")'::public.geography);";
	
						$db->setQuery($query);
						$newSearchablePoint = $db->execute();
					
					}

				}
				
			}
	
		}

		protected function updateRelatedItinerariesAndPoints($itinerariesWithPuntos, $connectedEquipments){
			
			$db = Factory::getDbo();			
			//loop the equipments and get all the connected itineraries
			foreach($connectedEquipments as $connectedEquip){
				$connectedItineraries = explode(",", $connectedEquip->value);
				//insert into the relateditinerariesandpoints table if not exists
				foreach($connectedItineraries as $connexion){
					//dd($query = "selct count(id) from itineraries where j_id = ".$connexion."");
					($query = "INSERT INTO public.relateditinerariesandpoints
					(point_gid, itinerary_id)
					SELECT (select gid from searchablepoints where article_id = ".$connectedEquip->id."), (select id from itineraries where j_id =".$connexion.")
					WHERE NOT EXISTS (
					SELECT gid FROM public.relateditinerariesandpoints WHERE point_gid = (select gid from searchablepoints where article_id = ".$connectedEquip->id.") and itinerary_id = (select id from itineraries where j_id =".$connexion."));");
					$db->setQuery($query);
					($newRelation = $db->execute());
				}

			}

			//loop the equipments and get all the connected itineraries
			foreach($itinerariesWithPuntos as $connectedItinerary){
				$connectedPoints = explode(",", $connectedItinerary->value);
				//insert into the relateditinerariesandpoints table if not exists
				foreach($connectedPoints as $connexion){
					//dd($query = "selct count(id) from itineraries where j_id = ".$connexion."");
					($query = "INSERT INTO public.relateditinerariesandpoints
					(point_gid, itinerary_id)
					SELECT (select gid from searchablepoints where article_id = ".$connexion."), (select id from itineraries where j_id =".$connectedItinerary->j_id.")
					WHERE NOT EXISTS (
					SELECT gid FROM public.relateditinerariesandpoints WHERE point_gid = (select gid from searchablepoints where article_id = ".$connexion.") and itinerary_id = (select id from itineraries where j_id =".$connectedItinerary->j_id."));");
					$db->setQuery($query);
					($newRelation = $db->execute());
				}

			}

		}

		protected function sendEmailSync($sync_id){
			if(is_null($sync_id)){
				die("Sync realizada");
			}
			$body ="Este es buena";
			$subject = "Hola tiu ".$sync_id;
			$to = "ivolado@gmail.com";
			//$to = $user = Factory::getUser()->email;
			$config = Factory::getConfig();
			$mailfrom = $config->get('mailfrom');
			$fromname =  $config->get('fromname');
			$from = array($mailfrom, $fromname);
	
			# Invoke JMail Class
			$mailer = Factory::getMailer();
			
			# Set sender array so that my name will show up neatly in your inbox
			$mailer->setSender($from);
	
			# Add a recipient -- this can be a single address (string) or an array of addresses
			$mailer->addRecipient($to);

			$config = Factory::getConfig();
			$mailer->setSubject($subject);
			$mailer->setBody($body);
			# Send once you have set all of your options
			if($mailer->send()){
				$response['success'] = "true";
				$response['message'] = "Email enviado correctamente";
				
			} else {
				$response['success'] = "false";
				$response['message'] = "No se ha podido enviar el mail";
			}
			$db = Factory::getDbo();

				$query = $db
				->getQuery(true)
				->update('#__sg_syncro_vente')
				->set("end_sync_date = '".strftime("%Y-%m-%d %X")."'")//current date
				->where('id = ' . $sync_id.'');

				// Reset the query using our newly populated query object.
				($db->setQuery($query));
				$db->execute();
			die(json_encode($response));
		}


		

}
