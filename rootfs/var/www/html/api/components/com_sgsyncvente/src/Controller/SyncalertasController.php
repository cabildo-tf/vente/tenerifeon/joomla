<?php
/**
 * @package     Joomla.API
 * @subpackage  com_users
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgsyncvente\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Arcgis\ArcgisClient;
use Firebase\JWT\Key;
use Joomla\CMS\Language\Associations;


/**
 * The users controller
 *
 * @since  4.0.0
 */
class SyncalertasController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function syncAlertas (){
		ini_set('memory_limit','512M');
		ini_set('max_execution_time','3600');

		$sync_id = $this->input->get('sync_id',null,'STRING');

		//Check if we have itinerarios search by codigo-alertas cfield
			$db = Factory::getDbo();

			$where = "1=1";
			
			//LLamada a arcgisclient
			$body = array('where' => $where,'text' => '','objectIds' => '','time' => '','geometry' => '','geometryType' => 'esriGeometryEnvelope','inSR' => '','spatialRel' => 'esriSpatialRelIntersects','relationParam' => '','outFields' => '*','returnGeometry' => 'true','returnTrueCurves' => 'false','maxAllowableOffset' => '','geometryPrecision' => '','outSR' => '','having' => '','returnIdsOnly' => 'false','returnCountOnly' => 'false','orderByFields' => '','groupByFieldsForStatistics' => '','outStatistics' => '','returnZ' => 'false','returnM' => 'false','gdbVersion' => '','historicMoment' => '','returnDistinctValues' => 'false','resultOffset' => '','resultRecordCount' => '','queryByDistance' => '','returnExtentOnly' => 'false','datumTransformation' => '','parameterValues' => '','rangeValues' => '','quantizationParameters' => '','featureEncoding' => 'esriDefault','f' => 'geojson');
			$config = Factory::getConfig();
			$client = new ArcgisClient($config->get('venteServicesUrl'));
			$path = '/server/rest/services/ciudadanos/v_ext_alertas/FeatureServer/2/query';
			$response = $client->post($path, $body);
			if(isset($response->error) && isset($response->error->code)){
				$error = $response->error->code;
				if($error == 400){
					$this->sendEmailSync($error);
				}
				elseif($error == 500){
					$this->sendEmailSync($error);
				}
			};

			//Fin llamada a arcgisclient
			$alertas = $response->features;
			//dd($alertas);
			$app = Factory::getApplication();			
	
			//superadmin user id
			$user_id = 848;

			$this->checkOldAlerts($alertas);
			
			$articleModel = $app->bootComponent('com_content')
			->getMVCFactory()->createModel('Article', 'Administrator');
				
			foreach($alertas as $alerta){

				//Check if already exists
				$cat_id = 55; //Alertas category id 
				//get associated categories
				$associations = Associations::getAssociations('com_content', '#__categories', 'com_categories.item', (int) $cat_id, 'id', 'alias', '');

				foreach($associations as $lang){

					($alerta_exists = $this->checkIfExists($alerta,$lang->language));
					//Clean observaciones
					$obs = str_replace("\n","",$alerta->properties->observaciones);
					$title = $alerta->properties->{'situacion_'.explode("-",$lang->language)[0]}." - ".$alerta->properties->{'causa_'.explode("-",$lang->language)[0]}." - ".$alerta->properties->codigo;
					$article = [
						"catid" => intval(explode(":", $lang->id)[0]),
						"title"		=> $title,
						"id"	=> is_null($alerta_exists['id']) ? 0 : $alerta_exists['id'],
						'created_user_id' => $user_id,
						'created_by_alias'=> '',
						'publish_up' => $alerta->properties->fecha_emision != null ? date('Y-m-d H:i:s', substr($alerta->properties->fecha_emision,0,10)) : "",
						'publish_down' => $alerta->properties->fecha_fin != null ? date('Y-m-d H:i:s', substr($alerta->properties->fecha_fin,0,10)) : "",
						'alias' => ApplicationHelper::stringURLSafe($title),
						'introtext' => " ",
						'language' => $lang->language,
						'metakey' => '',
						'metadesc' => '',
						'state' => $alerta->properties->visible ,
						'access' => 1,
						"com_fields" => array(
							"vid-alerta" => $alerta->properties->vid,
							"codigo-alerta" => $alerta->properties->codigo,
							"causa-alerta"=> $alerta->properties->{'causa_'.explode("-",$lang->language)[0]},
							"situacion-alerta" => $alerta->properties->{'situacion_'.explode("-",$lang->language)[0]},
							"observaciones-alerta" => $obs,
							"estado-alerta" => $alerta->properties->estado,
							"last-edited-date-alerta" => $alerta->properties->last_edited_date != null ? date('Y-m-d H:i:s', substr($alerta->properties->last_edited_date,0,10)) : "",
							"enlace-alerta" => $alerta->properties->enlace,
							"origen-alerta" => $alerta->properties->origen,
							"push-alerta" => $alerta->properties->push,
							"visible-alerta" => $alerta->properties->visible,
							"fecha-emision-alerta" => $alerta->properties->fecha_emision != null ? date('Y-m-d H:i:s', substr($alerta->properties->fecha_emision,0,10)) : "",
							"fecha-inicio-alerta" => $alerta->properties->fecha_inicio != null ? date('Y-m-d H:i:s', substr($alerta->properties->fecha_inicio,0,10)) : "",
							"fecha-fin-alerta" => $alerta->properties->fecha_fin != null ? date('Y-m-d H:i:s', substr($alerta->properties->fecha_fin,0,10)) : "",
						)
						];
		
					//dump($article);
					Form::addFormPath(JPATH_SITE.'/administrator/components/com_content/forms');
	
					// Needs to be set because com_fields needs the data in jform to determine the assigned catid
					$this->input->set('jform', $article);
					$form = $articleModel->getForm($article, false);
	
					($validData = $articleModel->validate($form, $article));
					
					$articleModel->save($validData);

					$articleModel->getErrors();	
					
					//If is not published yet anf push value == 1 the sends notification
					if( is_null($alerta_exists['id']) && ($alerta->properties->push == 1) ){
						$curl = curl_init();
	
						($to = $lang->language == 'es-ES' ? '/topics/alerts' : '/topics/alerts_'.explode("-",$lang->language)[0]);
						$content = '{"to": "'.$to.'","notification": {"title": "'.$alerta->properties->{'situacion_'.explode("-",$lang->language)[0]}.'","subtitle": "'.$alerta->properties->{'causa_'.explode("-",$lang->language)[0]}.'"}}';
						curl_setopt_array($curl, array(
						CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => '',
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => 'POST',
						CURLOPT_POSTFIELDS => $content,
						CURLOPT_HTTPHEADER => array(
							'Authorization: key=AAAAVS5KT-M:APA91bGh9cSycVgNA8gB7RssRESF-mCBJjBcXK2PRc6ZG3RkDz1cwI5NjIVs_mvZ0BfTxRoQitWf2q5yIl9s9xP9CzQh-q70Ihtr_cOFCYXDxJE7nUutBu0aYElj0PhhVXVrgf_kNthN',
							'Content-Type: application/json'
						),
						));
						
						if(curl_exec($curl) === false)
						{
							echo 'Curl error: ' . curl_error($curl);
						}
		
						curl_close($curl);
					}
					error_log($articleModel->getItem()->id." - ".$alerta->properties->codigo." mem: ". round(memory_get_usage() / 1024),0);	

				}
				//Alertas category id
			};	
			if($sync_id){
				$this->sendEmailSync($sync_id);	
			}
		}
	
		protected function checkIfExists($alerta, $lang){
	
			$codigoAlerta_field = 148;
			//Check if the alerta already exists in JOOMLA
			// Get a db connection.
			$db = Factory::getDbo();
	
			// Create a new query object.
			$query = "select fv.item_id from sooeg_fields_values fv left join sooeg_content sc
					on sc.id::varchar = fv.item_id where fv.field_id = '".$codigoAlerta_field."' and fv.value = '".$alerta->properties->codigo."'
					and sc.language = '".$lang."'";
	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
	
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$alerta_exists['id'] = $db->loadResult();

			return $alerta_exists;
	
		}
		protected function checkOldAlerts($alertas){

			//Search all item _id and the value (codigo alerta) already exists
			$db = Factory::getDbo();
			$codigoAlerta_field = 148;
			// Create a new query object.
			$query = $db
			->getQuery(true)
			->select(array('item_id','value'))
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('field_id') . " = " . $db->quote($codigoAlerta_field)); //id cf codigo alerta
			$db->setQuery($query);

			$alertasCode = [];
			foreach($alertas as $alerta){
				$alertasCode[] = $alerta->properties->codigo;
			}
			($alerta_exists = $db->loadObjectList());

			foreach($alerta_exists as $exists){
				if (!in_array($exists->value, $alertasCode))
				{
					$query = $db
					->getQuery(true)
					->update('#__content')
					->set("state = -2")               // increment the row's value
					->where($db->quoteName('id') . " = " . $db->quote($exists->item_id)) ;//id Espacio Natural
					$db->setQuery($query);
					$db->execute();
				}
			}
		}

		protected function sendEmailSync($error){

			date_default_timezone_set('Europe/Madrid');
			$dateTime = strftime("%Y-%m-%d %X");
			$body ="Ha habido un error en la sincronización de alertas, parece que vuestro servicio no está funcionando correctamente a las ".$dateTime.".";
			$subject = "Error ".$error."en la actualización de alertas";
			$to = Factory::getUser()->email;
			
			$config = Factory::getConfig();
			$mailfrom = $config->get('mailfrom');
			$fromname =  $config->get('fromname');
			$from = array($mailfrom, $fromname);
	
			# Invoke JMail Class
			$mailer = Factory::getMailer();
			
			# Set sender array so that my name will show up neatly in your inbox
			$mailer->setSender($from);
	
			# Add a recipient -- this can be a single address (string) or an array of addresses
			$mailer->addRecipient($to);

			$config = Factory::getConfig();
			$mailer->setSubject($subject);
			$mailer->setBody($body);
			# Send once you have set all of your options
			if($mailer->send()){
				$response['success'] = "true";
				$response['message'] = "Email enviado correctamente";
				
			} else {
				$response['success'] = "false";
				$response['message'] = "No se ha podido enviar el mail";
			}
			// $db = Factory::getDbo();

			// 	$query = $db
			// 	->getQuery(true)
			// 	->update('#__sg_syncro_vente')
			// 	->set("end_sync_date = '".$dateTime."'")//current date
			// 	->where('id = ' . $sync_id.'');

			// 	// Reset the query using our newly populated query object.
			// 	($db->setQuery($query));
			// 	$db->execute();
			die(json_encode($response));
		}
}

