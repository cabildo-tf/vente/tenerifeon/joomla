<?php
/**
 * @package     Joomla.API
 * @subpackage  com_articles
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Studiogenesis\Component\Sgmeteo\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Date\Date;
use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Tobscure\JsonApi\Exception\InvalidParameterException;
use Joomla\CMS\Factory;

/**
 * The articles controller
 *
 * @since  4.0.0
 */
class SyncmeteoController  extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'articles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $default_view = 'articles';

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */

	public function syncMeteo (){

		$db = Factory::getDbo();
		$id = 88;//Municipios
		$lat_field_id = "220";
		$lon_field_id = "221";
		$meteo_JSON_field_id = "222";

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__content');
		$query->where('catid='.$id);

		$db->setQuery((string)$query);
		$municipios = $db->loadObjectList();

		foreach($municipios as $municipio){

			$fields = FieldsHelper::getFields('com_content.article', $municipio, true);
			// Adding the fields to the object
			$municipio->jcfields = array();

			foreach ($fields as $key => $field)
			{
				$municipio->jcfields[$field->id] = $field;
			}
			$curl = curl_init();
			$lat = $municipio->jcfields[$lat_field_id]->rawvalue;
			$lon = $municipio->jcfields[$lon_field_id]->rawvalue;
			$apiKey = 'f57afb5404edbce5e1137a5157e40a5d&units=metric';
			curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.openweathermap.org/data/2.5/onecall?lat='.$lat.'&lon='.$lon.'&lang=ea&exclude=minutely,hourly,alerts&appid='.$apiKey.'',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			));

			$meteo_JSON = curl_exec($curl);

			curl_close($curl);

			//If already has a meteoJSON for this municipio

			$query = $db
			->getQuery(true)
			->select('COUNT(*)')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('field_id') . " = " . $db->quote($meteo_JSON_field_id))
			->where($db->quoteName('item_id') . " = " . $db->quote($municipio->id));	
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			$has_already_json = $db->loadResult();
	
			//Set insert values
			$item_id = $municipio->id;
			$field_id = $meteo_JSON_field_id;
			$field_value = $meteo_JSON;
			// Create a new query object.
			$query = $db->getQuery(true);

			//Check if it has already json value
			if(!$has_already_json){

				// Insert columns.
				$columns = array('item_id', 'field_id', 'value');

				// Insert values.
				$values = array($db->quote($item_id), $db->quote($field_id), $db->quote($field_value));

				// Prepare the insert query.
				$query
					->insert($db->quoteName('#__fields_values'))
					->columns($db->quoteName($columns))
					->values(implode(',', $values));


			}
			else{
				$fields = array(
					$db->quoteName('value') . ' = ' . $db->quote($meteo_JSON)
				);
				
				// Conditions for which records should be updated.
				$conditions = array(
					$db->quoteName('item_id') . ' = ' .$db->quote($item_id), 
					$db->quoteName('field_id') . ' = ' . $db->quote($field_id)
				);
				
				$query->update($db->quoteName('#__fields_values'))->set($fields)->where($conditions);

			}

			// Set the query using our newly populated query object and execute it.
			$db->setQuery($query);
			$db->execute();
		}
		return("Meteo sync");
	
	}
}