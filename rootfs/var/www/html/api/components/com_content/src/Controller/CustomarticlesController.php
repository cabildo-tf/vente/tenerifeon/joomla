<?php
/**
 * @package     Joomla.API
 * @subpackage  com_content
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Joomla\Component\Content\Api\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\Filter\InputFilter;
use Joomla\CMS\MVC\Controller\ApiController;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\CMS\Form\Form;
// use Joomla\CMS\MVC\Model\FormModelInterface;

use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Component\ComponentHelper;
use \Joomla\Database\DatabaseDriver;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;

/**
 * The article controller
 *
 * @since  4.0.0
 */
class CustomarticlesController extends ApiController
{
	/**
	 * The content type of the item.
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $contentType = 'customarticles';

	/**
	 * The default view for the display method.
	 *
	 * @var    string
	 * @since  3.0
	 */
	protected $default_view = 'customarticles';

	/**
	 * Article list view amended to add filtering of data
	 *
	 * @return  static  A BaseController object to support chaining.
	 *
	 * @since   4.0.0
	 */
	public function displayList()
	{
		$apiFilterInfo = $this->input->get('filter', [], 'array');
		$filter        = InputFilter::getInstance();

		if (\array_key_exists('author', $apiFilterInfo))
		{
			$this->modelState->set('filter.author_id', $filter->clean($apiFilterInfo['author'], 'INT'));
		}

		if (\array_key_exists('category', $apiFilterInfo))
		{
			$this->modelState->set('filter.category_id', $filter->clean($apiFilterInfo['category'], null, 'STRING'));
		}

		if (\array_key_exists('search', $apiFilterInfo))
		{
			$this->modelState->set('filter.search', $filter->clean($apiFilterInfo['search'], 'STRING'));
		}

		if (\array_key_exists('state', $apiFilterInfo))
		{
			$this->modelState->set('filter.published', $filter->clean($apiFilterInfo['state'], 'INT'));
		}

		if (\array_key_exists('language', $apiFilterInfo))
		{
			$this->modelState->set('filter.language', $filter->clean($apiFilterInfo['language'], 'STRING'));
		}
		if (\array_key_exists('favourites', $apiFilterInfo))
		{
			$this->modelState->set('filter.favourites', $filter->clean($apiFilterInfo['favourites'], 'INT'));
		}
		if (\array_key_exists('valoraciones', $apiFilterInfo))
		{
			$this->modelState->set('filter.valoraciones', $filter->clean($apiFilterInfo['valoraciones'], 'INT'));
		}
		if (\array_key_exists('articles_id', $apiFilterInfo))
		{
			$this->modelState->set('filter.articles_id', $filter->clean($apiFilterInfo['articles_id'],  null, 'STRING'));
		}
		if (\array_key_exists('search_planner', $apiFilterInfo))
		{
			$this->modelState->set('filter.search_planner', $filter->clean($apiFilterInfo['search_planner'],  null, 'ARRAY'));
		}
		if (\array_key_exists('distance', $apiFilterInfo))
		{
			$this->modelState->set('filter.distance', $filter->clean($apiFilterInfo['distance'],  null, 'STRING'));
		}
		if (\array_key_exists('routeType', $apiFilterInfo))
		{
			$this->modelState->set('filter.routeType', $filter->clean($apiFilterInfo['routeType'],  null, 'STRING'));
		}
		if (\array_key_exists('difficulty', $apiFilterInfo))
		{
			$this->modelState->set('filter.difficulty', $filter->clean($apiFilterInfo['difficulty'],  null, 'STRING'));
		}
		if (\array_key_exists('zones', $apiFilterInfo))
		{
			$this->modelState->set('filter.zones', $filter->clean($apiFilterInfo['zones'],  null, 'STRING'));
		}

		return parent::displayList();
	}

	/**
	 * Method to allow extended classes to manipulate the data to be saved for an extension.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  array
	 *
	 * @since   4.0.0
	 */
	protected function preprocessSaveData(array $data): array
	{
		foreach (FieldsHelper::getFields('com_content.article') as $field)
		{
			if (isset($data[$field->name]))
			{
				!isset($data['com_fields']) && $data['com_fields'] = [];

				$data['com_fields'][$field->name] = $data[$field->name];
				unset($data[$field->name]);
			}
		}

		return $data;
	}

}
