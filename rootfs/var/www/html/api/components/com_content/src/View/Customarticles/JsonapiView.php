<?php
/**
 * @package     Joomla.API
 * @subpackage  com_content
 *
 * @copyright   (C) 2019 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Joomla\Component\Content\Api\View\Customarticles;

\defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Multilanguage;
use Joomla\CMS\MVC\View\JsonApiView as BaseApiView;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\Component\Content\Api\Helper\ContentHelper;
use Joomla\Component\Content\Api\Serializer\ContentSerializer;
use Joomla\Component\Fields\Administrator\Helper\FieldsHelper;
use Joomla\Registry\Registry;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;
use Joomla\CMS\Uri\Uri;

/**
 * The article view
 *
 * @since  4.0.0
 */
class JsonapiView extends BaseApiView
{
	/**
	 * The fields to render item in the documents
	 *
	 * @var  array
	 * @since  4.0.0
	 */
	protected $fieldsToRenderItem = [
		'id',
		'typeAlias',
		'asset_id',
		'title',
		'text',
		'tags',
		'language',
		'state',
		'category',
		'images',
		'metakey',
		'metadesc',
		'metadata',
		'access',
		'featured',
		'alias',
		'note',
		'publish_up',
		'publish_down',
		'urls',
		'created',
		'created_by',
		'created_by_alias',
		'modified',
		'modified_by',
		'hits',
		'version',
		'featured_up',
		'featured_down',
		'url',
		'sub_category_name',
		'parent_id',
		'favourite',
		'valoracion',
		'mi_valoracion',
		'routeType','color','itinerarioAfectado','equipamientoAfectado'
	];

	/**
	 * The fields to render items in the documents
	 *
	 * @var  array
	 * @since  4.0.0
	 */
	protected $fieldsToRenderList = [
		'id',
		'typeAlias',
		'asset_id',
		'title',
		'text',
		'tags',
		'language',
		'state',
		'category',
		'images',
		'metakey',
		'metadesc',
		'metadata',
		'access',
		'featured',
		'alias',
		'note',
		'publish_up',
		'publish_down',
		'urls',
		'created',
		'created_by',
		'created_by_alias',
		'modified',
		'hits',
		'version',
		'featured_up',
		'featured_down',
		'url',
		'sub_category_name',
		'parent_id',
		'favourite',
		'valoracion',
		'mi_valoracion',
		'routeType','color','mostrar_matricula','itinerarioAfectado','equipamientoAfectado'
	];

	/**
	 * The relationships the item has
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $relationship = [
		'category',
		'created_by',
		'tags',
	];

	/**
	 * Constructor.
	 *
	 * @param   array  $config  A named configuration array for object construction.
	 *                          contentType: the name (optional) of the content type to use for the serialization
	 *
	 * @since   4.0.0
	 */
	public function __construct($config = [])
	{
		if (array_key_exists('contentType', $config))
		{
			$this->serializer = new ContentSerializer($config['contentType']);
		}

		parent::__construct($config);
	}

	/**
	 * Execute and display a template script.
	 *
	 * @param   array|null  $items  Array of items
	 *
	 * @return  string
	 *
	 * @since   4.0.0
	 */
	public function displayList(array $items = null)
	{
		foreach (FieldsHelper::getFields('com_content.article') as $field)
		{
			$this->fieldsToRenderList[] = $field->name;
		}

		return parent::displayList();
	}

	/**
	 * Execute and display a template script.
	 *
	 * @param   object  $item  Item
	 *
	 * @return  string
	 *
	 * @since   4.0.0
	 */
	public function displayItem($item = null)
	{
		$this->relationship[] = 'modified_by';

		foreach (FieldsHelper::getFields('com_content.article') as $field)
		{
			$this->fieldsToRenderItem[] = $field->name;
		}

		if (Multilanguage::isEnabled())
		{
			$this->fieldsToRenderItem[] = 'languageAssociations';
			$this->relationship[]       = 'languageAssociations';
		}

		return parent::displayItem();
	}

	/**
	 * Prepare item before render.
	 *
	 * @param   object  $item  The model item
	 *
	 * @return  object
	 *
	 * @since   4.0.0
	 */
	protected function prepareItem($item)
	{
		$item->text = $item->introtext . ' ' . $item->fulltext;

		// Process the content plugins.
		PluginHelper::importPlugin('content');
		Factory::getApplication()->triggerEvent('onContentPrepare', ['com_content.article', &$item, &$item->params]);
		foreach (FieldsHelper::getFields('com_content.article', $item, true) as $field)
		{
			$item->{$field->name} = isset($field->apivalue) ? $field->apivalue : $field->rawvalue;
			if($item->{$field->name} == ""){
				$item->{$field->name} = null;
			}
			//Municipios de tenerife
			if($field->id == 193 ){
				$item->{$field->name} = $field->rawvalue;
			}
			//Conexiones con otras rutas
			if($field->id == 68){
				$activeArticles = [];
				if(!empty($field->rawvalue)) { 
					($idRutas = explode(",",$field->rawvalue)); 
						foreach($idRutas as $idRuta) {
							if(!empty($idRuta)) {
								$db = Factory::getDbo();
	
								$query = $db
									->getQuery(true)
									->select($db->quoteName(array('state')))
									->from($db->quoteName('#__content'))
									->where($db->quoteName('id') . " = " . $db->quote($idRuta));
									// Reset the query using our newly populated query object.
									$db->setQuery($query);                                          
									// Load the results as a list of stdClass objects (see later for more options on retrieving data).
									($rutaState = $db->loadResult());
			
									if($rutaState == 1) {
										$activeArticles [] = $idRuta;
									}
							}
							
						}; 
				};
				$item->{$field->name} = implode(',', $activeArticles);
				$field->rawvalue = implode(',', $activeArticles);
				$field->value = implode(',', $activeArticles);
			}
			if($field->id == 230){
				if($item->parent_category_id == 38 || $item->parent_category_id == 104 || $item->parent_category_id == 105){
					$color_field_value = $item->jcfields[230]->rawvalue[0];
					if($color_field_value == ""){
						//dd($color_field_value);
						$color_field_value = '#ffffff';
					}
					$matricula = $item->jcfields[140]->rawvalue;
					$color = [];
					//VM abd BICA fill only one color
					if ( ($item->catid != 61) && ($item->catid != 123) && ($item->catid != 124) ) {
						$color = ['top'=>$color_field_value, 'bottom' => $color_field_value];
						
					}
					else{
						if(str_contains($item->jcfields[140]->value, 'PNT')){
		
							$color = ['top'=>'#006400', 'bottom' => '#006400'];
		
						}
						elseif( (str_contains($matricula, 'PR')) || (str_contains($matricula, 'GR')) || (str_contains($matricula, 'SL')) ){
		
							$color = ['top'=>'#ffffff', 'bottom' => $color_field_value];
		
						}
						else{
		
							$color = ['top'=> $color_field_value, 'bottom' => '#ffffff'];
		
						}
		
					}
		
			
					$item->color = $color;
				}
			}
			if($field->id == 283){
				if($field->rawvalue == "0"){
					$item->mostrar_matricula = 0;
					$item->jcfields[283]->apivalue = 0;	
				}elseif($field->rawvalue == "1"){
					$item->jcfields[283]->apivalue = 1;		
					$item->mostrar_matricula = 1;

				}
			}

		}

		if( ($item->parent_category_id == 38) || ($item->parent_category_id == 104) || ($item->parent_category_id == 105) ){
			$db = Factory::getDbo();
			$field_id_clase = 139;
			// Check if the clase id match a itinerarios subcategory
			$query = $db
			->getQuery(true)
			->select('value')
			->from($db->quoteName('#__fields_values'))
			->where($db->quoteName('item_id') . " = " . $db->quote($item->catid)) //id clase, sendero, bica, vm
			->where($db->quoteName('field_id') . " = " . $db->quote($field_id_clase)); //id cf used for clase value
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$itinerariesType = $db->loadResult();
			$item->routeType = intval($itinerariesType);
		}
		//Return itinerario afectado
		if(  ($item->catid == 92) || ($item->catid == 193) || ($item->catid == 194)  ){

			if(!empty($item->jcfields[242]->rawvalue)) {

				$db = Factory::getDbo();
				$itinerarioAfectado = $item->jcfields[242]->rawvalue;
				// Check if the clase id match a itinerarios subcategory
				$query = $db
				->getQuery(true)
				->select('*')
				->from($db->quoteName('#__content'))
				->where($db->quoteName('id') . " = " . $db->quote($itinerarioAfectado)); 
				$db->setQuery($query);

				$itinerarioAfectado = $db->loadObject();

				//Get routeType
				$field_id_clase = 139;
				$query = $db
				->getQuery(true)
				->select('value')
				->from($db->quoteName('#__fields_values'))
				->where($db->quoteName('item_id') . " = " . $db->quote($itinerarioAfectado->catid)) //id clase, sendero, bica, vm
				->where($db->quoteName('field_id') . " = " . $db->quote($field_id_clase)); //id cf used for clase value
				$db->setQuery($query);
				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				$itinerariesType = $db->loadResult();

				$routeType = intval($itinerariesType);

				$item->itinerarioAfectado[] = [
					'id' => $itinerarioAfectado->id,
					'catid' => $itinerarioAfectado->catid,
					'title' => $itinerarioAfectado->title,
					'routeType' => $routeType
				];


			}
			
		}

		//Return equipamiento afectado
		if(  ($item->catid == 91) || ($item->catid == 191) || ($item->catid == 192)  ){

			if(!empty($item->jcfields[233]->rawvalue)) {

				$db = Factory::getDbo();
				$equipamientoAfectado = $item->jcfields[233]->rawvalue;
				// Check if the clase id match a itinerarios subcategory
				$query = $db
				->getQuery(true)
				->select('*')
				->from($db->quoteName('#__content'))
				->where($db->quoteName('id') . " = " . $db->quote($equipamientoAfectado)); 
				$db->setQuery($query);

				$equipamientoAfectado = $db->loadObject();

				//Get equipmentType
				$field_id_tipo = 257;
				$query = $db
				->getQuery(true)
				->select('value')
				->from($db->quoteName('#__fields_values'))
				->where($db->quoteName('item_id') . " = " . $db->quote($equipamientoAfectado->catid)) //id clase, sendero, bica, vm
				->where($db->quoteName('field_id') . " = " . $db->quote($field_id_tipo)); //id cf used for clase value
				$db->setQuery($query);
				// Load the results as a list of stdClass objects (see later for more options on retrieving data).
				$equipmentType = $db->loadResult();

				$equipType = intval($equipmentType);

				$item->equipamientoAfectado[] = [
					'id' => $equipamientoAfectado->id,
					'catid' => $equipamientoAfectado->catid,
					'title' => $equipamientoAfectado->title,
					'equipmentType' => $equipType
				];


			}
			
		}


		$link = RouteHelper::getArticleRoute($item->id, $item->catid, $item->language);
		$link = Route::_($link, true, Route::TLS_IGNORE, true, 'site');
		$item->url = $link;
		//if(isset($item->sub_category_name)){
		$item->sub_category_name = $item->category_title;
		//}
		
		//If category has not parent id, the it returns the category id of the item
		if(isset($item->parent_category_id)){
			if($item->parent_category_id == '1'){

				$item->parent_id = $item->catid;
			}
			else{
				$item->parent_id = $item->parent_category_id;
			}
		}
		//Check if user has favourite content
		$user =   Factory::getUser();
		$user_id = $user->id;
		if(($user_id)){

			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select(array('item_id'))
			->from($db->quoteName('#__sg_favourites'))
			->where($db->quoteName('user_id') . " = " . $db->quote($user_id))
			->where($db->quoteName('item_id') . " = " . $db->quote($item->id))
			->setLimit(1);
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$hasFavourite = $db->loadObjectList();
		}
		$item->favourite = false;
		if(!empty($hasFavourite)){
			if($hasFavourite[0]->item_id == $item->id){ 
				$item->favourite = true;
			}
		}
        //get user valoracion on current item
		if(($user_id)){

			$db = Factory::getDbo();
			$query = $db
			->getQuery(true)
			->select(array('rating'))
			->from($db->quoteName('#__sg_valoraciones'))
			->where($db->quoteName('user_id') . " = " . $db->quote($user_id))
			->where($db->quoteName('item_id') . " = " . $db->quote($item->id))
			->setLimit(1);
			// Reset the query using our newly populated query object.
			$db->setQuery($query);
			// Load the results as a list of stdClass objects (see later for more options on retrieving data).
			$mi_valoracion = $db->loadResult();
		}
		$item->mi_valoracion = null;
		if(isset($mi_valoracion)){		
				$item->mi_valoracion = $mi_valoracion;		
		}

		$valoracion = null;
		// Get rating
		$db = Factory::getDbo();
		$query = $db
		->getQuery(true)
		->select(array('SUM(rating) as rating, COUNT(id) as count'))
		->from($db->quoteName('#__sg_valoraciones'))
		->where($db->quoteName('item_id') . " = " . $db->quote($item->id));
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		if($hasRating = $db->loadObjectList()){
			if(!is_null($hasRating[0]->rating)){
				$rating = $hasRating[0]->rating;
				$count = $hasRating[0]->count;
				$valoracion = (float)($rating/$count);
			}
		}
		$item->valoracion = $valoracion;

		

		//$item->url =   Route::_(RouteHelper::getArticleRoute($item->alias, $item->catid, $item->language));
		if (Multilanguage::isEnabled() && !empty($item->associations))
		{
			$associations = [];

			foreach ($item->associations as $language => $association)
			{
				$itemId = explode(':', $association)[0];

				$associations[] = (object) [
					'id'       => $itemId,
					'language' => $language,
				];
			}

			$item->associations = $associations;
		}

		if (!empty($item->tags->tags))
		{
			$tagsIds   = explode(',', $item->tags->tags);
			$tagsNames = $item->tagsHelper->getTagNames($tagsIds);

			$item->tags = array_combine($tagsIds, $tagsNames);
		}
		else
		{
			$item->tags = [];
		}

		if (isset($item->images))
		{
			$registry = new Registry($item->images);
			$item->images = $registry->toArray();

			if (!empty($item->images['image_intro']))
			{
				$item->images['image_intro'] = ContentHelper::resolve($item->images['image_intro']);
			}

			if (!empty($item->images['image_fulltext']))
			{
				$item->images['image_fulltext'] = ContentHelper::resolve($item->images['image_fulltext']);
			}
		}
		return parent::prepareItem($item);
	}
}
