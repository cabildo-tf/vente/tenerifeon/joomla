FROM joomla:4.1.5-php7.4-apache

# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends locales && rm -rf /var/lib/apt/lists/* \
	&& localedef -i es_ES -c -f UTF-8 -A /usr/share/locale/locale.alias es_ES.UTF-8

ENV	LANG="es_ES.UTF8" \
	LC_COLLATE="es_ES.UTF-8" \
	LC_CTYPE="es_ES.UTF-8" \
	LC_MONETARY="es_ES.UTF-8" \
	LC_NUMERIC="es_ES.UTF-8" \
	LC_TIME="es_ES.UTF-8"

COPY rootfs / 

RUN chown www-data:www-data -R /var/www/html
